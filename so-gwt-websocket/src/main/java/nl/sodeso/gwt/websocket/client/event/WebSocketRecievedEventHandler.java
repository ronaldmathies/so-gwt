package nl.sodeso.gwt.websocket.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * A handler that can be registered with the <code>WebSocketClientController</code> for receiving
 * notification when a message has been received from a web socket.
 *
 * @author Ronald Mathies
 */
public interface WebSocketRecievedEventHandler extends EventHandler {

    /**
     * {@inheritDoc}
     */
    void onEvent(WebSocketRecievedEvent event);

}
