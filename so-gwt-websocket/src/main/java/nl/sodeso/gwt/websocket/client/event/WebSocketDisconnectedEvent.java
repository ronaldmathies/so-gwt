package nl.sodeso.gwt.websocket.client.event;

import com.google.web.bindery.event.shared.Event;


/**
 * Event for notifying interested listeners when a disconnection has occurred from a web socket.
 *
 * @author Ronald Mathies
 */
public class WebSocketDisconnectedEvent extends Event<WebSocketDisconnectedEventHandler> {

    public static final Type<WebSocketDisconnectedEventHandler> TYPE = new Type<>();

    private boolean isAutoRestoreConnectionEnabled = false;

    public WebSocketDisconnectedEvent(boolean isAutoRestoreConnectionEnabled) {
        this.isAutoRestoreConnectionEnabled = isAutoRestoreConnectionEnabled;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Type<WebSocketDisconnectedEventHandler> getAssociatedType() {
        return TYPE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void dispatch(WebSocketDisconnectedEventHandler handler) {
        handler.onEvent(this);
    }

    /**
     * Flag indicating if the web socket will perform an action to reconnect automatically.
     * @return true if the web socket will, false if not.
     */
    public boolean isAutoRestoreConnectionEnabled() {
        return isAutoRestoreConnectionEnabled;
    }
}
