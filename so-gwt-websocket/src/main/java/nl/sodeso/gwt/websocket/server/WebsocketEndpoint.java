package nl.sodeso.gwt.websocket.server;

import nl.sodeso.gwt.websocket.client.KeepAliveMessage;
import nl.sodeso.gwt.websocket.client.Message;
import nl.sodeso.gwt.websocket.server.util.WebsocketUtil;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */

@ServerEndpoint(value = "/websocket") // configurator=WebsocketEndpointConfig.class
public class WebsocketEndpoint {

    private static final Logger log = Logger.getLogger(WebsocketEndpoint.class.getName());

    private EndpointConfig endpointConfig;

    /**
     * Method that will be called when a message has been received from a web socket connection.
     * @param incomingMessage the incoming message.
     * @param session the associated session.
     */
    @OnMessage
    public void onMessage(String incomingMessage, Session session) {
        try {
            final Message message = WebsocketUtil.deserializeMessage(incomingMessage);

            if (message instanceof KeepAliveMessage) {
                log.log(Level.INFO, "Keep alive message received.");
                return;
            }

            List<WebsocketMessageHandler> handlers =
                    WebsocketMessageFactory.instance().getHandlers();

            for (WebsocketMessageHandler handler : handlers) {
                handler.onMessage(session, message);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, "Failed to process incoming web socket message.", e);
        }
    }

    @OnOpen
    public void onOpen(Session session, EndpointConfig endpointConfig) {
        this.endpointConfig = endpointConfig;

        WebSocketServerController.instance().addSession(session);
    }

    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
        WebSocketServerController.instance().removeSession(session);
    }


}