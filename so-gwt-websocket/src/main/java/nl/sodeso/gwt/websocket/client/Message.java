package nl.sodeso.gwt.websocket.client;

import java.io.Serializable;

/**
 * Base class for sending messages, sub class this class to add additional fields.
 *
 * @author Ronald Mathies
 */
public class Message implements Serializable {

    private long correlationId;

    public Message() {}

    /**
     * Returns the correlation id which can be used to identify which incoming message
     * resulted from which outgoing message.
     *
     * @return the correlation id.
     */
    public long getCorrelationId() {
        return correlationId;
    }

    /**
     * Sets the correlation id which can be used to identify which incoming message
     * resulted from which outgoing message.
     *
     * @param correlationId the correlation id.
     */
    public void setCorrelationId(long correlationId) {
        this.correlationId = correlationId;
    }

}
