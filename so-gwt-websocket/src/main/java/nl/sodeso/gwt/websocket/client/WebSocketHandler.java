package nl.sodeso.gwt.websocket.client;

/**
 * @author Ronald Mathies
 */
public interface WebSocketHandler<T extends Message> {

	/**
	 * Called when a connection has been made.
	 */
	void onConnect();

	/**
	 * Called when a connection has been closed.
	 */
	void onDisconnect();

	/**
	 * Called when a message has been pushed from the server.
	 * @param message the message that was pushed by the server.
	 */
	void onMessage(T message);

	/**
	 * Called when an error occured.
	 * @param error the error message.
	 */
	void onError(String error);

}
