package nl.sodeso.gwt.websocket.client;

/**
 * A simple message that is used to keep the connection with the web socket alive. This
 * message is send in a regular interval so that the browser doesn't close the websocket.
 *
 * By a web socket connection is closed in 60000 milliseconds so this message has to be
 * send before that time.
 *
 * @author Ronald Mathies
 */
public class KeepAliveMessage extends Message {
}
