package nl.sodeso.gwt.websocket.server;

import nl.sodeso.gwt.websocket.client.Message;

import javax.websocket.Session;

/**
 * @author Ronald Mathies
 */
public interface WebsocketMessageHandler {

    void onMessage(Session session, Message message);

}
