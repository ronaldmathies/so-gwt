package nl.sodeso.gwt.websocket.client.event;

import com.google.web.bindery.event.shared.Event;


/**
 * Event for notifying interested listeners when a connection has been made with a web socket.
 *
 * @author Ronald Mathies
 */
public class WebSocketErrorEvent extends Event<WebSocketErrorEventHandler> {

    public static final Type<WebSocketErrorEventHandler> TYPE = new Type<>();

    public WebSocketErrorEvent() {}

    /**
     * {@inheritDoc}
     */
    @Override
    public Type<WebSocketErrorEventHandler> getAssociatedType() {
        return TYPE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void dispatch(WebSocketErrorEventHandler handler) {
        handler.onEvent(this);
    }

}
