package nl.sodeso.gwt.websocket.server.serialization;

import com.google.gwt.user.server.rpc.SerializationPolicy;
import com.google.gwt.user.server.rpc.SerializationPolicyProvider;

/**
 * @author Ronald Mathies
 */
public class CustomSerializationPolicyProvider implements SerializationPolicyProvider {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerializationPolicy getSerializationPolicy(String moduleBaseURL, String serializationPolicyStrongName) {
		return new SimpleSerializationPolicy();
	}

}
