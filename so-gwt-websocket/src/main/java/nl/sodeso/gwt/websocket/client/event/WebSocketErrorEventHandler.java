package nl.sodeso.gwt.websocket.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * A handler that can be registered with the <code>WebSocketClientController</code> for receiving
 * notification when a connection has been made with a web socket.
 *
 * @author Ronald Mathies
 */
public interface WebSocketErrorEventHandler extends EventHandler {

    /**
     * {@inheritDoc}
     */
    void onEvent(WebSocketErrorEvent event);

}
