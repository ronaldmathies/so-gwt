package nl.sodeso.gwt.websocket.server;

import com.google.gwt.user.client.rpc.SerializationException;
import nl.sodeso.gwt.websocket.client.Message;
import nl.sodeso.gwt.websocket.server.util.WebsocketUtil;

import javax.websocket.Session;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class WebSocketServerController {

    private static final Logger log = Logger.getLogger(WebsocketUtil.class.getName());

    private static WebSocketServerController instance = null;

    private Set<Session> sessions = new HashSet<>();

    private WebSocketServerController() {

    }

    /**
     * Returns the singleton instance of the <code>WebSocketController</code>.
     *
     * @return the singleton instance.
     */
    public static WebSocketServerController instance() {
        if (instance == null) {
            instance = new WebSocketServerController();
        }

        return instance;
    }

    public void send(Message message) {
        log.log(Level.INFO, "Attempting to send message.");

        if (sessions.isEmpty()) {
            log.log(Level.SEVERE, "Cannot send message to web sockets, no active sessions.");
            return;
        }

        String result;
        try {
            result = WebsocketUtil.serializeMessage(message);
        } catch (SerializationException e) {
            log.log(Level.SEVERE, "Failed to serialize outgoing web socket message.");
            return;
        }

        for (Session session : sessions) {

            try {
                if (session.isOpen()) {
                    session.getBasicRemote().sendText(result);
                } else {
                    log.log(Level.WARNING, "Session already closed.");
                }
            } catch (IOException e) {
                log.log(Level.SEVERE, "Failed to send to web socket.");
            }

        }
    }

    public void send(Session session, Message message) {
        log.log(Level.INFO, "Attempting to send message.");

        if (session == null) {
            log.log(Level.SEVERE, "Cannot send message to web socket, no session provided.");
            return;
        }

        String result;
        try {
            result = WebsocketUtil.serializeMessage(message);
        } catch (SerializationException e) {
            log.log(Level.SEVERE, "Failed to serialize outgoing web socket message.");
            return;
        }

        try {
            if (session.isOpen()) {
                session.getBasicRemote().sendText(result);
            } else {
                log.log(Level.WARNING, "Session already closed.");
            }
        } catch (IOException e) {
            log.log(Level.SEVERE, "Failed to send to web socket.");
        }
    }

    protected void addSession(Session session) {
        log.log(Level.INFO, "Added new session to websocket server controller.");

        this.sessions.add(session);
    }

    protected void removeSession(Session session) {
        log.log(Level.INFO, "Remove a session from websocket server controller.");

        this.sessions.remove(session);
    }

}
