package nl.sodeso.gwt.websocket.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamFactory;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import nl.sodeso.gwt.event.client.EventBusController;
import nl.sodeso.gwt.websocket.client.event.*;
import nl.sodeso.gwt.websocket.client.rpc.MessageService;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class WebSocket {

    private static Logger logger = Logger.getLogger("Websocket");

    private static final int AUTO_RECONNECT_DELAY = 2000;
    private static final int DELAY_KEEP_ALIVE_TIMER = 55000;

    private static final String WS_HTTP_PROTOCOL = "ws://";
    private static final String WS_HTTPS_PROTOCOL = "wss://";

    private JavaScriptObject jsWebsocket;
    private String url;

    private EventBusController eventbus;

    private SerializationStreamFactory factory;

    private boolean keepAlive = false;
    private boolean autoRestoreConnection = false;

    private Timer keepAliveTimer = null;
    private Timer autorestoreTimer = null;

    /**
     * Constructs a new web socket. The host and port will be deduced by the
     * current html connection with the server. The path will be fixed on <code>/websocket</code>.
     */
    public WebSocket() {
        this(Window.Location.getHostName(), Window.Location.getPort(), "websocket");
    }

    /**
     * Constructs a new web socket. The host and port will be deduced by the
     * current html connection with the server.
     *
     * @param path the path to use in the URL when creating a connection
     */
    public WebSocket(String path) {
        this(Window.Location.getHostName(), Window.Location.getPort(), path);
    }

    /**
     * Constructs a new web socket. The path will be fixed on <code>/websocket</code>.
     *
     * @param hostname to hostname to connect to.
     * @param port the port to connect to.
     */
    public WebSocket(String hostname, String port) {
        this(hostname, port, "websocket");
    }

    protected WebSocket(String hostname, String port, String path) {
        String protocol = null;
        if (Window.Location.getProtocol().toUpperCase().startsWith("HTTPS")) {
            protocol = WS_HTTPS_PROTOCOL;
        } else {
            protocol = WS_HTTP_PROTOCOL;
        }

        url = protocol + hostname + ":" + port;

        String contextPath = WebSocketClientControllerConfiguration.instance().getContextPath();
        if (contextPath != null && !contextPath.isEmpty()) {
            url += (contextPath.startsWith("/") ? "" : "/") + contextPath;
        }

        if (path != null && !path.isEmpty()) {
            url += (path.startsWith("/") ? "" : "/") + path;
        }

        eventbus = new EventBusController();

        factory = GWT.create(MessageService.class);
    }

    /**
     * Adds a new handler which will receive notifications when a message has been received
     * over a web socket connection.
     *
     * @param handler the handler to add.
     */
    public void addWebSocketReceivedEventHandler(WebSocketRecievedEventHandler handler) {
        this.eventbus.addHandler(WebSocketRecievedEvent.TYPE, handler);
    }

    /**
     * Removes a handler which would receive notifications when a message has been received
     * over a web socket connection.
     *
     * @param handler the handler to remove.
     */
    public void removeWebSocketReceivedEventHandler(WebSocketRecievedEventHandler handler) {
        this.eventbus.removeHandler(WebSocketRecievedEvent.TYPE, handler);
    }

    /**
     * Adds a new handler which will receive notifications when a connection has been established
     * with a web socket.
     *
     * @param handler the handler to add.
     */
    public void addWebSocketConnectedEventHandler(WebSocketConnectedEventHandler handler) {
        this.eventbus.addHandler(WebSocketConnectedEvent.TYPE, handler);
    }

    /**
     * Removes a new handler which would receive notifications when a connection has been established
     * with a web socket.
     *
     * @param handler the handler to remove.
     */
    public void removeWebSocketConnectedEventHandler(WebSocketConnectedEventHandler handler) {
        this.eventbus.removeHandler(WebSocketConnectedEvent.TYPE, handler);
    }

    /**
     * Adds a new handler which will receive notifications when a connection has been disconnected
     * with a web socket.
     *
     * @param handler the handler to add.
     */
    public void addWebSocketDisconnectedEventHandler(WebSocketDisconnectedEventHandler handler) {
        this.eventbus.addHandler(WebSocketDisconnectedEvent.TYPE, handler);
    }

    /**
     * Removes an existing handler which would receive notifications when a connection has been disconnected
     * with a web socket.
     *
     * @param handler the handler to remove.
     */
    public void removeWebSocketDisconnectedEventHandler(WebSocketDisconnectedEventHandler handler) {
        this.eventbus.removeHandler(WebSocketDisconnectedEvent.TYPE, handler);
    }

    /**
     * Adds a new handler which will receive notifications when an error has occurred
     * with a web socket.
     *
     * @param handler the handler to add.
     */
    public void addWebSocketErrorEventHandler(WebSocketErrorEventHandler handler) {
        this.eventbus.addHandler(WebSocketErrorEvent.TYPE, handler);
    }

    /**
     * Removes an existing handler which would receive notifications when an error would occur with a web socket.
     *
     * @param handler the handler to remove.
     */
    public void removeWebSocketErrorEventHandler(WebSocketErrorEventHandler handler) {
        this.eventbus.removeHandler(WebSocketDisconnectedEvent.TYPE, handler);
    }


    /**
     * Starts the connection with the host and port specified during construction.
     *
     * @param keepAlive flag indicating that we will send a message every so many seconds to keep
     *                  the connection alive.
     */
    public void connect(boolean keepAlive, boolean autoRestoreConnection) {
        logger.log(Level.FINE, "Connecting to websocket: " + url);

        this.keepAlive = keepAlive;
        this.autoRestoreConnection = autoRestoreConnection;

        jsWebsocket = init(url);
        if (isConnecting()) {
            logger.log(Level.FINE, "Connecting state changed to connecting to websocket: " + url);

        }
    }

    public void disconnect() {
        // Kill the keep alive timer when it is running.
        if (keepAliveTimer != null && keepAliveTimer.isRunning()) {
            keepAliveTimer.cancel();
            keepAliveTimer = null;
        }

        // Kill the auto restore timer when it is running.
        if (autorestoreTimer != null && autorestoreTimer.isRunning()) {
            autorestoreTimer.cancel();
            autorestoreTimer = null;
        }

        // Close the connection.
        close();

        // Destroy the JavaScriptObject reference.
        jsWebsocket = null;
    }

    /**
     * Sends a message to the server.
     *
     * @param message the message to send.
     *
     * @return true when the message was send successfully, false if not.
     */
    public boolean send(Message message) {
        try {
            if (isConnected()) {
                SerializationStreamWriter writer = factory.createStreamWriter();
                writer.writeObject(message);
                send(writer.toString());

                return true;
            }
        } catch (final SerializationException e) {
            logger.log(Level.SEVERE, "Failed to send message through web socket connection, serialization failed.");
        }

        return false;
    }

    /**
     * Called by the browser when a connection has been made.
     */
    protected void onOpen() {
        logger.log(Level.FINE, "Connection made with web socket: " + url);
        if (keepAlive) {
            if (keepAliveTimer != null && keepAliveTimer.isRunning()) {
                keepAliveTimer.cancel();
            }

            keepAliveTimer = new Timer() {
                @Override
                public void run() {
                    send(new KeepAliveMessage());
                }
            };
            keepAliveTimer.scheduleRepeating(DELAY_KEEP_ALIVE_TIMER);
        }

        this.eventbus.fireEvent(new WebSocketConnectedEvent());
    }

    /**
     * Method for disconnecting any open connection.
     */
    protected void onClose() {
        logger.log(Level.INFO, "Connection with web socket lost or failed to create connection: " + url);
        this.eventbus.fireEvent(new WebSocketDisconnectedEvent(autoRestoreConnection));

        if (keepAliveTimer != null && keepAliveTimer.isRunning()) {
            keepAliveTimer.cancel();
            keepAliveTimer = null;
        }

        if (autoRestoreConnection) {
            if (isClosed()) {
                if (autorestoreTimer != null && autorestoreTimer.isRunning()) {
                    autorestoreTimer.cancel();
                    autorestoreTimer = null;
                }

                autorestoreTimer = new Timer() {
                    @Override
                    public void run() {
                        logger.log(Level.INFO, "Attempting to reconnect with web socket: " + url);

                        connect(keepAlive, autoRestoreConnection);
                    }
                };
                autorestoreTimer.schedule(AUTO_RECONNECT_DELAY);
            }
        }
    }


    /**
     * Called by the browser when an error has occured.
     */
    protected void onError() {
        this.eventbus.fireEvent(new WebSocketErrorEvent());
    }

    /**
     * Called by the browser when there is an incomming message.
     * @param message the incomming message.
     */
    protected void onMessage(String message) {
        try {
            SerializationStreamReader streamReader = factory.createStreamReader(message);
            this.eventbus.fireEvent(new WebSocketRecievedEvent((Message) streamReader.readObject()));
        } catch (SerializationException e) {
            logger.log(Level.SEVERE, "Failed to deserialize incomming message '" + message + '"');
        }
    }

    /**
     * Method to determin if websockets are supported by the browser.
     * @return flag indicating if websockets are supported.
     */
    public static native boolean isSupported() /*-{
        return $wnd.WebSocket;
    }-*/;

    /**
     * Flag indicating the connection has not yet been established.
     * @return true if the connection has not yet been established.
     */
    public native boolean isConnecting() /*-{
        return this.@nl.sodeso.gwt.websocket.client.WebSocket::jsWebsocket.readyState == 0;
    }-*/;


    /**
     * Flag indicating the WebSocket connection is established and communication is possible.
     * @return true if the WebSocket connection is established and communication is possible.
     */
    public native boolean isConnected() /*-{
        return this.@nl.sodeso.gwt.websocket.client.WebSocket::jsWebsocket.readyState == 1;
    }-*/;

    /**
     * Flag indicating the connection is going through the closing handshake..
     * @return true if the connection is going through the closing handshake..
     */
    public native boolean isClosing() /*-{
        return this.@nl.sodeso.gwt.websocket.client.WebSocket::jsWebsocket.readyState == 2;
    }-*/;

    /**
     * Flag indicating the connection has been closed or could not be opened.
     * @return true if the connection has been closed or could not be opened.
     */
    public native boolean isClosed() /*-{
        return this.@nl.sodeso.gwt.websocket.client.WebSocket::jsWebsocket.readyState == 3;
    }-*/;

    /**
     * Method to inialize a websocket connection with the server.
     *
     * @param url the url to use for the connection/
     * @return the websocket after initialization.
     */
    private native JavaScriptObject init(String url) /*-{
        var websocket = new WebSocket(url);
        var wrapper = this;
        websocket.onopen = function(evt) {
            wrapper.@nl.sodeso.gwt.websocket.client.WebSocket::onOpen()();
        };
        websocket.onclose = function(evt) {
            wrapper.@nl.sodeso.gwt.websocket.client.WebSocket::onClose()();
        };
        websocket.onmessage = function(evt) {
            wrapper.@nl.sodeso.gwt.websocket.client.WebSocket::onMessage(Ljava/lang/String;)(evt.data);

        };
        websocket.onerror = function(evt) {
            wrapper.@nl.sodeso.gwt.websocket.client.WebSocket::onError()();
        };

        return websocket;
    }-*/;

    /**
     * Sends a message to the server.
     * @param message the message to send.
     */
    private native void send(String message) /*-{
        this.@nl.sodeso.gwt.websocket.client.WebSocket::jsWebsocket.send(message);
    }-*/;

    /**
     * Destroys the connection with the server.
     * @return flag indicating of the destroy action was succesfull.
     */
    private native boolean close() /*-{
        this.@nl.sodeso.gwt.websocket.client.WebSocket::jsWebsocket.close();
    }-*/;

}
