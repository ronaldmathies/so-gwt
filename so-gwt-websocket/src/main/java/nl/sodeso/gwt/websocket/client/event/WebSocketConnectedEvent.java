package nl.sodeso.gwt.websocket.client.event;

import com.google.web.bindery.event.shared.Event;


/**
 * Event for notifying interested listeners when a connection has been made with a web socket.
 *
 * @author Ronald Mathies
 */
public class WebSocketConnectedEvent extends Event<WebSocketConnectedEventHandler> {

    public static final Type<WebSocketConnectedEventHandler> TYPE = new Type<>();

    public WebSocketConnectedEvent() {}

    /**
     * {@inheritDoc}
     */
    @Override
    public Type<WebSocketConnectedEventHandler> getAssociatedType() {
        return TYPE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void dispatch(WebSocketConnectedEventHandler handler) {
        handler.onEvent(this);
    }

}
