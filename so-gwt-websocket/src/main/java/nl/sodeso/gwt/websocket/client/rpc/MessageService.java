package nl.sodeso.gwt.websocket.client.rpc;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import nl.sodeso.gwt.websocket.client.Message;

/**
 * The Interface is used to make sure that GWT creates the neccesary components to
 * serialize and deserialize the <code>Message</code>.
 */
@RemoteServiceRelativePath("MessageService")
public interface MessageService extends RemoteService {

    Message getMessage(Message message);

}
