package nl.sodeso.gwt.websocket.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * A handler that can be registered with the <code>WebSocketClientController</code> for receiving
 * notification when a connection with the web socket has been disconnected.
 *
 * @author Ronald Mathies
 */
public interface WebSocketDisconnectedEventHandler extends EventHandler {

    /**
     * {@inheritDoc}
     */
    void onEvent(WebSocketDisconnectedEvent event);

}
