package nl.sodeso.gwt.websocket.client.event;

import com.google.web.bindery.event.shared.Event;
import nl.sodeso.gwt.websocket.client.Message;


/**
 * Event for notifying interested listeners when a message has been received from a web socket.
 *
 * @author Ronald Mathies
 */
public class WebSocketRecievedEvent extends Event<WebSocketRecievedEventHandler> {

    public static final Type<WebSocketRecievedEventHandler> TYPE = new Type<>();

    private Message message;

    public WebSocketRecievedEvent(Message message) {
        this.message = message;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Type<WebSocketRecievedEventHandler> getAssociatedType() {
        return TYPE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void dispatch(WebSocketRecievedEventHandler handler) {
        handler.onEvent(this);
    }

    /**
     * Returns the message that was received from a web socket.
     * @return the message that was received.
     */
    public Message getMessage() {
        return this.message;
    }
}
