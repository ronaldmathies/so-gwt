package nl.sodeso.gwt.websocket.server;

import nl.sodeso.commons.web.classpath.ClasspathWebUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class WebsocketMessageFactory {

    private static final Logger LOG = Logger.getLogger(WebsocketMessageFactory.class.getName());

    private static WebsocketMessageFactory instance = null;

    private List<WebsocketMessageHandler> handlers = new ArrayList<>();

    protected WebsocketMessageFactory() {}

    public static WebsocketMessageFactory instance() {
        if (instance == null) {
            instance = new WebsocketMessageFactory();
        }

        return instance;
    }

    public List<WebsocketMessageHandler> getHandlers() {
        if (handlers.isEmpty()) {
            analyzeAndInstantiateMessageHandlers();
        }

        return handlers;
    }

    private void analyzeAndInstantiateMessageHandlers() {
        Set<Class<? extends WebsocketMessageHandler>> handlerClasses =
                ClasspathWebUtil.instance().subTypesOf(WebsocketMessageHandler.class);

        try {
            for (Class<? extends WebsocketMessageHandler> handlerClass : handlerClasses) {
                handlers.add(handlerClass.newInstance());
            }
        } catch (InstantiationException | IllegalAccessException e) {
            LOG.log(Level.SEVERE, "Failed to instantiate message handler.");
        }
    }
}
