package nl.sodeso.gwt.websocket.client;

/**
 * @author Ronald Mathies
 */
public class WebSocketClientController extends WebSocket {


    private static WebSocketClientController instance = null;

    private WebSocketClientController() {
    }

    /**
     * Returns the singleton instance of the <code>WebSocketController</code>.
     *
     * @return the singleton instance.
     */
    public static WebSocketClientController instance() {
        if (instance == null) {
            instance = new WebSocketClientController();
        }

        return instance;
    }

}
