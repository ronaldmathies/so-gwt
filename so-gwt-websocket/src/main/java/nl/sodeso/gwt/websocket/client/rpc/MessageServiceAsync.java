package nl.sodeso.gwt.websocket.client.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.gwt.websocket.client.Message;

/**
 * The Interface is used to make sure that GWT creates the neccesary components to
 * serialize and deserialize the <code>Message</code>.
 */
public interface MessageServiceAsync {

    void getMessage(Message message, AsyncCallback<Message> callback);

}
