package nl.sodeso.gwt.websocket.client;

/**
 * @author Ronald Mathies
 */
public class WebSocketClientControllerConfiguration {

    private static WebSocketClientControllerConfiguration INSTANCE = null;

    private String contextPath;

    public static WebSocketClientControllerConfiguration instance() {
        if (INSTANCE == null) {
            INSTANCE = new WebSocketClientControllerConfiguration();
        }

        return INSTANCE;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }

    public String getContextPath() {
        return this.contextPath;
    }

}
