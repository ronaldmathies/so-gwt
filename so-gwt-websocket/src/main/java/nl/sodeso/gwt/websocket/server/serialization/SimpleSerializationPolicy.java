package nl.sodeso.gwt.websocket.server.serialization;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.server.rpc.SerializationPolicy;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class SimpleSerializationPolicy extends SerializationPolicy {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean shouldDeserializeFields(Class<?> clazz) {
		return isSerializable(clazz);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean shouldSerializeFields(Class<?> clazz) {
		return isSerializable(clazz);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validateDeserialize(Class<?> clazz) throws SerializationException {
		if (!isSerializable(clazz)) {
			throw new SerializationException();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validateSerialize(Class<?> clazz) throws SerializationException {
		if (!isSerializable(clazz)) {
			throw new SerializationException();
		}
	}

	/**
	 * Method for deteremining if an object is serializable or not.
	 * @param clazz the class to check.
	 * @return true if the object is serializable.
	 */
	private boolean isSerializable(Class<?> clazz) {
		if (clazz != null) {
			if (clazz.isPrimitive()
					|| Serializable.class.isAssignableFrom(clazz)
					|| IsSerializable.class.isAssignableFrom(clazz)) {
				return true;
			}
		}
		return false;

	}
}
