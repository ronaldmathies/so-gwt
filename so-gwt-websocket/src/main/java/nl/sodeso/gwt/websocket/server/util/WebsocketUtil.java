package nl.sodeso.gwt.websocket.server.util;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.server.rpc.impl.ServerSerializationStreamReader;
import com.google.gwt.user.server.rpc.impl.ServerSerializationStreamWriter;
import nl.sodeso.gwt.websocket.client.Message;
import nl.sodeso.gwt.websocket.server.serialization.CustomSerializationPolicyProvider;
import nl.sodeso.gwt.websocket.server.serialization.SimpleSerializationPolicy;

/**
 * Utility class which provides the capability to serialize / deserialize incoming / outgoing messages.
 *
 * @author Ronald Mathies
 */
public class WebsocketUtil {

    /**
     * Deserializes a message received from a web socket connection.
     *
     * @param message the message to deserialize into a Java object.
     * @return the java object.
     *
     * @throws SerializationException when the deserialization failed.
     */
    public static Message deserializeMessage(String message) throws SerializationException {
        final ServerSerializationStreamReader streamReader =
                new ServerSerializationStreamReader(WebsocketUtil.class.getClassLoader(), new CustomSerializationPolicyProvider());
        streamReader.prepareToRead(message);
        return (Message)streamReader.readObject();
    }

    /**
     * Serializes a message that is to be send with a web socket connection.
     *
     * @param message the message to serialize.
     * @return the message.
     *
     * @throws SerializationException the the serialization failed.
     */
    public static String serializeMessage(final Message message) throws SerializationException {
        final ServerSerializationStreamWriter serverSerializationStreamWriter =
                new ServerSerializationStreamWriter(new SimpleSerializationPolicy());

        serverSerializationStreamWriter.writeObject(message);
        return serverSerializationStreamWriter.toString();
    }

}
