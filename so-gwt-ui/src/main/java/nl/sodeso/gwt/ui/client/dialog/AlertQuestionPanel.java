package nl.sodeso.gwt.ui.client.dialog;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.button.CancelButton;
import nl.sodeso.gwt.ui.client.form.button.YesButton;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.util.Align;

/**
 * @author Ronald Mathies
 */
public class AlertQuestionPanel extends PopupWindowPanel {

    private AlertQuestionHandler alertQuestionHandler;

    public AlertQuestionPanel(final String title, final String question, final AlertQuestionHandler alertQuestionHandler) {
        super("alert-confirmation", title, WindowPanel.Style.ALERT);

        this.alertQuestionHandler = alertQuestionHandler;

        addToBody(
            new EntryForm(null)
                .addEntry(new EntryWithDocumentation(null, question)));

        addToFooter(Align.RIGHT,
                new YesButton.WithLabel((event) -> yes()),
                new CancelButton.WithLabel((event) -> close()));
    }

    private void yes() {
        alertQuestionHandler.confirm();
    }

}
