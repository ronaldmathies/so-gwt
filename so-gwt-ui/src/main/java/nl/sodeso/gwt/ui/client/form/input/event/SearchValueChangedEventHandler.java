package nl.sodeso.gwt.ui.client.form.input.event;

/**
 * @author Ronald Mathies
 */
public interface SearchValueChangedEventHandler {

    void onSearchValueChangedEvent(SearchValueChangedEvent event);

}