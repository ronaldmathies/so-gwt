package nl.sodeso.gwt.ui.client.form.table;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ProvidesKey;

import static nl.sodeso.gwt.ui.client.util.Constants.FALSE;
import static nl.sodeso.gwt.ui.client.util.Constants.TRUE;

/**
 * @author Ronald Mathies
 */
public class DataGridField<T> extends DataGrid<T> {

    // Element attribute names.
    private static final String ATTR_FULL_HEIGHT = "so-full-height";

    private static DataGridResource RESOURCE = GWT.create(DataGridResource.class);

    private String key;

    public DataGridField(String key) {
        this(key, 5, RESOURCE);
    }

    public DataGridField(String key, final int pageSize) {
        super(pageSize);
//        this(key, pageSize, RESOURCE);
    }

    public DataGridField(String key, int pageSize, Resources resources) {
        super(pageSize, resources);
        this.key = key;
    }

    public DataGridField(String key, int pageSize, ProvidesKey<T> keyProvider) {
        super(pageSize, RESOURCE, keyProvider);
        this.key = key;
    }

    public DataGridField(String key, final int pageSize, ProvidesKey<T> keyProvider,
                         Widget loadingIndicator) {
        super(pageSize, RESOURCE, keyProvider, loadingIndicator);
        this.key = key;
    }

    /**
     * Sets the height of the widget to the full width of it's parent.
     * @param visible flag indicating if the full height should be visible.
     */
    public void setFullHeight(boolean visible) {
        getElement().setAttribute(ATTR_FULL_HEIGHT, visible ? TRUE : FALSE);
    }

    /**
     * Returns a flag indicating if the height attribute is set or not.
     * @return true if the height attribute is set, false if not.
     */
    public boolean isFullHeight() {
        return !FALSE.equals(getElement().getAttribute(ATTR_FULL_HEIGHT));
    }

    public void addColumn(Column<T, ?> col, Header<?> header) {
        super.addColumn(col, header);
    }

    public String getKey() {
        return this.key;
    }

}
