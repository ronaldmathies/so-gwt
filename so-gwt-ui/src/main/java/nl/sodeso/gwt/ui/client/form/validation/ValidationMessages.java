package nl.sodeso.gwt.ui.client.form.validation;

import com.google.gwt.i18n.client.Messages;

/**
 * @author Ronald Mathies
 */
public interface ValidationMessages extends Messages {

    @DefaultMessage("Value is too short {0}, should be {1}")
    String LengthValidationMinimumLength(int actual, int expected);

    @DefaultMessage("Value is too long {0}, should be {1}")
    String LengthValidationMaximumLength(int actual, int expected);

    @DefaultMessage("The value entered is not a number")
    String IntegerRangeValidationNotInt();

    @DefaultMessage("The value entered is smaller then {0}")
    String IntegerRangeValidationMinimum(int minimum);

    @DefaultMessage("The value entered is larger then {0}")
    String IntegerRangeValidationMaximum(int maximum);

    @DefaultMessage("The value entered is not a number")
    String FloatRangeValidationNotFloat();

    @DefaultMessage("The value entered is smaller then {0}")
    String FloatRangeValidationMinimum(float minimum);

    @DefaultMessage("The value entered is larger then {0}")
    String FloatRangeValidationMaximum(float maximum);

    @DefaultMessage("The value entered is smaller then {0}")
    String DateRangeValidationMinimum(String minimum);

    @DefaultMessage("The value entered is larger then {0}")
    String DateRangeValidationMaximum(String maximum);

    @DefaultMessage("The passwords do not match.")
    String PasswordsDoNotMath();

    @DefaultMessage("This field is mandatory, please enter a value.")
    String Mandatory();

    @DefaultMessage("This field {0} is mandatory, please enter a value.")
    String MandatoryWithLabel(String label);

    // TODO: Error does not belong here, is an application specific error (dep-deploykit-console)
    @DefaultMessage("The version number does not comply with the formatting rules.")
    String VersionNotComplyingWithRule();

}
