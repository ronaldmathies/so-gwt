package nl.sodeso.gwt.ui.client.form.input;

import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.FlowPanel;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.resources.Icon;

/**
 * @author Ronald Mathies
 */
public class TextAreaToolsField extends FlowPanel {

    // Keys for so-key entries.
    private static final String KEY_ZOOM_SUFFIX = "-zoom";

    private String title;
    private TextAreaField field = null;

    public TextAreaToolsField(String title, TextAreaField field) {
        this.title = title;
        this.field = field;
        this.add(field);

        this.getElement().getStyle().setPosition(Style.Position.RELATIVE);
    }

    public void addZoomOption() {
        SimpleButton zoomButton = new SimpleButton(field.getKey() != null ? field.getKey() + KEY_ZOOM_SUFFIX : null, Icon.ArrowsAlt, SimpleButton.Style.NONE);
        zoomButton.addClickHandler((event) -> {
            TextAreaZoomWindow window = new TextAreaZoomWindow(title, field);
            window.center(false, true);
        });

        zoomButton.getElement().getStyle().setPosition(Style.Position.ABSOLUTE);
        zoomButton.getElement().getStyle().setTop(5, Style.Unit.PX);
        zoomButton.getElement().getStyle().setRight(0, Style.Unit.PX);
        add(zoomButton);
    }

}
