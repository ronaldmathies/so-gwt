package nl.sodeso.gwt.ui.server.endpoint.properties;

import com.google.gwt.user.server.rpc.XsrfProtectedServiceServlet;
import nl.sodeso.gwt.ui.client.application.module.properties.ClientModuleProperties;
import nl.sodeso.gwt.ui.client.application.module.properties.rpc.ModulePropertiesRpc;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public abstract class AbstractModulePropertiesEndpoint<C extends ClientModuleProperties, S extends ServerModuleProperties> extends XsrfProtectedServiceServlet implements ModulePropertiesRpc {

    private static final Logger log = Logger.getLogger(AbstractModulePropertiesEndpoint.class.getName());

    public abstract String getModuleId();

    public C getProperties() {
        C clientAppProps = newClientAppPropertiesInstance();
        if (clientAppProps != null) {

            // Fill in the default properties.
            S serverAppProps = getServerModuleProperties();
            clientAppProps.setVersion(serverAppProps.getVersion(getModuleId()));
            fillApplicationProperties(serverAppProps, clientAppProps);
        }

        return clientAppProps;
    }

    /**
     * Fill in additional properties into the client properties.
     * @param serverModuleProperties the server side module properties.
     * @param clientModuleProperties the client side module properties.
     */
    public abstract void fillApplicationProperties(S serverModuleProperties, C clientModuleProperties);

    /**
     * Should return the server side module properties.
     * @return server side application properties.
     */
    public abstract S getServerModuleProperties();

    /**
     * Constructs a new client side properties class instance.
     * @return a new client side properties class instance.
     */
    private C newClientAppPropertiesInstance() {
        try {
            return getClientModulePropertiesClass().newInstance();
        } catch (IllegalAccessException | InstantiationException illegalAccessException) {
            log.log(Level.SEVERE, "Failed to instantiate module properties class.", illegalAccessException);
        }

        return null;
    }

    /**
     * Should return the class definition for the client side module properties.
     * @return the class definition for the client side module properties.
     */
    public abstract Class<C> getClientModulePropertiesClass();
}
