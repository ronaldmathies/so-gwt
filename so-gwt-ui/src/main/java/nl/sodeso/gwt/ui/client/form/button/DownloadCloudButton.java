package nl.sodeso.gwt.ui.client.form.button;

import com.google.gwt.event.dom.client.ClickHandler;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.gwt.ui.client.resources.Resources;

/**
 * Button representing an download from the button.
 *
 * @author Ronald Mathies
 */
public class DownloadCloudButton {

    // Keys for so-key entries.
    public static final String KEY = "download";

    public static class WithIcon extends SimpleButton {

        public WithIcon() {
            this(null);
        }

        public WithIcon(ClickHandler handler) {
            super(KEY, Icon.CloudDownload, Style.BLUE);

            if (handler != null) {
                addClickHandler(handler);
            }
        }
    }

}
