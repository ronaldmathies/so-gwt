package nl.sodeso.gwt.ui.client.application.module;

import nl.sodeso.gwt.ui.client.application.module.properties.ClientModuleProperties;
import nl.sodeso.gwt.ui.client.application.module.properties.rpc.ModulePropertiesGateway;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.trigger.Trigger;

/**
 * @author Ronald Mathies
 */
public abstract class ModuleEntryPoint<T extends ClientModuleProperties> {

    private T moduleProperties;

    public ModuleEntryPoint() {
    }

    public final T getModuleProperties() {
        return this.moduleProperties;
    }

    public abstract String getModuleId();

    public abstract String getModuleName();

    public abstract Icon getModuleIcon();

    @SuppressWarnings("unchecked")
    final public void initModule(final InitFinishedTrigger initFinishedTrigger) {
        onBeforeModuleLoad(() -> ModulePropertiesGateway.getModuleProperties(getModuleId(), new DefaultAsyncCallback<ClientModuleProperties>() {
            @Override
            public void success(ClientModuleProperties clientModuleProperties) {
                moduleProperties = (T) clientModuleProperties;

                onAfterModuleLoad(() -> initFinishedTrigger.fire());
            }
        }));
    }

    public abstract void onBeforeModuleLoad(BeforeModuleLoadFinishedTrigger trigger);

    public abstract void onAfterModuleLoad(AfterModuleLoadFinishedTrigger trigger);

    public abstract void activateModule(ActivateFinishedTrigger trigger);

    public abstract void suspendModule(SuspendFinishedTrigger trigger);

    public abstract String getWelcomeToken();

    public interface BeforeModuleLoadFinishedTrigger extends Trigger {
    }

    public interface AfterModuleLoadFinishedTrigger extends Trigger {
    }

    public interface InitFinishedTrigger extends Trigger {
    }

    public interface ActivateFinishedTrigger extends Trigger {
    }

    public interface SuspendFinishedTrigger extends Trigger {
    }

}
