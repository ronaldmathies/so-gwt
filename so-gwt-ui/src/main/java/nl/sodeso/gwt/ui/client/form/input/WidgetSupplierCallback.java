package nl.sodeso.gwt.ui.client.form.input;

/**
 * @author Ronald Mathies
 */
public interface WidgetSupplierCallback<T> {

    void created(T widget);
}
