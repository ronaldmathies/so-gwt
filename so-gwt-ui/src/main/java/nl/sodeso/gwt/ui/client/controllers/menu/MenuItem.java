package nl.sodeso.gwt.ui.client.controllers.menu;

import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Timer;
import nl.sodeso.gwt.ui.client.controllers.menu.events.MenuItemGroupExpandedEvent;
import nl.sodeso.gwt.ui.client.controllers.menu.events.MenuItemGroupExpandedHandler;
import nl.sodeso.gwt.ui.client.controllers.menu.events.MenuItemSelectedEvent;
import nl.sodeso.gwt.ui.client.controllers.menu.events.MenuItemSelectedEventHandler;
import nl.sodeso.gwt.ui.client.resources.Icon;

import java.util.ArrayList;
import java.util.Map;

import static nl.sodeso.gwt.ui.client.controllers.menu.ArgumentsUtil.toToken;

/**
 * @author Ronald Mathies
 */
public class MenuItem implements MenuItemSelectedEventHandler, MenuItemGroupExpandedHandler {

    private String key;
    private String token;

    private Icon icon;
    private String label;

    /**
     * The arguments hash value is used when a menu item is
     * clicked but was already selected, in case the arguments passed
     * to the menu item are different then we still would like to perform a click.
     */
    private long argumentsHash = 0l;

    private MenuItemClickTrigger menuItemClickTrigger;

    private MenuItem parent = null;
    private ArrayList<MenuItem> children = new ArrayList<>();

    private UnorderedListElement parentElement = null;
    private UnorderedListElement menuItemAsParentElement = null;
    private UnorderedListItemElement menuItemAsChildElement = null;

    public MenuItem(String key, String label, MenuItemClickTrigger menuItemClickTrigger) {
        this(key, null, label, menuItemClickTrigger);
    }

    public MenuItem(String key, Icon icon, String label, MenuItemClickTrigger menuItemClickTrigger) {
        this.key = key;
        this.token = key;

        this.icon = icon;
        this.label = label;

        this.menuItemClickTrigger = menuItemClickTrigger;
        this.menuItemAsChildElement = new UnorderedListItemElement(key, icon, label);

        menuItemAsChildElement.setSelected(false);
        setSelected(false);
    }

    public boolean canHandleLink(String token) {
        if (token.equals(this.token)) {
            return true;
        }

        if (!isLeaf()) {
            for (MenuItem child : children) {
                if (child.canHandleLink(token)) {
                    return true;
                }
            }
        }

        return false;
    }

    public void handleLink(String token, Map<String, String> arguments) {
        if (token.equals(this.token)) {
            execute(arguments);
        }

        if (!isLeaf()) {
            for (MenuItem child : children) {
                if (child.canHandleLink(token)) {
                    child.handleLink(token, arguments);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onEvent(MenuItemSelectedEvent event) {
        if (isLeaf()) {
            setSelected(event.getSelectedMenuItem().getKey().equals(this.getKey()));
        }
    }

    @Override
    public void onEvent(MenuItemGroupExpandedEvent event) {
        if (!isLeaf()) {
            if (!event.getMenuItem().getKey().equals(this.getKey())) {
                if (!this.isCollapsed()) {

                    boolean containsSelectedChild = false;
                    for (MenuItem child : children) {
                        if (child.isSelected())
                            containsSelectedChild = true;
                    }

                    if (!containsSelectedChild) {
                        this.expandOrCollapse();
                    }
                }
            }
        }
    }

    /**
     * Use this method to programmatically click this menu item as if the user
     * clicked this item itself.
     *
     * @param arguments arguments to pass along.
     */
    public void click(Map<String, String> arguments) {
        if (isLeaf() && !isEnabled()) {
            return;
        }

        // When we are a leaf and we have a parent (remember, leafs can also
        // act as a root), check if it is collapsed, if so then
        // expand it.
        if (isLeaf() && parent != null && parent.isCollapsed()) {
            parent.click(null);
        }

        // Since this is a programmaticaly click we can't be
        // sure that the menu item is visible or outside
        // the scroll bounds, so scroll it into the view so
        // it is visible.
        if (isLeaf()) {
            menuItemAsChildElement.getElement().scrollIntoView();
        }

        // Since this is a programatically click
        // we need to perform it using the history.
        if (arguments != null && !arguments.isEmpty()) {
            History.newItem(toToken(token, arguments));
        } else {
            History.newItem(token);
        }
    }

    /**
     * Executes the action associated with this menu item.
     *
     * @param arguments the arguments to pass along.
     */
    private void execute(Map<String, String> arguments) {
        if (isLeaf() && !isEnabled()) {
            return;
        }

        if (isLeaf()) {

            // If the item is not selected or when the item was already selected but the arguments
            // are different then still select the menu ite,.
            if (!menuItemAsChildElement.isSelected() || argumentsHash != arguments.hashCode()) {

                // Check if this menu item is a child, if so then also check of the parent menu item is collapsed
                // or not, if it is then expand the parent menu item.
                if (parent != null && parent.isCollapsed()) {
                    parent.execute(null);
                }

                MenuController.instance().fireMenuItemSelectedEvent(new MenuItemSelectedEvent(this));
                if (menuItemClickTrigger != null) {

                    // Store the arguments hashcode for feature reference.
                    argumentsHash = arguments.hashCode();
                    menuItemClickTrigger.click(arguments);
                }
            }
        } else {
            expandOrCollapse();

            MenuController.instance().fireMenuItemGroupExpendedEvent(new MenuItemGroupExpandedEvent(this));
        }
    }

    /**
     * Returns the key of this menu item.
     * @return the key of this menu item.
     */
    public String getKey() {
        return this.key;
    }

    /**
     * Flag indicating if the key is the same as the key passed in.
     * @param key the key to match
     * @return true if there is a match, false if not.
     */
    public boolean hasKey(String key) {
        return this.key.equals(key);
    }

    /**
     * The icon to use to represent this menu item.
     * @return the icon.
     */
    public Icon getIcon() {
        return this.icon;
    }

    /**
     * The label of the menu item.
     * @return the label.
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * Adds child menu items, this is only possible for menu-items that act as a root (parent), otherwise an
     * exception will be thrown.
     *
     * @param childrenToAdd the menu items to add.
     * @return self, so that method chaining is allowed.
     */
    public MenuItem addChildren(MenuItem ... childrenToAdd) {
        if (getParent() != null) {
            throw new RuntimeException("This item is already assigned to a parent, adding a child to this menu item is not allowed.");
        }

        // When we are a leaf then we need to transform oneself to a parent.
        if (isLeaf()) {
            if (!token.endsWith(":hide")) {
                token += ":hide";
            }

            menuItemAsParentElement = new UnorderedListElement(token, icon, label);

            // In case we have been added to the menu we need to remove ourselfs first,
            // otherwise we can just make the switch.
            if (isAttached()) {
                int index = parentElement.removeListItem(menuItemAsChildElement);
                parentElement.addList(index, menuItemAsParentElement);

                // If we were a leaf then we were visible, in this case we need to
                // directly display our self again, don't use a timer
                // so that it is instant because the transitions don't work when
                // adding an object an transitioning it within the same thread.
                menuItemAsParentElement.setVisible(true);
            }

            menuItemAsChildElement = null;
        }

        for (MenuItem child : childrenToAdd) {
            if (child.getParent() != null) {
                throw new RuntimeException("Child can only be added to one parent (child: '" + child.getLabel() + "'.");
            }

            child.setParent(this);
            children.add(child);

            // If the children are added after the parent was already
            // attached then attach the children directly,
            // otherwise they will be attached when the parent
            // is added.
            if (isAttached()) {
                child.attachToParent(menuItemAsParentElement);
            }
        }

        return this;
    }

    /**
     * Removes the specified children from this menu item.
     * @param childrenToRemove the menu items to remove.
     */
    public void removeChildren(MenuItem ... childrenToRemove) {
        for (MenuItem childToRemove : childrenToRemove) {

            for (MenuItem existingChild : children) {

                if (existingChild.hasKey(childToRemove.getKey())) {

                    existingChild.detachFromParent();
                    existingChild.setParent(null);
                    children.remove(existingChild);

                }

            }
        }

    }

    /**
     * Returns all the sub-menu items of this menu item.
     * @return all the sub-menu items of this menu item.
     */
    public ArrayList<MenuItem> getChildren() {
        return this.children;
    }

    /**
     * Returns the child at the specified index.
     *
     * @param index the index of the child to return.
     * @return the child at the specified index.
     */
    public MenuItem getChild(int index) {
        return children.get(index);
    }

    /**
     * Returns the child with the specified <code>key</code>.
     *
     * @param key the key of the child.
     * @return the child with the specified <code>key</code> or <code>null</code> when not found.
     */
    public MenuItem getChild(String key) {
        for (MenuItem child : children) {
            if (child.getKey().equals(key)) {
                return child;
            }
        }

        return null;
    }

    /**
     * Flag indicating if this a leaf or a parent.
     * @return true if this is a leaf, false if this is a parent.
     */
    public boolean isLeaf() {
        return menuItemAsChildElement != null;
    }

    /**
     * Flag indicating if this menu item is attached to a parent.
     * @return true if attached, false if not.
     */
    public boolean isAttached() {
        return parentElement != null;
    }

    /**
     * Returns the parent menu item of this menu item.
     * @return the parent menu item.
     */
    public MenuItem getParent() {
        return this.parent;
    }

    public void setParent(MenuItem parent) {
        this.parent = parent;
    }

    /**
     * Attached this menu item to the given parent.
     * @param parentElement the parent element
     */
    protected void attachToParent(UnorderedListElement parentElement) {
        this.parentElement = parentElement;

        if (isLeaf()) {
            parentElement.addListItem(menuItemAsChildElement);

            animateLeaf();

        } else {
            if (menuItemAsParentElement != null) {
                if (menuItemAsParentElement.getParent() != null) {
                    menuItemAsParentElement.removeFromParent();
                }
            } else {
                menuItemAsParentElement = new UnorderedListElement(token, icon, label);
            }

            parentElement.addList(menuItemAsParentElement);

            animateParent();

            for (MenuItem child : children) {
                child.attachToParent(menuItemAsParentElement);
            }
        }

        MenuController.instance().addMenuItemSelectedEventHandler(this);
        MenuController.instance().addMenuItemGroupExpendedEventHandler(this);
    }

    /**
     * Performing a transition directly after adding the item to the DOM
     * tree has as a side effect that the transitions don't work, apparently we
     * need to wait at least 1ms. (different thread maybe?) for the
     * transition to work.
     */
    private void animateLeaf() {
        Timer timer = new Timer() {
            @Override
            public void run() {

                // By default all leafs are hidden, so now we need to determin
                // the visibility state of the leaf.

                // If this leaf is not a root item then
                // adjust the visibility to the collapsed / expanded state.
                if (parent != null) {
                    setVisible(!parent.isCollapsed());
                }

                // If this menu item is a root then
                // make it visible.
                if (parent == null) {
                    setVisible(true);
                }
            }
        };
        timer.schedule(1);
    }

    /**
     * Performing a transition directly after adding the item to the DOM
     * tree has as a side effect that the transitions don't work, apparently we
     * need to wait at least 1ms. (different thread maybe?) for the
     * transition to work.
     */
    private void animateParent() {
        Timer timer = new Timer() {
            @Override
            public void run() {

                // By default all parents are hidden, so we need to make the parent
                // visible.
                setVisible(true);


            }
        };
        timer.schedule(1);
    }

    protected void detachFromParent() {
        // First hide our self.
        setVisible(false);

        // Any children should also be hidden.
        if (!isLeaf()) {
            for (MenuItem child : children) {
                child.setVisible(false);
            }
        }

        // Now give the system some time to perform the transition, wait for
        // 500ms and then remove the menu items and deregister the items.
        Timer timer = new Timer() {
            @Override
            public void run() {
                if (isLeaf()) {
                    parentElement.removeListItem(menuItemAsChildElement);
                } else {
                    parentElement.removeList(menuItemAsParentElement);
                }
                // added
                parentElement = null;

                MenuController.instance().removeMenuItemSelectedEventHandler(MenuItem.this);
                MenuController.instance().removeMenuItemGroupExpendedEventHandler(MenuItem.this);
            }
        };

        timer.schedule(500);


    }

    protected void expandOrCollapse() {
        if (!isLeaf()) {

            // Switch the token, otherwise the history protocol doesn't work,
            // collapsing or expanding can be done multiple times in a row,
            // to make sure that the back button keeps on working we need
            // to change the href of the resulting anchor.

            this.token = token.substring(0, token.indexOf(":")) + (isCollapsed() ? ":show" : ":hide");
            menuItemAsParentElement.setUriKey(this.token);
            menuItemAsParentElement.setCollapsed(!isCollapsed());
            for (MenuItem child : children) {
                child.setVisible(!child.isVisible());
            }
        }
    }

    public boolean isCollapsed() {
        if (!isLeaf()) {
            return menuItemAsParentElement.isCollapsed();
        } else {
            throw new RuntimeException("isCollapsed cannot be called on a leaf.");
        }
    }

    public void setVisible(boolean visible) {
        if (isLeaf()) {
            menuItemAsChildElement.setVisible(visible);
        } else {
            menuItemAsParentElement.setVisible(visible);
        }
    }

    public boolean isVisible() {
        if (isLeaf()) {
            return menuItemAsChildElement.isVisible();
        }

        return menuItemAsParentElement.isVisible();
    }

    public boolean isSelected() {
        if (!isLeaf()) {
            throw new RuntimeException("Parent elements don't have a mutable selected attribute.");
        }

        return menuItemAsChildElement.isSelected();
    }

    public void setSelected(boolean selected) {
        if (!isLeaf()) {
            throw new RuntimeException("Parent elements don't have a mutable selected attribute.");
        }

        menuItemAsChildElement.setSelected(selected);
    }

    public void setEnabled(boolean enabled) {
        if (!isLeaf()) {
            throw new RuntimeException("Parent elements don't have a mutable enabled attribute.");
        }

        menuItemAsChildElement.setEnabled(enabled);
    }

    public boolean isEnabled() {
        if (!isLeaf()) {
            throw new RuntimeException("Parent elements don't have a mutable enabled attribute.");
        }

        return menuItemAsChildElement.isEnabled();
    }

}
