package nl.sodeso.gwt.ui.client.form.validation.rules;

import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.resources.Resources;

import static nl.sodeso.gwt.ui.client.form.validation.ValidationMessage.Level;

/**
 * @author Ronald Mathies
 */
public abstract class FloatRangeValidationRule implements ValidationRule {

    // Keys for so-key entries.
    private static final String KEY_NOT_FLOAT_SUFFIX = "-not-float";
    private static final String KEY_MINIMUM_SUFFIX = "-minimum";
    private static final String KEY_MAXIMUM_SUFFIX = "-maximum";

    private String key = null;
    private Level level = null;

    private boolean useMinimum = false;
    private float minimum = -1;
    private boolean useMaximum = false;
    private float maximum = -1;

    /**
     * Constructs a new validation rule.
     *
     * @param key The key to use in the validation result.
     * @param level The message level to use in the validation result.
     */
    public FloatRangeValidationRule(String key, Level level) {
        this.key = key;
        this.level = level;
    }

    /**
     * Set the minimum value that is required.
     *
     * @param minimum the minimum value that is required.
     *
     * @return this instance.
     */
    public FloatRangeValidationRule setMinimum(float minimum) {
        this.useMinimum = true;
        this.minimum = minimum;
        return this;
    }

    /**
     * Set the maximum value that is required.
     *
     * @param maximum the maximum value that is required.
     *
     * @return this instance.
     */
    public FloatRangeValidationRule setMaximum(float maximum) {
        this.useMaximum = true;
        this.maximum = maximum;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public void validate(ValidationCompletedHandler handler) {
        ValidationResult result = new ValidationResult();
        if (!checkIsFloat()) {
            result.add(new ValidationMessage(this.key != null ? key + KEY_NOT_FLOAT_SUFFIX : null, level, Resources.VALIDATION_I18N.FloatRangeValidationNotFloat()));
        } else {
            if (useMinimum && checkMinimum()) {
                result.add(new ValidationMessage(this.key != null ? key + KEY_MINIMUM_SUFFIX : null, level, Resources.VALIDATION_I18N.FloatRangeValidationMinimum(this.minimum)));
            }

            if (useMaximum && checkMaximum()) {
                result.add(new ValidationMessage(this.key != null ? key + KEY_MAXIMUM_SUFFIX : null, level, Resources.VALIDATION_I18N.FloatRangeValidationMaximum(this.maximum)));
            }
        }

        handler.completed(result);
    }

//    /**
//     * {@inheritDoc}
//     */
//    public boolean isValid() {
//        if (!checkIsFloat()) {
//            return false;
//        }
//
//        if (useMinimum && !checkMinimum()) {
//            return false;
//        }
//
//        if (useMaximum && !checkMaximum()) {
//            return false;
//        }
//
//        return true;
//    }

    /**
     * {@inheritDoc}
     */
    public boolean isApplicable() {
        return true;
    }

    /**
     * Implement this method to return the string value to check for.
     *
     * @return the string value to check.
     */
    public abstract String getValue();

    /**
     * Check if the value entered is a number.
     * @return
     */
    private boolean checkIsFloat() {
        try {
            Float.parseFloat(getValue());
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

    private boolean checkMinimum() {
        float value = Float.parseFloat(getValue());
        return value < minimum;
    }

    private boolean checkMaximum() {
        float value = Float.parseFloat(getValue());
        return value > maximum;
    }

}
