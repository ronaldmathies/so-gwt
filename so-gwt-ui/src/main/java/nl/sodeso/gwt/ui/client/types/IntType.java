package nl.sodeso.gwt.ui.client.types;

/**
 * @author Ronald Mathies
 */
public class IntType extends ValueType<Integer> {

    /**
     * Constructs a new instance with an unspecified value.
     */
    public IntType() {}

    /**
     * Constructs a new instance with the specified value.
     *
     * @param value the initial value.
     */
    public IntType(Integer value) {
        super(value);
    }

    /**
     * {@inheritDoc}
     */
    public void setValueAsString(String value) {
        this.setValue(Integer.valueOf(value));
    }

}
