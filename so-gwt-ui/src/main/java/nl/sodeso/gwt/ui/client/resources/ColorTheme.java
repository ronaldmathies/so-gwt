package nl.sodeso.gwt.ui.client.resources;

/**
 * @author Ronald Mathies
 */
public enum ColorTheme {

    Red("red", "border", "red background"),
    Green("green", "green border", "green background"),
    Blue("blue", "blue border", "blue background");

    private String textStyle = null;
    private String borderStyle = null;
    private String backgroundStyle = null;

    ColorTheme(String textStyle, String borderStyle, String backgroundStyle) {
        this.textStyle = textStyle;
        this.borderStyle = borderStyle;
        this.backgroundStyle = backgroundStyle;
    }

    public String getTextStyle() {
        return textStyle;
    }

    public String getBorderStyle() {
        return borderStyle;
    }

    public String getBackgroundStyle() {
        return backgroundStyle;
    }

    public String getStyle(boolean text, boolean border, boolean background) {
        return "";
    }
}
