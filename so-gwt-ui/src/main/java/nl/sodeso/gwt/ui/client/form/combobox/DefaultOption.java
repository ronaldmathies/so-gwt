package nl.sodeso.gwt.ui.client.form.combobox;

/**
 * @author Ronald Mathies
 */
public class DefaultOption extends Option {

    public DefaultOption() {}

    public DefaultOption(String code) {
        super(code);
    }

    public DefaultOption(String code, String description) {
        super(code, description);
    }

    public DefaultOption(String key, String code, String description) {
        super(key, code, description);
    }

}
