package nl.sodeso.gwt.ui.client.form.button;

import com.google.gwt.i18n.client.Constants;

/**
 * @author Ronald Mathies
 */
public interface ButtonConstants extends Constants {

    @DefaultStringValue("First")
    String First();

    @DefaultStringValue("Previous")
    String Previous();

    @DefaultStringValue("Next")
    String Next();

    @DefaultStringValue("Last")
    String Last();

    @DefaultStringValue("Copy")
    String Copy();

    @DefaultStringValue("Duplicate")
    String Duplicate();

    @DefaultStringValue("Add")
    String Add();

    @DefaultStringValue("New")
    String New();

    @DefaultStringValue("Refresh")
    String Refresh();

    @DefaultStringValue("Remove")
    String Remove();

    @DefaultStringValue("Remove All")
    String RemoveAll();

    @DefaultStringValue("Save")
    String Save();

    @DefaultStringValue("Cancel")
    String Cancel();

    @DefaultStringValue("OK")
    String Ok();

    @DefaultStringValue("Yes")
    String Yes();

    @DefaultStringValue("No")
    String No();

    @DefaultStringValue("Search")
    String Search();

    @DefaultStringValue("Filter")
    String Filter();

    @DefaultStringValue("Clear")
    String Clear();

    @DefaultStringValue("Close")
    String Close();

    @DefaultStringValue("Login")
    String Login();

    @DefaultStringValue("Logout")
    String Logout();

    @DefaultStringValue("Logging")
    String Logging();

}
