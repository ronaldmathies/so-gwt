package nl.sodeso.gwt.ui.server.listener;

import nl.sodeso.commons.web.initialization.AbstractWebInitContext;
import nl.sodeso.gwt.ui.server.util.ServletContextInfo;

import javax.servlet.ServletContextEvent;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class ServletContextRootContextListener extends AbstractWebInitContext {

    private static final Logger log = Logger.getLogger(ServletContextRootContextListener.class.getName());

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(ServletContextEvent servletContextEvent) {
        log.info("Registering the servlet context path in the servlet context info.");
        ServletContextInfo.instance().setContextPath(servletContextEvent.getServletContext().getContextPath());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy(ServletContextEvent servletContextEvent) {
    }
}
