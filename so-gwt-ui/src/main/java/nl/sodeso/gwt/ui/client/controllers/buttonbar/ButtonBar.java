package nl.sodeso.gwt.ui.client.controllers.buttonbar;

import nl.sodeso.gwt.ui.client.form.button.SimpleButton;

import java.util.ArrayList;
import java.util.List;

/**
 * Container for holding buttons for the {@link ButtonBarController}
 *
 * @author Ronald Mathies
 */
public class ButtonBar {

    private List<SimpleButton> leftButtons = new ArrayList<>();
    private List<SimpleButton> rightButtons = new ArrayList<>();

    private boolean isFullwidth = false;

    public ButtonBar() {
    }

    /**
     * Adds buttons which will be shown on the left side of the button bar.
     * @param buttons the buttons to add.
     */
    public void addLeftButtons(List<SimpleButton> buttons) {
        this.leftButtons.addAll(buttons);
    }

    /**
     * Adds buttons which will be shown on the right side of the button bar.
     * @param buttons the buttons to add.
     */
    public void addRightButtons(List<SimpleButton> buttons) {
        this.rightButtons.addAll(buttons);
    }

    /**
     * Sets a flag indicating if the button bar is operating in full-width mode.
     * @param fullwidth true of the button bar is operating in full-width mode.
     */
    public void setFullwidth(boolean fullwidth) {
        this.isFullwidth = fullwidth;
    }

    /**
     * Flag indicating if the button bar is operating in full-width mode.
     * @return true if the button bar is operating in full-width mode.
     */
    public boolean isFullwidth() {
        return this.isFullwidth;
    }

}
