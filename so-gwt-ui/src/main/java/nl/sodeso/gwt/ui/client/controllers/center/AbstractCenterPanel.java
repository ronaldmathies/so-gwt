package nl.sodeso.gwt.ui.client.controllers.center;

import com.google.gwt.user.client.ui.FlowPanel;
import nl.sodeso.gwt.ui.client.trigger.Trigger;

import static nl.sodeso.gwt.ui.client.util.Constants.FALSE;
import static nl.sodeso.gwt.ui.client.util.Constants.TRUE;

/**
 * @author Ronald Mathies
 */
public abstract class AbstractCenterPanel extends FlowPanel {

    // CSS style classes.
    private static final String STYLE_CENTER_PANEL_ITEM = "center-panel-item";

    // Element attribute names.
    private static final String ATTR_FULL_HEIGHT = "so-full-height";

    public AbstractCenterPanel() {
        super();

        setStyleName(STYLE_CENTER_PANEL_ITEM);
    }

    /**
     * Sets the height of the widget to the full width of it's parent.
     * @param visible flag indicating if the full height should be visible.
     */
    public void setFullHeight(boolean visible) {
        getElement().setAttribute(ATTR_FULL_HEIGHT, visible ? TRUE : FALSE);
    }

    /**
     * Returns a flag indicating if the height attribute is set or not.
     * @return true if the height attribute is set, false if not.
     */
    public boolean isFullHeight() {
        return !FALSE.equals(getElement().getAttribute(ATTR_FULL_HEIGHT));
    }

    /**
     * Method is called just before the widget is added to the <code>CenterController</code>.
     *
     * Use this method to build up your user interface components.
     *
     * @param trigger call this trigger when you have finished your work.
     */
    public void beforeAdd(Trigger trigger) {
        trigger.fire();
    }

    /**
     * Method is called just after the widget is added to the <code>CenterController</code>.
     *
     * Use this method to perform Async calls or functionality that needs to be performed after the
     * widget has build up it's user interface and has been added to the <code>CenterController</code>.
     *
     * @param trigger call this trigger when you have finished your work.
     */
    public void afterAdd(Trigger trigger) {
        trigger.fire();
    }

    /**
     * Method is called just before the widget is being removed from the <code>CenterController</code>.
     *
     * @param trigger call this trigger when you have finished your work.
     */
    public void beforeRemoval(Trigger trigger) {
        trigger.fire();
    }

    /**
     * Method is called just after the widget has been removed from the <code>CenterController</code>.
     *
     * @param trigger call this trigger when you have finished your work.
     */
    public void afterRemoval(Trigger trigger) {
        trigger.fire();
    }

}
