package nl.sodeso.gwt.ui.client.form.validation.events;

import com.google.web.bindery.event.shared.Event;
import nl.sodeso.gwt.ui.client.form.validation.IsValidatable;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;

/**
 * @author Ronald Mathies
 */
public class ValidationChangedEvent extends Event<ValidationChangedEventHandler> {

    public static final Event.Type<ValidationChangedEventHandler> TYPE = new Event.Type<>();

    private IsValidatable source;
    private ValidationResult result;

    public ValidationChangedEvent(IsValidatable source, ValidationResult result) {
        this.source = source;
        this.result = result;
    }

    @Override
    public Event.Type<ValidationChangedEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(ValidationChangedEventHandler handler) {
        handler.onValidationChanged(this);
    }


    public IsValidatable getSource() {
        return this.source;
    }

    public ValidationResult getValidationResult() {
        return this.result;
    }

}