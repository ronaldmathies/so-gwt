package nl.sodeso.gwt.ui.client.form.input;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.LabelElement;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.validation.IsValidatable;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationEventBusController;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.rules.ValidationRule;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.gwt.ui.client.util.HasKey;

import java.util.ArrayList;

/**
 * LabelField results in a &gt;label/&lt; element which is used for example to donate a label for a field using
 * the #htmlFor method. To display labels at 'random' it is better to use the <code>SpanField</code>
 * which is a normal representation of text.
 *
 * @see SpanField
 *
 * @author Ronald Mathies
 */
public class LabelField extends Widget implements IsValidatable, HasKey {

    private static final String KEY_LABEL_SUFFIX = "-label";

    public final static String STYLE_LABEL = "label";

    private LabelElement labelElement = null;

    private ArrayList<ValidationRule> rules = new ArrayList<>();
    private ValidationEventBusController eventbus = new ValidationEventBusController();

    public LabelField(String key, String value) {
        super();

        labelElement = Document.get().createLabelElement();
        setElement(labelElement);
        setStyleName(STYLE_LABEL);
        setText(value);

        setKey(key);
    }

    public LabelField(String key, StringType valueType) {
        this(key, valueType.getValue());

        valueType.addValueChangedEventHandler(new ValueChangedEventHandler<StringType>(this) {
            @Override
            public void onValueCanged(ValueChangedEvent<StringType> event) {
                setText(event.getValueType().getValue());

                validate(new ValidationCompletedHandler() {
                    @Override
                    public void completed(ValidationResult result) {

                    }
                });
            }
        });

        setKey(key);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_LABEL_SUFFIX;
    }

    public void setText(String text) {
        labelElement.setInnerHTML(text);
    }

    public void setHtmlFor(String htmlFor) {
        labelElement.setHtmlFor(htmlFor);
    }

    /**
     * {@inheritDoc}
     */
    public ValidationEventBusController getValidationEventbus() {
        return eventbus;
    }

    /**
     * {@inheritDoc}
     */
    public ArrayList<ValidationRule> getValidationRules() {
        return this.rules;
    }

}
