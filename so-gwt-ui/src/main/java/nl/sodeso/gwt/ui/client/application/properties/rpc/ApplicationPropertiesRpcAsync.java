package nl.sodeso.gwt.ui.client.application.properties.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.gwt.ui.client.application.properties.ClientApplicationProperties;

/**
 * @author Ronald Mathies
 */
public interface ApplicationPropertiesRpcAsync {

    void getProperties(AsyncCallback<ClientApplicationProperties> properties);

}
