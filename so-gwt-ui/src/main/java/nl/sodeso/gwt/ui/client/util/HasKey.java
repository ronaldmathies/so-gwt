package nl.sodeso.gwt.ui.client.util;

import com.google.gwt.user.client.ui.Widget;

/**
 * @author Ronald Mathies
 */
public interface HasKey<X extends Widget> {

    /**
     * Set the key.
     * @param key the key.
     */
    @SuppressWarnings("unchecked")
    default void setKey(String key) {
        if (key != null) {
            KeyUtil.setKey((X) this, key, getKeySuffix());
        }
    }

    /**
     * Returns the key.
     * @return the key.
     */
    @SuppressWarnings("unchecked")
    default String getKey() {
        return KeyUtil.getKey((X) this, getKeySuffix());
    }

    /**
     * Flag indicating if there is a key.
     * @return true if there is a key, false if not.
     */
    @SuppressWarnings("unchecked")
    default boolean hasKey() {
        return KeyUtil.hasKey((X)this);
    }

    String getKeySuffix();
}
