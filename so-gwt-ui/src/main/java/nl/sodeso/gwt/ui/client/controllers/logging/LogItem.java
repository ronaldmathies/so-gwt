package nl.sodeso.gwt.ui.client.controllers.logging;

/**
 * @author Ronald Mathies
 */
public class LogItem {

    public enum LogLevel {
        SEVERE("SEVERE"),
        WARNING("WARNING"),
        INFO("INFO"),
        FINE("FINE"),
        FINER("FINER"),
        FINEST("FINEST");

        private String level;

        LogLevel(String level) {
            this.level = level;
        }

        public static LogLevel parse(String level) {
            if (level.equals(SEVERE.name())) {
                return SEVERE;
            } else if (level.equals(WARNING.name())) {
                return WARNING;
            } else if (level.equals(INFO.name())) {
                return INFO;
            } else if (level.equals(FINE.name())) {
                return FINE;
            } else if (level.equals(FINER.name())) {
                return FINER;
            } else  {
                return FINEST;
            }
        }

        public String getLevel() {
            return this.level;
        }
    }

    private LogLevel level;
    private String time;
    private String message;
    private String stackTrace;

    public LogItem() {
    }

    public void setLevel(String level) {
        this.level = LogLevel.parse(level);
    }

    public LogLevel getLevel() {
        return this.level;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return this.time;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStackTrace() {
        return this.stackTrace;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }
}
