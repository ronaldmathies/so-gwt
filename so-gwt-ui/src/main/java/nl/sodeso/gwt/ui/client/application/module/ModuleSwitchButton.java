package nl.sodeso.gwt.ui.client.application.module;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import nl.sodeso.gwt.ui.client.application.GwtApplicationEntryPoint;
import nl.sodeso.gwt.ui.client.controllers.navigation.NavigationButton;
import nl.sodeso.gwt.ui.client.resources.Icon;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ModuleSwitchButton extends NavigationButton {

    // Keys for so-key entries.
    private static final String KEY_SWITCH = "switch";

    // CSS style classes.
    private static final String STYLE_SWITCH_POPUP = "switch-popup";
    private static final String STYLE_SWITCH_CONTENTS = "contents";
    private static final String STYLE_SWITCH_ICON = "icon";

    // Element attribute names.
    private static final String ATTR_ICON_DISABLED = "so-disabled";

    public ModuleSwitchButton() {
        super(KEY_SWITCH, Icon.Desktop);

        addClickHandler((event) -> {
           switchModule();
        });
    }

    @SuppressWarnings("unchecked")
    public void switchModule() {
        final PopupPanel listFieldPopup = new PopupPanel(true, false);
        listFieldPopup.addStyleName(STYLE_SWITCH_POPUP);

        FlowPanel contents = new FlowPanel();
        contents.addStyleName(STYLE_SWITCH_CONTENTS);
        listFieldPopup.add(contents);

        ModuleEntryPoint activeModule = GwtApplicationEntryPoint.instance().getCurrentActiveModule();
        List<ModuleEntryPoint> availableModules = GwtApplicationEntryPoint.instance().getAvailableModules();

        for (final ModuleEntryPoint module : availableModules) {

            FlowPanel icon = new FlowPanel();
            icon.setStyleName(STYLE_SWITCH_ICON);

            if (module.getModuleId().equals(activeModule.getModuleId())) {
                icon.getElement().setAttribute(ATTR_ICON_DISABLED, "true");
            }  else {
                icon.addDomHandler((event) -> {
                    listFieldPopup.hide();
                    GwtApplicationEntryPoint.instance().activateModule(module);
                }, ClickEvent.getType());
            }

            // Create the icon contents.
            Element iconElement = Document.get().createElement("I");
            iconElement.addClassName(module.getModuleIcon().getStyle());
            icon.getElement().appendChild(iconElement);
            contents.add(icon);
        }

        listFieldPopup.showRelativeTo(this);
    }

}
