package nl.sodeso.gwt.ui.server.listener;

import nl.sodeso.commons.web.initialization.AbstractWebInitContext;
import nl.sodeso.gwt.ui.server.endpoint.properties.ModulePropertiesContainer;
import nl.sodeso.gwt.ui.server.endpoint.properties.ServerModuleProperties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public abstract class AbstractModulePropertiesInitializerContextListener<T extends ServerModuleProperties> extends AbstractWebInitContext {

    private static final Logger log = Logger.getLogger(AbstractModulePropertiesInitializerContextListener.class.getName());

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(ServletContextEvent event) {
        log.log(Level.INFO, String.format("Initializing module '%s' properties.", getModuleId()));

        ModulePropertiesContainer.instance().setModuleProperties(getModuleId(), newServerModulePropertiesInstance());

        log.log(Level.INFO, String.format("Finished initializing module '%s' properties.", getModuleId()));

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy(ServletContextEvent event) {
    }

    /**
     * Constructs a new server side application properties instance.
     * @return the new server side application properties instance.
     */
    private T newServerModulePropertiesInstance() {
        try {
            return (T) getServerModulePropertiesClass().getDeclaredConstructor().newInstance();
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException | NoSuchMethodException illegalAccessException) {
            log.log(Level.SEVERE, "Failed to instantiate application properties class.", illegalAccessException);
        }
        return null;
    }

    /**
     * The moduleId.
     * @return the moduleId.
     */
    public abstract String getModuleId();

    /**
     * Should return the class definition for the server side module properties.
     * @return the class definition for the server side module properties.
     */
    public abstract Class<T> getServerModulePropertiesClass();

}