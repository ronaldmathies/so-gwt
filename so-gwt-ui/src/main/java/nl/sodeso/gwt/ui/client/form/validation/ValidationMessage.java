package nl.sodeso.gwt.ui.client.form.validation;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class ValidationMessage implements Serializable {

    // CSS style classes.
    private transient static final String STYLE_INFO = "info";
    private transient static final String STYLE_WARN = "warn";
    private transient static final String STYLE_ERROR = "error";

    public enum Level {
        INFO(STYLE_INFO),
        WARN(STYLE_WARN),
        ERROR(STYLE_ERROR);

        private String style = null;

        Level(String style) {
            this.style = style;
        }

        public String getStyle() {
            return this.style;
        }

        public boolean isInfo() {
            return this.equals(INFO);
        }

        public boolean isWarning() {
            return this.equals(WARN);
        }

        public boolean isError() {
            return this.equals(ERROR);
        }
    }

    private String key = null;
    private String message = null;
    private Level level = Level.INFO;

    /**
     * No-arg constructor for serialization purposes.
     */
    public ValidationMessage() {}

    public String toString() {
        String result = level.name() + ": ";
        result += key != null && !key.isEmpty() ? key + " - " : "";
        result += message != null && !message.isEmpty() ? message : "";
        return result;
    }

    /**
     * Constructs a new validation message.
     *
     * @param key a unique key identifying this specific validation message.
     * @param level the level identifying the seriousness of this validation message.
     * @param message the validation message itself.
     */
    public ValidationMessage(String key, Level level, String message) {
        this.key = key;
        this.message = message;
        this.level = level;
    }

    /**
     * Return the unique key identifying this validation message.
     * @return the unique key.
     */
    public String getKey() {
        return this.key;
    }

    /**
     * Returns the validation message.
     * @return the validation message.
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * Returns the level of seriousness.
     * @return the level of seriousness.
     */
    public Level getLevel() {
        return this.level;
    }

    /**
     * Flag indicating if this is an info validation message.
     * @return true if this is an info validation message, false if not.
     */
    public boolean isInfo() {
        return level.equals(Level.INFO);
    }

    /**
     * Flag indicating if this is a warning validation message.
     * @return true if this is a warning validation message, false if not.
     */
    public boolean isWarning() {
        return level.equals(Level.WARN);
    }

    /**
     * Flag indicating if this is an error validation message.
     * @return true if this is an error validation message, false if not.
     */
    public boolean isError() {
        return level.equals(Level.ERROR);
    }
}
