package nl.sodeso.gwt.ui.client.types;

/**
 * @author Ronald Mathies
 */
public class LongType extends ValueType<Long> {

    /**
     * Constructs a new instance with an unspecified value.
     */
    public LongType() {}

    /**
     * Constructs a new instance with the specified value.
     *
     * @param value the initial value.
     */
    public LongType(Long value) {
        super(value);
    }

    /**
     * {@inheritDoc}
     */
    public void setValueAsString(String value) {
        this.setValue(Long.valueOf(value));
    }

}
