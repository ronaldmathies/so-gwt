package nl.sodeso.gwt.ui.client.form.input;

import com.google.gwt.user.client.ui.CheckBox;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.util.HasKey;
import nl.sodeso.gwt.ui.client.util.KeyUtil;

/**
 * @author Ronald Mathies
 */
public class CheckboxField extends CheckBox implements HasKey {

    private static final String KEY_CHECKBOX_SUFFIX = "-field";

    // Element attribute names.
    public final static String ATTR_ALIGNMENT = "so-alignment";

    protected CheckboxField(String key, String label) {
        super(label);
        setAlignment(Align.VERTICAL);

        setKey(key);
    }

    public void setAlignment(Align align) {
        if (!align.isHorizontal() && !align.isVertical()) {
            throw new RuntimeException("Only horizontal and vertical alignment is allowed");
        }

        getElement().setAttribute(ATTR_ALIGNMENT, align.getAlignment());
    }

    public Align getAlignment() {
        String value = getElement().getAttribute(ATTR_ALIGNMENT);
        return value == null || value.equals(Align.HORIZONTAL) ? Align.HORIZONTAL : Align.VERTICAL;
    }

    /**
     * {@inheritDoc}
     */
    public void setKey(String key) {
        KeyUtil.setKey(getElement().getFirstChildElement(), key, KEY_CHECKBOX_SUFFIX);
    }

    /**
     * {@inheritDoc}
     */
    public String getKey() {
        return KeyUtil.getKey(getElement().getFirstChildElement(), KEY_CHECKBOX_SUFFIX);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_CHECKBOX_SUFFIX;
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasKey() {
        return KeyUtil.hasKey(getElement().getFirstChildElement());
    }
}
