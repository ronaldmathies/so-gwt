package nl.sodeso.gwt.ui.client.form.validation.rules;

import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.resources.Resources;

/**
 * @author Ronald Mathies
 */
public abstract class PasswordMatchValidationRule implements ValidationRule {

    // Keys for so-key entries.
    private static final String KEY_DO_NOT_MATCH_SUFFIX = "-do-not-match";

    private String key = null;
    private ValidationMessage.Level level = null;

    /**
     * Constructs a new validation rule.
     *
     * @param key The key to use in the validation result.
     * @param level The message level to use in the validation result.
     */
    public PasswordMatchValidationRule(String key, ValidationMessage.Level level) {
        this.key = key;
        this.level = level;
    }

    /**
     * {@inheritDoc}
     */
    public void validate(ValidationCompletedHandler handler) {
        ValidationResult result = new ValidationResult();
        if (!getPasswordValue().equals(getPasswordVerifyValue())) {
            result.add(new ValidationMessage(this.key != null ? key + KEY_DO_NOT_MATCH_SUFFIX : null, level, Resources.VALIDATION_I18N.PasswordsDoNotMath()));
        }

        handler.completed(result);
    }

//    /**
//     * {@inheritDoc}
//     */
//    public boolean isValid() {
//        return getPasswordValue().equals(getPasswordVerifyValue());
//    }

    /**
     * {@inheritDoc}
     */
    public boolean isApplicable() {
        return true;
    }

    /**
     * Should return the value of the first password field.
     * @return the value of the first password field.
     */
    public abstract String getPasswordValue();

    /**
     * Should return the value of the password verify field.
     * @return the value of the password verify field.
     */
    public abstract String getPasswordVerifyValue();

}
