package nl.sodeso.gwt.ui.client.controllers.menu.events;

import com.google.web.bindery.event.shared.Event;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;


/**
 * @author Ronald Mathies
 */
public class MenuItemEnableEvent extends Event<MenuItemEnableEventHandler> {

    public static final Type<MenuItemEnableEventHandler> TYPE = new Type<>();

    private MenuItem menuItem;

    public MenuItemEnableEvent(MenuItem menuItem) {
        this.menuItem = menuItem;
    }

    @Override
    public Type<MenuItemEnableEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(MenuItemEnableEventHandler handler) {
        handler.onEvent(this);
    }

    public MenuItem getMenuItem() {
        return this.menuItem;
    }
}
