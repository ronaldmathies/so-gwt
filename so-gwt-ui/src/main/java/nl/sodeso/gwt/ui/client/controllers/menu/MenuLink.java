package nl.sodeso.gwt.ui.client.controllers.menu;


import nl.sodeso.gwt.ui.client.link.Link;

import java.util.Map;

/**
 * Menu Link will check if a given token can be interpreted by any menu item,
 * and if the caller finds this to be usefull it gives the ability to invoke (navigate) this menu
 * item.
 *
 * @author Ronald Mathies
 */
public class MenuLink implements Link {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canProcessToken(String token) {
        return MenuController.instance().canHandleLink(token);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void navigate(String token, Map<String, String> arguments) {
        MenuController.instance().handleLink(token, arguments);
    }

}
