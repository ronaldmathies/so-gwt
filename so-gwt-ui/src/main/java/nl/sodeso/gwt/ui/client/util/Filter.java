package nl.sodeso.gwt.ui.client.util;

/**
 * @author Ronald Mathies
 */
public interface Filter<O> {

    boolean filter(O object);

}
