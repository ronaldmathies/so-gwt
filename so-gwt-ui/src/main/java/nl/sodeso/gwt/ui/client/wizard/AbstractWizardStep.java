package nl.sodeso.gwt.ui.client.wizard;

import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.trigger.Trigger;

/**
 * @author Ronald Mathies
 */
public abstract class AbstractWizardStep<C extends WizardContext> {

    private C wizardContext = null;
    private AbstractWizardStep<C> callingStep;

    public AbstractWizardStep(AbstractWizardStep<C> callingStep) {
        this.callingStep = callingStep;
    }

    protected AbstractWizardStep<C> getCallingStep() {
        return this.callingStep;
    }

    /**
     * Should return the previous step.
     * @return the previous step.
     */
    public abstract AbstractWizardStep<C> getPreviousStep();

    /**
     * Will be called before the previous step is displayed.
     * @param trigger fire this trigger when the work for this step has been performed. (for example async-calls)
     */
    public void onBeforePrevious(Trigger trigger) {
        trigger.fire();
    }

    /**
     * Should return the next step.
     * @return the next step.
     */
    public abstract AbstractWizardStep<C> getNextStep();

    /**
     * Returns the index of this step within the whole wizard.
     * @return the index of this step within the whole wizard.
     */
    public int getStepIndex() {
        int index = 0;
        AbstractWizardStep<C> previousStep = getPreviousStep();
        while (previousStep != null) {
            index++;
            previousStep = previousStep.getPreviousStep();
        }

        return index;
    }

    /**
     * Will be called before the next stop is displayed.
     * @param trigger fire this trigger when the work for this step has been performed. (for example async-calls)
     */
    public void onBeforeNext(Trigger trigger) {
        trigger.fire();
    }

    /**
     * Should return the body which will be displayed in the body of the window.
     * This method is only called when the step is actually going to be displayed.
     *
     * @return the body to be displayed.
     */
    public abstract Widget getBodyWidget();

    protected void setWizardContext(C wizardContext) {
        this.wizardContext = wizardContext;
    }

    protected C getWizardContext() {
        return this.wizardContext;
    }
}
