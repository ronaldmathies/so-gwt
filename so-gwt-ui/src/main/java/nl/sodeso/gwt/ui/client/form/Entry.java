package nl.sodeso.gwt.ui.client.form;

import com.google.gwt.user.client.ui.FlowPanel;
import nl.sodeso.gwt.ui.client.util.HasKey;

/**
 * @author Ronald Mathies
 */
public abstract class Entry extends FlowPanel implements HasKey {

    // Keys for so-key entries.
    protected final static String KEY_ENTRY_SUFFIX = "-entry";

    private EntryForm entryForm = null;

    public Entry() {
    }

    public void attachToParent(EntryForm entryForm) {
        this.entryForm = entryForm;
    }

    public void detachFromParent() {
        this.removeFromParent();
    }

    public EntryForm getEntryForm() {
        return this.entryForm;
    }

}
