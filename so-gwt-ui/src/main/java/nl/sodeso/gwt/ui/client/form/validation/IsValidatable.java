package nl.sodeso.gwt.ui.client.form.validation;

import com.google.web.bindery.event.shared.Event.Type;
import nl.sodeso.gwt.ui.client.form.validation.events.ValidationChangedEvent;
import nl.sodeso.gwt.ui.client.form.validation.rules.ValidationRule;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface IsValidatable {

    ValidationEventBusController getValidationEventbus();

    /**
     * Should return the validation rules that are applicable.
     * @return the validation rules that are applicable.
     */
    ArrayList<ValidationRule> getValidationRules();

    /**
     * Add a new validation rule.
     * @param rule the rule to add.
     */
    @SuppressWarnings("unchecked")
    default <X> X addValidationRule(ValidationRule rule) {
        getValidationRules().add(rule);
        return (X)this;
    }

    @SuppressWarnings("unchecked")
    default <X> X addValidationRules(ValidationRule ... rules) {
        for (ValidationRule rule : rules) {
            addValidationRule(rule);
        }
        return (X)this;
    }

    /**
     * Adds a new validation handler which will receive events when the validation situation
     * changes.
     *
     * @param type the event type to register the handler to.
     * @param handler the handler which will receive the events.
     * @param <H> The handler type.
     */
    @SuppressWarnings("unchecked")
    default <H, X> X addValidationHandler(Type<H> type, H handler) {
        getValidationEventbus().addHandler(type, handler);
        return (X)this;
    }

    /**
     * Removes an existing validation handler.
     *
     * @param type the event type the handler is registered to.
     * @param handler the handler which should be removed.
     */
    @SuppressWarnings("unchecked")
    default <X> X removeValidationHandler(Type type, Object handler) {
        getValidationEventbus().removeHandler(type, handler);
        return (X) this;
    }

    /**
     * Fires a validation event which will be received by all registered handlers.
     *
     * @param validationChangedEvent the event.
     */
    default void fireValidationEvent(ValidationChangedEvent validationChangedEvent) {
        getValidationEventbus().fireEvent(validationChangedEvent);
    }

    /**
     * Should perform the validation of all registered validation rules.
     *
     * @param handler the handler to be called when the validation is completed.
     */
    default void validate(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, this);
    }

}
