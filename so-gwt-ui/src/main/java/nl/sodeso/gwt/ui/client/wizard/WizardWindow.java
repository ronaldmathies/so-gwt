package nl.sodeso.gwt.ui.client.wizard;

import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.resources.Resources;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.util.Align;

/**
 * @author Ronald Mathies
 */
public class WizardWindow<C extends WizardContext> {

    private static final String KEY_NEXT = "next";
    private static final String KEY_PREVIOUS = "previous";

    private AbstractWizardStep<C> currentStep = null;

    private WindowPanel window = null;

    private SimpleButton previous = null;
    private SimpleButton next = null;

    private C wizardContext;

    public WizardWindow(WindowPanel window, C wizardContext) {
        this.window = window;
        this.wizardContext = wizardContext;

        this.previous = new SimpleButton(KEY_PREVIOUS, Resources.BUTTON_I18N.Previous(), SimpleButton.Style.BLUE);
        this.previous.addClickHandler((event) -> previous());
        this.previous.setVisible(false);

        this.next = new SimpleButton(KEY_NEXT, Resources.BUTTON_I18N.Next(), SimpleButton.Style.BLUE);
        this.next.addClickHandler((event) -> next());
        this.next.setVisible(false);

        window.addToFooter(Align.RIGHT, next, previous);
    }

    public C getWizardContext() {
        return this.wizardContext;
    }

    public WindowPanel getWindow() {
        return this.window;
    }

    /**
     * Sets the initial wizard step which is used to display the first
     * screen within the wizard window.
     *
     * @param step the initial wizard step.
     */
    public void setWizardStep(AbstractWizardStep<C> step) {
        setCurrentStep(step);
    }

    private void setCurrentStep(AbstractWizardStep<C> step) {
        this.currentStep = step;

        if (step.getWizardContext() == null) {
            step.setWizardContext(wizardContext);
        }

        window.clearBody();
        window.addToBody(currentStep.getBodyWidget());

        this.evaluateNavigation();
    }

    /**
     * Evaluates the navigation button based on current steps previous and next step.
     */
    public void evaluateNavigation() {
        this.previous.setVisible(currentStep.getPreviousStep() != null);
        this.next.setVisible(currentStep.getNextStep() != null);
    }

    /**
     * Changes the step to the previous step.
     */
    private void previous() {
        currentStep.onBeforePrevious(() -> {
            AbstractWizardStep<C> previousStep = currentStep.getPreviousStep();
            if (previousStep != null) {
                setCurrentStep(previousStep);
            }
        });
    }

    /**
     * Changes the step to the next step.
     */
    private void next() {
        currentStep.onBeforeNext(() -> {
            AbstractWizardStep<C> nextStep = currentStep.getNextStep();
            if (nextStep != null) {
                setCurrentStep(nextStep);
            }
        });
    }

    public void setVisible(boolean visible) {
        if (this.window instanceof PopupWindowPanel) {
            if (visible) {
                ((PopupWindowPanel) this.window).center(false, true);
            } else {
                ((PopupWindowPanel) this.window).close();
            }
        } else {
            this.window.setVisible(visible);
        }
    }

    public boolean isVisible() {
        return this.window.isVisible();
    }

}
