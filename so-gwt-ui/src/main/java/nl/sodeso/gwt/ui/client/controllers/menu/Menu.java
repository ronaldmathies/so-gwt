package nl.sodeso.gwt.ui.client.controllers.menu;

import nl.sodeso.gwt.event.client.EventBusController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class Menu {

    private ArrayList<MenuItem> items = new ArrayList<MenuItem>();
    private EventBusController eventbus = null;

    private boolean fullwidth = true;

    public Menu() {
        eventbus = new EventBusController();
    }

    /**
     * Returns the event bus used by the menu.
     * @return the event bus used by the menu.
     */
    public EventBusController getEventbus() {
        return this.eventbus;
    }

    /**
     * Returns the menu items.
     * @return the menu items.
     */
    public List<MenuItem> getItems() {
        return this.items;
    }

    /**
     * Adds the menu items to the menu.
     * @param itemsToAdd the items to add.
     */
    public Menu addMenuItems(MenuItem ... itemsToAdd) {
        for (MenuItem item : itemsToAdd) {
            if (item.getParent() != null) {
                throw new RuntimeException("Adding an item which in itself is already added to a parent is not allowed.");
            }

            items.add(item);
        }

        return this;
    }

    /**
     * Removes the menu items from the menu.
     * @param itemsToRemove the items to remove.
     */
    public void removeMenuItems(MenuItem ... itemsToRemove) {
        for (MenuItem itemToRemove : itemsToRemove) {
            for (MenuItem existingItem : items) {

                existingItem.removeChildren(itemToRemove);

                if (itemToRemove.hasKey(existingItem.getKey())) {
                    itemToRemove.detachFromParent();
                    items.remove(itemToRemove);
                }

            }
        }
    }

    /**
     * Retrieves the menu item with the specified <code>key</code>.
     * @param key the key of the menu item.
     * @return the menu item associated with the specified <code>key</code>
     */
    public MenuItem getMenuItem(String key) {
        for (MenuItem item : items) {
            if (item.getKey().equals(key)) {
                return item;
            }

            MenuItem child;
            if ((child = item.getChild(key)) != null) {
                return child;
            }
        }

        return null;
    }

    /**
     * Returns an array with all the menu items, however the structure is flattened so all
     * parent items en child items are on one level.
     *
     * @return the flattened menu items.
     */
    @SuppressWarnings("unchecked")
    protected ArrayList<MenuItem> flatten() {
        ArrayList<MenuItem> flattened = new ArrayList();

        for (int rootIdx = 0; rootIdx < items.size(); rootIdx++) {
            MenuItem root = items.get(rootIdx);

            flattened.add(root);
            if (!root.isLeaf()) {
                flattened.addAll(root.getChildren());
            }
        }

        return flattened;
    }

    public boolean isFullwidth() {
        return this.fullwidth;
    }

    public void setFullwidth(boolean fullwidth) {
        this.fullwidth = fullwidth;
    }
}
