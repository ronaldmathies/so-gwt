package nl.sodeso.gwt.ui.client.form;

import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.input.LabelField;

/**
 * @author Ronald Mathies
 */
public class EntryWithLabelAndWidget extends EntryWithWidgetAndWidget  {

    // Keys for so-key entries.
    private static final String KEY_LABEL_SUFFIX = "-label";

    // CSS style classes
    private final static String STYLE_ENTRY_LABEL = "entry-label-widget";
    private final static String STYLE_VALUE_CONTAINER = "value-container";

    /**
     * Constructs a new EntryLabel with the specified key, label and rightWidget to display.
     *
     * @param key the unique identifier for this row.
     * @param label the label to display before the rightWidget.
     * @param widget the rightWidget to display after the label.
     */
    public EntryWithLabelAndWidget(String key, String label, Widget widget) {
        super(key, createLabelWidget(key, label), widget);
    }

    private static LabelField createLabelWidget(String key, String label) {
        LabelField labelField = new LabelField(key + KEY_LABEL_SUFFIX, label);
        labelField.setHtmlFor(key + "-field");
        return labelField;
    }

    /**
     * {@inheritDoc}
     */
    protected String getWidgetStyle() {
        return STYLE_ENTRY_LABEL;
    }

    /**
     * {@inheritDoc}
     */
    protected String getLeftWidgetContainerStyle() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    protected String getRightWidgetContainerStyle() {
        return STYLE_VALUE_CONTAINER;
    }

}