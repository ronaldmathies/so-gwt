package nl.sodeso.gwt.ui.client.panel;

import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.PopupPanel;

/**
 * @author Ronald Mathies
 */
public class PopupWindowPanel extends WindowPanel {

    // Keys for so-key entries.
    private static final String KEY_POPUP_SUFFIX = "-popup";

    // CSS style classes.
    private static final String STYLE_POPUP_WINDOW = "popover-window";

    private int dragStartX = 0;
    private int dragStartY = 0;

    private int clientLeft;
    private int clientTop;
    private int windowWidth;

    private boolean isCurrentlyDragging = false;

    private PopupPanel popup = null;

    private HandlerRegistration mouseMoveHandlerRegistration = null;
    private HandlerRegistration mouseDownHandlerRegistration = null;
    private HandlerRegistration mouseUpHandlerRegistration = null;

    public PopupWindowPanel(String key, Style style) {
        this(key, null, style);
    }

    public PopupWindowPanel(String key, String title, Style style) {
        super(key != null ? key + KEY_POPUP_SUFFIX : KEY_POPUP_SUFFIX, title, style);

        addStyleName(STYLE_POPUP_WINDOW);

        // Add a attach handler to make sure then when a window panel is removed from the DOM
        // the handlers for the mouse are also removed (especially from the RootPanel).
        this.addAttachHandler(event -> {
            if (event.isAttached()) {
                attach();
            } else {
                detach();
            }
        });

        clientLeft = Document.get().getBodyOffsetLeft();
        clientTop = Document.get().getBodyOffsetTop();
        windowWidth = Window.getClientWidth();
    }

    private void attach() {
        mouseDownHandlerRegistration = this.header.addDomHandler(event -> {
            if (DOM.getCaptureElement() == null) {
                isCurrentlyDragging = true;
                DOM.setCapture(header.getElement());

                dragStartX = event.getX();
                dragStartY = event.getY();
            }
        }, MouseDownEvent.getType());

        mouseUpHandlerRegistration = this.header.addDomHandler(event -> {
            isCurrentlyDragging = false;
            DOM.releaseCapture(this.header.getElement());
        }, MouseUpEvent.getType());

        mouseMoveHandlerRegistration = this.header.addDomHandler(event -> {
            if (!isCurrentlyDragging) {
                return;
            }

            int absX = event.getX() + header.getAbsoluteLeft();
            int absY = event.getY() + header.getAbsoluteTop();

            if (absX < clientLeft || absX >= windowWidth || absY < clientTop) {
                return;
            }

            popup.setPopupPosition(absX - dragStartX, absY - dragStartY);

            event.preventDefault();
        }, MouseMoveEvent.getType());
    }



    private void detach() {
        if (mouseMoveHandlerRegistration != null) {
            mouseMoveHandlerRegistration.removeHandler();
        }

        if (mouseDownHandlerRegistration != null) {
            mouseDownHandlerRegistration.removeHandler();
        }

        if (mouseUpHandlerRegistration != null) {
            mouseUpHandlerRegistration.removeHandler();
        }
    }

    public PopupWindowPanel center(boolean autohide, boolean modal) {
        popup = new PopupPanel(autohide, modal);
        popup.setWidget(this);
        popup.show();

        // Might not take in to account if the page has scrolled in its entire form.
        int left = (Window.getClientWidth() / 2) - (this.getOffsetWidth() / 2);
        int top = (Window.getClientHeight() / 2) - (this.getOffsetHeight() / 2);

        popup.setPopupPosition(left, top);

        return this;
    }

    public void close() {
        popup.hide();
    }

}
