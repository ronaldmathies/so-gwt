package nl.sodeso.gwt.ui.client.form.upload;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.InputElement;
import com.google.gwt.i18n.client.NumberFormat;
import nl.sodeso.gwt.ui.client.resources.Resources;

/**
 * @author Ronald Mathies
 */
public class FileUploadUtil {

    public static File[] getFiles(InputElement inputElement) {
        final JsArray<File> selectedFiles = getSelectedFiles(inputElement);
        final File[] result = new File[selectedFiles.length()];
        for (int i = 0; i < selectedFiles.length(); ++i) {
            result[i] = selectedFiles.get(i);
        }

        return result;
    }

    public static native JsArray<File> getSelectedFiles(InputElement inputElement) /*-{
        return inputElement.files;
    }-*/;

    /**
     * Returns the size of the file in a displayable format.
     *
     * @param bytes the file size in bytes.
     * @param si
     * @return a displayable format of the file size.
     */
    public static String getFileSizeFormatted(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "B" : "iB");
        return "" + (Math.round(bytes / Math.pow(unit, exp) * 10d) / 10d)  + " " + pre;
    }

//    public static String getFileSizeFormattedTest(long filesize) {
//        double _1024 = 1024.0;
//
//        double psize = filesize;
//        double size = filesize / _1024;
//        if (filesize / (_1024 * _1024) < 1) {
//            return "" + psize + " bytes";
//        }
//
//        psize = Math.round(size) + ((psize % _1024) / 1000);
//        size  = size / _1024;
//        if (size < _1024) {
//            return "" + psize + "kb";
//        }
//
//        size  = Math.round(size / _1024);
//        size = size / _1024;
//        if (size < _1024) {
//            return "" + psize + "mb";
//        }
//
//        size = size / _1024;
//        return "" + size + "gb";
//    }
//
//    public static void main(String arg[] ) {
//        System.out.println(getFileSizeFormatted(512, true));
//        System.out.println(getFileSizeFormatted(1024 + 40, true));
//        System.out.println(getFileSizeFormatted(1024 * 1024 + 40, true));
//        System.out.println(getFileSizeFormatted(1024 * 1024 * 1024 + 40, true));
//        System.out.println(getFileSizeFormatted(512, true));
//    }


}
