package nl.sodeso.gwt.ui.client.form.table.summary;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Ronald Mathies
 */
public abstract class AbstractSummaryItem implements Serializable {

    public AbstractSummaryItem() {}

    public abstract String getLabel();

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        if (!super.equals(other)) {
            return false;
        }

        AbstractSummaryItem that = (AbstractSummaryItem) other;
        return Objects.equals(getLabel(), that.getLabel());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getLabel());
    }
}
