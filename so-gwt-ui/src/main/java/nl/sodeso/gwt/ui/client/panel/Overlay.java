package nl.sodeso.gwt.ui.client.panel;

import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Can be used to display an overlay over the complete screen, it is only possible
 * to add a single overlay, adding multiple will not result in multiple overlays.
 *
 * @author Ronald Mathies
 */
public class Overlay extends Widget {

    // CSS style classes.
    private static final String STYLE_OVERLAY = "overlay";
    private static final String STYLE_OVERLAY_MESSAGE = "overlay-message";

    private static Overlay instance = new Overlay();

    private Overlay() {
        DivElement element = Document.get().createDivElement();
        element.addClassName(STYLE_OVERLAY);

        DivElement messageElement = Document.get().createDivElement();
        messageElement.addClassName(STYLE_OVERLAY_MESSAGE);
        messageElement.insertFirst(Document.get().createTextNode("Please wait..."));

        element.insertFirst(messageElement);
        setElement(element);
    }

    /**
     * Shows the overlay.
     */
    public static void show() {
        RootPanel rootpanel = RootPanel.get();

        boolean hasOverlay = false;
        for (int index = 0; index < rootpanel.getWidgetCount(); index++) {
            hasOverlay |= rootpanel.getWidget(index) instanceof Overlay;
        }

        if (!hasOverlay) {
            rootpanel.add(instance);
        }
    }

    /**
     * Hides the overlay.
     */
    public static void hide() {
        RootPanel.get().remove(instance);
    }

}
