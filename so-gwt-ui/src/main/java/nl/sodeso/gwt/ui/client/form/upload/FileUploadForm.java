package nl.sodeso.gwt.ui.client.form.upload;

import com.google.gwt.core.client.GWT;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.user.client.ui.FormPanel;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.trigger.Trigger;

/**
 * @author Ronald Mathies
 */
public class FileUploadForm extends FormPanel implements FormPanel.SubmitHandler, FormPanel.SubmitCompleteHandler {

    private transient FileUploadField fileUploadField = null;

    private transient Trigger fileUploadCompleteTrigger = null;

    public FileUploadForm(FileUploadField fileUploadField, String action) {
        super();

        setAction(GWT.getHostPageBaseURL() + (action.startsWith("/") ? action.substring(1) : action));

        this.fileUploadField = fileUploadField;

        this.addSubmitHandler(this);
        this.addSubmitCompleteHandler(this);

        this.setEncoding(FormPanel.ENCODING_MULTIPART);
        this.setMethod(FormPanel.METHOD_POST);

        this.add(fileUploadField);
    }

    @Override
    public void onSubmit(SubmitEvent event) {
        fileUploadField.validate(result -> {
            if (ValidationUtil.findWorstLevel(result) == ValidationMessage.Level.ERROR) {
                event.cancel();
            }
        });
    }

    public void onSubmitComplete(SubmitCompleteEvent event) {
        JSONObject response = (JSONObject) JSONParser.parseStrict(event.getResults());
        String uuid = ((JSONString) response.get("uuid")).stringValue();
        fileUploadField.getFileUploadType().getValue().setUuid(uuid);

        if (this.fileUploadCompleteTrigger != null) {
            this.fileUploadCompleteTrigger.fire();
        }
    }

    public void setFileUploadCompleteTrigger(Trigger fileUploadCompleteTrigger) {
        this.fileUploadCompleteTrigger = fileUploadCompleteTrigger;
    }

}
