package nl.sodeso.gwt.ui.client.controllers.menu;

import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.user.client.ui.FlowPanel;
import nl.sodeso.gwt.event.client.EventBusController;
import nl.sodeso.gwt.ui.client.application.GwtApplicationEntryPoint;
import nl.sodeso.gwt.ui.client.application.event.ModuleActivateEvent;
import nl.sodeso.gwt.ui.client.application.event.ModuleActivateEventHandler;
import nl.sodeso.gwt.ui.client.application.event.ModuleSuspendEvent;
import nl.sodeso.gwt.ui.client.application.event.ModuleSuspendEventHandler;
import nl.sodeso.gwt.ui.client.controllers.menu.events.*;
import nl.sodeso.gwt.ui.client.link.LinkFactory;
import nl.sodeso.gwt.ui.client.util.HasKey;

import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;

import static nl.sodeso.gwt.ui.client.util.Constants.FALSE;
import static nl.sodeso.gwt.ui.client.util.Constants.TRUE;

/**
 * @author Ronald Mathies
 */
public class MenuController extends FlowPanel implements HasKey, ModuleActivateEventHandler, ModuleSuspendEventHandler{

    // Keys for so-key entries.
    private static final String KEY_MENU_SUFFIX = "-menu";
    private static final String KEY_MENU_PANEL = "main";

    // CSS style classes.
    private static final String STYLE_MENU_PANEL = "menu-panel";

    // Element attribute names.
    private static final String ATTR_VISIBLE = "so-visible";
    private static final String ATTR_EXPANDED = "so-expanded";

    public static MenuController instance;

    private UnorderedListElement menuListElement;

    private Menu active = new Menu();
    private Map<String, Menu> menus = new HashMap<>();

    private EventBusController eventbus = new EventBusController();

    private MenuController() {
        addStyleName(STYLE_MENU_PANEL);

        GwtApplicationEntryPoint.instance().addModuleActivateEventHandler(this);
        GwtApplicationEntryPoint.instance().addModuleSuspendEventHandler(this);

        menuListElement = new UnorderedListElement();
        getElement().appendChild(menuListElement.getElement());

        LinkFactory.addLink(new MenuLink());

        this.addDomHandler(event -> {
            setExpanded(true);
        }, MouseOverEvent.getType());

        this.addDomHandler(event -> {
            setExpanded(false);
        }, MouseOutEvent.getType());

        // Perform only after creating the eventbus, since it triggers
        // an event.
        setFullwidth(true);
        setExpanded(false);

        setKey(KEY_MENU_PANEL);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_MENU_SUFFIX;
    }

    public static MenuController instance() {
        if (instance == null) {
            instance = new MenuController();
        }

        return instance;
    }

    /**
     * Adds a handler which will receive events when the menu has changed visibility (full width or hidden), this is
     * a menu controller scope event.
     *
     * @param handler the handler to add.
     */
    public void addMenuVisibilityChangedEventHandler(MenuVisiblityChangedEventHandler handler) {
        this.eventbus.addHandler(MenuVisibilityChangedEvent.TYPE, handler);
    }

    /**
     * Adds a handler which will receive events when a menu item has been selected, this is a
     * module scope event.
     *
     * @param handler the handler to add.
     */
    public void addMenuItemSelectedEventHandler(MenuItemSelectedEventHandler handler) {
        this.active.getEventbus().addHandler(MenuItemSelectedEvent.TYPE, handler);
    }

    /**
     * Removes the specified handler from the eventbus.
     *
     * @param handler the handler to remove.
     */
    public void removeMenuItemSelectedEventHandler(MenuItemSelectedEventHandler handler) {
        this.active.getEventbus().removeHandler(MenuItemSelectedEvent.TYPE, handler);
    }

    /**
     * Fires a menu item selected event to all handlers registered.
     *
     * @param event the event to fire.
     */
    public void fireMenuItemSelectedEvent(MenuItemSelectedEvent event) {
        this.active.getEventbus().fireEvent(event);
    }

    /**
     * Adds a handler which will receive events when a menu group has been expanded or collapsed, this is a
     * module scope event.
     *
     * @param handler the handler to add.
     */
    public void addMenuItemGroupExpendedEventHandler(MenuItemGroupExpandedHandler handler) {
        this.active.getEventbus().addHandler(MenuItemGroupExpandedEvent.TYPE, handler);
    }

    /**
     * Removes the specified handler from the eventbus.
     *
     * @param handler the handler to remove.
     */
    public void removeMenuItemGroupExpendedEventHandler(MenuItemGroupExpandedHandler handler) {
        this.active.getEventbus().removeHandler(MenuItemGroupExpandedEvent.TYPE, handler);
    }

    /**
     * Fires a menu group expended / collapsed event to all handlers registered.
     *
     * @param event the event to fire.
     */
    public void fireMenuItemGroupExpendedEvent(MenuItemGroupExpandedEvent event) {
        this.active.getEventbus().fireEvent(event);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onEvent(ModuleActivateEvent event) {
        // Check if the module has been activated before, if not, create the menu.
        if (!menus.containsKey(event.getModule().getModuleId())) {
            menus.put(event.getModule().getModuleId(), new Menu());
        }

        // Retrieve the menu of the module we are about to active.
        active = menus.get(event.getModule().getModuleId());

        // Restore the full width setting since it can be different between modules.
        setFullwidth(active.isFullwidth());

        // Add the menu items if they were missing.
        for (MenuItem menuItem : active.getItems()) {
            menuItem.attachToParent(menuListElement);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onEvent(ModuleSuspendEvent event) {
        // First remove the current menu items if there are any.
        if (active != null) {
            for (MenuItem menuItem : active.getItems()) {
                menuItem.detachFromParent();
            }
        }
    }

    /**
     * Flag indicating if the menu contains a menu item with the specified token.
     * @param token the token of the menu item.
     * @return the menu item.
     */
    public boolean canHandleLink(String token) {
        for (MenuItem item : this.active.getItems()) {
            if (item.canHandleLink(token)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Finds the menu item associated with the specified token and invokes its handle method.
     * @param token the token
     * @param arguments the arguments to pass along.
     */
    public void handleLink(String token, Map<String, String> arguments) {
        for (MenuItem item : this.active.getItems()) {
            if (item.canHandleLink(token)) {
                item.handleLink(token, arguments);
            }
        }
    }

    /**
     * Adds the specified menu items to the menu.
     * @param itemsToAdd the menu items to add.
     */
    public void addMenuItems(MenuItem ... itemsToAdd) {
        this.active.addMenuItems(itemsToAdd);

        for (MenuItem item : itemsToAdd) {
            item.attachToParent(menuListElement);
        }
    }

    /**
     * Removes the specified menu items.
     *
     * @param itemsToRemove the menu items to remove.
     */
    public void removeMenuItems(MenuItem ... itemsToRemove) {
        this.active.removeMenuItems(itemsToRemove);
    }

    /**
     * Returns a menu item based on its key or <code>null</code> when no menu item was found.
     * @param key the key of the menu item.
     * @return the menu item found or <code>null</code> when no matching menu item was found.
     */
    public MenuItem getMenuItem(String key) {
        return this.active.getMenuItem(key);
    }

    /**
     * Returns the active menu item.
     *
     * @return the active menu item or <code>null</code> when no menu item is active.
     */
    public MenuItem getActiveMenuItem() {
        for (MenuItem item : this.active.flatten()) {
            if (item.isLeaf() && item.isSelected()) {
                return item;
            }
        }

        return null;
    }

    /**
     * Deselects any current selected menu item.
     */
    public void deselectActiveMenuItem() {
        MenuItem activeMenuItem = getActiveMenuItem();
        if (activeMenuItem != null) {
            activeMenuItem.setSelected(false);
        }
    }

    /**
     * Selects the previous menu item when there is one, returns the menu item that has been selected or <code>null</code>
     * if no menu item was selected due to its absents.
     *
     * @return the menu item that was selected, or <code>null</code>
     */
    public MenuItem previous() {
        ListIterator<MenuItem> listIterator = this.active.flatten().listIterator();
        while(listIterator.hasNext()) {
            MenuItem item = listIterator.next();

            if (item.isLeaf() && item.isSelected()) {
                // See documentation, calling previous after next
                // results in the same element. But if we are at the first
                // element we still need to check if we have a previous.
                if (listIterator.hasPrevious()) {
                    listIterator.previous();
                }

                while(listIterator.hasPrevious()) {
                    MenuItem previous = listIterator.previous();

                    if (previous.isLeaf() && previous.isEnabled()) {
                        return previous;
                    }

                }

                // Don't remove this break otherwise we end-up in an
                // endless loop because of the next / previous / next cycle.
                break;
            }
        }

        return null;
    }

    /**
     * Selects the next menu item when there is one, returns the menu item that has been selected or <code>null</code>
     * if no menu item was selected due to its absents.
     *
     * @return the menu item that was selected, or <code>null</code>
     */
    public MenuItem next() {
        ListIterator<MenuItem> listIterator = this.active.flatten().listIterator();
        while(listIterator.hasNext()) {
            MenuItem item = listIterator.next();
            if (item.isLeaf() && item.isSelected()) {
                while(listIterator.hasNext()) {
                    MenuItem next = listIterator.next();

                    if (next.isLeaf() && next.isEnabled()) {
                        return next;
                    }

                }

            }
        }

        return null;
    }

    /**
     * Flag indicating if there is a previous menu item that can be selected.
     *
     * @return true if so, false if not.
     */
    public boolean hasPrevious() {
        return previous() != null;
    }

    /**
     * Flag indicating if there is a next menu item that can be selected.
     *
     * @return true if so, false if not.
     */
    public boolean hasNext() {
        return next() != null;
    }

    /**
     * Expand the menu in width (item names visible)
     *
     * @param expanded true to display the item names.
     */
    public void setExpanded(boolean expanded) {
        getElement().setAttribute(ATTR_EXPANDED, expanded ? TRUE : FALSE);
    }

    /**
     * Flag indicating if the menu is expanded or not.
     *
     * @return true if so, false if not
     */
    public boolean isExpanded() {
        return !FALSE.equals(getElement().getAttribute(ATTR_EXPANDED));
    }

    /**
     * Se the width of the menu bar to it's normal size or hidden state, also
     * affects the center panel and the buttonbar.
     *
     * @param fullwidth if the menu should have it's normal full width.
     */
    public void setFullwidth(boolean fullwidth) {
        active.setFullwidth(fullwidth);

        getElement().setAttribute(ATTR_VISIBLE, fullwidth ? TRUE : FALSE);
        eventbus.fireEvent(new MenuVisibilityChangedEvent(fullwidth));
    }

    /**
     * Flag indicating if the menu is in it's normal full width or if it is hidden.
     *
     * @return true if so, false if not.
     */
    public boolean isFullwidth() {
        // TODO: Why ATTR_VISIBLE and method is isFullwidth?
        return !FALSE.equals(getElement().getAttribute(ATTR_VISIBLE));
    }

}
