package nl.sodeso.gwt.ui.client.link;

import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class NavigationLink implements Link {

    @Override
    public void navigate(String token, Map<String, String> arguments) {
        // TODO: Display 404
    }

    @Override
    public boolean canProcessToken(String token) {
        return true;
    }
}
