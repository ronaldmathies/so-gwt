package nl.sodeso.gwt.ui.client.panel;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.button.OkButton;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.util.Align;

/**
 * @author Ronald Mathies
 */
public class ValidationResultPanel extends PopupWindowPanel {

    public ValidationResultPanel(ValidationResult result, String title, String documentation) {
        super("validation-window-popup", title, WindowPanel.Style.INFO);
        this.setWidth("600px");

        addToBody(
            new EntryForm(null)
                .addEntry(new EntryWithDocumentation("result", documentation, Align.LEFT))
                .addEntry(new EntryWithContainer("result", result.toWidget())));

        addToFooter(Align.RIGHT, new OkButton.WithLabel((event) -> close()));
    }

}
