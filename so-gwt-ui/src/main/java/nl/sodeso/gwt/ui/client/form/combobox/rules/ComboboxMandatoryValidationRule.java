package nl.sodeso.gwt.ui.client.form.combobox.rules;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.types.StringType;

/**
 * @author Ronald Mathies
 */
@SuppressWarnings("unchecked")
public class ComboboxMandatoryValidationRule extends MandatoryValidationRule {

    private OptionType<DefaultOption> optionValue = null;
    private StringType otherValue = null;
    private DefaultOption otherOption = null;

    public ComboboxMandatoryValidationRule(String key, ValidationMessage.Level level, OptionType optionValue) {
        super(key, level);

        this.optionValue = optionValue;
    }

    public ComboboxMandatoryValidationRule(String key, String label, ValidationMessage.Level level, OptionType optionValue) {
        super(key, label, level);

        this.optionValue = optionValue;
    }

    public ComboboxMandatoryValidationRule(String key, ValidationMessage.Level level, OptionType optionValue, StringType otherValue, DefaultOption otherOption) {
        super(key, level);

        this.optionValue = optionValue;
        this.otherValue = otherValue;
        this.otherOption = otherOption;
    }

    public ComboboxMandatoryValidationRule(String key, String label, ValidationMessage.Level level, OptionType optionValue, StringType otherValue, DefaultOption otherOption) {
        super(key, label, level);

        this.optionValue = optionValue;
        this.otherValue = otherValue;
        this.otherOption = otherOption;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getValue() {
        String value = null;
        if (this.optionValue.getValue() != null) {

            if (this.otherOption != null) {

                if (this.optionValue.getValue().equals(this.otherOption)) {
                    value = otherValue.getValue();
                }

            } else {
                value = this.optionValue.getValue().getCode();
            }

        }


        return value;
    }
}
