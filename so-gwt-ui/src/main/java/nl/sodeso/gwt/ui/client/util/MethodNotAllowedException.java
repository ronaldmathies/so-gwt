package nl.sodeso.gwt.ui.client.util;

/**
 * @author Ronald Mathies
 */
public class MethodNotAllowedException extends RuntimeException {

    public MethodNotAllowedException() {
        super("Method not allowed exception");
    }

}
