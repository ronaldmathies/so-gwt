package nl.sodeso.gwt.ui.client.form.upload;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Ronald Mathies
 */
public class FileUpload implements Serializable {

    private String uuid = null;
    private String filename = null;
    private Long filesize = null;

    public FileUpload() {}

    public FileUpload(String uuid, String filename, Long filesize) {
        this(filename, filesize);
        this.uuid = uuid;
    }

    public FileUpload(String filename, Long filesize) {
        this.filename = filename;
        this.filesize = filesize;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Long getFilesize() {
        return filesize;
    }

    public void setFilesize(Long filesize) {
        this.filesize = filesize;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileUpload that = (FileUpload) o;
        return Objects.equals(uuid, that.uuid) &&
                Objects.equals(filename, that.filename) &&
                Objects.equals(filesize, that.filesize);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, filename, filesize);
    }
}
