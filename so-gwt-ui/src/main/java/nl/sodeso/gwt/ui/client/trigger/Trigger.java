package nl.sodeso.gwt.ui.client.trigger;

/**
 * @author Ronald Mathies
 */
public interface Trigger {

    void fire();

}
