package nl.sodeso.gwt.ui.client.form.table;

import com.google.gwt.user.cellview.client.DataGrid;

/**
 * @author Ronald Mathies
 */
public interface DataGridResource extends DataGrid.Resources {

    @Override
    @Source(DataGridStyle.DEFAULT_CSS)
    DataGridStyle dataGridStyle();

}
