package nl.sodeso.gwt.ui.server.endpoint.session;

import com.google.gwt.user.server.rpc.XsrfProtectedServiceServlet;
import nl.sodeso.gwt.ui.client.controllers.session.domain.LoginResult;
import nl.sodeso.gwt.ui.client.controllers.session.domain.User;
import nl.sodeso.gwt.ui.client.controllers.session.rpc.SessionRpc;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.server.endpoint.properties.ApplicationPropertiesContainer;
import nl.sodeso.gwt.ui.server.endpoint.properties.ServerApplicationProperties;

import javax.servlet.ServletException;
import java.security.Principal;

/**
 * @author Ronald Mathies
 */
public abstract class SessionEndpoint extends XsrfProtectedServiceServlet implements SessionRpc {

    /**
     * {@inheritDoc}
     */
    @Override
    public LoginResult login(String username, String password) {
        onBeforeLoggingIn();

        LoginResult loginResult = new LoginResult(null);
        try {
            getThreadLocalRequest().login(username, password);
            loginResult = new LoginResult(new User(username));
        } catch (ServletException e) {}

        onAfterLoggedIn(loginResult);

        return loginResult;
    }

    /**
     * {@inheritDoc}
     */
    public LoginResult isLoggedIn() {
        ServerApplicationProperties serverApplicationProperties =
                ApplicationPropertiesContainer.instance().getApplicationProperties();

        if (serverApplicationProperties.isSecurityEnabled()) {
            Principal principal = getThreadLocalRequest().getUserPrincipal();
            return new LoginResult(principal != null ? new User(principal.getName()) : null);
        }

        return new LoginResult(new User(serverApplicationProperties.getAnonymousUsername()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Void logout() {
        try {
            getThreadLocalRequest().logout();
        } catch (ServletException e) {}

        return null;
    }

    public abstract void onBeforeLoggingIn();

    public abstract void onAfterLoggedIn(LoginResult loginResult);

}
