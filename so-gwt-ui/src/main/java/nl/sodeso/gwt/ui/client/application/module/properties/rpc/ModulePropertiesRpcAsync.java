package nl.sodeso.gwt.ui.client.application.module.properties.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.gwt.ui.client.application.module.properties.ClientModuleProperties;

/**
 * @author Ronald Mathies
 */
public interface ModulePropertiesRpcAsync {

    void getProperties(AsyncCallback<ClientModuleProperties> properties);

}
