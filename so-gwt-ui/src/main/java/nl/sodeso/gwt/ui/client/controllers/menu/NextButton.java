package nl.sodeso.gwt.ui.client.controllers.menu;

import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.resources.Resources;

/**
 * Button representing an <code>Next</code> button, default implementation will
 * when pressed click on the next available / active menu item.
 *
 * @author Ronald Mathies
 */
public class NextButton extends SimpleButton {

    // Keys for so-key entries.
    public static final String KEY = "next";

    /**
     * Constructs a default next button with a default label.
     */
    public NextButton() {
        this(Resources.BUTTON_I18N.Next(), Style.BLUE);
    }

    /**
     * Constructs a next button with the specified title and style.
     *
     * @param title the title for the previous button.
     * @param style the style for the previous button.
     */
    public NextButton(String title, Style style) {
        super(KEY, title, style);

        addClickHandler((event) -> {
            MenuItem menuItem =
                    MenuController.instance().next();

            if (menuItem != null) {
                menuItem.click(null);
            }
        });
    }

}
