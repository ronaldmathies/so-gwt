package nl.sodeso.gwt.ui.client.controllers.buttonbar;

import com.google.gwt.user.client.ui.FlowPanel;
import nl.sodeso.gwt.ui.client.application.GwtApplicationEntryPoint;
import nl.sodeso.gwt.ui.client.application.event.ModuleActivateEvent;
import nl.sodeso.gwt.ui.client.application.event.ModuleActivateEventHandler;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuController;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.util.HasKey;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static nl.sodeso.gwt.ui.client.util.Constants.FALSE;
import static nl.sodeso.gwt.ui.client.util.Constants.TRUE;

/**
 * @author Ronald Mathies
 */
public class ButtonBarController extends FlowPanel implements ModuleActivateEventHandler, HasKey {

    // Keys for so-key entries.
    private static final String KEY_BUTTONBAR_SUFFIX = "-buttonbar";
    private static final String KEY_BUTTONBAR_PANEL = "main";

    // CSS style classes.
    private static final String STYLE_BUTTONBAR_PANEL = "buttonbar-panel";
    private static final String STYLE_BUTTONBAR_OBJECT_LEFT = "buttonbar-object-left";
    private static final String STYLE_BUTTONBAR_OBJECT_RIGHT = "buttonbar-object-right";

    // Element attribute names.
    private static final String ATTR_FULL_WIDTH = "so-full-width";


    private static ButtonBarController instance;

    private ArrayList<SimpleButton> leftAlignedButtons = new ArrayList<>();
    private ArrayList<SimpleButton> rightAlignedButtons = new ArrayList<>();

    private HashMap<String, ArrayList<SimpleButton>> moduleLeftButtons = new HashMap<>();
    private HashMap<String, ArrayList<SimpleButton>> moduleRightButtons = new HashMap<>();

    private ButtonBarController() {
        addStyleName(STYLE_BUTTONBAR_PANEL);
        setFullWidth(false);

        GwtApplicationEntryPoint.instance().addModuleActivateEventHandler(this);

        setKey(KEY_BUTTONBAR_PANEL);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_BUTTONBAR_SUFFIX;
    }

    /**
     * Returns the ButtonBarcontroller singleton instance.
     *
     * @return the ButtonBarcontroller singleton instance.
     */
    public static ButtonBarController instance() {
        if (instance == null) {
            instance = new ButtonBarController();

            MenuController.instance().addMenuVisibilityChangedEventHandler(event -> ButtonBarController.instance().setFullWidth(!event.isVisible()));
        }

        return instance;
    }

    /**
     * @see ModuleActivateEventHandler#onEvent(ModuleActivateEvent)
     */
    public void onEvent(ModuleActivateEvent event) {
        if (!moduleLeftButtons.containsKey(event.getModule())) {
            moduleLeftButtons.put(event.getModule().getModuleId(), new ArrayList<>());
            moduleRightButtons.put(event.getModule().getModuleId(), new ArrayList<>());
        }

        // Restore the width of the center panel based on the full width
        // of the menu, menu is leading.
        setFullWidth(MenuController.instance().isFullwidth());

        leftAlignedButtons = moduleLeftButtons.get(event.getModule().getModuleId());
        rightAlignedButtons = moduleRightButtons.get(event.getModule().getModuleId());

        redraw();
    }

    /**
     * Add buttons to the button bar.
     *
     * @param alignment on which side the buttons need to be added.
     * @param button the buttons that need to be added.
     */
    public void addButtons(Align alignment, SimpleButton ... button) {
        if (alignment.isLeft()) {
            leftAlignedButtons.addAll(Arrays.asList(button));
        } else if (alignment.isRight()) {
            rightAlignedButtons.addAll(Arrays.asList(button));
        } else {
            throw new RuntimeException("Only left and right alignment is allowerd.");
        }

        redraw();
    }

    /**
     * Add a button the to button bar but add it before an existing button on the button
     * bar.
     *
     * @param alignment on which side the button needs to be added.
     * @param buttonToAdd the button to add.
     * @param beforeButton the existing button.
     */
    public void addButtonBefore(Align alignment, SimpleButton buttonToAdd, SimpleButton beforeButton) {
        addButtonBefore(alignment, buttonToAdd, beforeButton.getKey());
    }

    /**
     * Add a button the to button bar but add it before an existing button on the button
     * bar.
     *
     * @param alignment on which side the button needs to be added.
     * @param buttonToAdd the button to add.
     * @param key the key of the existing button.
     */
    public void addButtonBefore(Align alignment, SimpleButton buttonToAdd, String key) {
        ArrayList<SimpleButton> buttons;

        if (alignment.isLeft()) {
            buttons = leftAlignedButtons;
        } else if (alignment.isRight()) {
            buttons = rightAlignedButtons;
        } else {
            throw new RuntimeException("Only left and right alignment is allowerd.");
        }

        for (int index = 0; index < buttons.size(); index++) {
            SimpleButton existingButton = buttons.get(index);

            if (existingButton.getKey().equals(key)) {
                buttons.add(index, buttonToAdd);
                break;
            }
        }

        redraw();
    }

    /**
     * Add a button the to button bar but add it after an existing button on the button
     * bar.
     *
     * @param alignment on which side the button needs to be added.
     * @param buttonToAdd the button to add.
     * @param afterButton the existing button.
     */
    public void addButtonAfter(Align alignment, SimpleButton buttonToAdd, SimpleButton afterButton) {
        addButtonAfter(alignment, buttonToAdd, afterButton.getKey());
    }

    /**
     * Add a button the to button bar but add it after an existing button on the button
     * bar.
     *
     * @param alignment on which side the button needs to be added.
     * @param buttonToAdd the button to add.
     * @param key the key of the existing button.
     */
    public void addButtonAfter(Align alignment, SimpleButton buttonToAdd, String key) {
        ArrayList<SimpleButton> buttons;

        if (alignment.isLeft()) {
            buttons = leftAlignedButtons;
        } else if (alignment.isRight()) {
            buttons = rightAlignedButtons;
        } else {
            throw new RuntimeException("Only left and right alignment is allowerd.");
        }

        for (int index = 0; index < buttons.size(); index++) {
            SimpleButton existingButton = buttons.get(index);

            if (existingButton.getKey().equals(key)) {
                buttons.add(++index, buttonToAdd);
                break;
            }
        }

        redraw();
    }

    /**
     * Removes the buttons from the button bar.
     *
     * @param buttons the buttons to remove.
     */
    public void removeButtons(SimpleButton ... buttons) {
        for (int activeList = 0; activeList < 2; activeList++) {
            ArrayList<SimpleButton> listWithExistingButtons = (activeList == 0 ? leftAlignedButtons : rightAlignedButtons);
            for (int index = listWithExistingButtons.size() - 1; index >= 0; index--) {
                SimpleButton existingButton = listWithExistingButtons.get(index);
                for (SimpleButton buttonToRemove : buttons) {
                    if (existingButton.getKey().equals(buttonToRemove.getKey())) {
                        remove(existingButton);
                        listWithExistingButtons.remove(index);
                    }
                }
            }
        }

        redraw();
    }

    private void redraw() {
        for (int index = 0; index < getWidgetCount(); index++) {
            remove(getWidget(index));
        }

        for (SimpleButton simpleButton : leftAlignedButtons) {

            simpleButton.addStyleName(STYLE_BUTTONBAR_OBJECT_LEFT);
            add(simpleButton);
        }

        for (SimpleButton simpleButton : rightAlignedButtons) {

            simpleButton.addStyleName(STYLE_BUTTONBAR_OBJECT_RIGHT);
            add(simpleButton);
        }
    }

    private void setFullWidth(boolean isFullWidth) {
        getElement().setAttribute(ATTR_FULL_WIDTH, isFullWidth ? TRUE : FALSE);
    }

    private boolean isFullWidth() {
        return !FALSE.equals(getElement().getAttribute(ATTR_FULL_WIDTH));
    }

}
