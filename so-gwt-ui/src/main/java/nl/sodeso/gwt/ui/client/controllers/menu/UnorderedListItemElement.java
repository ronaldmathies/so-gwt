package nl.sodeso.gwt.ui.client.controllers.menu;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.LIElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.ComplexPanel;
import nl.sodeso.gwt.ui.client.resources.Icon;

import static nl.sodeso.gwt.ui.client.util.Constants.FALSE;
import static nl.sodeso.gwt.ui.client.util.Constants.TRUE;

/**
 * @author Ronald Mathies
 */
public class UnorderedListItemElement extends ComplexPanel {

    // CSS style classes.
    private static final String STYLE_LEAF = "leaf";
    private static final String STYLE_ANCHOR = "anchor";
    private static final String STYLE_ICON = "icon";
    private static final String STYLE_NOICON = "noicon";

    // Element attribute names.
    private static final String ATTR_SELECTED = "so-selected";
    private static final String ATTR_VISIBLE = "so-visible";
    private static final String ATTR_ENABLED = "so-enabled";

    private Anchor anchor = null;

    public UnorderedListItemElement(String key, String label) {
        this(key, null, label);
    }

    public UnorderedListItemElement(String key, Icon icon, String label) {
        LIElement lieElement = Document.get().createLIElement();
        lieElement.addClassName(STYLE_LEAF);
        setElement(lieElement);

        setVisible(false);
        setEnabled(true);

        anchor = new Anchor("", (key != null ? "#" + key : "javascript:void(0)"));
        anchor.getElement().setId(key);

        if (icon != null) {
            Element iconElement = Document.get().createElement("I");
            iconElement.addClassName(icon.getStyle());
            anchor.getElement().appendChild(iconElement);
        }

        SpanElement labelSpan = Document.get().createSpanElement();
        labelSpan.setInnerText(label);
        labelSpan.addClassName(icon != null ? STYLE_ICON : STYLE_NOICON);
        anchor.getElement().appendChild(labelSpan);
        anchor.addStyleName(STYLE_ANCHOR);
        lieElement.appendChild(anchor.getElement());
    }

    public void addClassName(String styleClass) {
        getElement().addClassName(styleClass);
    }

    public Anchor getAnchor() {
        return this.anchor;
    }

    public void removeClassName(String styleClass) {
        getElement().removeClassName(styleClass);
    }

    public void setVisible(boolean visible) {
        getElement().setAttribute(ATTR_VISIBLE, visible ? TRUE : FALSE);
    }

    public boolean isVisible() {
        String value = getElement().getAttribute(ATTR_VISIBLE);
        return value == null || value.equals(TRUE);
    }

    public void setSelected(boolean visible) {
        getElement().setAttribute(ATTR_SELECTED, visible ? TRUE : FALSE);
    }

    public boolean isSelected() {
        String value = getElement().getAttribute(ATTR_SELECTED);
        return value == null || value.equals(TRUE);
    }

    public void setEnabled(boolean enabled) {
        getElement().setAttribute(ATTR_ENABLED, enabled ? TRUE : FALSE);
    }

    public boolean isEnabled() {
        String value = getElement().getAttribute(ATTR_ENABLED);
        return value == null || value.equals(TRUE);
    }
}
