package nl.sodeso.gwt.ui.client.rpc;

/**
 * Classes implementing this interface will be used for genering an implementation
 * class by the RpgGatewayGenerator.
 *
 * @author Ronald Mathies
 */
public interface RpcGateway<X> {

    /**
     * Should return the appropriate RpgAsync interface.
     * @return the appropriate RpgAsync interface.
     */
    X getRpcAsync();

}
