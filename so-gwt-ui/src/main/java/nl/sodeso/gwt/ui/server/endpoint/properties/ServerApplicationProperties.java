package nl.sodeso.gwt.ui.server.endpoint.properties;

import nl.sodeso.commons.properties.PropertyConfiguration;

/**
 * @author Ronald Mathies
 */
public class ServerApplicationProperties extends AbstractServerProperties {

    //String configurationFile
    public ServerApplicationProperties() {
        super(); //configurationFile
    }

    /**
     * Returns the version number.
     * @return the version number.
     */
    public String getVersion() {
        return PropertyConfiguration.getInstance().getStringProperty("application", "application.version", "[no.version]");
    }

    /**
     * Returns a flag indicating if the dev kit should be enabled or not.
     * @return a flag indicating if the dev kit should be enabled or not.
     */
    public boolean isDevkitEnabled() {
        return PropertyConfiguration.getInstance().getBooleanProperty("application", "application.devkit.enabled", false);
    }

    /**
     * Returns a flag indicating if the application has security enabled, false if not.
     * @return a flag indicating if the application has security enabled, false if not.
     */
    public boolean isSecurityEnabled() {
        return PropertyConfiguration.getInstance().getBooleanProperty("application", "application.security.enabled", false);
    }

    /**
     * Returns the username that will be used when access is anonymous.
     * @return the username that will be used when access is anonymous.
     */
    public String getAnonymousUsername() {
        return PropertyConfiguration.getInstance().getStringProperty("application", "application.security.anonymous", "anonymous");
    }

    /**
     * Returns a flag indicating if the Websocket service should be enabled or not.
     * @return a flag indicating if the Websocket service should be enabled or not.
     */
    public boolean isWebsocketEnabled() {
        return PropertyConfiguration.getInstance().getBooleanProperty("application", "application.websocket.enabled", false);
    }

}
