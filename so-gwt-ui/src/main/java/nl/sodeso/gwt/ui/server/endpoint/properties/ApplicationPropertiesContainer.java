package nl.sodeso.gwt.ui.server.endpoint.properties;

/**
 * @author Ronald Mathies
 */
public class ApplicationPropertiesContainer {

    private ServerApplicationProperties serverApplicationProperties = null;

    private static ApplicationPropertiesContainer INSTANCE = null;

    /**
     * Returns the singleton instance.
     * @return the singleton instance.
     */
    public static ApplicationPropertiesContainer instance() {
        if (INSTANCE == null) {
            INSTANCE = new ApplicationPropertiesContainer();
        }

        return INSTANCE;
    }

    /**
     * Sets the server application properties.
     * @param serverApplicationProperties the server application properties.
     */
    public void setApplicationProperties(ServerApplicationProperties serverApplicationProperties) {
        this.serverApplicationProperties = serverApplicationProperties;
    }

    /**
     * Returns the application properties.
     * @param <X>
     * @return the server side application properties.
     */
    @SuppressWarnings("unchecked")
    public <X> X getApplicationProperties() {
        return (X) serverApplicationProperties;
    }


}
