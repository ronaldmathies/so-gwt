package nl.sodeso.gwt.ui.client.form.upload;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.InputElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.FlowPanel;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.form.validation.IsValidatable;
import nl.sodeso.gwt.ui.client.form.validation.ValidationEventBusController;
import nl.sodeso.gwt.ui.client.form.validation.rules.ValidationRule;
import nl.sodeso.gwt.ui.client.resources.Resources;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.gwt.ui.client.util.HasKey;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class FileUploadField extends FlowPanel implements HasKey, IsValidatable {

    private static final String KEY_FILE_UPLOAD_SUFFIX = "-file-upload";

    // Keys for so-key entries.
    private static final String KEY_CHOOSE_FILE = "choose-file";
    private static final String KEY_CLEAR_FILE = "clear-file";

    // CSS style classes.
    private static final String STYLE_FILE_UPLOAD = "file-upload";
    private static final String STYLE_FILENAME_PANEL = "filename-panel";
    private static final String STYLE_FILENAME = "filename";
    private static final String STYLE_BUTTON_HIDDEN = "button-hidden";
    private static final String STYLE_BUTTON_PANEL = "button-panel";
    private static final String STYLE_BUTTON = "button";

    private ArrayList<ValidationRule> rules = new ArrayList<>();

    private ValidationEventBusController eventbus = new ValidationEventBusController();

    private SpanElement filenameField = null;
    private SimpleButton selectFileButton = null;
    private SimpleButton clearFileButton = null;

    private FileUploadType fileUploadType = null;

    public FileUploadField(String key, final FileUploadType fileUploadType) {
        addStyleName(STYLE_FILE_UPLOAD);

        this.fileUploadType = fileUploadType;
        this.fileUploadType.addValueChangedEventHandler(new ValueChangedEventHandler<FileUploadType>(this) {
            @Override
            public void onValueCanged(ValueChangedEvent<FileUploadType> event) {
                if (fileUploadType.getValue() != null) {
                    setFilename();
                } else {
                    clearFilename();
                }
            }
        });

        FlowPanel filenamePanel = new FlowPanel();
        filenamePanel.setStyleName(STYLE_FILENAME_PANEL);

        // Create the field that will display the selected filename.
        filenameField = Document.get().createSpanElement();
        filenameField.addClassName(STYLE_FILENAME);
        filenamePanel.getElement().appendChild(filenameField);
        add(filenamePanel);

        // Create the choose file button, this is a hidden
        // button which will be called by our custom button.
        final InputElement chooseFileButton = Document.get().createFileInputElement();
        chooseFileButton.addClassName(STYLE_BUTTON_HIDDEN);
        chooseFileButton.setName("fileupload");
        chooseFileButton.getStyle().setOpacity(0.0);
        getElement().appendChild(chooseFileButton);

        // Add an on-change event handler on the hidden choose file
        // button.
        Event.sinkEvents(chooseFileButton, Event.ONCHANGE);
        Event.setEventListener(chooseFileButton, event -> {
            if (event.getTypeInt() == Event.ONCHANGE) {
                File[] files = FileUploadUtil.getFiles(chooseFileButton);
                if (files.length > 0) {
                    File file = files[0];
                    fileUploadType.setValue(new FileUpload(file.getFilename(), (long) file.getFilesize()));
                    validate(result -> {

                    });
                } else {
                    fileUploadType.setValue(null);
                }
            }
        });

        Event.sinkEvents(filenameField, Event.ONCLICK);
        Event.setEventListener(filenameField, event -> {
            if (event.getTypeInt() == Event.ONCLICK) {
                chooseFileButton.click();
            }
        });

        // Create a button panel which will display the buttons.
        FlowPanel buttonsPanel = new FlowPanel();
        buttonsPanel.setStyleName(STYLE_BUTTON_PANEL);

        // Create the actual select file button.
        selectFileButton = new SimpleButton(KEY_CHOOSE_FILE, Resources.FILE_UPLOAD_I18N.ChooseFile(), SimpleButton.Style.BLUE);
        selectFileButton.addClickHandler((event) -> chooseFileButton.click());
        selectFileButton.addStyleName(STYLE_BUTTON);
        buttonsPanel.add(selectFileButton);

        // Create the clear file button.
        clearFileButton = new SimpleButton(KEY_CLEAR_FILE, Resources.FILE_UPLOAD_I18N.ClearFile(), SimpleButton.Style.BLUE);
        clearFileButton.addClickHandler((event) -> {
            FileUploadField.this.fileUploadType.setValue(null);
            validate(result -> {

            });
        });
        clearFileButton.setVisible(false);
        clearFileButton.addStyleName(STYLE_BUTTON);
        buttonsPanel.add(clearFileButton);

        add(buttonsPanel);

        setKey(key);

        if (fileUploadType.getValue() != null) {
            setFilename();
        }
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_FILE_UPLOAD_SUFFIX;
    }

    private void clearFilename() {
        filenameField.setInnerText("");

        clearFileButton.setVisible(false);
        selectFileButton.setVisible(true);
    }

    private void setFilename() {
        FileUpload fileUpload = fileUploadType.getValue();
        filenameField.setInnerText(fileUpload.getFilename() + " ( " + FileUploadUtil.getFileSizeFormatted(fileUpload.getFilesize(), false) + " )");

        clearFileButton.setVisible(true);
        selectFileButton.setVisible(false);
    }


    public FileUploadType getFileUploadType() {
        return fileUploadType;
    }

    /**
     * Returns the name of the file that the user selected.
     * @return the name of the file that the user selected or null when no file is selected.
     */
    public String getValue() {
        return filenameField.getInnerText();
    }

    /**
     * {@inheritDoc}
     */
    public ValidationEventBusController getValidationEventbus() {
        return eventbus;
    }

    /**
     * {@inheritDoc}
     */
    public ArrayList<ValidationRule> getValidationRules() {
        return this.rules;
    }

}
