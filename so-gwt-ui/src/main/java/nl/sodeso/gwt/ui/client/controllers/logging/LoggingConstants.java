package nl.sodeso.gwt.ui.client.controllers.logging;

import com.google.gwt.i18n.client.Constants;

/**
 * @author Ronald Mathies
 */
public interface LoggingConstants extends Constants {

    @DefaultStringValue("Application Logging")
    String Title();

    @DefaultStringValue("Detail Log Line")
    String LogDetailsTitle();

    @DefaultStringValue("Time")
    String Time();

    @DefaultStringValue("Message")
    String Message();

    @DefaultStringValue("Stacktrace")
    String Stacktrace();

    @DefaultStringValue("Level")
    String Level();

}
