package nl.sodeso.gwt.ui.client.rpc;

import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException;
import com.google.gwt.user.client.rpc.InvocationException;
import nl.sodeso.gwt.security.client.exception.PermissionDeniedException;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.button.OkButton;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.util.Align;

/**
 * Default async callback.
 *
 * @author Ronald Mathies
 */
public abstract class DefaultAsyncCallback<T> implements AsyncCallback<T> {

    PopupWindowPanel remoteConnectionFailedPopup = null;

    /**
     * When a failure occurs in the request it will be passed to the failure method for default
     * handling, unless the developer overrides that method for a custom implementation. Then that
     * custom implementation will be called.
     *
     * @param caught the throwable.
     */
    @Override
    public final void onFailure(Throwable caught) {
        failure(caught);
    }

    /**
     * Default implementation of the failure method. When this method is called it will display
     * the message of the throwable within a popup panel for the user to see.
     *
     * @param caught the throwable.
     */
    public void failure(Throwable caught) {
        String message;

        try {
            throw caught;
        } catch (PermissionDeniedException e) {
            remoteConnectionFailedPopup = new PopupWindowPanel("connection-error", "Connection Error", WindowPanel.Style.INFO);
            message = e.getCheckFailedMessage();
        } catch (RemoteException e) {
            remoteConnectionFailedPopup = new PopupWindowPanel("connection-error", "Action could not be performed", WindowPanel.Style.INFO);
            message = e.getMessage();
        } catch (IncompatibleRemoteServiceException e) {
            remoteConnectionFailedPopup = new PopupWindowPanel("connection-error", "Client out-of-date", WindowPanel.Style.ALERT);
            message = "You browser session is out-of-date, please reload the page.";
        } catch (InvocationException e) {
            remoteConnectionFailedPopup = new PopupWindowPanel("connection-error", "Connection Error", WindowPanel.Style.ALERT);
            message = "An error occurred during the connection with the server.";
        } catch (Throwable e) {
            remoteConnectionFailedPopup = new PopupWindowPanel("connection-error", "Unknown Error Occurred", WindowPanel.Style.ALERT);
            message = e.getMessage();
        }

        if (message.length() > 512) {
            message = message.substring(0, 512).concat("...");
        }

        SafeHtml safeHtml = new SafeHtmlBuilder().appendEscapedLines(message).toSafeHtml();

        EntryForm entryForm = new EntryForm(null);
        entryForm.addEntry(new EntryWithDocumentation("message", safeHtml.asString()));
        remoteConnectionFailedPopup.addToBody(entryForm);
        remoteConnectionFailedPopup.addToFooter(Align.RIGHT, new OkButton.WithLabel((clickEvent) -> remoteConnectionFailedPopup.close()));
        remoteConnectionFailedPopup.center(true, true);
    }

    /**
     * Default method that is called by the gateway when a successful connection and request was made.
     * @param result the result of the request.
     */
    @Override
    public final void onSuccess(T result) {
        success(result);
    }

    /**
     * Method that needs to be implemented by the subclass in case the request was successful.
     *
     * @param result the result of the call.
     */
    public abstract void success(T result);

}
