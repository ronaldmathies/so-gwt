package nl.sodeso.gwt.ui.client.application;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimpleLayoutPanel;
import nl.sodeso.gwt.event.client.EventBusController;
import nl.sodeso.gwt.ui.client.application.event.ModuleActivateEvent;
import nl.sodeso.gwt.ui.client.application.event.ModuleActivateEventHandler;
import nl.sodeso.gwt.ui.client.application.event.ModuleSuspendEvent;
import nl.sodeso.gwt.ui.client.application.event.ModuleSuspendEventHandler;
import nl.sodeso.gwt.ui.client.application.module.ModuleEntryPoint;
import nl.sodeso.gwt.ui.client.application.properties.ClientApplicationProperties;
import nl.sodeso.gwt.ui.client.application.properties.rpc.ApplicationPropertiesGateway;
import nl.sodeso.gwt.ui.client.controllers.logging.LoggingController;
import nl.sodeso.gwt.ui.client.controllers.session.LoginWindow;
import nl.sodeso.gwt.ui.client.controllers.session.SessionController;
import nl.sodeso.gwt.ui.client.controllers.session.event.UserLoginEvent;
import nl.sodeso.gwt.ui.client.controllers.session.event.UserLoginEventHandler;
import nl.sodeso.gwt.ui.client.controllers.session.event.UserLogoutEvent;
import nl.sodeso.gwt.ui.client.controllers.session.event.UserLogoutEventHandler;
import nl.sodeso.gwt.ui.client.link.LinkFactory;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.rpc.XsrfGateway;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.websocket.client.WebSocketClientController;
import nl.sodeso.gwt.websocket.client.WebSocketClientControllerConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static nl.sodeso.gwt.ui.client.controllers.menu.ArgumentsUtil.fromToken;

/**
 * @author Ronald Mathies
 */
public abstract class GwtApplicationEntryPoint<T extends ClientApplicationProperties> implements EntryPoint, UserLoginEventHandler, UserLogoutEventHandler {

    // CSS style classes.
    private static final String STYLE_APPLICATION = "application";

    private static Logger logger = Logger.getLogger("Desktop.Boot");

    private T applicationProperties;
    private static GwtApplicationEntryPoint instance;

    public static GwtApplicationEntryPoint instance() {
        return instance;
    }

    private List<ModuleEntryPoint> modules = new ArrayList<>();
    private List<String> initializedModules = new ArrayList<>();
    private ModuleEntryPoint currentActiveModule = null;

    private EventBusController eventbus = null;

    @Override
    public final void onModuleLoad() {
        instance = this;

        eventbus = new EventBusController();

        setRootPanel();

        // Setup XSRF Cookie first since any Async call requires this.
        XsrfGateway.setupXsrfCookie();

        // Create the instance of the logging controller first so that we never miss out on
        // any interesting logging messages during application startup.
        LoggingController.instance();

        removeLoadingMessage();

        SessionController.instance().addUserLoginEventHandler(this);
        SessionController.instance().addUserLogoutEventHandler(this);

        // Check if we have a current session, if so then skip the login process and directly move
        // on to the application.
        SessionController.instance().isLoggedIn(state -> {
            if (state.isSuccess()) {
                SessionController.instance().getEventBus().fireEvent(new UserLoginEvent(state));
            } else {
                LoginWindow loginWindow = new LoginWindow();
                loginWindow.center();
            }
        });

    }

    @Override
    public void onEvent(UserLoginEvent event) {
        if (event.getLoginResult().isSuccess()) {
            initializeApplication();
        }
    }

    public void onEvent(UserLogoutEvent event) {
        Window.Location.assign(GWT.getHostPageBaseURL());
    }

    @SuppressWarnings("unchecked")
    private void initializeApplication() {
        History.addValueChangeHandler(getValueChangeHandler());

        onBeforeApplicationLoad();

        ApplicationPropertiesGateway.getApplicationProperties(getModuleId(), new DefaultAsyncCallback<ClientApplicationProperties>() {

            @Override
            public void success(ClientApplicationProperties clientApplicationProperties) {
                applicationProperties = (T) clientApplicationProperties;

                modules = modules();
                if (modules.isEmpty()) {
                    throw new RuntimeException("No modules have been registered, please register your modules using the modules() method.");
                }

                setupUncaughtExceptionHandler();
                GWT.runAsync(new RunAsyncCallback() {
                    @Override
                    public void onFailure(Throwable reason) {
                    }

                    @Override
                    public void onSuccess() {
                        RootPanelSetup.setup();

                        if (clientApplicationProperties.isWebsocketEnabled()) {
                            logger.log(Level.FINE, "Initializing websocket controller.");
                            WebSocketClientControllerConfiguration.instance().setContextPath(applicationProperties.getContextPath());
                            WebSocketClientController.instance().connect(true, true);
                        } else {
                            logger.log(Level.FINE, "Websocket controller disabled.");
                        }

                        onAfterApplicationLoad();
                    }
                });

            }

        });
    }

    private static void setRootPanel() {
        SimpleLayoutPanel simpleLayoutPanel = new SimpleLayoutPanel();
        simpleLayoutPanel.addStyleName(STYLE_APPLICATION);
        RootPanel.get().add(simpleLayoutPanel);

        FlowPanel panel = new FlowPanel();
        panel.addStyleName(STYLE_APPLICATION);
        simpleLayoutPanel.setWidget(panel);
    }

    public static FlowPanel getRootPanel() {
        SimpleLayoutPanel simpleLayoutPanel =
                (SimpleLayoutPanel)RootPanel.get().getWidget(0);
        return (FlowPanel)simpleLayoutPanel.getWidget();
    }

    public final T getApplicationProperties() {
        return this.applicationProperties;
    }

    /**
     * Must return a unique module identifier.
     * @return a unique module identifier.
     */
    public abstract String getModuleId();

    public abstract void onBeforeApplicationLoad();

    public abstract void onAfterApplicationLoad();

    /**
     * Return a list of modules that should be available.
     * @return a list of modules that should be available.
     */
    protected abstract List<ModuleEntryPoint> modules();

    public ModuleEntryPoint getCurrentActiveModule() {
        return this.currentActiveModule;
    }

    public List<ModuleEntryPoint> getAvailableModules() {
        return this.modules;
    }

    /**
     * Activate the module with the specified moduleId.
     * @param moduleId the moduleId.
     */
    public void activateModule(String moduleId) {
        boolean found = false;
        for (ModuleEntryPoint module : modules) {
            if (module.getModuleId().equals(moduleId)) {
                found = true;
                activateModule(module);
            }
        }

        if (!found) {
            throw new RuntimeException("Cannot activate module '" + moduleId + "', module does not exist.");
        }
    }

    /**
     * Deactivate any active module.
     */
    public void deactivateModule() {
        ModuleEntryPoint.SuspendFinishedTrigger suspendFinishedTrigger = () -> {
            logger.log(Level.INFO, "Suspended module '" + currentActiveModule.getModuleId() + "'.");

            eventbus.fireEvent(new ModuleSuspendEvent(currentActiveModule));

            currentActiveModule = null;
        };

        if (currentActiveModule != null) {
            logger.log(Level.INFO, "Suspending module '" + currentActiveModule.getModuleId() + "'.");

            currentActiveModule.suspendModule(suspendFinishedTrigger);
        }
    }

    /**
     * Activates the specified module.
     * @param module the module to activate.
     */
    public void activateModule(final ModuleEntryPoint module) {
        final Trigger activateModule = () -> {
            logger.log(Level.INFO, "Activating module '" + currentActiveModule.getModuleId() + "'.");

            currentActiveModule.activateModule(() -> logger.log(Level.INFO, "Activated module '" + currentActiveModule.getModuleId() + "'."));
        };

        ModuleEntryPoint.SuspendFinishedTrigger suspendFinishedTrigger = () -> {
            if (currentActiveModule != null) {
                logger.log(Level.INFO, "Suspended module '" + currentActiveModule.getModuleId() + "'.");

                eventbus.fireEvent(new ModuleSuspendEvent(currentActiveModule));
            }

            currentActiveModule = module;

            // First fire the switch event, we are now right in the middle between the switch.
            eventbus.fireEvent(new ModuleActivateEvent(currentActiveModule));

            if (!initializedModules.contains(module.getModuleId())) {
                logger.log(Level.INFO, "Initializing module '" + module.getModuleId() + "'.");
                module.initModule(() -> {
                    logger.log(Level.INFO, "Finished initializing module '" + module.getModuleId() + "'.");

                    // Mark module as being initialized.
                    initializedModules.add(module.getModuleId());

                    activateModule.fire();
                });
            } else {
                activateModule.fire();
            }

        };

        if (currentActiveModule != null) {
            logger.log(Level.INFO, "Suspending module '" + currentActiveModule.getModuleId() + "'.");

            currentActiveModule.suspendModule(suspendFinishedTrigger);
        } else {
            suspendFinishedTrigger.fire();
        }
    }

    public void addModuleActivateEventHandler(ModuleActivateEventHandler moduleActivateEventHandler) {
        this.eventbus.addHandler(ModuleActivateEvent.TYPE, moduleActivateEventHandler);
    }

    public void addModuleSuspendEventHandler(ModuleSuspendEventHandler moduleSuspendEventHandler) {
        this.eventbus.addHandler(ModuleSuspendEvent.TYPE, moduleSuspendEventHandler);
    }

    private void setupUncaughtExceptionHandler() {
        logger.log(Level.FINE, "Registering uncaught exception handler.");
        GWT.UncaughtExceptionHandler handler = throwable -> logger.log(Level.SEVERE, "Unexpected exception occured.", throwable);

        GWT.setUncaughtExceptionHandler(handler);
    }

    private void removeLoadingMessage() {
        RootPanel loadingpanel = RootPanel.get("loadingpanel");
        if (loadingpanel != null) {
            loadingpanel.setVisible(false);
        }
    }

    private ValueChangeHandler<String> getValueChangeHandler() {
        return event -> {
            String token = getToken(event.getValue());
            Map<String, String> arguments = fromToken(event.getValue());
            if (!"".equals(token)) {
                LinkFactory.getLink(token).navigate(token, arguments);
            }
        };
    }

    private String getToken(String link) {
        if (link.indexOf("?") > 0) {
            return link.substring(0, link.indexOf("?"));
        }

        return link;
    }

}
