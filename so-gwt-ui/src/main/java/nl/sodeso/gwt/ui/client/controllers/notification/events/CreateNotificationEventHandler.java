package nl.sodeso.gwt.ui.client.controllers.notification.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * @author Ronald Mathies
 */
public interface CreateNotificationEventHandler extends EventHandler {

    void onEvent(CreateNotificationEvent event);

}
