package nl.sodeso.gwt.ui.client.form.validation;

import nl.sodeso.gwt.ui.client.form.validation.rules.ValidationRule;

/**
 * @author Ronald Mathies
 */
public class ValidationCriteria {

    public static class or implements ValidationRule {

        private ValidationRule left;
        private ValidationRule right;

        public or(ValidationRule left, ValidationRule right) {
            this.left = left;
            this.right = right;
        }


        public void validate(final ValidationCompletedHandler handler) {
            if (left.isApplicable()) {
                left.validate(result -> {

                    if (result.hasMessages()) {
                        handler.completed(result);
                    } else {
                        if (right.isApplicable()) {
                            right.validate(handler);
                        }
                    }
                });

            }

            if (right.isApplicable()) {
                right.validate(handler);
            }
        }

//        public boolean isValid() {
//            return left.isValid() || right.isValid();
//        }

        public boolean isApplicable() {
            return left.isApplicable() || right.isApplicable();
        }
    }

    public static class and implements ValidationRule {

        private ValidationRule left;
        private ValidationRule right;

        public and(ValidationRule left, ValidationRule right) {
            this.left = left;
            this.right = right;
        }


        public void validate(ValidationCompletedHandler handler) {
            final ValidationResult allResults = new ValidationResult();

            if (left.isApplicable()) {
                left.validate(result -> {
                    allResults.merge(result);

                    if (right.isApplicable()) {
                        right.validate(result1 -> {
                            allResults.merge(result1);

                            handler.completed(allResults);
                        });
                    }
                });
            }

            if (right.isApplicable()) {
                right.validate(handler);
            }
        }

//        public boolean isValid() {
//            return left.isValid() && right.isValid();
//        }

        public boolean isApplicable() {
            return left.isApplicable() && right.isApplicable();
        }
    }

}
