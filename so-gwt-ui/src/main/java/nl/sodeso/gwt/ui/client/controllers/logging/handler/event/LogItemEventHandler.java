package nl.sodeso.gwt.ui.client.controllers.logging.handler.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * @author Ronald Mathies
 */
public interface LogItemEventHandler extends EventHandler {

    void onEvent(LogItemEvent event);

}
