package nl.sodeso.gwt.ui.client.util;

import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Ronald Mathies
 */
public class AttrUtils {

    // Element attribute names.
    public final static String ATTR_PLACEHOLDER = "placeholder";

    public static void setWidth(Widget widget, double width, Style.Unit unit) {
        widget.getElement().getStyle().setWidth(width, unit);
    }

    public static void setFullWidth(Widget widget) {
        widget.getElement().getStyle().setWidth(100, Style.Unit.PCT);
    }

    public static void setPlaceHolder(Widget widget, String value) {
        widget.getElement().setAttribute(ATTR_PLACEHOLDER, value);
    }

}
