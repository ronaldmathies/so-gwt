package nl.sodeso.gwt.ui.client.form.input;

/**
 * @author Ronald Mathies
 */
public interface WidgetSupplier<T> {

    void create(WidgetSupplierCallback<T> callback);
}
