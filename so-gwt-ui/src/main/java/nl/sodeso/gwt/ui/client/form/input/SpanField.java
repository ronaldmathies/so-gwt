package nl.sodeso.gwt.ui.client.form.input;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.validation.IsValidatable;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationEventBusController;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.rules.ValidationRule;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.gwt.ui.client.util.HasKey;

import java.util.ArrayList;

/**
 * A SpanField is for displaying text at 'random' spots in the UI, to donate a label for a text field it is
 * better to use the <code>LabelField</code> which results in an actual &gt;label/&lt;.
 *
 * @see LabelField
 *
 * @author Ronald Mathies
 */
public class SpanField extends Widget implements IsValidatable, HasKey {

    private static final String KEY_SPAN_SUFFIX = "-documentation";

    private SpanElement spanElement;

    private ArrayList<ValidationRule> rules = new ArrayList<>();
    private ValidationEventBusController eventbus = new ValidationEventBusController();

    public SpanField(String key, String value) {
        super();

        spanElement = Document.get().createSpanElement();
        setElement(spanElement);
        setText(value);

        setKey(key);
    }

    public SpanField(String key, StringType valueType) {
        this(key, valueType.getValue());

        valueType.addValueChangedEventHandler(new ValueChangedEventHandler<StringType>(this) {
            @Override
            public void onValueCanged(ValueChangedEvent<StringType> event) {
                setText(event.getValueType().getValue());

                validate(result -> {});
            }
        });

        setKey(key);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_SPAN_SUFFIX;
    }

    public void setText(String text) {
        spanElement.setInnerText(text);
    }

    /**
     * {@inheritDoc}
     */
    public ValidationEventBusController getValidationEventbus() {
        return eventbus;
    }

    /**
     * {@inheritDoc}
     */
    public ArrayList<ValidationRule> getValidationRules() {
        return this.rules;
    }
}
