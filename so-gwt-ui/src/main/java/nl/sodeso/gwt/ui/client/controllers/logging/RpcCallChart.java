package nl.sodeso.gwt.ui.client.controllers.logging;


//import com.arcadiacharts.axis.AxisIdentity;
//import com.arcadiacharts.charts.ChartException;
//import com.arcadiacharts.charts.DisplaySettings;
//import com.arcadiacharts.charts.linechart.ACLineChart;
//import com.arcadiacharts.charts.linechart.ACLineChartBuilder;
//import com.arcadiacharts.model.adapter.DataAdapterFactory;
//import com.arcadiacharts.model.axis.AxisType;
//import com.arcadiacharts.model.datatypes.IntDataType;
//import com.arcadiacharts.model.datatypes.StringDataType;
//import com.arcadiacharts.model.values.Value2D;
//import com.arcadiacharts.model.values.ValueSetImpl;

import com.google.gwt.user.client.ui.FlowPanel;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.panel.VerticalPanel;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;

//import nl.sodeso.gwt.ui.client.types.ValueType;

//import java.util.ArrayList;
//import java.util.List;

/**
 * @author Ronald Mathies
 */
public class RpcCallChart extends VerticalPanel implements RpcCallChartDataProviderChanged {

    private OptionType<DefaultOption> optionType = new OptionType<>();

//    private ACLineChart lineChart = null;
    private RpcCallChartDataProvider provider;

    private EntryForm entryForm = null;
    private ComboboxField<DefaultOption> methodList = null;

    public RpcCallChart() {
        createRpcMethodList();

        provider = new RpcCallChartDataProvider();
        provider.setRpcCallChartDataProviderChanged(this);

        FlowPanel chartContainer = new FlowPanel();
        chartContainer.getElement().setId("chart");

        addWidgets(entryForm, chartContainer);

        addAttachHandler(event -> {
            if (event.isAttached()) {
                createChart();
            }
        });
    }

    private void createRpcMethodList() {
        entryForm = new EntryForm(null);
        methodList = new ComboboxField<>(optionType);

        optionType.addValueChangedEventHandler(new ValueChangedEventHandler<OptionType<DefaultOption>>(this) {
            @Override
            public void onValueCanged(ValueChangedEvent<OptionType<DefaultOption>> event) {
                provider.setActiveDataset(methodList.getSelectedItem());
                updateChart();
            }
        });

        entryForm.addEntry(new EntryWithLabelAndWidget(null, "RPC Methods", methodList));

    }

    private void createChart() {
//        try {
//            lineChart = new ACLineChartBuilder()
//                    .setDataImportType(DataAdapterFactory.DataImportType.CSV)
//                    .setWidth(650)
//                    .setHeight(190)
//                    .setAxisCaption(AxisIdentity.Y_AXIS, "ms.")
//                    .setTheme("theme_standard_minimal.xml")
//                    .setShowLegend(false)
//                    .setDefaultPopupText("Milliseconds: $valueY")
//                    .setAxisType(AxisIdentity.X_AXIS, AxisType.CATEGORY)
//                    .build();
//
//            lineChart.addToDom("chart");
//        } catch (ChartException e) {
//
//        }
    }

    private void updateChart() {
//        try {
//            lineChart.setValueSet(0, provider.getActiveChartData());
//            lineChart.update();
//        } catch (ChartException e) {}
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void methodsChanged() {
        if (methodList != null) {
            methodList.replaceItemsRetainSelection(provider.getMethodOptions());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void activeDatasetChanged() {
//        if (lineChart != null) {
//            updateChart();
//        }
    }
}
