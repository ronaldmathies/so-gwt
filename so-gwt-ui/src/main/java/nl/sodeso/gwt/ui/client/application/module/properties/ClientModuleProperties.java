package nl.sodeso.gwt.ui.client.application.module.properties;

import nl.sodeso.gwt.ui.server.endpoint.properties.AbstractApplicationPropertiesEndpoint;

import java.io.Serializable;

/**
 * Container for holding information about the application in general, no user information
 * should reside in this class. Sub-class this container to add extra properties.
 *
 * @see AbstractApplicationPropertiesEndpoint
 *
 * @author Ronald Mathies
 */
public class ClientModuleProperties implements Serializable {

    private String name;
    private String version;

    public ClientModuleProperties() {
    }

    /**
     * Construct a new ApplicationProperties with the given name.
     *
     * @param name the name of the application
     * @return this instance.
     */
    public ClientModuleProperties setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * Returns the name of the application, this is based on the
     * contents of the <code>application.properties</code> file.
     *
     * @return the name of the application.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returns the version number of the application, this is based on the
     * contents of the <code>application.properties</code> file.
     *
     * @return the version of the application.
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the version number of the application.
     *
     * By default this method will be filled by the default endpoint based on the contents
     * of the <code>application.properties</code>.
     *
     * @see AbstractApplicationPropertiesEndpoint
     *
     * @param version the version of the application.
     */
    public void setVersion(String version) {
        this.version = version;
    }

}
