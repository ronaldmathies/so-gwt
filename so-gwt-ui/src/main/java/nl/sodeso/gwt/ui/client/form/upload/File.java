package nl.sodeso.gwt.ui.client.form.upload;

import com.google.gwt.core.client.JavaScriptObject;


/**
 * @author Ronald Mathies
 */
public class File extends JavaScriptObject {

    protected File() {
    }

    public final native String getFilename() /*-{
        return this.name;
    }-*/;

    public final native int getFilesize() /*-{
        return this.size;
    }-*/;



}