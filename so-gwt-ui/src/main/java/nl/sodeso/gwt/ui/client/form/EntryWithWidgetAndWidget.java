package nl.sodeso.gwt.ui.client.form;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.LabelElement;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.form.validation.events.ValidationChangedEvent;
import nl.sodeso.gwt.ui.client.form.validation.rules.ValidationRule;
import nl.sodeso.gwt.ui.client.util.HasKey;
import nl.sodeso.gwt.ui.client.util.KeyUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class EntryWithWidgetAndWidget extends EntryWithWidget implements IsValidationContainer, IsValidatable, IsRevertable, HasKey {

    // Keys for so-key entries.
    private static final String KEY_MESSAGE_SUFFIX = "-message";
    private static final String KEY_HINT_SUFFIX = "-hint";
    private static final String KEY_LEFT_WIDGET_CONTAINER_SUFFIX = "-left-widget-container";
    private static final String KEY_RIGHT_WIDGET_CONTAINER_SUFFIX = "-right-widget-container";

    // CSS style classes
    public final static String STYLE_MESSAGE = "message";
    public final static String STYLE_HINT = "hint";
    public final static String STYLE_ENTRY_WIDGET = "entry-widget-widget";
    public final static String STYLE_LEFT_WIDGET_CONTAINER = "left-widget-container";
    public final static String STYLE_RIGHT_WIDGET_CONTAINER = "right-widget-container";
    public final static String STYLE_BUTTON_CONTAINER = "button-container";

    private ArrayList<ValidationRule> rules = new ArrayList<>();
    private Map<IsValidatable, ValidationResult> validationResults = new HashMap<>();

    private ValidationEventBusController eventbus = new ValidationEventBusController();

    private FlowPanel leftWidgetContainer = null;
    private FlowPanel rightWidgetContainer = null;
    private Widget leftWidget = null;
    private Widget rightWidget = null;
    private LabelElement hintElement = null;
    private FlowPanel messageContainer = null;
    private FlowPanel entryButtonContainer = null;

    /**
     * Constructs a new EntryLabel with the specified key, label and widget to display.
     *
     * @param key the unique identifier for this row.
     * @param leftWidget the left widget to display.
     * @param rightWidget the right widget to display.
     */
    public EntryWithWidgetAndWidget(String key, Widget leftWidget, Widget rightWidget) {
        super();
        addStyleName(getWidgetStyle());

        this.leftWidget = leftWidget;
        this.rightWidget = rightWidget;

        this.leftWidgetContainer = new FlowPanel();
        if (getLeftWidgetContainerStyle() != null) {
            this.leftWidgetContainer.setStyleName(getLeftWidgetContainerStyle());
        }
        if (this.leftWidget != null) {
            this.leftWidgetContainer.add(leftWidget);
        }
        add(this.leftWidgetContainer);

        this.rightWidgetContainer = new FlowPanel();
        if (getRightWidgetContainerStyle() != null) {
            this.rightWidgetContainer.setStyleName(getRightWidgetContainerStyle());
        }
        if (this.rightWidget != null) {
            this.rightWidgetContainer.add(this.rightWidget);
        }
        add(this.rightWidgetContainer);

        this.entryButtonContainer = new FlowPanel();
        this.entryButtonContainer.setStyleName(STYLE_BUTTON_CONTAINER);
        add(this.entryButtonContainer);

        this.hintElement = Document.get().createLabelElement();
        this.hintElement.addClassName(STYLE_HINT);
        getElement().appendChild(this.hintElement);

        this.messageContainer = new FlowPanel();
        this.messageContainer.setStyleName(STYLE_MESSAGE);
        this.rightWidgetContainer.add(this.messageContainer);

        if (leftWidget instanceof IsValidatable) {
            addValidatableWidget((IsValidatable) leftWidget);
        }

        if (rightWidget instanceof IsValidatable) {
            addValidatableWidget((IsValidatable) rightWidget);
        }

        this.addValidatableWidget(this);

        setKey(key);
    }

    /**
     * Adds an additional validatable.
     *
     * @param validatable a validatable.
     *
     * @return this instance
     */
    public EntryWithWidgetAndWidget addValidatableWidget(IsValidatable validatable) {
        validatable.addValidationHandler(ValidationChangedEvent.TYPE, event -> {
            validationResults.put(event.getSource(), event.getValidationResult());
            setMessage(
                new ValidationResult()
                    .mergeAll(validationResults.values()).toWidget());
        });

        return this;
    }

    protected String getWidgetStyle() {
        return STYLE_ENTRY_WIDGET;
    }

    protected String getLeftWidgetContainerStyle() {
        return STYLE_LEFT_WIDGET_CONTAINER;
    }

    protected String getRightWidgetContainerStyle() {
        return STYLE_RIGHT_WIDGET_CONTAINER;
    }

    /**
     * Sets a button on the row on the right side.
     * @param simpleButton the button to add.
     * @return this instance.
     */
    public EntryWithWidgetAndWidget addButton(SimpleButton simpleButton) {
        this.entryButtonContainer.add(simpleButton);
        return this;
    }

    @Override
    public ArrayList<SimpleButton> getButtons() {
        ArrayList<SimpleButton> buttons = new ArrayList<>();
        for (int index = 0; index < this.entryButtonContainer.getWidgetCount(); index++) {
            buttons.add((SimpleButton)this.entryButtonContainer.getWidget(index));
        }
        return buttons;
    }

    /**
     * Sets the specified widget as the message.
     *
     * @param widget the widget.
     */
    public EntryWithWidgetAndWidget setMessage(Widget widget) {
        messageContainer.clear();
        messageContainer.add(widget);
        return this;
    }

    /**
     * Adds an additional hint after the widget.
     * @param hint the hint to display after the widget.
     */
    public EntryWithWidgetAndWidget setHint(String hint) {
        this.hintElement.setInnerHTML(hint);
        return this;
    }

    /**
     * Returns the hint.
     * @return the hint.
     */
    public String getHint() {
        return this.hintElement.getInnerText();
    }

    /**
     * {@inheritDoc}
     */
    public void setKey(String key) {
        super.setKey(key != null ? key + KEY_ENTRY_SUFFIX : null);

        KeyUtil.setKey(messageContainer, key, KEY_MESSAGE_SUFFIX);
        KeyUtil.setKey(hintElement, key, KEY_HINT_SUFFIX);
        KeyUtil.setKey(leftWidgetContainer, key, KEY_LEFT_WIDGET_CONTAINER_SUFFIX);
        KeyUtil.setKey(rightWidgetContainer, key, KEY_RIGHT_WIDGET_CONTAINER_SUFFIX);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_ENTRY_SUFFIX;
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, Arrays.asList(this.leftWidget, this.rightWidget));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationEventBusController getValidationEventbus() {
        return eventbus;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ArrayList<ValidationRule> getValidationRules() {
        return rules;
    }


    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(new Widget[] {this.leftWidget, this.rightWidget});
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.leftWidget) || RevertUtil.hasChanges(this.rightWidget);
    }
}