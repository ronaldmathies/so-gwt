package nl.sodeso.gwt.ui.client.controllers.navigation;

import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.gwt.ui.client.util.Align;

/**
 * @author Ronald Mathies
 */
public class NavigationButton extends SimpleButton {

    public NavigationButton(String key, String html) {
        super(key, html, Style.NAVIGATION);
    }

    public NavigationButton(String key, Icon icon) {
        super(key, icon, Style.NAVIGATION);
    }

    public NavigationButton(String key, Align align, Icon icon, String html) {
        super(key, html, icon, Style.NAVIGATION, align);
    }
}
