package nl.sodeso.gwt.ui.client.form.validation.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * @author Ronald Mathies
 */
public interface ValidationChangedEventHandler extends EventHandler {

    void onValidationChanged(ValidationChangedEvent event);

}
