package nl.sodeso.gwt.ui.client.panel;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.HeadingElement;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.util.HasKey;
import nl.sodeso.gwt.ui.client.util.KeyUtil;

import java.util.ArrayList;

import static nl.sodeso.gwt.ui.client.util.Constants.FALSE;
import static nl.sodeso.gwt.ui.client.util.Constants.TRUE;

/**
 * @author Ronald Mathies
 */
public class WindowPanel extends FlowPanel implements IsValidationContainer, IsRevertable, HasKey {

    // Keys for so-key entries.
    private static final String KEY_WINDOW_PANEL = "-window";
    private static final String KEY_HEADER = "-header";
    private static final String KEY_COLLAPSE = "collapse";
    private static final String KEY_BODY = "-body";
    private static final String KEY_FOOTER = "-footer";

    // CSS style classes.
    private static final String STYLE_WINDOW_PANEL = "window-panel";
    private static final String STYLE_WINDOW_PANEL_HEADER = "window-panel-header";
    private static final String STYLE_WINDOW_PANEL_HEADER_CLOSE = "close";
    private static final String STYLE_WINDOW_PANEL_TOOLBAR = "window-panel-toolbar";
    private static final String STYLE_WINDOW_PANEL_BODY = "window-panel-body";
    private static final String STYLE_WINDOW_PANEL_FOOTER = "window-panel-footer";
    private static final String STYLE_WINDOW_OBJECT_LEFT = "window-object-left";
    private static final String STYLE_WINDOW_OBJECT_RIGHT = "window-object-right";

    // Element attribute names.
    private static final String ATTR_VISIBLE = "so-visible";
    private static final String ATTR_FULL_HEIGHT = "so-full-height";
    private static final String ATTR_WINDOW_BODY_PADDING = "so-padding";

    protected FlowPanel header = null;
    protected FlowPanel toolbar = null;
    protected FlowPanel body = null;
    protected FlowPanel footer = null;

    private ArrayList<Widget> bodyWidgets = new ArrayList<>();

    public enum Style {
        INFO("window-panel-info"),
        ALERT("window-panel-alert");

        String style = null;

        Style(String style) {
            this.style = style;
        }

        public String getStyle() {
            return style;
        }
    }

    public WindowPanel(String key, Style style) {
        this(key, null, style);
    }

    public WindowPanel(String key, String title, Style style) {
        this.setStyleName(STYLE_WINDOW_PANEL);
        this.addStyleName(style.getStyle());

        // Create the header of the panel.
        this.header = new FlowPanel();
        this.header.setStyleName(STYLE_WINDOW_PANEL_HEADER);
        super.add(this.header);

        this.toolbar = new FlowPanel();
        this.toolbar.setStyleName(STYLE_WINDOW_PANEL_TOOLBAR);
        super.add(this.toolbar);

        // Create the body of the panel.
        this.body = new FlowPanel();
        this.body.setStyleName(STYLE_WINDOW_PANEL_BODY);
        super.add(this.body);

        // Create the footer of the panel.
        this.footer = new FlowPanel();
        this.footer.setStyleName(STYLE_WINDOW_PANEL_FOOTER);
        super.add(this.footer);

        // By default hide the toolbar.
        this.setToolbarVisible(false);

        // By default hide the footer.
        this.setFooterVisible(false);

        // By default add padding to the body.
        this.setBodyPadding(true);

        this.setTitle(title);
        this.setKey(key);
    }

    public void showCollapseButton(boolean show) {
        if (show) {
            final SimpleButton collapseButton = new SimpleButton(KEY_COLLAPSE, Icon.MinusSquareO, null);
            collapseButton.addStyleName(STYLE_WINDOW_PANEL_HEADER_CLOSE);
            collapseButton.addClickHandler((event) -> {
                SimpleButton btn = (SimpleButton)event.getSource();

                boolean currentStateIsExpanded = Icon.MinusSquareO.name().equals(btn.getIcon().name());
                btn.setIcon(currentStateIsExpanded ? Icon.PlusSquareO : Icon.MinusSquareO, Align.LEFT);

                setBodyVisible(!currentStateIsExpanded);
                setToolbarVisible(!currentStateIsExpanded && this.toolbar.getWidgetCount() > 0);
                setFooterVisible(!currentStateIsExpanded && this.footer.getWidgetCount() > 0);
            });

            this.header.add(collapseButton);
        }
    }

    public void setTitle(String title) {
        if (title != null) {
            HeadingElement heading;

            if (!this.header.getElement().hasChildNodes()) {
                heading = Document.get().createHElement(4);
                this.header.getElement().appendChild(heading);
            } else {
                heading = (HeadingElement)this.header.getElement().getFirstChild();
            }
            heading.setInnerText(title);
        } else {
            this.header.clear();
        }
    }

    public String getTitle() {
        if (this.header.getElement().hasChildNodes()) {
            HeadingElement heading = Document.get().createHElement(4);
            return heading.getInnerText();
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public <X extends WindowPanel> X addToBody(Widget widget) {
        this.bodyWidgets.add(widget);
        this.body.add(widget);

        return (X)this;
    }

    @SuppressWarnings("unchecked")
    public <X extends WindowPanel> X setBody(Widget widget) {
        this.clearBody();

        this.bodyWidgets.add(widget);
        this.body.add(widget);

        return (X)this;
    }

    public ArrayList<Widget> getBodyWidgets() {
        return this.bodyWidgets;
    }

    public WindowPanel clearBody() {
        this.bodyWidgets.clear();
        this.body.clear();

        return this;
    }

    @SuppressWarnings("unchecked")
    public <X extends WindowPanel> X addToToolbar(Align align, Widget ... widgets) {
        if (!align.isLeft() && !align.isRight()) {
            throw new RuntimeException("Only left and right alignment is allowed");
        }

        for (Widget widget : widgets) {
            widget.addStyleName(align.isLeft() ? STYLE_WINDOW_OBJECT_LEFT : STYLE_WINDOW_OBJECT_RIGHT);
            this.toolbar.add(widget);
            setToolbarVisible(true);
        }

        return (X)this;
    }

    @SuppressWarnings("unchecked")
    public <X extends WindowPanel> X addToFooter(Align align, Widget ... widgets) {
        if (!align.isLeft() && !align.isRight()) {
            throw new RuntimeException("Only left and right alignment is allowed");
        }

        for (Widget widget : widgets) {
            widget.addStyleName(align.isLeft() ? STYLE_WINDOW_OBJECT_LEFT : STYLE_WINDOW_OBJECT_RIGHT);
            this.footer.add(widget);
            setFooterVisible(true);
        }

        return (X)this;
    }

    private void setToolbarVisible(boolean visible) {
        this.toolbar.getElement().setAttribute(ATTR_VISIBLE, visible ? TRUE : FALSE);
    }

    public boolean isToolbarVisible() {
        return !FALSE.equals(this.toolbar.getElement().getAttribute(ATTR_VISIBLE));
    }

    private void setBodyVisible(boolean visible) {
        this.body.getElement().setAttribute(ATTR_VISIBLE, visible ? TRUE : FALSE);
    }

    private boolean isBodyVisible() {
        return !FALSE.equals(this.body.getElement().getAttribute(ATTR_VISIBLE));
    }

    private void setFooterVisible(boolean visible) {
        this.footer.getElement().setAttribute(ATTR_VISIBLE, visible ? TRUE : FALSE);
    }

    public boolean isFooterVisible() {
        return !FALSE.equals(footer.getElement().getAttribute(ATTR_VISIBLE));
    }

    /**
     * {@inheritDoc}
     */
    public void setKey(String key) {
        KeyUtil.setKey(this, key, KEY_WINDOW_PANEL);
        KeyUtil.setKey(this.header, key, KEY_HEADER);
        KeyUtil.setKey(this.body, key, KEY_BODY);
        KeyUtil.setKey(this.footer, key, KEY_FOOTER);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_WINDOW_PANEL;
    }

    /**
     * Sets the height of the widget to the full width of it's parent.
     * @param visible flag indicating if the full height should be visible.
     * @return this instance.
     */
    @SuppressWarnings("unchecked")
    public <X extends WindowPanel> X setFullHeight(boolean visible) {
        getElement().setAttribute(ATTR_FULL_HEIGHT, visible ? TRUE : FALSE);
        return (X)this;
    }

    /**
     * Returns a flag indicating if the height attribute is set or not.
     * @return true if the height attribute is set, false if not.
     */
    public boolean isFullHeight() {
        return !FALSE.equals(getElement().getAttribute(ATTR_FULL_HEIGHT));
    }

    /**
     * Sets the height of the widget to the full width of it's parent.
     * @param padding flag indicating if the full height should be visible.
     * @return this instance.
     */
    @SuppressWarnings("unchecked")
    public <X extends WindowPanel> X setBodyPadding(boolean padding) {
        this.body.getElement().setAttribute(ATTR_WINDOW_BODY_PADDING, padding ? TRUE : FALSE);
        return (X)this;
    }

    /**
     * Returns a flag indicating if the height attribute is set or not.
     * @return true if the height attribute is set, false if not.
     */
    public boolean isBodyPadding() {
        return !FALSE.equals(this.body.getElement().getAttribute(ATTR_WINDOW_BODY_PADDING));
    }

    @Override
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, this.bodyWidgets);
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.bodyWidgets);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.bodyWidgets);
    }

    public void setVisible(boolean visible) {
        getElement().setAttribute(ATTR_VISIBLE, visible ? TRUE : FALSE);
    }

    public boolean isVisible() {
        return !FALSE.equals(getElement().getAttribute(ATTR_VISIBLE));
    }

}
