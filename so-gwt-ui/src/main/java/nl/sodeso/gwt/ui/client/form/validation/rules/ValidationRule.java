package nl.sodeso.gwt.ui.client.form.validation.rules;

import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;

/**
 * @author Ronald Mathies
 */
public interface ValidationRule {

    /**
     * Perform the validation of this rule.
     *
     * @param handler the validation handler.
     */
    void validate(ValidationCompletedHandler handler);

    /**
     * Flag indicating if this validation rule is applicable or not.
     *
     * @return true if this validation rule is applicabale, false if not.
     */
    boolean isApplicable();

}
