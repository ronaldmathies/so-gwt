package nl.sodeso.gwt.ui.client.form.input.filters;

/**
 * @author Ronald Mathies
 */
public class NumericInputFilter implements InputFilter {

    private static final String REGEXP_NUMBERS_ONLY = "^(\\s*|\\d+)$";

    private boolean allowNegative;

    public NumericInputFilter() {
        this.allowNegative = false;
    }

    public NumericInputFilter(boolean allowNegative) {
        this.allowNegative = allowNegative;
    }

    /**
     * Checks if the character only consists of digits.
     * @param character the character to check.
     * @return true if allowed, false if not.
     */
    @Override
    public boolean isAllowedValue(String character) {
        return character.equals("-") && allowNegative || character.matches(REGEXP_NUMBERS_ONLY);
    }
}
