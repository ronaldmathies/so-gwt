package nl.sodeso.gwt.ui.client.resources;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.controllers.logging.LoggingConstants;
import nl.sodeso.gwt.ui.client.controllers.session.LoginConstants;
import nl.sodeso.gwt.ui.client.form.button.ButtonConstants;
import nl.sodeso.gwt.ui.client.form.upload.FileUploadConstants;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessages;

/**
 * @author Ronald Mathies
 */
public final class Resources {

    private Resources() {}

    public final static ButtonConstants BUTTON_I18N = GWT.create(ButtonConstants.class);
    public final static LoggingConstants LOGGING_I18N = GWT.create(LoggingConstants.class);
    public final static LoginConstants LOGIN_I18N = GWT.create(LoginConstants.class);
    public final static FileUploadConstants FILE_UPLOAD_I18N = GWT.create(FileUploadConstants.class);
    public final static ValidationMessages VALIDATION_I18N = GWT.create(ValidationMessages.class);
    public final static GeneralConstants GENERAL_I18N = GWT.create(GeneralConstants.class);

}
