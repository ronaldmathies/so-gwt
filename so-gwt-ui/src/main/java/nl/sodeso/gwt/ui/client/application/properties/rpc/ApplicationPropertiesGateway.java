package nl.sodeso.gwt.ui.client.application.properties.rpc;

import com.google.gwt.core.client.Duration;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.HasRpcToken;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.rpc.XsrfToken;
import nl.sodeso.gwt.ui.client.application.properties.ClientApplicationProperties;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.rpc.XsrfGateway;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class ApplicationPropertiesGateway {

    private static Logger logger = Logger.getLogger("Application.Endpoint");

    private static ApplicationPropertiesRpcAsync applicationPropertiesRpcAsync = GWT.create(ApplicationPropertiesRpc.class);

    private ApplicationPropertiesGateway() {}

    public static void getApplicationProperties(final String moduleId, final AsyncCallback<ClientApplicationProperties> callback) {
        logger.log(Level.FINE, "Performing retrieval of application properties.");

        XsrfGateway.getNewXsrfToken(new DefaultAsyncCallback<XsrfToken>() {
            @Override
            public void success(XsrfToken token) {
                final Duration duration = new Duration();

                ((HasRpcToken) applicationPropertiesRpcAsync).setRpcToken(token);
                ((ServiceDefTarget)applicationPropertiesRpcAsync).setServiceEntryPoint(GWT.getModuleBaseURL() + "endpoint." + moduleId + "-properties");

                applicationPropertiesRpcAsync.getProperties(new AsyncCallback<ClientApplicationProperties>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        logger.log(Level.SEVERE, "Failed to perform retrieval of application properties.", caught);

                        callback.onFailure(caught);
                        // Oepsie.. display error message.
                    }

                    @Override
                    public void onSuccess(ClientApplicationProperties result) {
                        logger.log(Level.FINE, "Successfully performed retrieval of application properties. (duration: " + duration.elapsedMillis() + "ms)");

                        callback.onSuccess(result);
                    }
                });
            }
        });


    }

}
