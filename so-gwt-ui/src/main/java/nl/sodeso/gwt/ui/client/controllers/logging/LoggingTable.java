package nl.sodeso.gwt.ui.client.controllers.logging;

import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.TextHeader;
import com.google.gwt.view.client.SingleSelectionModel;
import nl.sodeso.gwt.ui.client.controllers.logging.handler.LocalLogHandler;
import nl.sodeso.gwt.ui.client.form.table.CellTableField;
import nl.sodeso.gwt.ui.client.util.NullSafeComparator;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class LoggingTable extends CellTableField<LogItem> {

    // Keys for so-key entries.
    private static final String KEY_APPLICATION_LOG_TABLE = "application-log-table";

    private static final int MAX_LOG_ENTRIES = 100;

    public LoggingTable() {
        super(KEY_APPLICATION_LOG_TABLE, 5);

        addColumns();
        addSelectionModel();
        addLoggingHandler();
    }

    private void addColumns() {
        // Columns
        TextColumn<LogItem> levelColumn = new TextColumn<LogItem>() {
            @Override
            public String getValue(LogItem logLine) {
                return logLine.getLevel().getLevel();
            }
        };
        levelColumn.setSortable(true);
        addColumn(levelColumn, new TextHeader(nl.sodeso.gwt.ui.client.resources.Resources.LOGGING_I18N.Level()));
        TextColumn<LogItem> timeColumn = new TextColumn<LogItem>() {
            @Override
            public String getValue(LogItem logLine) {
                return logLine.getTime();
            }
        };
        timeColumn.setSortable(true);
        addColumn(timeColumn, new TextHeader(nl.sodeso.gwt.ui.client.resources.Resources.LOGGING_I18N.Time()));
        TextColumn<LogItem> messageColumn = new TextColumn<LogItem>() {
            @Override
            public String getValue(LogItem logLine) {
                if (logLine.getMessage().length() > 80) {
                    return logLine.getMessage().substring(0, 77) + "...";
                }
                return logLine.getMessage();
            }
        };
        messageColumn.setSortable(true);
        addColumn(messageColumn, new TextHeader(nl.sodeso.gwt.ui.client.resources.Resources.LOGGING_I18N.Message()));

        // Sort handlers.
        ColumnSortEvent.ListHandler<LogItem> columnSortHandler = new ColumnSortEvent.ListHandler<LogItem>(getListDataProvider().getList());
        columnSortHandler.setComparator(levelColumn, new NullSafeComparator<LogItem>() {
            public int safeCompare(LogItem o1, LogItem o2) {
                Integer o1Ordinal = Integer.valueOf(o1.getLevel().ordinal());
                Integer o2Ordinal = Integer.valueOf(o2.getLevel().ordinal());
                return o1Ordinal.compareTo(o2Ordinal);
            }
        });
        columnSortHandler.setComparator(timeColumn, new NullSafeComparator<LogItem>() {
            public int safeCompare(LogItem o1, LogItem o2) {
                return o1.getTime().compareTo(o2.getTime());
            }
        });
        columnSortHandler.setComparator(messageColumn, new NullSafeComparator<LogItem>() {
            public int safeCompare(LogItem o1, LogItem o2) {
                return o1.getMessage().compareTo(o2.getMessage());
            }
        });
        addColumnSortHandler(columnSortHandler);

        // Column width.
        setColumnWidth(levelColumn, 100, com.google.gwt.dom.client.Style.Unit.PX);
        setColumnWidth(timeColumn, 100, com.google.gwt.dom.client.Style.Unit.PX);
        setColumnWidth(messageColumn, 100, com.google.gwt.dom.client.Style.Unit.PCT);

    }

    private void addSelectionModel() {
        final SingleSelectionModel<LogItem> singleSelectionModel = new SingleSelectionModel<LogItem>();
        singleSelectionModel.addSelectionChangeHandler(event -> {
            LogItem logLine = getFirstSelected();
            if (logLine == null) {
                return;
            }

            // Directly deselect the row.
            deselect(getFirstSelected());

            LoggingDetailsWindow loggingDetailsWindow = new LoggingDetailsWindow(logLine);
            loggingDetailsWindow.center(true, true);
        });
        setSelectionModel(singleSelectionModel);
    }

    private void addLoggingHandler() {
        LocalLogHandler.instance().addLogItemEventHandler(event -> {
            List<LogItem> list = getListDataProvider().getList();
            if (list.size() > MAX_LOG_ENTRIES) {
                list.remove(list.size() - 1);
            }

            add(event.getLogItem(), 0);
        });
    }
}
