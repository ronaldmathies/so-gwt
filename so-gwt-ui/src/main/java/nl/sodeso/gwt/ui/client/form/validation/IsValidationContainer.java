package nl.sodeso.gwt.ui.client.form.validation;

/**
 * @author Ronald Mathies
 */
public interface IsValidationContainer {

    void validateContainer(ValidationCompletedHandler handler);

}
