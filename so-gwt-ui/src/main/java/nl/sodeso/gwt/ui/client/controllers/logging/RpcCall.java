package nl.sodeso.gwt.ui.client.controllers.logging;

/**
 * @author Ronald Mathies
 */
public class RpcCall {

    private String method;
    private String time;
    private double duration;

    public RpcCall() {}

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }
}
