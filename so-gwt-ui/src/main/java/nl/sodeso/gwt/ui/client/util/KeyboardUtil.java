package nl.sodeso.gwt.ui.client.util;

import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.KeyCodeEvent;
import com.google.gwt.event.dom.client.KeyCodes;

/**
 * @author Ronald Mathies
 */
public class KeyboardUtil {

    /**
     * Returns the native key code.
     * @param event the native event.
     * @return the native key code.
     */
    public static native char getKeyCode(NativeEvent event)/*-{
        return event.keyCode;
    }-*/;

    /**
     * Flag indicating if the specified keycode is a <code>KeyCodes.KEY_BACKSPACE</code>
     * @param keyCode the key code.
     * @return flag indicating true or false.
     */
    public static boolean isBackspace(int keyCode) {
        return keyCode == KeyCodes.KEY_BACKSPACE;
    }

    /**
     * Flag indicating if the specified keycode is a <code>KeyCodes.KEY_DELETE</code>
     * @param keyCode the key code.
     * @return flag indicating true or false.
     */
    public static boolean isDelete(int keyCode) {
        return keyCode == KeyCodes.KEY_DELETE;
    }

    /**
     * Flag indicating if the specified keycode is a <code>KeyCodes.KEY_SPACE</code>
     * @param keyCode the key code.
     * @return flag indicating true or false.
     */
    public static boolean isSpace(int keyCode) {
        return keyCode == KeyCodes.KEY_SPACE;
    }

    /**
     * Flag indicating if the specified keycode is a letter in the range of <code>KeyCodes.KEY_ZERO</code> and <code>KeyCodes.KEY_NINE</code> or
     * <code>KeyCodes.KEY_NUM_ZERO</code> and <code>KeyCodes.KEY_NUM_NINE</code>.
     * @param keyCode the key code.
     * @return flag indicating true or false.
     */
    public static boolean isDigit(int keyCode) {
        return (keyCode >= KeyCodes.KEY_ZERO && keyCode <= KeyCodes.KEY_NINE) ||
                (keyCode >= KeyCodes.KEY_NUM_ZERO && keyCode <= KeyCodes.KEY_NUM_NINE);
    }

    /**
     * Flag indicating if the specified keycode is a letter in the range of <code>KeyCodes.KEY_A</code> and <code>KeyCodes.KEY_Z</code>
     * @param keyCode the key code.
     * @return flag indicating true or false.
     */
    public static boolean isLetter(int keyCode) {
        return (keyCode >= KeyCodes.KEY_A && keyCode <= KeyCodes.KEY_Z);
    }

    /**
     * Flag indicating if the specified keycode is a <code>KeyCodes.KEY_ENTER</code> or <code>KeyCodes.KEY_MAC_ENTER</code>
     * @param event the key event.
     * @return flag indicating true or false.
     */
    public static boolean isEnter(KeyCodeEvent event) {
        return isEnter(event.getNativeEvent().getKeyCode());
    }

    /**
     * Flag indicating if the specified keycode is a <code>KeyCodes.KEY_ENTER</code> or <code>KeyCodes.KEY_MAC_ENTER</code>
     * @param keyCode the key code.
     * @return flag indicating true or false.
     */
    public static boolean isEnter(int keyCode) {
        return keyCode == KeyCodes.KEY_ENTER ||
                keyCode == KeyCodes.KEY_MAC_ENTER;
    }


    /**
     * Flag indicating if the specified keycode is a <code>KeyCodes.KEY_TAB</code>
     * @param keyCode the key code.
     * @return flag indicating true or false.
     */
    public static boolean isTab(int keyCode) {
        return keyCode == KeyCodes.KEY_TAB;
    }

}
