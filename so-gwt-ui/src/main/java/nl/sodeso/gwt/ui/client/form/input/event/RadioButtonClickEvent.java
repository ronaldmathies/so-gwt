package nl.sodeso.gwt.ui.client.form.input.event;

import nl.sodeso.gwt.ui.client.form.input.RadioButtonField;

/**
 * @author Ronald Mathies
 */
public class RadioButtonClickEvent {

    private RadioButtonField source;

    public RadioButtonClickEvent(RadioButtonField source) {
        this.source = source;
    }

    public RadioButtonField getSource() {
        return this.source;
    }

}
