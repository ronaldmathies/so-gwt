package nl.sodeso.gwt.ui.client.form.validation.rules;

import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.resources.Resources;

import static nl.sodeso.gwt.ui.client.form.validation.ValidationMessage.Level;

/**
 * @author Ronald Mathies
 */
public abstract class LengthValidationRule implements ValidationRule {

    // Keys for so-key entries.
    private static final String KEY_MINIMUM_SUFFIX = "-minimum";
    private static final String KEY_MAXIMUM_SUFFIX = "-maximum";

    private String key = null;
    private Level level = null;

    private boolean useMinimum = false;
    private int minimum = -1;
    private boolean useMaximum = false;
    private int maximum = -1;

    /**
     * Constructs a new validation rule.
     *
     * @param key The key to use in the validation result.
     * @param level The message level to use in the validation result.
     */
    public LengthValidationRule(String key, Level level) {
        this.key = key;
        this.level = level;
    }

    /**
     * Set the minimum length of a string that is required.
     *
     * @param minimum the minimum length of a string that is required.
     * @return this instance.
     */
    public LengthValidationRule setMinimum(int minimum) {
        this.useMinimum = true;
        this.minimum = minimum;
        return this;
    }

    /**
     * Set the maximum length of a string that is required.
     *
     * @param maximum the maximum length of a string that is required.
     * @return this instance.
     */
    public LengthValidationRule setMaximum(int maximum) {
        this.useMaximum = true;
        this.maximum = maximum;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public void validate(ValidationCompletedHandler handler) {
        ValidationResult result = new ValidationResult();
        if (useMinimum && checkMinimumLength()) {
            result.add(new ValidationMessage(this.key != null ? key + KEY_MINIMUM_SUFFIX : null, level, Resources.VALIDATION_I18N.LengthValidationMinimumLength(getValue().length(), this.minimum)));
        }

        if (useMaximum && checkMaximumLength()) {
            result.add(new ValidationMessage(this.key != null ? key + KEY_MAXIMUM_SUFFIX : null, level, Resources.VALIDATION_I18N.LengthValidationMaximumLength(getValue().length(), this.maximum)));
        }

        handler.completed(result);
    }

//    /**
//     * {@inheritDoc}
//     */
//    public boolean isValid() {
//        if (useMinimum && !checkMinimumLength()) {
//            return false;
//        }
//
//        if (useMaximum && !checkMaximumLength()) {
//            return false;
//        }
//
//        return true;
//    }

    /**
     * {@inheritDoc}
     */
    public boolean isApplicable() {
        return true;
    }

    /**
     * Implement this method to return the string value to check for.
     *
     * @return the string value to check.
     */
    public abstract String getValue();

    private boolean checkMinimumLength() {
        return getValue().length() < this.minimum;
    }

    private boolean checkMaximumLength() {
        return getValue().length() > this.maximum;
    }

}
