package nl.sodeso.gwt.ui.client.controllers.session;

import com.google.gwt.event.dom.client.KeyUpHandler;
import nl.sodeso.gwt.ui.client.controllers.session.event.UserLoginEvent;
import nl.sodeso.gwt.ui.client.controllers.session.event.UserLoginEventHandler;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.button.LoginButton;
import nl.sodeso.gwt.ui.client.form.button.OkButton;
import nl.sodeso.gwt.ui.client.form.input.PasswordField;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.PopupPanel;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.resources.Resources;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.util.KeyboardUtil;

import static nl.sodeso.gwt.ui.client.form.validation.ValidationMessage.Level;

/**
 * @author Ronald Mathies
 */
public class LoginWindow extends PopupPanel implements UserLoginEventHandler {

    // Keys for so-key entries.
    private static final String KEY_LOGIN_WINDOW = "login";
    private static final String KEY_LOGIN_FORM = "login";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASSWORD = "password";

    // CSS style classes.
    private static final String STYLE_LOGIN_WINDOW = "login-window";

    private TextField usernameField = null;
    private PasswordField passwordField = null;

    public LoginWindow() {
        super(KEY_LOGIN_WINDOW, WindowPanel.Style.INFO, Resources.LOGIN_I18N.Title(), false, false);
        this.setAnimationEnabled(false);
        this.addStyleNameToWindow(STYLE_LOGIN_WINDOW);

        EntryForm loginForm = new EntryForm(KEY_LOGIN_FORM);

        KeyUpHandler keyUpHandler = (event)-> {
            if (KeyboardUtil.isEnter(event)) {
                onLogin();
            }
        };

        StringType username = new StringType();
        this.usernameField = new TextField<>(KEY_USERNAME, username);
        this.usernameField.addKeyUpHandler(keyUpHandler);
        this.usernameField.addValidationRule(new MandatoryValidationRule(KEY_USERNAME, Level.ERROR) {
            public String getValue() {
                return usernameField.getValue();
            }
        });
        loginForm.addEntry(new EntryWithLabelAndWidget(KEY_USERNAME, Resources.LOGIN_I18N.Username(), usernameField));

        StringType password = new StringType();
        this.passwordField = new PasswordField(KEY_PASSWORD, password);
        this.passwordField.addKeyUpHandler(keyUpHandler);
        this.passwordField.addValidationRule(new MandatoryValidationRule(KEY_PASSWORD, Level.ERROR) {
            public String getValue() {
                return passwordField.getValue();
            }
        });
        loginForm.addEntry(new EntryWithLabelAndWidget(KEY_PASSWORD, Resources.LOGIN_I18N.Password(), passwordField));

        addToBody(loginForm);
        addToFooter(Align.RIGHT, new LoginButton.WithLabel((event) -> onLogin()));

        SessionController.instance().getEventBus().addHandler(UserLoginEvent.TYPE, this);

        usernameField.setFocus();
    }

    private void onLogin() {
        ValidationUtil.validate(result -> {
            if (ValidationUtil.findWorstLevel(result) == Level.INFO) {
                SessionController.instance().login(usernameField.getValue(), passwordField.getValue());
            }
        }, LoginWindow.this);
    }

    @Override
    public void onEvent(UserLoginEvent event) {
        if (event.getLoginResult().isSuccess()) {
            SessionController.instance().getEventBus().removeHandler(UserLoginEvent.TYPE, this);
            removeFromParent();
        } else {
            this.usernameField.setFocus(false);
            this.passwordField.setFocus(false);

            PopupWindowPanel failedToLoginPopup = new PopupWindowPanel("invalid-login-popup", "Failed to login", WindowPanel.Style.ALERT);
            EntryForm entryForm = new EntryForm(null);
            entryForm.addEntry(new EntryWithDocumentation(null, "Invalid username or password."));
            failedToLoginPopup.addToBody(entryForm);

            OkButton.WithLabel okButton = new OkButton.WithLabel();
            okButton.addClickHandler((clickEvent) -> failedToLoginPopup.close());
            failedToLoginPopup.addToFooter(Align.RIGHT, okButton);
            failedToLoginPopup.center(true, true);

        }
    }
}