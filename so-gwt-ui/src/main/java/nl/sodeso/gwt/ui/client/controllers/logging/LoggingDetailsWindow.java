package nl.sodeso.gwt.ui.client.controllers.logging;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.button.OkButton;
import nl.sodeso.gwt.ui.client.form.input.TextAreaField;
import nl.sodeso.gwt.ui.client.form.input.TextAreaToolsField;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.resources.Resources;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.Align;

/**
 * @author Ronald Mathies
 */
public class LoggingDetailsWindow extends PopupWindowPanel {

    // Keys for so-key entries.
    private static final String KEY_LOG_TIME = "log-time";
    private static final String KEY_LOG_MESSAGE = "log-message";
    private static final String KEY_LOG_STACKTRACE = "log-stacktrace";
    private static final String KEY_LOG_MESSAGE_DETAILS = "log-message-details";

    // CSS style classes.
    private static final String STYLE_LOGGING_DETAILS = "logging-details-window";

    public LoggingDetailsWindow(LogItem logLine) {
        super(KEY_LOG_MESSAGE_DETAILS, Resources.LOGGING_I18N.LogDetailsTitle(), WindowPanel.Style.ALERT);

        addStyleName(STYLE_LOGGING_DETAILS);

        TextField timeField = new TextField<>(KEY_LOG_TIME, new StringType(logLine.getTime()));
        timeField.setReadOnly(true);

        TextAreaField messageField = new TextAreaField(KEY_LOG_MESSAGE, new StringType(logLine.getMessage()));
        messageField.setReadOnly(true);

        TextAreaToolsField zoomedMessageField = new TextAreaToolsField("Console", messageField);
        zoomedMessageField.addZoomOption();

        TextAreaField stackTractField = new TextAreaField(KEY_LOG_STACKTRACE, new StringType(logLine.getStackTrace()));
        stackTractField.setReadOnly(true);

        TextAreaToolsField zoomedStackTractField = new TextAreaToolsField("Console", stackTractField);
        zoomedStackTractField.addZoomOption();

        addToBody(
            new EntryForm(null)
                .addEntries(
                    new EntryWithLabelAndWidget(KEY_LOG_TIME, Resources.LOGGING_I18N.Time(), timeField),
                    new EntryWithLabelAndWidget(KEY_LOG_MESSAGE, Resources.LOGGING_I18N.Message(), zoomedMessageField),
                    new EntryWithLabelAndWidget(KEY_LOG_STACKTRACE, Resources.LOGGING_I18N.Stacktrace(), zoomedStackTractField)));

        addToFooter(Align.RIGHT, new OkButton.WithLabel((event) -> close()));
    }

}
