package nl.sodeso.gwt.ui.client.form;


import nl.sodeso.gwt.ui.client.form.button.SimpleButton;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public abstract class EntryWithWidget<T extends EntryWithWidget> extends Entry {

    public abstract T addButton(SimpleButton simpleButton);

    public ArrayList<SimpleButton> getButtons() {
        return new ArrayList<>();
    }



}
