package nl.sodeso.gwt.ui.client.form;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.LabelElement;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.util.HasKey;
import nl.sodeso.gwt.ui.client.util.KeyUtil;

/**
 * @author Ronald Mathies
 */
public class EntryWithDocumentation extends Entry implements HasKey {

    // Keys for so-key entries.
    private static final String KEY_SUFFIX = "-documentation";

    // CSS style classes
    private static final String STYLE_ENTRY_DOCUMENTATION = "entry-documentation";
    private static final String STYLE_ENTRY_LABEL = "label";


    private LabelElement documentationElement = null;

    /**
     *
     * @param key the unique key for debugging / testing "-documentation" is automatically added as a suffix.
     * @param documentation
     */
    public EntryWithDocumentation(String key, String documentation) {
        this(key, documentation, Align.LEFT);
    }

    public EntryWithDocumentation(String key, String documentation, Align align) {
        super();

        if (!align.isLeft() && !align.isCenter() && !align.isRight()) {
            throw new RuntimeException("Only left, center and right alignment is allowed");
        }

        addStyleName(STYLE_ENTRY_DOCUMENTATION);

        this.documentationElement = Document.get().createLabelElement();
        this.documentationElement.addClassName(STYLE_ENTRY_LABEL);
        this.documentationElement.addClassName("align-" + align.getAlignment());

        this.documentationElement.setHtmlFor(key);
        setDocumentation(documentation);

        getElement().appendChild(this.documentationElement);

        setKey(key);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_ENTRY_SUFFIX;
    }

    public void setDocumentation(String documentation) {
        this.documentationElement.setInnerHTML(documentation);
    }

    public String getDocumentation() {
        return this.documentationElement.getInnerHTML();
    }

    /**
     * {@inheritDoc}
     */
    public void setKey(String key) {
        super.setKey(key);

        KeyUtil.setKey(documentationElement, key, KEY_SUFFIX);
    }

}
