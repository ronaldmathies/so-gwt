package nl.sodeso.gwt.ui.client.form.input;

import com.google.gwt.user.client.ui.FlowPanel;
import nl.sodeso.gwt.ui.client.form.input.event.RadioButtonClickEvent;
import nl.sodeso.gwt.ui.client.form.input.event.RadioButtonClickEventHandler;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.validation.IsValidatable;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationEventBusController;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.rules.ValidationRule;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.ValueType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.gwt.ui.client.util.Align;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class RadioButtonGroupField extends FlowPanel implements IsValidatable, IsRevertable {

    private String key;

    private ArrayList<RadioButtonClickEventHandler> clickHandlers = new ArrayList<>();
    private ArrayList<RadioButtonField> radiobuttons = new ArrayList<>();

    private ArrayList<ValidationRule> rules = new ArrayList<>();
    private ValidationEventBusController eventbus = new ValidationEventBusController();

    private ValueType valueType = null;

    public RadioButtonGroupField(String key, BooleanType valueType) {
        this(key, (ValueType) valueType);
    }

    public RadioButtonGroupField(String key, IntType valueType) {
        this(key, (ValueType)valueType);
    }

    @SuppressWarnings("unchecked")
    private RadioButtonGroupField(String key, ValueType valueType) {
        super();

        this.key = key;
        this.valueType = valueType;

        this.valueType.addValueChangedEventHandler(new ValueChangedEventHandler(this) {

            @Override
            public void onValueCanged(ValueChangedEvent event) {
                selectByValue(true);
            }
        });

        this.addAttachHandler(event -> {
            selectByValue(false);
        });
    }

    private void selectByValue(boolean fireEvents) {
        ValueType valueType1 = RadioButtonGroupField.this.valueType;
        if (valueType1 != null) {
            if (valueType1 instanceof BooleanType) {
                Boolean value = ((BooleanType) valueType1).getValue();
                for (RadioButtonField radioButtonField : RadioButtonGroupField.this.radiobuttons) {
                    if (Boolean.valueOf(radioButtonField.getKey()).equals(value)) {
                        radioButtonField.setValue(true, fireEvents);
                    }
                }
            } else {
                Integer value = ((IntType) valueType1).getValue();
                for (RadioButtonField radioButtonField : RadioButtonGroupField.this.radiobuttons) {
                    if (Integer.valueOf(radioButtonField.getKey().substring(0)).equals(value)) {
                        radioButtonField.setValue(true, fireEvents);
                    }
                }
            }


        }
    }

    public void addClickHandler(RadioButtonClickEventHandler handler) {
        this.clickHandlers.add(handler);
    }

    /**
     * Manually fires a click event.
     */
    public void fireClickEvent() {
        RadioButtonField radioButtonField = getSelected();
        RadioButtonClickEvent event = new RadioButtonClickEvent(radioButtonField);

        for (RadioButtonClickEventHandler handler : this.clickHandlers) {
            handler.onClick(event);
        }
    }

    public RadioButtonField addRadioButton(String key, String label) {
        final RadioButtonField radioButtonField = new RadioButtonField(this.key, key, label);

        radioButtonField.addClickHandler(gwtEvent -> {
            valueType.setValueAsString(radioButtonField.getKey());

            RadioButtonClickEvent event = new RadioButtonClickEvent(radioButtonField);
            for (RadioButtonClickEventHandler handler : clickHandlers) {
                handler.onClick(event);
            }

            validate(new ValidationCompletedHandler() {
                @Override
                public void completed(ValidationResult result) {

                }
            });
        });

        radioButtonField.addValueChangeHandler(gwtEvent -> {
            RadioButtonClickEvent event = new RadioButtonClickEvent(radioButtonField);
            for (RadioButtonClickEventHandler handler : clickHandlers) {
                handler.onClick(event);
            }
        });

        this.radiobuttons.add(radioButtonField);

        add(radioButtonField);

        return radioButtonField;
    }

    public void select(String key, boolean fireEvent) {
        for (RadioButtonField radioButtonField : radiobuttons) {
            if (key.equals(radioButtonField.getKey())) {
                select(radioButtonField, fireEvent);

                validate((result) -> {});
            }
        }
    }

    public void select(RadioButtonField radioButtonField, boolean fireEvent) {
        radioButtonField.setValue(true);

        if (fireEvent) {

        }
    }

    public RadioButtonField getSelected() {
        for (RadioButtonField radioButtonField : this.radiobuttons) {
            if (radioButtonField.getValue()) {
                return radioButtonField;
            }
        }

        return null;
    }

    public void setEnabled(boolean enabled) {
        for (RadioButtonField radioButtonField : radiobuttons) {
            radioButtonField.setEnabled(enabled);
        }
    }

    public void setAlignment(Align align) {
        for (RadioButtonField radioButtonField : radiobuttons) {
            radioButtonField.setAlignment(align);
        }
    }

    /**
     * {@inheritDoc}
     */
    public String getKey() {
        return this.key;
    }
    /**
     * {@inheritDoc}
     */
    public ValidationEventBusController getValidationEventbus() {
        return eventbus;
    }

    /**
     * {@inheritDoc}
     */
    public ArrayList<ValidationRule> getValidationRules() {
        return this.rules;
    }

    @Override
    public void revert() {
        valueType.revert();
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return valueType.isChanged();
    }
}