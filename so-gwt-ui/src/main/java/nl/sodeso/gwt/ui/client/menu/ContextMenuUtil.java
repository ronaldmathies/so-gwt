package nl.sodeso.gwt.ui.client.menu;

import com.google.gwt.event.dom.client.ContextMenuEvent;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.controllers.menu.UnorderedListElement;

/**
 * @author Ronald Mathies
 */
public class ContextMenuUtil {

    // CSS style classes.
    private static final String STYLE_MENU_PANEL = "menu-panel";
    private static final String STYLE_CONTEXT_MENU_PANEL = "context-menu-panel";

    public static void add(Widget widget, final ContextMenuItem ... menuItems) {
        widget.sinkEvents(Event.ONCONTEXTMENU);
        widget.addDomHandler(event -> {
            event.preventDefault();
            event.stopPropagation();

            PopupPanel contextMenu = new PopupPanel(true, false);
            contextMenu.addStyleName(STYLE_MENU_PANEL);
            contextMenu.addStyleName(STYLE_CONTEXT_MENU_PANEL);

            UnorderedListElement menuListElement = new UnorderedListElement();
            for (ContextMenuItem menuItem : menuItems) {
                menuItem.setContextMenu(contextMenu);
                menuItem.attachToParent(menuListElement);
            }

            contextMenu.add(menuListElement);
            contextMenu.hide();

            contextMenu.setPopupPosition(event.getNativeEvent().getClientX(), event.getNativeEvent().getClientY());
            contextMenu.show();
        }, ContextMenuEvent.getType());

    }

}
