package nl.sodeso.gwt.ui.client.form.revertable;

/**
 * @author Ronald Mathies
 */
public interface RevertableExecutor {

    void revert();

}
