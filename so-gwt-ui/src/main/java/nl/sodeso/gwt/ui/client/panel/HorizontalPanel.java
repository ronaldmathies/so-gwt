package nl.sodeso.gwt.ui.client.panel;

import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static nl.sodeso.gwt.ui.client.util.Constants.FALSE;
import static nl.sodeso.gwt.ui.client.util.Constants.TRUE;

/**
 * A container on which all widgets that are added are horizontally stacked.
 *
 * @author Ronald Mathies
 */
public class HorizontalPanel extends FlowPanel implements IsValidationContainer, IsRevertable {

    // CSS style classes.
    private final static String STYLE_HORIZONTAL_PANEL = "horizontal-panel";

    // Element attribute names.
    private static final String ATTR_FULL_HEIGHT = "so-full-height";

    private ArrayList<Widget> widgets = new ArrayList<>();

    /**
     * Constructs a new horizontal panel with the specified widgets. All widgets
     * will have equal space.
     */
    public HorizontalPanel() {
        super();

        setStyleName(STYLE_HORIZONTAL_PANEL);
    }

    public HorizontalPanel addWidget(Widget widget) {
        addWidget(widget, 0.0, null);
        return this;
    }

    /**
     * Adds a widget with a fixed width.
     *
     * @param widget the widget to add.
     * @param width the width.
     * @param unit the unit used to specify the width.
     */
    public HorizontalPanel addWidget(Widget widget, double width, Style.Unit unit) {
        if (!this.widgets.isEmpty()) {
            this.widgets.get(this.widgets.size() - 1).getElement().getStyle().setPaddingRight(10, Style.Unit.PX);
        }

        this.widgets.add(widget);

        FlowPanel box = new FlowPanel();
        Style style = box.getElement().getStyle();
        if (unit != null) {
            style.setWidth(width, unit);
        }
        box.add(widget);
        add(box);

        return this;
    }

    /**
     * Adds a list of widgets to the container.
     *
     * @param widgets the widgets to add.
     */
    public HorizontalPanel addWidgets(Widget ... widgets) {
        this.widgets.clear();

        this.widgets.addAll(Arrays.asList(widgets));

        for (int index = 0; index < widgets.length; index++) {
            Widget widget = widgets[index];

            FlowPanel box = new FlowPanel();

            Style style = box.getElement().getStyle();
            style.setWidth(100.0 / widgets.length, Style.Unit.PCT);

            if (index + 1 < widgets.length) {
                style.setPaddingRight(10, Style.Unit.PX);
            }

            box.add(widget);
            add(box);
        }

        return this;
    }

    /**
     * Returns all widgets that are available on this panel.
     * @return all widgets that are available on this panel.
     */
    public List<Widget> widgets() {
        return this.widgets;
    }

    /**
     * Sets the height of the widget to the full width of it's parent.
     * @param visible flag indicating if the full height should be visible.
     */
    public HorizontalPanel setFullHeight(boolean visible) {
        getElement().setAttribute(ATTR_FULL_HEIGHT, visible ? TRUE : FALSE);
        return this;
    }

    /**
     * Returns a flag indicating if the height attribute is set or not.
     * @return true if the height attribute is set, false if not.
     */
    public boolean isFullHeight() {
        return !FALSE.equals(getElement().getAttribute(ATTR_FULL_HEIGHT));
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, widgets);
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(widgets);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(widgets);
    }
}
