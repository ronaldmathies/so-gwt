package nl.sodeso.gwt.ui.client.controllers.center;

/**
 * @author Ronald Mathies
 */
public class Center {

    private AbstractCenterPanel abstractCenterPanel;
    private boolean isFullwidth = false;

    public Center(AbstractCenterPanel centerPanel) {
        this.abstractCenterPanel = centerPanel;
    }

    public AbstractCenterPanel getCenterPanel() {
        return this.abstractCenterPanel;
    }

    public void setFullwidth(boolean fullwidth) {
        this.isFullwidth = fullwidth;
    }

    public boolean isFullwidth() {
        return this.isFullwidth;
    }

}
