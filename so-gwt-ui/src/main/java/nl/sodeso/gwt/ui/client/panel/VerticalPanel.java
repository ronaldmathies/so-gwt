package nl.sodeso.gwt.ui.client.panel;

import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static nl.sodeso.gwt.ui.client.util.Constants.FALSE;
import static nl.sodeso.gwt.ui.client.util.Constants.TRUE;

/**
 * A container on which all widgets that are added are vertically stacked.
 *
 * @author Ronald Mathies
 */
public class VerticalPanel extends FlowPanel implements IsValidationContainer, IsRevertable {

    // CSS style classes.
    private final static String STYLE_VERTICAL_PANEL = "vertical-panel";

    // Element attribute names.
    private static final String ATTR_FULL_HEIGHT = "so-full-height";

    private List<Widget> widgets = new ArrayList<>();

    /**
     * Constructs a new vertical panel.
     */
    public VerticalPanel() {
        super();
        setStyleName(STYLE_VERTICAL_PANEL);
    }

    /**
     * Constructs a new vertical panel with the widgets supplied.
     * @param widgets the widgets to add.
     */
    public VerticalPanel(Widget ... widgets) {
        this();
        addWidgets(widgets);
    }

    public void addWidget(Widget widget) {
        addWidget(widget, 0.0, null);
    }

    /**
     * Adds a widget with a fixed width.
     *
     * @param widget the widget to add.
     * @param height the height.
     * @param unit the unit used to specify the width.
     */
    public void addWidget(Widget widget, double height, Style.Unit unit) {
        if (!this.widgets.isEmpty()) {
            this.widgets.get(this.widgets.size() - 1).getElement().getStyle().setPaddingBottom(10, Style.Unit.PX);
        }

        this.widgets.add(widget);

        FlowPanel box = new FlowPanel();
        Style style = box.getElement().getStyle();
        if (unit != null) {
            style.setHeight(height, unit);
        }
        box.add(widget);
        add(box);
    }

    /**
     * Adds a list of widgets to the container.
     *
     * @param widgets the widgets to add.
     */
    public void addWidgets(Widget ... widgets) {
        this.widgets.clear();

        this.widgets = Arrays.asList(widgets);

        for (int index = 0; index < widgets.length; index++) {
            Widget widget = widgets[index];

            FlowPanel box = new FlowPanel();

            Style style = box.getElement().getStyle();

            if (index + 1 < widgets.length) {
                style.setPaddingBottom(10, Style.Unit.PX);
            }

            box.add(widget);
            add(box);
        }
    }

    /**
     * Sets the height of the widget to the full width of it's parent.
     * @param visible flag indicating if the full height should be visible.
     */
    public void setFullHeight(boolean visible) {
        getElement().setAttribute(ATTR_FULL_HEIGHT, visible ? TRUE : FALSE);
    }

    /**
     * Returns a flag indicating if the height attribute is set or not.
     * @return true if the height attribute is set, false if not.
     */
    public boolean isFullHeight() {
        return !FALSE.equals(getElement().getAttribute(ATTR_FULL_HEIGHT));
    }

    /**
     * Returns all widgets that are available on this panel.
     * @return all widgets that are available on this panel.
     */
    public List<Widget> widgets() {
        return this.widgets;
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, this.widgets);
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.widgets);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.widgets);
    }
}
