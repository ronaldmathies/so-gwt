package nl.sodeso.gwt.ui.client.controllers.session.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * @author Ronald Mathies
 */
public interface UserLogoutEventHandler extends EventHandler {

    void onEvent(UserLogoutEvent event);

}
