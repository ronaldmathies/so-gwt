package nl.sodeso.gwt.ui.server.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * Filter that will add additional header attributes to the GWT JavaScript
 * files that contain .nocache., these files should not be cached by the client
 * since these files contain the checks wether or not there is a new version
 * of the JavaScript files that contain the actual application.
 *
 * @author Ronald Mathies
 */
@WebFilter("/*")
public class GwtNoCacheFilter implements Filter {

    private static final String HEADER_DATE = "Date";
    private static final String HEADER_EXPIRES = "Expires";
    private static final String HEADER_PRAGMA = "Pragma";
    private static final String HEADER_CACHE_CONTROL = "Cache-control";

    private static final long ONE_DAY = 86400000L;

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;

        // Check if the request contains .nocache. in the URI, if so
        // add the additional headers.
        String requestURI = httpRequest.getRequestURI();
        if (requestURI.contains(".nocache.")) {
            Date now = new Date();
            if (response instanceof HttpServletResponse){
                HttpServletResponse httpResponse = (HttpServletResponse) response;
                httpResponse.setDateHeader(HEADER_DATE, now.getTime()); // one day old
                httpResponse.setDateHeader(HEADER_EXPIRES, now.getTime() - ONE_DAY);
                httpResponse.setHeader(HEADER_PRAGMA, "no-cache");
                httpResponse.setHeader(HEADER_CACHE_CONTROL, "no-cache, no-store, must-revalidate");
            }
        }

        chain.doFilter(request, response);
    }

    public void destroy() {
    }
}