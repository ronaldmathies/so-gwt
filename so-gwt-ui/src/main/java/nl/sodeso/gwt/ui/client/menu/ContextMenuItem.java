package nl.sodeso.gwt.ui.client.menu;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.PopupPanel;
import nl.sodeso.gwt.ui.client.controllers.menu.*;
import nl.sodeso.gwt.ui.client.controllers.menu.events.MenuItemSelectedEvent;
import nl.sodeso.gwt.ui.client.controllers.menu.events.MenuItemSelectedEventHandler;
import nl.sodeso.gwt.ui.client.resources.Icon;

/**
 * @author Ronald Mathies
 */
public class ContextMenuItem {

    private String key;

    private Icon icon;
    private String label;

    private ContextMenuItemClickTrigger menuItemClickTrigger;

    private UnorderedListElement parentElement = null;
    private UnorderedListItemElement menuItemAsChildElement = null;

    private PopupPanel contextMenu = null;

    public ContextMenuItem(String key, String label, ContextMenuItemClickTrigger menuItemClickTrigger) {
        this(key, null, label, menuItemClickTrigger);
    }

    public ContextMenuItem(String key, Icon icon, String label, ContextMenuItemClickTrigger menuItemClickTrigger) {
        this.key = key;

        this.icon = icon;
        this.label = label;

        this.menuItemClickTrigger = menuItemClickTrigger;
        this.menuItemAsChildElement = new UnorderedListItemElement(null, icon, label);
        this.menuItemAsChildElement.addDomHandler(event -> {
            contextMenu.hide();
            this.menuItemClickTrigger.click();
        }, ClickEvent.getType());

        menuItemAsChildElement.setSelected(false);
        setSelected(false);
    }

    protected void setContextMenu(PopupPanel contextMenu) {
        this.contextMenu = contextMenu;
    }

    /**
     * Returns the key of this menu item.
     * @return the key of this menu item.
     */
    public String getKey() {
        return this.key;
    }

    /**
     * Flag indicating if the key is the same as the key passed in.
     * @param key the key to match
     * @return true if there is a match, false if not.
     */
    public boolean hasKey(String key) {
        return this.key.equals(key);
    }

    /**
     * The icon to use to represent this menu item.
     * @return the icon.
     */
    public Icon getIcon() {
        return this.icon;
    }

    /**
     * The label of the menu item.
     * @return the label.
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * Returns the trigger associated with this menu item.
     * @return the trigger associated with this menu item.
     */
    public ContextMenuItemClickTrigger getMenuItemClickTrigger() {
        return this.menuItemClickTrigger;
    }

    /**
     * Flag indicating if this menu item is attached to a parent.
     * @return true if attached, false if not.
     */
    public boolean isAttached() {
        return parentElement != null;
    }

    /**
     * Attached this menu item to the given parent.
     * @param parentElement the parent element
     */
    protected void attachToParent(UnorderedListElement parentElement) {
        this.parentElement = parentElement;

        parentElement.addListItem(menuItemAsChildElement);
        animateLeaf();

    }

    /**
     * Performing a transition directly after adding the item to the DOM
     * tree has as a side effect that the transitions don't work, apparently we
     * need to wait at least 1ms. (different thread maybe?) for the
     * transition to work.
     */
    private void animateLeaf() {
        Timer timer = new Timer() {
            @Override
            public void run() {
                setVisible(true);
            }
        };
        timer.schedule(1);
    }

    public void setVisible(boolean visible) {
        menuItemAsChildElement.setVisible(visible);
    }

    public boolean isVisible() {
        return menuItemAsChildElement.isVisible();
    }

    public boolean isSelected() {
        return menuItemAsChildElement.isSelected();
    }

    public void setSelected(boolean selected) {
        menuItemAsChildElement.setSelected(selected);
    }

    public void setEnabled(boolean enabled) {
        menuItemAsChildElement.setEnabled(enabled);
    }

    public boolean isEnabled() {
        return menuItemAsChildElement.isEnabled();
    }

}
