package nl.sodeso.gwt.ui.client.form.table;

import com.google.gwt.user.cellview.client.AbstractPager;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.view.client.HasRows;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.resources.Resources;

import static nl.sodeso.gwt.ui.client.util.Constants.FALSE;
import static nl.sodeso.gwt.ui.client.util.Constants.TRUE;

/**
 * @author Ronald Mathies
 */
public class TablePagerField extends AbstractPager {

    // Keys for so-key entries.
    private static final String KEY_PAGER_FIRST = "first";
    private static final String KEY_PAGER_PREVIOUS = "previous";
    private static final String KEY_PAGER_NEXT = "next";
    private static final String KEY_PAGER_LAST = "last";

    private PagerButton firstPage;
    private PagerButton previousPage;
    private PagerButton nextPage;
    private PagerButton lastPage;

    private FlowPanel component;

    public TablePagerField() {
        super();

        component = new FlowPanel();

        firstPage = new PagerButton(KEY_PAGER_FIRST, Resources.BUTTON_I18N.First());
        firstPage.addClickHandler((event) -> firstPage());

        previousPage = new PagerButton(KEY_PAGER_PREVIOUS, Resources.BUTTON_I18N.Previous());
        previousPage.addClickHandler((event) -> previousPage());

        nextPage = new PagerButton(KEY_PAGER_NEXT, Resources.BUTTON_I18N.Next());
        nextPage.addClickHandler((event) -> nextPage());

        lastPage = new PagerButton(KEY_PAGER_LAST, Resources.BUTTON_I18N.Last());
        lastPage.addClickHandler((event) -> lastPage());

        component.add(lastPage);
        component.add(nextPage);
        component.add(previousPage);
        component.add(firstPage);

        initWidget(component);
    }

    @Override
    protected void onRangeOrRowCountChanged() {
        HasRows display = getDisplay();
        setPrevPageButtonsDisabled(!hasPreviousPage());
        if (isRangeLimited() || !display.isRowCountExact()) {
            setNextPageButtonsDisabled(!hasNextPage());
        }

        int currentPage = getPage();

        int startPage = currentPage - 2;
        int endPage = currentPage + 2;

        if (startPage < 0) {
            endPage += (startPage * -1);
            startPage = 0;
        }

        if (endPage > getPageCount()-1) {
            startPage -= endPage - (getPageCount()-1);
            endPage = getPageCount()-1;

            if (startPage < 0) {
                startPage = 0;
            }
        }

        for (int index = component.getWidgetCount() - 1; index >= 0; index--) {
            PagerButton pagerButton = ((PagerButton)component.getWidget(index));
            if (pagerButton.getKey().startsWith("page")) {
                pagerButton.removeFromParent();
            }
        }


        for (int index = endPage; index >= startPage; index--) {
            final int page = index;
            final PagerButton pagerButton = new PagerButton("page-" + index + 1, String.valueOf(index + 1));
            pagerButton.addClickHandler((event) -> setPage(page));
            if (currentPage == index) {
                pagerButton.setSelected(true);
            }

            component.insert(pagerButton, component.getWidgetIndex(previousPage));
        }

    }

    /**
     * Enable or disable the next page buttons.
     *
     * @param disabled true to disable, false to enable
     */
    private void setNextPageButtonsDisabled(boolean disabled) {
        nextPage.setEnabled(!disabled);
        if (lastPage != null) {
            lastPage.setEnabled(!disabled);
        }
    }

    /**
     * Enable or disable the previous page buttons.
     *
     * @param disabled true to disable, false to enable
     */
    private void setPrevPageButtonsDisabled(boolean disabled) {
        if (firstPage != null) {
            firstPage.setEnabled(!disabled);
        }
        previousPage.setEnabled(!disabled);
    }

    private class PagerButton extends SimpleButton {

        // CSS style classes.
        public static final String STYLE_PAGER_BUTTON = "pager-button";

        // Element attribute names.
        public final static String ATTR_SELECTED = "so-selected";

        public PagerButton(String key, String html) {
            super(key, html, Style.PAGING);
            addStyleName(STYLE_PAGER_BUTTON);

            setSelected(false);
        }

        public void setSelected(boolean visible) {
            getElement().setAttribute(ATTR_SELECTED, visible ? TRUE : FALSE);
        }

        public boolean isSelected() {
            return !FALSE.equals(getElement().getAttribute(ATTR_SELECTED));
        }

    }
}
