package nl.sodeso.gwt.ui.client.form.button;

import com.google.gwt.event.dom.client.ClickHandler;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.gwt.ui.client.resources.Resources;

/**
 * Button representing an upload to the cloud button.
 *
 * @author Ronald Mathies
 */
public class UploadCloudButton {

    // Keys for so-key entries.
    public static final String KEY = "upload";

    public static class WithIcon extends SimpleButton {

        public WithIcon() {
            this(null);
        }

        public WithIcon(ClickHandler handler) {
            super(KEY, Icon.CloudUpload, Style.BLUE);

            if (handler != null) {
                addClickHandler(handler);
            }
        }
    }

}
