package nl.sodeso.gwt.ui.client.controllers.notification;

import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.HeadingElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;

import java.util.Date;

import static nl.sodeso.gwt.ui.client.util.Constants.FALSE;
import static nl.sodeso.gwt.ui.client.util.Constants.TRUE;

/**
 * @author Ronald Mathies
 */
public class Notification extends FlowPanel {

    // CSS style classes.
    private static final String STYLE_NOTIFICATION = "notification";
    private static final String STYLE_NOTIFICATION_ICON = "icon";
    private static final String STYLE_NOTIFICATION_TITLE = "title";
    private static final String STYLE_NOTIFICATION_MESSAGE = "message";
    private static final String STYLE_NOTIFICATION_CLOSE = "close";


    private static final String STYLE_INFO = "info";
    private static final String STYLE_INFO_ICON = "fa fa-info";
    private static final String STYLE_SUCCESS = "success";
    private static final String STYLE_SUCCESS_ICON = "fa fa-check";
    private static final String STYLE_ERROR = "error";
    private static final String STYLE_ERROR_ICON = "fa fa-warning";
    private static final String STYLE_CLOSE_ICON = "fa fa-times";

    // Element attribute names.
    private static final String ATTR_VISIBLE = "so-visible";

    public enum Style {
        INFO(STYLE_INFO, STYLE_INFO_ICON, SimpleButton.Style.BLUE),
        SUCCESS(STYLE_SUCCESS, STYLE_SUCCESS_ICON, SimpleButton.Style.GREEN),
        ERROR(STYLE_ERROR, STYLE_ERROR_ICON, SimpleButton.Style.RED);

        String notificationStyle;
        String iconStyle;
        SimpleButton.Style buttonStyle;

        Style(String notificationStyle, String iconStyle, SimpleButton.Style buttonStyle) {
            this.notificationStyle = notificationStyle;
            this.iconStyle = iconStyle;
            this.buttonStyle = buttonStyle;
        }

        public String getNotificationStyle() {
            return notificationStyle;
        }

        public String getIconStyle() {
            return iconStyle;
        }

        public SimpleButton.Style getButtonStyle() {
            return buttonStyle;
        }
    }

    private Style style = null;
    private String title = null;
    private String message = null;
    private Date time;

    private DivElement iconElement = null;
    private HeadingElement titleElement = null;
    private DivElement messageElement = null;

    private Timer hideNotificationTimer = null;

    private FocusPanel pauseNotification;
    private FocusPanel closeElement;

    private boolean autohide;

    public Notification(Style style, String title, String message) {
        this(style, title, message, true);
    }

    public Notification(Style style, String title, String message, final boolean autohide) {
        super();

        this.style = style;
        this.title = title;
        this.message = message;
        this.time = new Date();

        this.autohide = autohide;

        setStyleName(STYLE_NOTIFICATION);
        addStyleName(STYLE_NOTIFICATION + "-"+ style.getNotificationStyle());

        // Icon
        iconElement = Document.get().createDivElement();
        iconElement.setClassName(STYLE_NOTIFICATION_ICON);
        SpanElement spanElement = Document.get().createSpanElement();
        spanElement.setClassName(style.getIconStyle());
        iconElement.appendChild(spanElement);
        getElement().appendChild(iconElement);

        // Close icon
        closeElement = new FocusPanel();
        closeElement.addClickHandler(event -> {
            if (autohide && hideNotificationTimer.isRunning()) {
                hideNotificationTimer.cancel();
            }

            removeNotification();
        });
        closeElement.setStyleName(STYLE_NOTIFICATION_CLOSE);
        spanElement = Document.get().createSpanElement();
        spanElement.setClassName(STYLE_CLOSE_ICON);
        closeElement.getElement().appendChild(spanElement);
        add(closeElement);

        // Title
        titleElement = Document.get().createHElement(4);
        titleElement.setClassName(STYLE_NOTIFICATION_TITLE);
        getElement().appendChild(titleElement);
        titleElement.setInnerText(title);

        // Message
        if (message != null && !message.isEmpty()) {
            messageElement = Document.get().createDivElement();
            messageElement.setClassName(STYLE_NOTIFICATION_MESSAGE);
            getElement().appendChild(messageElement);
            messageElement.setInnerHTML(message);
        }

        setVisible(false);
    }

    public void startAutohideTimer() {
        if (autohide) {
            hideNotificationTimer = new Timer() {
                @Override
                public void run() {
                    removeNotification();
                }
            };
            hideNotificationTimer.schedule(5000);
        }
    }

    public Style getStyle() {
        return this.style;
    }

    public String getTitle() {
        return this.title;
    }

    public String getMessage() {
        return this.message;
    }

    public Date getTime() {
        return this.time;
    }

    public void setVisible(boolean visible) {
        getElement().setAttribute(ATTR_VISIBLE, visible ? TRUE : FALSE);
    }

    public boolean isVisible() {
        String value = getElement().getAttribute(ATTR_VISIBLE);
        return value == null || value.equals(TRUE);
    }

    private void removeNotification() {
        setVisible(false);

        Timer removeTimer = new Timer() {
            @Override
            public void run() {
                removeFromParent();
            }
        };
        removeTimer.schedule(500);
    }
}
