package nl.sodeso.gwt.ui.server.endpoint.properties;

import com.google.gwt.user.server.rpc.XsrfProtectedServiceServlet;
import nl.sodeso.gwt.ui.client.application.properties.ClientApplicationProperties;
import nl.sodeso.gwt.ui.client.application.properties.rpc.ApplicationPropertiesRpc;
import nl.sodeso.gwt.ui.server.util.ServletContextInfo;

import javax.servlet.ServletContext;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public abstract class AbstractApplicationPropertiesEndpoint<C extends ClientApplicationProperties, S extends ServerApplicationProperties> extends XsrfProtectedServiceServlet implements ApplicationPropertiesRpc {

    private static final Logger log = Logger.getLogger(AbstractApplicationPropertiesEndpoint.class.getName());

    public C getProperties() {
        C clientAppProps = newClientAppPropertiesInstance();
        if (clientAppProps != null) {

            // Fill in the default properties.
            S serverAppProps = getServerAppProperties();
            clientAppProps.setVersion(serverAppProps.getVersion());
            clientAppProps.setDevkitEnabled(serverAppProps.isDevkitEnabled());
            clientAppProps.setWebsocketEnabled(serverAppProps.isWebsocketEnabled());
            clientAppProps.setSecurityEnabled(serverAppProps.isSecurityEnabled());
            clientAppProps.setAnonymousUsername(serverAppProps.getAnonymousUsername());
            clientAppProps.setContextPath(ServletContextInfo.instance().getContextPath());

            fillApplicationProperties(serverAppProps, clientAppProps);
        }

        return clientAppProps;
    }

    /**
     * Fill in additional properties into the client properties.
     * @param serverApplicationProperties the server side application properties.
     * @param clientApplicationProperties the client side application properties.
     */
    public abstract void fillApplicationProperties(S serverApplicationProperties, C clientApplicationProperties);

    /**
     * Should return the server side applicaton properties.
     * @return server side application properties.
     */
    public abstract S getServerAppProperties();

    /**
     * Constructs a new client side properties class instance.
     * @return a new client side properties class instance.
     */
    private C newClientAppPropertiesInstance() {
        try {
            return getClientAppPropertiesClass().newInstance();
        } catch (IllegalAccessException | InstantiationException illegalAccessException) {
            log.log(Level.SEVERE, "Failed to instantiate application properties class.", illegalAccessException);
        }

        return null;
    }

    /**
     * Should return the class definition for the client side application properties.
     * @return the class definition for the client side application properties.
     */
    public abstract Class<C> getClientAppPropertiesClass();
}
