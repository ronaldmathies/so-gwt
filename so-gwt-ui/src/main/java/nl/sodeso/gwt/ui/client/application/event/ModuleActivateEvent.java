package nl.sodeso.gwt.ui.client.application.event;

import com.google.web.bindery.event.shared.Event;
import nl.sodeso.gwt.ui.client.application.module.ModuleEntryPoint;


/**
 * Event which is used when a module is about to be activated within an application. This event will be fired
 * every time when a module is activated (for example after a suspension of a module).
 *
 * To receive these events simple register a {@link ModuleActivateEventHandler} using the {@link nl.sodeso.gwt.ui.client.application.GwtApplicationEntryPoint#addModuleActivateEventHandler(ModuleActivateEventHandler)}
 *
 * NOTE: this event will also be fired before a modules {@link ModuleEntryPoint#initModule(ModuleEntryPoint.InitFinishedTrigger)} has been called.
 *
 * @author Ronald Mathies
 */
public class ModuleActivateEvent extends Event<ModuleActivateEventHandler> {

    public static final Type<ModuleActivateEventHandler> TYPE =
            new Type<>();

    private ModuleEntryPoint module = null;

    /**
     * Constrcuts a new event with the specified module.
     *
     * @param module the module that is activated.
     */
    public ModuleActivateEvent(ModuleEntryPoint module) {
        this.module = module;
    }

    /**
     * Returns the associated handler type.
     *
     * @return the associated handler type.
     */
    @Override
    public Type<ModuleActivateEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(ModuleActivateEventHandler handler) {
        handler.onEvent(this);
    }

    /**
     * Returns the module that is going to be activated.
     *
     * @return the module.
     */
    public ModuleEntryPoint getModule() {
        return this.module;
    }

}
