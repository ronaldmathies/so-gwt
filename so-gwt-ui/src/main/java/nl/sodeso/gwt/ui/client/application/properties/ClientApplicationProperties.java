package nl.sodeso.gwt.ui.client.application.properties;

import nl.sodeso.gwt.ui.server.endpoint.properties.AbstractApplicationPropertiesEndpoint;

import java.io.Serializable;

/**
 * Container for holding information about the application in general, no user information
 * should reside in this class. Sub-class this container to add extra properties.
 *
 * @see AbstractApplicationPropertiesEndpoint
 *
 * @author Ronald Mathies
 */
public class ClientApplicationProperties implements Serializable {

    private String name;
    private String contextPath = "";
    private String version;
    private boolean devkitEnabled;
    private boolean securityEnabled;
    private String anonymousUsername;
    private boolean websocketEnabled;

    public ClientApplicationProperties() {
    }

    /**
     * Construct a new ApplicationProperties with the given name.
     *
     * @param name the name of the application
     * @return this instance.
     */
    public ClientApplicationProperties setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * Returns the name of the application, this is based on the
     * contents of the <code>application.properties</code> file.
     *
     * @return the name of the application.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets the application context path.
     *
     * @param contextPath the application context path.
     */
    public void setContextPath(String contextPath) {
        if (contextPath != null) {
            this.contextPath = contextPath;
        }
    }

    /**
     * Returns the application context root
     *
     * @return the application context root or "" when none is set.
     */
    public String getContextPath() {
        return this.contextPath;
    }

    /**
     * Returns the version number of the application, this is based on the
     * contents of the <code>application.properties</code> file.
     *
     * @return the version of the application.
     */
    public String getVersion() {
        return version;
    }

    /**
     * Returns the indication if the Devkit should be enabled or not, this is based
     * on the build profile used to compile the application.
     *
     * profile:
     *
     * development: true
     * release: false
     *
     * @return true of the devkit is enabled, false if not.
     */
    public boolean isDevkitEnabled() {
        return devkitEnabled;
    }

    /**
     * Returns a flag indicating if the application has security enabled.
     * @return a flag indicating if the application has security enabled.
     */
    public boolean isSecurityEnabled() {
        return securityEnabled;
    }

    /**
     * Returns the anonymous username for when the security is disabled.
     * @return the anonymous username for when the security is disabled.
     */
    public String getAnonymousUsername() {
        return this.anonymousUsername;
    }

    /**
     * Returns a flag indicating if the application would like to use a WebSocket connection to
     * communicate with the server.
     *
     * @return true to enable websocket services, false to keep it disabled.
     */
    public boolean isWebsocketEnabled() {
        return websocketEnabled;
    }

    /**
     * Sets the version number of the application.
     *
     * By default this method will be filled by the default endpoint based on the contents
     * of the <code>application.properties</code>.
     *
     * @see AbstractApplicationPropertiesEndpoint
     *
     * @param version the version of the application.
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * Sets the devkit indication.
     *
     * By default this method will be filled by the default endpoint based on the contents
     * of the <code>application.properties</code>.
     *
     * The contents of the <code>application.properties</code> is based on the build profile used
     * to compile this application.
     *
     * profile:
     *
     * development: true
     * release: false
     *
     * @see AbstractApplicationPropertiesEndpoint
     *
     * @param devkitEnabled true of the devkit is enabled, false if not.
     */
    public void setDevkitEnabled(boolean devkitEnabled) {
        this.devkitEnabled = devkitEnabled;
    }

    /**
     * Sets a flag indicating if the application has security enabled.
     * @param securityEnabled true to enable security false, false if not.
     */
    public void setSecurityEnabled(boolean securityEnabled) {
        this.securityEnabled = securityEnabled;
    }

    /**
     * Sets the anonymous username for when security is disabled.
     * @param username the username.
     */
    public void setAnonymousUsername(String username) {
        this.anonymousUsername = username;
    }

    /**
     * Sets the websocket flag.
     *
     * @param websocketEnabled
     */
    public void setWebsocketEnabled(boolean websocketEnabled) {
        this.websocketEnabled = websocketEnabled;
    }
}
