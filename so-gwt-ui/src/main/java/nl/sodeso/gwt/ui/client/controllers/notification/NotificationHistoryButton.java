package nl.sodeso.gwt.ui.client.controllers.notification;

import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.LIElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import nl.sodeso.gwt.ui.client.controllers.navigation.NavigationButton;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.gwt.ui.client.util.DateFormatterUtil;

import java.util.List;

import static nl.sodeso.gwt.ui.client.util.Constants.FALSE;
import static nl.sodeso.gwt.ui.client.util.Constants.TRUE;

/**
 * @author Ronald Mathies
 */
public class NotificationHistoryButton extends NavigationButton {

    // Keys for so-key entries.
    private static final String KEY_HISTORY = "history";

    // CSS style classes.
    private static final String STYLE_HISTORY_POPUP = "history-popup";
    private static final String STYLE_HISTORY_CONTENTS = "contents";
    private static final String STYLE_HISTORY_NO_NOTIFICATIONS = "no-notifications";
    private static final String STYLE_HISTORY_ICON = "icon";
    private static final String STYLE_HISTORY_TITLE = "title";
    private static final String STYLE_HISTORY_TIME = "time";
    private static final String STYLE_HISTORY_MESSAGE = "message";

    // Element attribute names.
    private static final String ATTR_VISIBLE = "so-visible";

    public NotificationHistoryButton() {
        super(KEY_HISTORY, Icon.Bell);
        addClickHandler((event) -> showNotificationHistory());
    }

    public void showNotificationHistory() {
        final PopupPanel listFieldPopup = new PopupPanel(true, false);
        listFieldPopup.addStyleName(STYLE_HISTORY_POPUP);

        FlowPanel contents = new FlowPanel();
        contents.addStyleName(STYLE_HISTORY_CONTENTS);
        listFieldPopup.add(contents);
        List<Notification> notifications = NotificationController.instance().getNotifications();

        if (notifications.isEmpty()) {
            LIElement liElement = Document.get().createLIElement();

            SpanElement titleElement = Document.get().createSpanElement();
            titleElement.setClassName(STYLE_HISTORY_NO_NOTIFICATIONS);
            titleElement.setInnerText("No notifications");
            liElement.appendChild(titleElement);

            contents.getElement().appendChild(liElement);
        }

        for (final Notification notification : notifications) {
            final LIElement liElement = Document.get().createLIElement();

            SpanElement timeElement = Document.get().createSpanElement();
            timeElement.setClassName(STYLE_HISTORY_TIME);
            timeElement.setInnerText(DateFormatterUtil.formatTimePassed(notification.getTime()));
            liElement.appendChild(timeElement);

            SpanElement iconElement = Document.get().createSpanElement();
            iconElement.setClassName(STYLE_HISTORY_ICON);
            iconElement.addClassName(notification.getStyle().getIconStyle());
            liElement.appendChild(iconElement);

            SpanElement titleElement = Document.get().createSpanElement();
            titleElement.setClassName(STYLE_HISTORY_TITLE);
            titleElement.setInnerText(notification.getTitle());
            liElement.appendChild(titleElement);

            final LIElement liElementMessage = Document.get().createLIElement();
            liElementMessage.setClassName(STYLE_HISTORY_MESSAGE);
            contents.getElement().appendChild(liElement);

            DivElement messageElement = Document.get().createDivElement();
            messageElement.setInnerText(notification.getMessage());
            liElementMessage.appendChild(messageElement);
            contents.getElement().appendChild(liElementMessage);

            Event.sinkEvents(liElement, Event.ONCLICK);
            Event.sinkEvents(liElementMessage, Event.ONCLICK);

            EventListener clickListener = event -> {
                if (Event.ONCLICK == event.getTypeInt()) {
                    setMessageVisible(liElementMessage, !isMessageVisible(liElementMessage));
                }
            };

            Event.setEventListener(liElement, clickListener);
            Event.setEventListener(liElementMessage, clickListener);

        }

        listFieldPopup.showRelativeTo(this);
    }

    public void setMessageVisible(LIElement li, boolean visible) {
        li.setAttribute(ATTR_VISIBLE, visible ? TRUE : FALSE);
    }

    public boolean isMessageVisible(LIElement li) {
        return !FALSE.equals(li.getAttribute(ATTR_VISIBLE));
    }

}