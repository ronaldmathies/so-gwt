package nl.sodeso.gwt.ui.client.form.validation;

/**
 * @author Ronald Mathies
 */
public interface ValidationCompletedHandler {

    void completed(ValidationResult result);

}
