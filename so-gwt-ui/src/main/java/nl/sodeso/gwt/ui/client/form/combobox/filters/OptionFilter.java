package nl.sodeso.gwt.ui.client.form.combobox.filters;

import nl.sodeso.gwt.ui.client.form.combobox.Option;

/**
 * @author Ronald Mathies
 */
public interface OptionFilter<T extends Option> {

    boolean isOptionAllowed(T option);

}
