package nl.sodeso.gwt.ui.client.dialog;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.button.CancelButton;
import nl.sodeso.gwt.ui.client.form.button.YesButton;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.util.Align;

/**
 * TODO: Optioneel maken sluiten
 * TODO: Bepalen welke knoppen tonen.
 *
 * @author Ronald Mathies
 */
public class InfoQuestionPanel extends PopupWindowPanel {

    private InfoQuestionHandler infoQuestionHandler;

    public InfoQuestionPanel(final String title, final String question, final InfoQuestionHandler infoQuestionHandler) {
        super("info-confirmation", title, Style.INFO);

        this.infoQuestionHandler = infoQuestionHandler;

        addToBody(
            new EntryForm(null)
                .addEntry(new EntryWithDocumentation(null, question)));

        addToFooter(Align.RIGHT,
                new YesButton.WithLabel((event) -> yes()),
                new CancelButton.WithLabel((event) -> close()));
    }

    private void yes() {
        infoQuestionHandler.confirm();
    }

}
