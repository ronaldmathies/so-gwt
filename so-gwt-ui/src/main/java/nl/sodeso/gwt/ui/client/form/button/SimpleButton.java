package nl.sodeso.gwt.ui.client.form.button;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.Button;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.util.HasKey;
import nl.sodeso.gwt.ui.client.util.StringUtils;

import static nl.sodeso.gwt.ui.client.util.Constants.FALSE;
import static nl.sodeso.gwt.ui.client.util.Constants.TRUE;

/**
 * @author Ronald Mathies
 */
public class SimpleButton extends Button implements HasKey {

    private static final String KEY_BUTTON_SUFFIX = "-button";

    // CSS style classes.
    private static final String STYLE_BUTTON_ICON_LEFT = "button-icon-left";
    private static final String STYLE_BUTTON_ICON_RIGHT = "button-icon-right";

    // Element attribute names.
    private static final String ATTR_VISIBLE = "so-visible";

    private Icon icon = null;
    private Align iconAlignment = null;

    private Element iconElement = null;

    public enum Style {
        NONE("none"),
        GRAY("gray"),
        RED("red"),
        BLUE("blue"),
        GREEN("green"),
        BLACK("black"),
        ORANGE("orange"),
        PAGING("paging"),
        NAVIGATION("navigation");

        String style = null;

        Style(String style) {
            this.style = style;
        }

        public String getStyle() {
            return style;
        }
    }

    /**
     * Constructs a new button.
     *
     * @param key the key of the button.
     * @param title the title of the button.
     */
    public SimpleButton(String key, String title) {
        this(key, title, Style.GRAY);
    }

    /**
     * Constructs a new button.
     *
     * @param key the key of the button.
     * @param icon the icon of the button.
     */
    public SimpleButton(String key, Icon icon) {
        this(key, icon, Style.GRAY);
    }

    /**
     * Constructs a new button.
     *
     * @param key the key of the button.
     * @param title the title of the button.
     * @param style the color of the button.
     */
    public SimpleButton(String key, String title, Style style) {
        this(key, title, null, style, Align.LEFT);
    }

    /**
     * Constructs a new button.
     *
     * @param key the key of the button.
     * @param icon the icon of the button.
     * @param style the color of the button.
     */
    public SimpleButton(String key, Icon icon, Style style) {
        this(key, null, icon, style, Align.LEFT);
    }

    /**
     * Constructs a new button.
     *
     * @param key the key of the button.
     * @param title the title of the button.
     * @param icon the icon of the button.
     * @param style the color of the button.
     * @param iconAlignment the alignment of the icon relative to the title.
     */
    public SimpleButton(String key, String title, Icon icon, Style style, Align iconAlignment) {
        super(title);

        if (style != null) {
            addStyleName(style.getStyle());
        }

        setIcon(icon, iconAlignment);
        setKey(key);
    }

    @Override
    public void setTitle(String title) {
        super.setTitle(title);
        setIcon(this.icon, this.iconAlignment);
    }

    @Override
    public void setText(String text) {
        super.setText(text);
        setIcon(this.icon, this.iconAlignment);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_BUTTON_SUFFIX;
    }

    /**
     * Returns the icon this button represents.
     * @return the icon this button represents.
     */
    public Icon getIcon() {
        return this.icon;
    }

    /**
     * Sets the icon that this button must represent.
     * @param icon the icon that this button must represent.
     */
    public void setIcon(Icon icon, Align iconAlignment) {
        this.icon = icon;
        this.iconAlignment = iconAlignment;

        if (iconElement != null) {
            iconElement.removeFromParent();
        }

        if (this.icon != null) {
            iconElement = Document.get().createElement("I");
            iconElement.addClassName(icon.getStyle());

            if (getText() != null && !getText().isEmpty()) {
                if (iconAlignment.equals(Align.LEFT)) {
                    this.iconElement.addClassName(STYLE_BUTTON_ICON_LEFT);
                    this.getElement().insertFirst(iconElement);
                } else if (iconAlignment.equals(Align.RIGHT)) {
                    this.iconElement.addClassName(STYLE_BUTTON_ICON_RIGHT);
                    this.getElement().appendChild(iconElement);
                }
            } else {
                this.getElement().appendChild(iconElement);
            }
        }
    }

    /**
     * Sets this button to be visible or not.
     *
     * @param visible flag indicating if this button should be visible or not.
     */
    public void setVisible(boolean visible) {
        getElement().setAttribute(ATTR_VISIBLE, visible ? TRUE : FALSE);
    }

    /**
     * Returns the visibility state of this button.
     *
     * @return flag indicating if this button is visible or not.
     */
    public boolean isVisible() {
        return !FALSE.equals(getElement().getAttribute(ATTR_VISIBLE));
    }
}
