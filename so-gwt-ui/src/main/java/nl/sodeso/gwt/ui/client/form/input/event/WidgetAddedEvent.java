package nl.sodeso.gwt.ui.client.form.input.event;

import com.google.web.bindery.event.shared.Event;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionFieldWidget;

/**
 * @author Ronald Mathies
 */
public class WidgetAddedEvent extends Event<WidgetAddedEventHandler> {

    public static final Type<WidgetAddedEventHandler> TYPE = new Type<>();

    private WidgetCollectionField source;
    private WidgetCollectionFieldWidget widget;

    public WidgetAddedEvent(WidgetCollectionField source, WidgetCollectionFieldWidget widget) {
        this.source = source;
        this.widget = widget;
    }

    @Override
    public Type<WidgetAddedEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(WidgetAddedEventHandler handler) {
        handler.onEvent(this);
    }

    public WidgetCollectionField getSource() {
        return this.source;
    }

    public WidgetCollectionFieldWidget getWidget() {
        return this.widget;
    }

}