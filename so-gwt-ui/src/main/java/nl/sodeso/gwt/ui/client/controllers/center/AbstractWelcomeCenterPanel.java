package nl.sodeso.gwt.ui.client.controllers.center;

import nl.sodeso.gwt.ui.client.controllers.menu.MenuController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;
import nl.sodeso.gwt.ui.client.trigger.Trigger;

/**
 * Abstract welcome panel which can be used to welcome a new user.
 *
 * @author Ronald Mathies
 */
public abstract class AbstractWelcomeCenterPanel extends AbstractCenterPanel {

    private boolean isFullwithBeforeAdd = false;

    public final void beforeAdd(Trigger trigger) {
        MenuController instance = MenuController.instance();
        isFullwithBeforeAdd = instance.isFullwidth();

        instance.setFullwidth(false);

        beforeAddWelcomePanel(trigger::fire);
    }

    public abstract void beforeAddWelcomePanel(Trigger trigger);

    protected void backToActiveMenuItem() {
        MenuController.instance().setFullwidth(isFullwithBeforeAdd);

        MenuItem activeMenuItem = MenuController.instance().getActiveMenuItem();
        if (activeMenuItem != null) {
            MenuController.instance().deselectActiveMenuItem();
            activeMenuItem.click(null);
        } else {
            CenterController.instance().removeWidget();
        }
    }

}
