package nl.sodeso.gwt.ui.client.application.module.properties.rpc;

import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.gwt.ui.client.application.module.properties.ClientModuleProperties;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
public interface ModulePropertiesRpc extends XsrfProtectedService {

    ClientModuleProperties getProperties();

}
