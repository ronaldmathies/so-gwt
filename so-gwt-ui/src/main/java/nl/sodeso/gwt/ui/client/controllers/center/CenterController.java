package nl.sodeso.gwt.ui.client.controllers.center;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.application.GwtApplicationEntryPoint;
import nl.sodeso.gwt.ui.client.application.event.ModuleActivateEvent;
import nl.sodeso.gwt.ui.client.application.event.ModuleActivateEventHandler;
import nl.sodeso.gwt.ui.client.application.event.ModuleSuspendEvent;
import nl.sodeso.gwt.ui.client.application.event.ModuleSuspendEventHandler;
import nl.sodeso.gwt.ui.client.application.module.ModuleEntryPoint;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuController;
import nl.sodeso.gwt.ui.client.trigger.Trigger;

import java.util.HashMap;
import java.util.Map;

import static nl.sodeso.gwt.ui.client.util.Constants.FALSE;
import static nl.sodeso.gwt.ui.client.util.Constants.TRUE;

/**
 * @author Ronald Mathies
 */
public class CenterController extends FlowPanel implements ModuleActivateEventHandler, ModuleSuspendEventHandler {

    // CSS style classes.
    private static final String STYLE_CENTER_PANEL = "center-panel";

    // Element attribute names.
    private static final String ATTR_FULL_WIDTH = "so-full-width";

    private static CenterController instance;

    private Map<String, Center> centerpanels = new HashMap<>();

    private CenterController() {
        addStyleName(STYLE_CENTER_PANEL);
        setFullWidth(false);

        GwtApplicationEntryPoint.instance().addModuleActivateEventHandler(this);
        GwtApplicationEntryPoint.instance().addModuleSuspendEventHandler(this);
    }

    /**
     * Returns the singleton instance of the <code>CenterController</code>.
     *
     * @return the singleton instance.
     */
    public static CenterController instance() {
        if (instance == null) {
            instance = new CenterController();

            MenuController.instance().addMenuVisibilityChangedEventHandler(event -> CenterController.instance().setFullWidth(!event.isVisible()));
        }

        return instance;
    }

    /**
     * Returns the height of the center panel.
     * @return the height of the center panel.
     */
    public int getHeight() {
        return getOffsetHeight();
    }

    /**
     * Returns the width of the center panel.
     * @return the width of the center panel.
     */
    public int getWidth() {
        return getOffsetWidth();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onEvent(ModuleActivateEvent event) {
        // Check if the module has been activated before, if not, create the menu.
        if (!centerpanels.containsKey(event.getModule().getModuleId())) {
            centerpanels.put(event.getModule().getModuleId(), new Center(null));
        }

        Center active = centerpanels.get(event.getModule().getModuleId());

        // Restore the width of the center panel based on the full width
        // of the menu, menu is leading.
        setFullWidth(active.isFullwidth());

        if (active.getCenterPanel() != null) {
            add(active.getCenterPanel());
        }
    }

    /**
     * {@inheritDoc}
     */
    public void onEvent(ModuleSuspendEvent event) {
        Center active = centerpanels.get(event.getModule().getModuleId());
        if (active.getCenterPanel() != null) {
            remove(active.getCenterPanel());
        }
    }

    /**
     * Removes the center panel widget through the normal flow.
     */
    public void removeWidget() {
        setWidget(null);
    }

    /**
     * Set the provided <code>item</code> as the center panel's widget. Any existing
     * widget residing on the center panel will first be removed.
     *
     * @param widget widget to be displayed.
     */
    public void setWidget(final AbstractCenterPanel widget) {
        ModuleEntryPoint currentModule = GwtApplicationEntryPoint.instance().getCurrentActiveModule();
        centerpanels.put(currentModule.getModuleId(), new Center(widget));

        final Trigger addNewWidgetTrigger = new Trigger() {
            @Override
            public void fire() {
                widget.beforeAdd(new Trigger() {
                    @Override
                    public void fire() {
                        add((Widget) widget);
                        widget.afterAdd(new Trigger() {
                                @Override
                                public void fire() {
                                    Window.scrollTo(0, 0);
                                }
                            });
                    }
                });

            }
        };

        final boolean widgetAdded[] = { false };
        final int index[] = {0};
        for (; index[0] < getWidgetCount(); index[0]++) {
            widgetAdded[0] = true;

            final AbstractCenterPanel oldWidget = (AbstractCenterPanel)getWidget(index[0]);

            oldWidget.beforeRemoval(new Trigger() {

                @Override
                public void fire() {
                    if (remove((Widget)oldWidget)) {
                        oldWidget.afterRemoval(new Trigger() {
                            @Override
                            public void fire() {
                                if (index[0] == getWidgetCount()) {

                                    // When there is a widget we can add it, otherwide we leaf the center panel empty.
                                    if (widget != null) {
                                        addNewWidgetTrigger.fire();
                                    }
                                }
                            }
                        });
                    }
                }

            });
        }

        if (!widgetAdded[0]) {
            addNewWidgetTrigger.fire();
        }
    }

    /**
     * Changes the center panel to occupy the whole width of the screen (menu should not be visible for this).
     * @param fullwidth true or false to occupy the whole width of the screen.
     */
    private void setFullWidth(boolean fullwidth) {
        ModuleEntryPoint currentModule = GwtApplicationEntryPoint.instance().getCurrentActiveModule();
        if (currentModule != null) {
            Center center = centerpanels.get(currentModule.getModuleId());
            if (center != null) {
                center.setFullwidth(fullwidth);
            }
        }

        getElement().setAttribute(ATTR_FULL_WIDTH, fullwidth ? TRUE : FALSE);
    }

    /**
     * Flag indicating if the center panel is occupying the whole with of the screen.
     * @return true or false if the center panel is occupying the whole with of the screen.
     */
    private boolean isFullWidth() {
        return !FALSE.equals(getElement().getAttribute(ATTR_FULL_WIDTH));
    }
}
