package nl.sodeso.gwt.ui.client.controllers.notification.events;

import com.google.web.bindery.event.shared.Event;
import nl.sodeso.gwt.ui.client.controllers.notification.Notification;

/**
 * @author Ronald Mathies
 */
public class CreateNotificationEvent extends Event<CreateNotificationEventHandler> {

    public static final Type<CreateNotificationEventHandler> TYPE =
            new Type<>();

    private Notification.Style style;
    private String title;
    private String message;
    private boolean autohide;

    public CreateNotificationEvent(Notification.Style style, String title) {
        this(style, title, null, true);
    }

    public CreateNotificationEvent(Notification.Style style, String title, String message) {
        this(style, title, message, true);
    }

    public CreateNotificationEvent(Notification.Style style, String title, String message, boolean autohide) {
        this.style = style;
        this.title = title;
        this.message = message;
        this.autohide = autohide;
    }

    @Override
    public Type<CreateNotificationEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(CreateNotificationEventHandler handler) {
        handler.onEvent(this);
    }

    public Notification.Style getStyle() {
        return style;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public boolean isAutohide() {
        return this.autohide;
    }
}
