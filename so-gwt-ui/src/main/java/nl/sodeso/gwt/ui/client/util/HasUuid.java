package nl.sodeso.gwt.ui.client.util;

/**
 * @author Ronald Mathies
 */
public interface HasUuid {

    String getUuid();

}
