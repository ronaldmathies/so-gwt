package nl.sodeso.gwt.ui.client.controllers.header;

import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import nl.sodeso.gwt.ui.client.application.GwtApplicationEntryPoint;
import nl.sodeso.gwt.ui.client.application.event.ModuleActivateEvent;
import nl.sodeso.gwt.ui.client.application.event.ModuleActivateEventHandler;
import nl.sodeso.gwt.ui.client.application.module.ModuleEntryPoint;
import nl.sodeso.gwt.ui.client.application.properties.ClientApplicationProperties;
import nl.sodeso.gwt.ui.client.controllers.session.SessionController;
import nl.sodeso.gwt.ui.client.controllers.session.event.UserLoginEvent;
import nl.sodeso.gwt.ui.client.controllers.session.event.UserLoginEventHandler;
import nl.sodeso.gwt.ui.client.controllers.session.event.UserLogoutEvent;
import nl.sodeso.gwt.ui.client.controllers.session.event.UserLogoutEventHandler;
import nl.sodeso.gwt.ui.client.form.button.LoggingButton;
import nl.sodeso.gwt.ui.client.form.button.LoginButton;
import nl.sodeso.gwt.ui.client.form.button.LogoutButton;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.util.HasKey;


/**
 * @author Ronald Mathies
 */
public class HeaderController extends FlowPanel implements UserLoginEventHandler, UserLogoutEventHandler, ModuleActivateEventHandler, HasKey {

    // Keys for so-key entries.
    private static final String KEY_HEADER_SUFFIX = "-header";
    private static final String KEY_HEADER_PANEL = "main";

    // CSS style classes.
    private static final String STYLE_HEADER_PANEL = "header-panel";
    private static final String STYLE_HEADER_PANEL_TITLE = "title";
    private static final String STYLE_HEADER_PANEL_SESSION = "session";
    private static final String STYLE_HEADER_PANEL_LOGGING = "logging";

    private static HeaderController instance;

    private Label applicationTitle;

    private LoginButton.WithLabel loginButton = null;
    private LogoutButton.WithLabel logoutButton = null;
    private SimpleButton loggingButton = null;

    private HeaderController() {
        addStyleName(STYLE_HEADER_PANEL);

        GwtApplicationEntryPoint.instance().addModuleActivateEventHandler(this);

        ClientApplicationProperties appProp = GwtApplicationEntryPoint.instance().getApplicationProperties();
        applicationTitle = new Label();
        applicationTitle.setStyleName(STYLE_HEADER_PANEL_TITLE);
        add(applicationTitle);

        applicationTitle.addClickHandler(event -> applicationTitleClicked());

        loginButton = new LoginButton.WithLabel();
        loginButton.addStyleName(STYLE_HEADER_PANEL_SESSION);
        loginButton.setVisible(false);
        add(loginButton);

        logoutButton = new LogoutButton.WithLabel((event) -> SessionController.instance().logout());
        logoutButton.addStyleName(STYLE_HEADER_PANEL_SESSION);
        logoutButton.setVisible(false);
        add(logoutButton);

        if (appProp.isDevkitEnabled()) {
            loggingButton = new LoggingButton();
            loggingButton.addStyleName(STYLE_HEADER_PANEL_LOGGING);
            add(loggingButton);
        }

        SessionController.instance().addUserLoginEventHandler(event -> {
            loginButton.setVisible(false);
            logoutButton.setVisible(true);
        });

        SessionController.instance().addUserLogoutEventHandler(event -> {
            loginButton.setVisible(true);
            logoutButton.setVisible(false);
        });

        setKey(KEY_HEADER_PANEL);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_HEADER_SUFFIX;
    }

    /**
     * Method for receiving {@link ModuleActivateEvent} events, which in turn will update
     * the title of the application to the title specified by the module that was activated.
     *
     * @param event the event.
     */
    @Override
    public void onEvent(ModuleActivateEvent event) {
        applicationTitle.setText(event.getModule().getModuleName());
    }

    private void applicationTitleClicked() {
        ModuleEntryPoint currentActiveModule = GwtApplicationEntryPoint.instance().getCurrentActiveModule();
        if (currentActiveModule != null) {
            History.newItem(currentActiveModule.getWelcomeToken());
        }
    }

    /**
     * Returns the singleton instance of the controller.
     *
     * @return the instance.
     */
    public static HeaderController instance() {
        if (instance == null) {
            instance = new HeaderController();
        }

        return instance;
    }

    /**
     * Method for receiving {@link UserLoginEvent} events, which in turn will change the
     * state of the login / logout buttons within the header.
     *
     * @param event the event.
     */
    @Override
    public void onEvent(UserLoginEvent event) {
        loginButton.setVisible(false);
        logoutButton.setVisible(true);
    }

    /**
     * Method for receiving {@link UserLogoutEvent} events, which in turn will change the
     * state of the login / logout buttons within the header.
     *
     * @param event the event.
     */
    @Override
    public void onEvent(UserLogoutEvent event) {
        loginButton.setVisible(true);
        logoutButton.setVisible(false);
    }

}
