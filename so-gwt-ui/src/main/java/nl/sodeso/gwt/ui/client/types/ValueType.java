package nl.sodeso.gwt.ui.client.types;

import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 * @author Ronald Mathies
 */
public abstract class ValueType<T> implements Serializable {

    private transient ArrayList<ValueChangedEventHandler<? extends ValueType>> handlers = new ArrayList<>();

    private T value = null;
    private T originalValue = null;

    /**
     * Constructs a new instance with an unspecified value.
     */
    public ValueType() {}

    /**
     * Constructs a new instance with the specified value.
     *
     * @param value the initial value.
     */
    public ValueType(T value) {
        this.value = value;
        this.originalValue = value;
    }

    /**
     * Adds a value changed event handler, these will be called when a value has been changed.
     * @param handler the handler to add.
     */
    public void addValueChangedEventHandler(ValueChangedEventHandler<? extends ValueType> handler) {
        this.handlers.add(handler);
    }

    /**
     * Fires an event to inform all interested parties about a value change.
     */
    @SuppressWarnings("unchecked")
    protected void fireEvent() {
        final ValueChangedEvent<StringType> event = new ValueChangedEvent(this);
        for (ValueChangedEventHandler handler : handlers) {
            handler.onValueCanged(event);
        }
    }

    /**
     * Returns the current value of the ValueType, to know if the value
     * has been changed simply use the isChanged method.
     *
     * @see #isChanged()
     *
     * @return the current value.
     */
    public T getValue() {
        return this.value;
    }

    /**
     * Returns a <code>String</code> representation of the value contained in the <code>ValueType</code>.
     * @return a <code>String</code> representation of the value.
     */
    public String getValueAsString() {
        if (this.value != null) {
            return this.value.toString();
        }

        return null;
    }

    /**
     * Sets the new value for this ValueType.
     * @param value the value to set.
     */
    public void setValue(T value) {
        boolean fireEvent = false;
        if (!Objects.equals(value, this.value)) {
            fireEvent = true;
        }

        this.value = value;

        if (fireEvent) {
            fireEvent();
        }
    }

    /**
     * Sets the value of this ValueType using a String representation. The String representation
     * will be parsed into the definitive value.
     *
     * @param value the value.
     */
    public abstract void setValueAsString(String value);

    /**
     * Returns the original value that was set at the original creation of this object.
     * @return the original value.
     */
    public T getOriginalValue() {
        return this.originalValue;
    }

    /**
     * Reverts the value back to it's original state when the ValueType was created.
     */
    public void revert() {
        if (isChanged()) {
            setValue(getOriginalValue());
        }
    }

    /**
     * Flag indicating if the value has changed.
     * @return true if the value has changed, false if not.
     */
    public boolean isChanged() {
        if (getOriginalValue() == null && getValue() == null) {
            return false;
        } else if (getOriginalValue() != null && getValue() == null) {
            return true;
        } else if (getOriginalValue() == null) {
            return true;
        }

        return !getOriginalValue().equals(getValue());
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValueType<T> that = (ValueType<T>) o;
        return Objects.equals(isChanged(), that.isChanged()) &&
                Objects.equals(originalValue, that.getOriginalValue()) &&
                Objects.equals(value, that.getValue());
    }

    /**
     * {@inheritDoc}
     */
    public int hashCode() {
        return Objects.hash(originalValue, value);
    }

}
