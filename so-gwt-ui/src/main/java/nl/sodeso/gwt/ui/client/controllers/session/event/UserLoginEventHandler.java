package nl.sodeso.gwt.ui.client.controllers.session.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * @author Ronald Mathies
 */
public interface UserLoginEventHandler extends EventHandler {

    void onEvent(UserLoginEvent event);

}
