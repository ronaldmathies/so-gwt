package nl.sodeso.gwt.ui.client.controllers.session.event;

import com.google.web.bindery.event.shared.Event;


/**
 * @author Ronald Mathies
 */
public class UserLogoutEvent extends Event<UserLogoutEventHandler> {

    public static final Type<UserLogoutEventHandler> TYPE = new Type<>();

    public UserLogoutEvent() {
    }

    @Override
    public Type<UserLogoutEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(UserLogoutEventHandler handler) {
        handler.onEvent(this);
    }

}
