package nl.sodeso.gwt.ui.server.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class RemoteLoggingServiceImpl extends com.google.gwt.logging.server.RemoteLoggingServiceImpl{

    private static final String INIT_PARAM = "module-name";

    private static final Logger log = Logger.getLogger(RemoteLoggingServiceImpl.class.getName());

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        String moduleName = config.getInitParameter(INIT_PARAM);
        if (moduleName == null || moduleName.isEmpty()) {
            log.log(Level.SEVERE, "No module name defined. Please set the module-name servlet init "
                    + "parameter to point to the GWT module.");
        } else {
            String path = config.getServletContext().getRealPath(
                    "/WEB-INF/deploy/" + moduleName + "/symbolMaps/");

            if (path != null) {
                setSymbolMapsDirectory(path);
            }
        } // end else.
    }

}
