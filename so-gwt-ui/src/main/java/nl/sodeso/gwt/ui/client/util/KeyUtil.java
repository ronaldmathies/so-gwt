package nl.sodeso.gwt.ui.client.util;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Ronald Mathies
 */
public class KeyUtil {

    private static final String ATTR_KEY = "so-key";

    /**
     * Sets a key attribute on the widget.
     *
     * @param widget the widget to add the key attribute to.
     * @param key the key value.
     */
    public static void setKey(Widget widget, String key, String keySuffix) {
        if (key != null) {
            setKey(widget.getElement(), key, keySuffix);
        }
    }

    /**
     * Sets a key attribute on the element.
     *
     * @param element the element to add the key attributer to.
     * @param key the key value.
     */
    public static void setKey(Element element, String key, String keySuffix) {
        if (key != null) {
            element.setAttribute(ATTR_KEY, keySuffix != null ? key + keySuffix : key);
        }
    }

    /**
     * Returns the key attribute value from the widget.
     *
     * @param widget the widget.
     * @return the key attribute value.
     */
    public static String getKey(Widget widget, String keySuffix) {
        return getKey(widget.getElement(), keySuffix);
    }

    /**
     * Returns the key attribute value from the element. If no key is reistered then it will return "[no-key]" as a value.
     * This value is defined as a constant on <code>Constants.NO_KEY</code>.
     *
     * @param element the element.
     * @return the key attribute value.
     */
    public static String getKey(Element element, String keySuffix) {
        String key = null;
        if (hasKey(element)) {
            key = element.getAttribute(ATTR_KEY);

            if (keySuffix != null) {
                key = key.substring(0, key.indexOf(keySuffix));
            }
        }

        return key;

    }

    /**
     * Flag indicating if the widget has the key attribute.
     *
     * @param widget the widget.
     * @return true if the widget has the key attribute, false if not.
     */
    public static boolean hasKey(Widget widget) {
        return hasKey(widget.getElement());
    }

    /**
     * Flag indicating if the widget has the key attribute.
     *
     * @param element the element.
     * @return true if the element has the key attribute false if not.
     */
    public static boolean hasKey(Element element) {
        return element.hasAttribute(ATTR_KEY);
    }
}
