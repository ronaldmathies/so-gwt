package nl.sodeso.gwt.ui.client.form.upload;

import com.google.gwt.i18n.client.Constants;

/**
 * @author Ronald Mathies
 */
public interface FileUploadConstants extends Constants {

    @Constants.DefaultStringValue("Bytes")
    String Bytes();

    @Constants.DefaultStringValue("KB")
    String Kilobytes();

    @Constants.DefaultStringValue("MB")
    String Megabytes();

    @Constants.DefaultStringValue("GB")
    String Gigabyte();

    @Constants.DefaultStringValue("Select file")
    String ChooseFile();

    @Constants.DefaultStringValue("Clear file")
    String ClearFile();

}
