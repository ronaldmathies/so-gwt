package nl.sodeso.gwt.ui.server.listener;

import nl.sodeso.gwt.ui.server.endpoint.properties.ApplicationPropertiesContainer;
import nl.sodeso.gwt.ui.server.endpoint.properties.ServerApplicationProperties;

import nl.sodeso.commons.web.initialization.AbstractWebInitContext;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public abstract class AbstractApplicationPropertiesInitializerContextListener<T extends ServerApplicationProperties> extends AbstractWebInitContext {

    private static final Logger log = Logger.getLogger(AbstractApplicationPropertiesInitializerContextListener.class.getName());

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(ServletContextEvent event) {
        log.log(Level.INFO, "Initializing properties {0}.");

        ApplicationPropertiesContainer.instance().setApplicationProperties(newServerAppPropertiesInstance());

        log.log(Level.INFO, "Finished initializating properties.");

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy(ServletContextEvent event) {
    }

    /**
     * Constructs a new server side application properties instance.
     * @return the new server side application properties instance.
     */
    private T newServerAppPropertiesInstance() {
        try {
            return (T) getServerAppPropertiesClass().getDeclaredConstructor().newInstance();
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException | NoSuchMethodException illegalAccessException) {
            log.log(Level.SEVERE, "Failed to instantiate application properties class.", illegalAccessException);
        }
        return null;
    }

    /**
     * Should return the class definition for the server side application properties.
     * @return the class definition for the server side application properties.
     */
    public abstract Class<T> getServerAppPropertiesClass();

}