package nl.sodeso.gwt.ui.client.form.button;

import com.google.gwt.event.dom.client.ClickHandler;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.gwt.ui.client.resources.Resources;
import nl.sodeso.gwt.ui.client.util.Align;

/**
 * Button representing an <code>Add</code> button.
 *
 * @author Ronald Mathies
 */
public class AddButton {

    // Keys for so-key entries.
    public static final String KEY = "add";

    public static class WithLabel extends SimpleButton {

        public WithLabel() {
            this(null);
        }

        public WithLabel(ClickHandler handler) {
            super(KEY, Resources.BUTTON_I18N.Add(), Style.BLUE);

            if (handler != null) {
                addClickHandler(handler);
            }
        }
    }

    public static class WithIcon extends SimpleButton {

        public WithIcon() {
            this(null);
        }

        public WithIcon(ClickHandler handler) {
            super(KEY, Icon.Add, Style.BLUE);

            if (handler != null) {
                addClickHandler(handler);
            }
        }
    }

    public static class WithIconAndLabel extends SimpleButton {

        public WithIconAndLabel() {
            this(null);
        }

        public WithIconAndLabel(ClickHandler handler) {
            super(KEY, Resources.BUTTON_I18N.Add(), Icon.Add, Style.BLUE, Align.LEFT);

            if (handler != null) {
                addClickHandler(handler);
            }
        }
    }

}
