package nl.sodeso.gwt.ui.client.rpc;

import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.button.OkButton;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.util.Align;

/**
 * @author Ronald Mathies
 */
public abstract class DefaultRunAsyncCallback implements RunAsyncCallback {

    /**
     * When a failure occurs in the request it will be passed to the failure method for default
     * handling, unless the developer overrides that method for a custom implementation. Then that
     * custom implementation will be called.
     *
     * @param caught the throwable.
     */
    @Override
    public final void onFailure(Throwable caught) {
        failure(caught);
    }

    /**
     * Default implementation of the failure method. When this method is called it will display
     * the message of the throwable within a popup panel for the user to see.
     *
     * @param caught the throwable.
     */
    public void failure(Throwable caught) {
        String message = caught.getMessage();
        if (message.length() > 512) {
            message = message.substring(0, 512).concat("...");
        }

        PopupWindowPanel removeConnectionFailedPopup = new PopupWindowPanel("invalid-login", "Failed to login", WindowPanel.Style.ALERT);
        SafeHtml safeHtml = new SafeHtmlBuilder().appendEscapedLines(message).toSafeHtml();

        EntryForm entryForm = new EntryForm(null);
        entryForm.addEntry(new EntryWithDocumentation("message", safeHtml.asString()));
        removeConnectionFailedPopup.addToBody(entryForm);

        removeConnectionFailedPopup.addToFooter(Align.RIGHT, new OkButton.WithLabel((clickEvent) -> removeConnectionFailedPopup.close()));
        removeConnectionFailedPopup.center(true, true);
    }

    /**
     * Default method that is called by the gateway when a successful connection and request was made.
     */
    @Override
    public final void onSuccess() {
        success();
    }

    /**
     * Method that needs to be implemented by the subclass in case the request was successful.
     */
    public abstract void success();

}
