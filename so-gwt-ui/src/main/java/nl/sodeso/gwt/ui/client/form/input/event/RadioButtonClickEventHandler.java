package nl.sodeso.gwt.ui.client.form.input.event;

/**
 * @author Ronald Mathies
 */
public interface RadioButtonClickEventHandler {

    void onClick(RadioButtonClickEvent event);

}