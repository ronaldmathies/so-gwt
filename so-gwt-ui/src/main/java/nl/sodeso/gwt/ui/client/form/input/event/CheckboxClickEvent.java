package nl.sodeso.gwt.ui.client.form.input.event;

import com.google.gwt.event.dom.client.ClickEvent;
import nl.sodeso.gwt.ui.client.form.input.CheckboxField;

/**
 * @author Ronald Mathies
 */
public class CheckboxClickEvent {

    private CheckboxField source;
    private ClickEvent gwtEvent;

    public CheckboxClickEvent(CheckboxField source, ClickEvent gwtEvent) {
        this.source = source;
        this.gwtEvent = gwtEvent;
    }

    public CheckboxField getSource() {
        return this.source;
    }

    public ClickEvent getGwtEvent() {
        return this.gwtEvent;
    }

}