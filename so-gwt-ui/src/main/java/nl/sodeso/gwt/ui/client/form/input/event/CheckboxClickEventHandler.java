package nl.sodeso.gwt.ui.client.form.input.event;

/**
 * @author Ronald Mathies
 */
public interface CheckboxClickEventHandler {

    void onClick(CheckboxClickEvent event);

}
