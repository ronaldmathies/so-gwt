package nl.sodeso.gwt.ui.client.form;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.LabelElement;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.form.validation.events.ValidationChangedEvent;
import nl.sodeso.gwt.ui.client.form.validation.rules.ValidationRule;
import nl.sodeso.gwt.ui.client.util.HasKey;
import nl.sodeso.gwt.ui.client.util.KeyUtil;

import java.util.*;

/**
 * @author Ronald Mathies
 */
public class EntryWithSingleWidget extends EntryWithWidget implements IsValidationContainer, IsValidatable, IsRevertable, HasKey {

    // Keys for so-key entries.
    private static final String KEY_MESSAGE_SUFFIX = "-message";
    private static final String KEY_HINT_SUFFIX = "-hint";
    private static final String KEY_WIDGET_CONTAINER_SUFFIX = "-widget-container";

    // CSS style classes
    private final static String STYLE_MESSAGE = "message";
    private final static String STYLE_HINT = "hint";
    private final static String STYLE_ENTRY_WIDGET = "entry-widget";
    private final static String STYLE_WIDGET_CONTAINER = "widget-container";
    private final static String STYLE_BUTTON_CONTAINER = "button-container";

    private ArrayList<ValidationRule> rules = new ArrayList<>();
    private Map<IsValidatable, ValidationResult> validationResults = new HashMap<>();
    private ValidationEventBusController eventbus = new ValidationEventBusController();

    private FlowPanel widgetContainer = null;
    private Widget widget = null;
    private LabelElement hintElement = null;
    private FlowPanel messageContainer = null;
    private FlowPanel entryButtonContainer = null;

    /**
     * Constructs a new EntryLabel with the specified key, label and widget to display.
     *
     * @param key the unique identifier for this row.
     * @param widget the widget to display.
     */
    public EntryWithSingleWidget(String key, Widget widget) {
        super();
        addStyleName(STYLE_ENTRY_WIDGET);

        this.widget = widget;

        this.widgetContainer = new FlowPanel();
        this.widgetContainer.setStyleName(STYLE_WIDGET_CONTAINER);
        this.widgetContainer.add(widget);
        add(this.widgetContainer);

        this.entryButtonContainer = new FlowPanel();
        this.entryButtonContainer.setStyleName(STYLE_BUTTON_CONTAINER);
        add(this.entryButtonContainer);

        this.hintElement = Document.get().createLabelElement();
        this.hintElement.addClassName(STYLE_HINT);
        getElement().appendChild(this.hintElement);

        this.messageContainer = new FlowPanel();
        this.messageContainer.setStyleName(STYLE_MESSAGE);
        this.widgetContainer.add(this.messageContainer);

        if (widget instanceof IsValidatable) {
            addValidatableWidget((IsValidatable)widget);
        }

        this.addValidatableWidget(this);

        setKey(key);
    }

    /**
     * Adds an additional validatable.
     *
     * @param validatable a validatable.
     *
     * @return this instance
     */
    public EntryWithSingleWidget addValidatableWidget(IsValidatable validatable) {
        validatable.addValidationHandler(ValidationChangedEvent.TYPE, event -> {
            validationResults.put(event.getSource(), event.getValidationResult());
            setMessage(
                new ValidationResult()
                    .mergeAll(validationResults.values()).toWidget());
        });

        return this;
    }

    /**
     * Sets a button on the row on the right side.
     * @param simpleButton the button to add.
     * @return this instance.
     */
    public EntryWithSingleWidget addButton(SimpleButton simpleButton) {
        this.entryButtonContainer.add(simpleButton);
        return this;
    }

    @Override
    public ArrayList<SimpleButton> getButtons() {
        ArrayList<SimpleButton> buttons = new ArrayList<>();
        for (int index = 0; index < this.entryButtonContainer.getWidgetCount(); index++) {
            buttons.add((SimpleButton)this.entryButtonContainer.getWidget(index));
        }
        return buttons;
    }

    /**
     * Sets the string value as the message, the string value will be encapsulated in a
     * label widget.
     *
     * @param message the message to display.
     */
    public void setMessage(String message) {
        setMessage(new Label(message));
    }

    /**
     * Sets the specified widget as the message.
     *
     * @param widget the widget.
     */
    public void setMessage(Widget widget) {
        messageContainer.clear();
        messageContainer.add(widget);
    }

    /**
     * Adds an additional hint after the widget.
     * @param hint the hint to display after the widget.
     */
    public void setHint(String hint) {
        this.hintElement.setInnerText(hint);
    }

    /**
     * Returns the hint.
     * @return the hint.
     */
    public String getHint() {
        return this.hintElement.getInnerText();
    }

    /**
     * {@inheritDoc}
     */
    public void setKey(String key) {
        super.setKey(key);

        KeyUtil.setKey(messageContainer, key, KEY_MESSAGE_SUFFIX);
        KeyUtil.setKey(hintElement, key, KEY_HINT_SUFFIX);
        KeyUtil.setKey(widgetContainer, key, KEY_WIDGET_CONTAINER_SUFFIX);

    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_ENTRY_SUFFIX;
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, Arrays.asList(this.widget));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationEventBusController getValidationEventbus() {
        return eventbus;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ArrayList<ValidationRule> getValidationRules() {
        return rules;
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.widget);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.widget);
    }
}