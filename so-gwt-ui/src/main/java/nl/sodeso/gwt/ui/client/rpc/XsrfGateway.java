package nl.sodeso.gwt.ui.client.rpc;

import com.google.gwt.core.client.Duration;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.*;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.button.OkButton;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.util.Align;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class XsrfGateway {

    private final static String XSRF_SESSIONID = "XSRF-SESSIONID";

    private static Logger logger = Logger.getLogger("EndpointLogger.XsrfEndpoint");

    private static XsrfTokenServiceAsync xsrf = (XsrfTokenServiceAsync) GWT.create(XsrfTokenService.class);

    static {
        ((ServiceDefTarget)xsrf).setServiceEntryPoint(GWT.getModuleBaseURL() + "xsrf");
    }

    /**
     * Requests a new XSRF token from the server to perform an RPC call.
     *
     * @param callback the callback to call after a success or failer.
     */
    public static void getNewXsrfToken(final AsyncCallback<XsrfToken> callback) {
        logger.log(Level.FINE, "Requesting new Xsrf Token.");

        if (isXsrfCookieValid()) {
            final Duration duration = new Duration();

            xsrf.getNewXsrfToken(new AsyncCallback<XsrfToken>() {
                @Override
                public void onFailure(Throwable caught) {
                    logger.log(Level.SEVERE, "Failed to request Xsrf token.", caught);
                    showSessionExpiredMessage();
                }

                @Override
                public void onSuccess(XsrfToken result) {
                    logger.log(Level.FINE, "Successfully requested new Xsrf token. (duration: " + duration.elapsedMillis() + "ms)");
                    callback.onSuccess(result);
                }
            });
        } else {
            showSessionExpiredMessage();
        }
    }

    /**
     * Creates the XSRF cookie.
     */
    public static void setupXsrfCookie() {
        logger.log(Level.FINE, "Setting up XSRF-Cookie.");
        if (Cookies.isCookieEnabled()) {
            Cookies.setCookie(XSRF_SESSIONID, XSRF_SESSIONID, null, null, "/", false);
        } else {
            PopupWindowPanel cookiesDisabledPopup = new PopupWindowPanel("session-expired-popup", "Cookies Disabled", WindowPanel.Style.ALERT);
            EntryForm entryForm = new EntryForm(null);
            entryForm.addEntry(new EntryWithDocumentation("message", "Your browser has cookies disabled, this application uses cookies to protect the client / server communication, please enabled cookies."));
            cookiesDisabledPopup.addToBody(entryForm);

            cookiesDisabledPopup.addToFooter(Align.RIGHT, new OkButton.WithLabel((clickEvent) -> Window.Location.assign(GWT.getHostPageBaseURL())));
            cookiesDisabledPopup.center(true, true);
        }
    }

    /**
     * Checks if the XSRF cookie is still there.
     * @return a flag indicating if the cookie is still there.
     */
    private static boolean isXsrfCookieValid() {
        String value = Cookies.getCookie(XSRF_SESSIONID);
        if (XSRF_SESSIONID.equals(value)) {
            return true;
        }

        return false;
    }

    /**
     * Displays a message explaining that the browser session has expired.
     */
    public static void showSessionExpiredMessage() {
        PopupWindowPanel sessionExpiredPopup = new PopupWindowPanel("session-expired-popup", "Session Expired", WindowPanel.Style.ALERT);
        EntryForm entryForm = new EntryForm(null);
        entryForm.addEntry(new EntryWithDocumentation("message", "Your browser session has expired due to prolonged inactivity, click Refresh Browser to automatically refresh the browser or Cancel to ignore the message."));
        sessionExpiredPopup.addToBody(entryForm);

        sessionExpiredPopup.addToFooter(Align.RIGHT, new OkButton.WithLabel((clickEvent) -> Window.Location.assign(GWT.getHostPageBaseURL())));
        sessionExpiredPopup.center(true, true);
    }

}
