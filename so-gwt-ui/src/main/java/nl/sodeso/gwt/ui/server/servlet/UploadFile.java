package nl.sodeso.gwt.ui.server.servlet;

import nl.sodeso.gwt.ui.client.util.ContentType;

import java.io.File;

/**
 * @author Ronald Mathies
 */
public class UploadFile {

    private File tempFile = null;
    private String filename;
    private ContentType contentType;

    public UploadFile() {}

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public ContentType getContentType() {
        return contentType;
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }

    public File getTempFile() {
        return tempFile;
    }

    public void setTempFile(File tempFile) {
        this.tempFile = tempFile;
    }

}
