package nl.sodeso.gwt.ui.client.form.input;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DatePicker;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.validation.IsValidatable;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationEventBusController;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.rules.ValidationRule;
import nl.sodeso.gwt.ui.client.types.DateType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.gwt.ui.client.util.DateFormatterUtil;
import nl.sodeso.gwt.ui.client.util.HasKey;
import nl.sodeso.gwt.ui.client.util.KeyUtil;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author Ronald Mathies
 */
public class DateField extends DateBox implements IsValidatable, IsRevertable, HasKey {

    private static final String KEY_DATE_SUFFIX = "-field";

    private static final DateTimeFormat FORMAT = DateFormatterUtil.dateShort;
    private ArrayList<ValidationRule> rules = new ArrayList<>();

    private DateType valueType = null;

    private ValidationEventBusController eventbus = new ValidationEventBusController();

    public DateField(String key, DateType valueType) {
        super(new DatePicker(), valueType.getValue(), new DefaultFormat(FORMAT));

        this.valueType = valueType;

        getDatePicker().setYearArrowsVisible(true);

        // Paste event is captured so that we can direct it to the value change handler.
        sinkEvents(Event.ONPASTE);

        // On-Blur for performing a validation
        sinkEvents(Event.ONBLUR);

        addValueChangeHandler(new ValueChangeHandler<Date>() {
            @Override
            public void onValueChange(ValueChangeEvent<Date> event) {
                DateField.this.valueType.setValue(getValue());
                validate(new ValidationCompletedHandler() {
                    @Override
                    public void completed(ValidationResult result) {

                    }
                });
            }
        });

        valueType.addValueChangedEventHandler(new ValueChangedEventHandler<DateType>(this) {
            @Override
            public void onValueCanged(ValueChangedEvent<DateType> event) {

                if (event.getValueType().getValue() != getValue()) {
                    setValue(event.getValueType().getValue());
                }
            }
        });

        setKey(key);
    }

    public void onBrowserEvent(Event event) {
        super.onBrowserEvent(event);

        switch (event.getTypeInt()) {
            case Event.ONPASTE:
                Scheduler.get().scheduleDeferred(() -> ValueChangeEvent.fire(DateField.this, DateField.this.getValue()));
                break;
            case Event.ONBLUR:
                validate(new ValidationCompletedHandler() {
                    @Override
                    public void completed(ValidationResult result) {

                    }
                });
        }
    }

    /**
     * {@inheritDoc}
     */
    public void setKey(String key) {
        KeyUtil.setKey(getTextBox(), key, KEY_DATE_SUFFIX);
    }

    /**
     * {@inheritDoc}
     */
    public String getKey() {
        return KeyUtil.getKey(getTextBox(), KEY_DATE_SUFFIX);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_DATE_SUFFIX;
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasKey() {
        return KeyUtil.hasKey(getTextBox());
    }

    /**
     * {@inheritDoc}
     */
    public ValidationEventBusController getValidationEventbus() {
        return eventbus;
    }

    /**
     * {@inheritDoc}
     */
    public ArrayList<ValidationRule> getValidationRules() {
        return this.rules;
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        this.valueType.revert();
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return this.valueType.isChanged();
    }
}
