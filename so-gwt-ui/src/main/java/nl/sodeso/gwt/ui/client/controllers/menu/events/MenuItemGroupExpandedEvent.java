package nl.sodeso.gwt.ui.client.controllers.menu.events;

import com.google.web.bindery.event.shared.Event;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;

/**
 * @author Ronald Mathies
 */
public class MenuItemGroupExpandedEvent extends Event<MenuItemGroupExpandedHandler> {

    public static final Type<MenuItemGroupExpandedHandler> TYPE = new Type<>();

    private MenuItem menuItem;

    public MenuItemGroupExpandedEvent(MenuItem menuItem) {
        this.menuItem = menuItem;
    }

    @Override
    public Type<MenuItemGroupExpandedHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(MenuItemGroupExpandedHandler handler) {
        handler.onEvent(this);
    }

    public MenuItem getMenuItem() {
        return this.menuItem;
    }
}