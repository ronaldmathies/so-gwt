package nl.sodeso.gwt.ui.client.types.event;

import nl.sodeso.gwt.ui.client.types.ValueType;

/**
 * Event which will be used when the value of a {@link ValueType} has changed. Subscribing to events of this
 * type can be done by registering a {@link ValueChangedEventHandler} on a {@link ValueType} using the
 * {@link ValueType#addValueChangedEventHandler(ValueChangedEventHandler)} method.
 *
 * @author Ronald Mathies
 */
public class ValueChangedEvent<T extends ValueType> {

    private T valueType;

    /**
     * Constrcuts a new event with the specified value type.
     *
     * @param valueType the value type that changed resulting in this event.
     */
    public ValueChangedEvent(T valueType) {
        this.valueType = valueType;
    }

    /**
     * Returns the value type that was changed resuling in this event.
     * @return the value type that was changed.
     */
    public T getValueType() {
        return this.valueType;
    }

}
