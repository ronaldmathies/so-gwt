package nl.sodeso.gwt.ui.client.form.table;

import com.google.gwt.dom.builder.shared.DivBuilder;
import com.google.gwt.dom.builder.shared.TableCellBuilder;
import com.google.gwt.dom.builder.shared.TableRowBuilder;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.DefaultCellTableBuilder;
import com.google.gwt.view.client.ListDataProvider;

import java.util.Objects;

/**
 * @author Ronald Mathies
 */
public abstract class DefaultTableCellGroupHeader<T> extends DefaultCellTableBuilder<T> {

    private ListDataProvider<T> listDataProvider = null;

    public DefaultTableCellGroupHeader(CellTable<T> cellTable, ListDataProvider<T> listDataProvider) {
        super(cellTable);
        this.listDataProvider = listDataProvider;
    }

    public void buildRowImpl(T object, int absRowIndex) {
        boolean showHeader = false;

        T currentObject = listDataProvider.getList().get(absRowIndex);
        String currentObjectFirstChar = getHeaderText(currentObject);

        T previousObject = null;
        if (absRowIndex > 0) {
            previousObject = listDataProvider.getList().get(absRowIndex - 1);
        } else {
            showHeader = true;
        }

        if (previousObject != null) {
            String previousObjectFirstChar = getHeaderText(previousObject);

            if (!Objects.equals(currentObjectFirstChar, previousObjectFirstChar)) {
                showHeader = true;
            }
        }

        if (showHeader) {
            TableRowBuilder tr = startRow();
            int columnCount = cellTable.getColumnCount();

            TableCellBuilder td = tr.startTD();
            td.colSpan(columnCount);
            td.className("cellTableGroupCell");

            td.align("left");
            DivBuilder div = td.startDiv();
            div.style().outlineStyle(com.google.gwt.dom.client.Style.OutlineStyle.NONE).endStyle();

            div.text(currentObjectFirstChar.toUpperCase());

            div.endDiv();
            td.endTD();

            tr.endTR();
        }

        super.buildRowImpl(object, absRowIndex);
    }

    public abstract String getHeaderText(T object);

}