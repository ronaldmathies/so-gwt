package nl.sodeso.gwt.ui.client.form.validation.rules;

import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.resources.Resources;

import static nl.sodeso.gwt.ui.client.form.validation.ValidationMessage.Level;

/**
 * @author Ronald Mathies
 */
public abstract class VersionValidationRule implements ValidationRule {

    private String key = null;
    private Level level = null;

    private static final String REGEXP_VERSION = "^[0-9]{1,3}([.]{1}[0-9]{1,3})*?$";

    /**
     * Constructs a new validation rule. That will check if the value
     * is either <code>NULL</code> or complies with the version regulair expression.
     *
     * To comply the value should have the format:
     *
     * one to three digits, dot, one to three digits whereas the last two groups may be repeated.
     *
     * @param key The key to use in the validation result.
     * @param level The message level to use in the validation result.
     */
    public VersionValidationRule(String key, Level level) {
        this.key = key;
        this.level = level;
    }

    /**
     * {@inheritDoc}
     */
    public void validate(ValidationCompletedHandler handler) {
        ValidationResult result = new ValidationResult();
        if (!checkValid()) {
            result.add(new ValidationMessage(key, level, Resources.VALIDATION_I18N.VersionNotComplyingWithRule()));
        }

        handler.completed(result);
    }

    /**
     * {@inheritDoc}
     */
    public boolean isApplicable() {
        return true;
    }

    /**
     * Implement this method to return the string value to check for.
     *
     * @return the string value to check.
     */
    public abstract String getValue();

    private boolean checkValid() {
        return getValue() == null || getValue().matches(REGEXP_VERSION);
    }

}
