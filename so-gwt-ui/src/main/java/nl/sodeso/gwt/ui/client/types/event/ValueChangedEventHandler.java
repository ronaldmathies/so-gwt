package nl.sodeso.gwt.ui.client.types.event;

import nl.sodeso.gwt.ui.client.types.ValueType;

/**
 * @author Ronald Mathies
 */
public abstract class ValueChangedEventHandler<T extends ValueType> {

    private Object source = null;

    public ValueChangedEventHandler(Object source) {
        this.source = source;
    }

    /**
     * Implement this method to receive the notification.
     *
     * @param event the event containing the value type that was changed.
     */
    public abstract void onValueCanged(ValueChangedEvent<T> event);

    /**
     * Returns the source that created this event, this does not have to be the value type.
     *
     * @return the source that created this event.
     */
    public Object getSource() {
        return this.source;
    }
}