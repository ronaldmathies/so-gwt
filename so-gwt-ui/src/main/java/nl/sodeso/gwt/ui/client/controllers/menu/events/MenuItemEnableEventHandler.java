package nl.sodeso.gwt.ui.client.controllers.menu.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * @author Ronald Mathies
 */
public interface MenuItemEnableEventHandler extends EventHandler {

    void onEvent(MenuItemEnableEvent event);

}
