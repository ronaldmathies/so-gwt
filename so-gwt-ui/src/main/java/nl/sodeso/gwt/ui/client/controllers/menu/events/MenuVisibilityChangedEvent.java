package nl.sodeso.gwt.ui.client.controllers.menu.events;

import com.google.web.bindery.event.shared.Event;


/**
 * @author Ronald Mathies
 */
public class MenuVisibilityChangedEvent extends Event<MenuVisiblityChangedEventHandler> {

    public static final Type<MenuVisiblityChangedEventHandler> TYPE = new Type<>();

    private boolean isVisible;

    public MenuVisibilityChangedEvent(boolean isVisible) {
        this.isVisible = isVisible;
    }

    @Override
    public Type<MenuVisiblityChangedEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(MenuVisiblityChangedEventHandler handler) {
        handler.onEvent(this);
    }

    public boolean isVisible() {
        return this.isVisible;
    }
}
