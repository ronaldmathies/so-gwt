package nl.sodeso.gwt.ui.client.form.input.event;

import nl.sodeso.gwt.ui.client.form.input.SearchField;

/**
 * @author Ronald Mathies
 */
public class SearchValueChangedEvent {

    private SearchField source = null;

    public SearchValueChangedEvent(SearchField source) {
        this.source = source;
    }

    public SearchField getSource() {
        return this.source;
    }

}