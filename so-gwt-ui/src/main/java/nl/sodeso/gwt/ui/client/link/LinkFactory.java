package nl.sodeso.gwt.ui.client.link;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class LinkFactory {

    private static ArrayList<Link> links = new ArrayList<>();

    private LinkFactory() {

    }

    public static void addLink(Link link) {
        links.add(link);
    }

    public static Link getLink(String token) {
        for (Link link : links) {
            if (link.canProcessToken(token)) {
                return link;
            }
        }

        return new NavigationLink();
    }

    public static boolean hasLink(String token) {
        for (Link link : links) {
            if (link.canProcessToken(token)) {
                return true;
            }
        }

        return false;
    }

}
