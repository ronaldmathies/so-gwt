package nl.sodeso.gwt.ui.client.form.button;

import com.google.gwt.event.dom.client.ClickHandler;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.gwt.ui.client.resources.Resources;
import nl.sodeso.gwt.ui.client.util.Align;

/**
 * Button representing an <code>Up</code> button.
 *
 * @author Ronald Mathies
 */
public class UpButton {

    // Keys for so-key entries.
    public static final String KEY = "up";

    public static class WithIcon extends SimpleButton {

        public WithIcon() {
            this(null);
        }

        public WithIcon(ClickHandler handler) {
            super(KEY, Icon.ChevronUp, Style.BLUE);

            if (handler != null) {
                addClickHandler(handler);
            }
        }
    }

}
