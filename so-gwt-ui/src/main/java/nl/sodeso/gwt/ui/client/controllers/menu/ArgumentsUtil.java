package nl.sodeso.gwt.ui.client.controllers.menu;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class ArgumentsUtil {

    private static final String TOKEN_ARGS_START = "?";
    private static final String TOKEN_ARGS_ADDITIONAL = "&";
    private static final String TOKEN_EQUALS = "=";

    public static String toToken(String token, KeyValue ... args) {
        return toToken(token, toArgs(args));
    }

    public static Map<String, String> fromToken(String token) {
        Map<String, String> arguments = new HashMap<>();
        if (token.indexOf(TOKEN_ARGS_START) > 0) {

            token = token.substring(token.indexOf(TOKEN_ARGS_START) + 1);
            if (token.indexOf(TOKEN_ARGS_ADDITIONAL) > 0) {
                for (String argument : token.split(TOKEN_ARGS_ADDITIONAL)) {
                    String[] keyValue = argument.split(TOKEN_EQUALS);
                    arguments.put(keyValue[0], keyValue.length > 1 ? keyValue[1] : null);
                }
            } else {
                String[] keyValue = token.split(TOKEN_EQUALS);
                arguments.put(keyValue[0], keyValue.length > 1 ? keyValue[1] : null);
            }

        }

        return arguments;
    }

    public static String toToken(String token, Map<String, String> args) {
        String tokenWithArguments = token + TOKEN_ARGS_START;

        for (Map.Entry<String, String> argument : args.entrySet()) {

            if (!tokenWithArguments.endsWith(TOKEN_ARGS_START))
                tokenWithArguments += TOKEN_ARGS_ADDITIONAL;

            tokenWithArguments += argument.getKey() + TOKEN_EQUALS + argument.getValue();
        }

        return tokenWithArguments;
    }

    public static Map<String, String> toArgs(KeyValue... args) {
        Map<String, String> arguments = new HashMap<>();
        for (KeyValue kv : args) {
            arguments.put(kv.getKey(), kv.getValue());
        }

        return arguments;
    }

    public static KeyValue add(String key, String value) {
        return new KeyValue(key, value);
    }

    public static class KeyValue {

        private String key;
        private String value;

        public KeyValue(String key, String value) {
            this.key = key;
            this.value = value;
        }

        private String getKey() {
            return this.key;
        }

        public String getValue() {
            return this.value;
        }

    }

}
