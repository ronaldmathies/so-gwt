package nl.sodeso.gwt.ui.client.controllers.menu;

import com.google.gwt.dom.client.*;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.ComplexPanel;
import nl.sodeso.gwt.ui.client.resources.Icon;

import static nl.sodeso.gwt.ui.client.util.Constants.FALSE;
import static nl.sodeso.gwt.ui.client.util.Constants.TRUE;

/**
 * @author Ronald Mathies
 */
public class UnorderedListElement extends ComplexPanel {

    // CSS style classes.
    private static final String STYLE_NOINDENT = "noindent";
    private static final String STYLE_PARENT = "parent";
    private static final String STYLE_ANCHOR = "anchor";
    private static final String STYLE_TOGGLE = "toggle";
    private static final String STYLE_ICON = "icon";
    private static final String STYLE_NOICON = "noicon";

    // Element attribute names.
    private final static String ATTR_COLLAPSED = "so-collapsed";
    private final static String ATTR_SELECTED = "so-selected";
    private static final String ATTR_VISIBLE = "so-visible";

    private SpanElement labelSpan = null;
    private Anchor anchor = null;

    private LIElement lieElement = null;

    public UnorderedListElement() {
        this(null, null);
    }

    public UnorderedListElement(String uriKey, String label) {
        this(uriKey, null, label);
    }

    public UnorderedListElement(String uriKey, Icon icon, String label) {
        UListElement uListElement = Document.get().createULElement();
        uListElement.addClassName(STYLE_NOINDENT);
        setElement(uListElement);

        if (label != null) {
            lieElement = Document.get().createLIElement();
            lieElement.addClassName(STYLE_PARENT);

            lieElement.setAttribute(ATTR_VISIBLE, "true");
            lieElement.setAttribute(ATTR_COLLAPSED, "true");

            // Always false, necessary for the syling.
            lieElement.setAttribute(ATTR_SELECTED, "false");

            anchor = new Anchor("", "#" + uriKey);
            anchor.addStyleName(STYLE_ANCHOR);
            lieElement.appendChild(anchor.getElement());

            if (icon != null) {
                Element iconElement = Document.get().createElement("I");
                iconElement.addClassName(icon.getStyle());
                anchor.getElement().appendChild(iconElement);
            }

            labelSpan = Document.get().createSpanElement();
            labelSpan.addClassName(icon != null ? STYLE_ICON : STYLE_NOICON);
            labelSpan.setInnerText(label);

            SpanElement toggleSpan = Document.get().createSpanElement();
            toggleSpan.setClassName(STYLE_TOGGLE);

            anchor.getElement().appendChild(labelSpan);
            anchor.getElement().appendChild(toggleSpan);
            uListElement.appendChild(lieElement);
        }

    }

    public void setUriKey(String uriKey) {
        anchor.setHref("#" + uriKey);
    }

    @SuppressWarnings("deprecation")
    public void addListItem(UnorderedListItemElement listItem) {
        add(listItem, getElement());
    }

    @SuppressWarnings("deprecation")
    public void addListItem(int index, UnorderedListItemElement listItem) {
        insert(listItem, getElement(), index, true);
    }

    @SuppressWarnings("deprecation")
    public void addList(UnorderedListElement list) {
        add(list, getElement());
    }

    @SuppressWarnings("deprecation")
    public void addList(int index, UnorderedListElement list) {
        insert(list, getElement(), index, true);
    }

    public int removeListItem(UnorderedListItemElement listItem) {
        int index = getWidgetIndex(listItem);

        if (index != -1) {
            remove(listItem);
        }

        return index;
    }

    public int removeList(UnorderedListElement list) {
        int index = getWidgetIndex(list);

        if (index != -1) {
            remove(list);
        }

        return index;
    }

    public void setCollapsed(boolean expanded) {
        lieElement.setAttribute(ATTR_COLLAPSED, expanded ? TRUE : FALSE);
    }

    public boolean isCollapsed() {
        String value = lieElement.getAttribute(ATTR_COLLAPSED);
        return value == null || value.equals(TRUE);
    }

    public void setVisible(boolean visible) {
        lieElement.setAttribute(ATTR_VISIBLE, visible ? TRUE : FALSE);
    }

    public boolean isVisible() {
        String value = lieElement.getAttribute(ATTR_VISIBLE);
        return value == null || value.equals(TRUE);
    }

}
