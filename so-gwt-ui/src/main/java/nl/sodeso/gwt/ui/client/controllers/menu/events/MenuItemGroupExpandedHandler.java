package nl.sodeso.gwt.ui.client.controllers.menu.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * @author Ronald Mathies
 */
public interface MenuItemGroupExpandedHandler extends EventHandler {

    void onEvent(MenuItemGroupExpandedEvent event);

}
