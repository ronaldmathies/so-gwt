package nl.sodeso.gwt.ui.server.servlet;

import nl.sodeso.gwt.ui.client.util.ContentType;

import java.io.File;
import java.io.InputStream;

/**
 * @author Ronald Mathies
 */
public class DownloadFile {

    private InputStream inputStream = null;
    private String filename;
    private ContentType contentType;

    public DownloadFile() {}

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public ContentType getContentType() {
        return contentType;
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

}
