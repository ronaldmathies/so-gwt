package nl.sodeso.gwt.ui.client.resources;

/**
 * Various CSS icon definitions based on the FontAwesome font files.
 *
 * For the various css definitions check font.css.
 *
 * @author Ronald Mathies
 */
public enum Icon {

    Success("fa fa-check"),
    Info("fa fa-info"),
    Warning("fa fa-warning"),
    Error("fa fa-warning"),

    MinusCircle("fa fa-minus-circle"),

    Cogs("fa fa-cogs"),
    Settings("fa fa-cogs"),
    Key("fa fa-key"),
    Refresh("fa fa-refresh"),
    Tasks("fa fa-tasks"),
    Tags("fa fa-tags"),
    Search("fa fa-search"),
    Filter("fa fa-filter"),
    SearchMinus("fa fa-search-minus"),
    SearchPlus("fa fa-search-plus"),

    Bookmark("fa fa-bookmark"),
    BookmarkO("fa fa-bookmark-o"),

    ChevronDown("fa fa-chevron-down"),
    ChevronLeft("fa fa-chevron-left"),
    ChevronUp("fa fa-chevron-up"),
    ChevronRight("fa fa-chevron-right"),

    SignIn("fa fa-sign-in"),
    SignOut("fa fa-sign-out"),

    Forward("fa fa-share"),
    Reply("fa fa-reply"),


    Plus("fa fa-plus"),
    Add("fa fa-plus"),
    Remove("fa fa-times"),
    Copy("fa fa-files-o"),
    Save("fa fa-floppy-o"),
    User("fa fa-user"),
    Users("fa fa-users"),

    CloudDownload("fa fa-cloud-download"),
    CloudUpload("fa fa-cloud-upload"),

    Database("fa fa-database "),
    Server("fa fa-server"),

    Bell("fa fa-bell"),
    BellSlash("fa fa-bell-slash"),
    BellO("fa fa-bell-o"),
    BellOSlash("fa fa-bell-o-slash"),

    Desktop("fa fa-desktop"),

    Document("fa fa-file-text-o"),
    Book("fa fa-book"),
    BulletList("fa fa-list"),

    ArrowCircleRight("fa fa-arrow-circle-right"),
    ArrowCircleRightO("fa fa-arrow-circle-o-right"),
    ArrowCircleLeft("fa fa-arrow-circle-left"),
    ArrowCircleLeftO("fa fa-arrow-circle-o-left"),
    ArrowsAlt("fa fa-arrows-alt"),

    PlusSquare("fa fa-plus-square"),
    MinusSquare("fa fa-minus-square"),
    PlusSquareO("fa fa-plus-square-o"),
    MinusSquareO("fa fa-minus-square-o");

    private String style = null;

    Icon(String style) {
        this.style = style;
    }

    public String getStyle() {
        return this.style;
    }
}
