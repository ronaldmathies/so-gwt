package nl.sodeso.gwt.ui.client.form.input;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.TextBox;
import nl.sodeso.gwt.ui.client.form.input.filters.InputFilter;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.validation.IsValidatable;
import nl.sodeso.gwt.ui.client.form.validation.ValidationEventBusController;
import nl.sodeso.gwt.ui.client.form.validation.rules.ValidationRule;
import nl.sodeso.gwt.ui.client.types.ValueType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.gwt.ui.client.util.AttrUtils;
import nl.sodeso.gwt.ui.client.util.HasKey;
import nl.sodeso.gwt.ui.client.util.KeyboardUtil;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@SuppressWarnings("unused")
public class TextField<V extends ValueType> extends TextBox implements IsValidatable, IsRevertable, HasKey {

    // Keys for so-key entries.
    private static final String KEY_SUFFIX = "-field";

    private ArrayList<InputFilter> filters = new ArrayList<>();
    private ArrayList<ValidationRule> rules = new ArrayList<>();

    private ValidationEventBusController eventbus = new ValidationEventBusController();

    private V valueType = null;

    /**
     *
     * @param key the unique key for debugging / testing "-field" is automatically added as a suffix.
     * @param valueType the value type for holding the value.
     */
    @SuppressWarnings("unchecked")
    public TextField(String key, final V valueType) {
        super();

        this.valueType = valueType;

        // Paste event is captured so that we can direct it to the value change handler.
        sinkEvents(Event.ONPASTE);

        // On-Blur for performing a validation
        sinkEvents(Event.ONBLUR);

        super.addKeyPressHandler(event -> {
            int keyCode = event.getNativeEvent().getKeyCode();

            if (Character.isLetterOrDigit(event.getCharCode()) ||
                    KeyboardUtil.isSpace(event.getCharCode()) ||
                    KeyboardUtil.isBackspace(keyCode) ||
                    KeyboardUtil.isDelete(KeyCodes.KEY_DELETE)) {

                if (!filters.isEmpty()) {
                    for (InputFilter filter : filters) {
                        String character = String.valueOf(KeyboardUtil.getKeyCode(event.getNativeEvent()));
                        if (!filter.isAllowedValue(character)) {
                            event.preventDefault();
                            return;
                        }
                    }
                }
            }
        });

        super.addKeyUpHandler(event -> {
            if (!getValue().equals(valueType.getValue())) {
                TextField.this.valueType.setValueAsString(getValue());
            }

            if (!KeyboardUtil.isTab(KeyboardUtil.getKeyCode(event.getNativeEvent()))) {
                validate(result -> {
                });
            }
        });

        // Seems to result in double validations, for example wheb tabbing out of the field.
//        addValueChangeHandler(event -> {
//            TextField.this.valueType.setValueAsString(getValue());
//            validate(result -> {
//                logger.info("VALIDATE VALUE CHANGED");
//
//            });
//        });

        valueType.addValueChangedEventHandler(new ValueChangedEventHandler<V>(this) {
            @Override
            public void onValueCanged(ValueChangedEvent<V> event) {

                if (event.getValueType().getValue() != getValue()) {
                    setValue(event.getValueType().getValueAsString());
                }
            }
        });

        setValue(valueType.getValueAsString());
        setKey(key);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_SUFFIX;
    }

    public void setId(String id) {
        getElement().setId(id);
    }

    public void setFocus() {
        Scheduler.get().scheduleDeferred(() -> TextField.this.setFocus(true));
    }

    public void setValue(String value) {
        if (isAllowed(value)) {
            super.setValue(value);
            validate((result) -> {});
        }
    }


    private boolean isAllowed(String value) {
        if (!filters.isEmpty()) {
            for (InputFilter filter : filters) {
                if (!filter.isAllowedValue(value)) {
                    return false;
                }
            }
        }

        return true;
    }

    public TextField<V> setPlaceholder(String value) {
        AttrUtils.setPlaceHolder(this, value);
        return this;
    }

    public void onBrowserEvent(Event event) {
        super.onBrowserEvent(event);

        switch (event.getTypeInt()) {
            case Event.ONPASTE:
                Scheduler.get().scheduleDeferred(() -> ValueChangeEvent.fire(TextField.this, TextField.this.getValue()));
                break;
            case Event.ONBLUR:
                validate((result) -> {});
        }
    }

    public TextField<V> addFilter(InputFilter filter) {
        this.filters.add(filter);
        return this;
    }
    /**
     * {@inheritDoc}
     */
    public ValidationEventBusController getValidationEventbus() {
        return eventbus;
    }

    /**
     * {@inheritDoc}
     */
    public ArrayList<ValidationRule> getValidationRules() {
        return this.rules;
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        valueType.revert();
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return valueType.isChanged();

    }
}
