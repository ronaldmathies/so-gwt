package nl.sodeso.gwt.ui.client.controllers.logging;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import nl.sodeso.gwt.ui.client.form.button.ClearButton;
import nl.sodeso.gwt.ui.client.form.button.CloseButton;
import nl.sodeso.gwt.ui.client.form.table.TablePagerField;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.resources.Resources;
import nl.sodeso.gwt.ui.client.util.Align;


/**
 * @author Ronald Mathies
 */
public class LoggingWindow extends PopupWindowPanel {

    // Keys for so-key entries.
    private static final String KEY_LOGGING_WINDOW = "logging-window";

    // CSS style classes.
    private static final String STYLE_LOGGING_WINDOW = "logging-window";

    public LoggingWindow() {
        super(KEY_LOGGING_WINDOW, Resources.LOGGING_I18N.Title(), Style.ALERT);
        addStyleName(STYLE_LOGGING_WINDOW);

        showCollapseButton(true);
        setWidth("700px");

        LoggingTable table = new LoggingTable();
        table.setFullHeight(true);

        TabLayoutPanel tabLayoutPanel = new TabLayoutPanel(50, com.google.gwt.dom.client.Style.Unit.PX);
        FlowPanel tableTabPane = new FlowPanel();
        tableTabPane.add(table);
        tabLayoutPanel.add(tableTabPane, "Logging");

        FlowPanel chartTabPane = new FlowPanel();
        RpcCallChart rpcCallChart = new RpcCallChart();
        chartTabPane.add(rpcCallChart);
        tabLayoutPanel.add(chartTabPane, "RPC Calls");

        addToBody(tabLayoutPanel);

        // Pagination controls
        TablePagerField tablePagerField = new TablePagerField();
        tablePagerField.setDisplay(table);

        addToFooter(Align.RIGHT, tablePagerField);

        addToFooter(Align.LEFT,
                new CloseButton.WithLabel((event) -> close()),
                new ClearButton.WithLabel((event) -> table.removeAll()));
    }

}
