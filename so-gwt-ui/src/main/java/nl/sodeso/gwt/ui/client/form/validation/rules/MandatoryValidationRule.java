package nl.sodeso.gwt.ui.client.form.validation.rules;

import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.resources.Resources;

import static nl.sodeso.gwt.ui.client.form.validation.ValidationMessage.Level;

/**
 * @author Ronald Mathies
 */
public abstract class MandatoryValidationRule implements ValidationRule {

    // Keys for so-key entries.
    private static final String KEY_MANDATORY_SUFFIX = "-mandatory";

    private String key = null;
    private String label = null;
    private Level level = null;

    /**
     * Constructs a new validation rule.
     *
     * @param key The key to use in the validation result.
     * @param level The message level to use in the validation result.
     */
    public MandatoryValidationRule(String key, Level level) {
        this.key = key;
        this.level = level;
    }

    /**
     * Constructs a new validation rule.
     *
     * @param key The key to use in the validation result.
     * @param label The label to use in the validation message.
     * @param level The message level to use in the validation result.
     */
    public MandatoryValidationRule(String key, String label, Level level) {
        this.key = key;
        this.level = level;
        this.label = label;
    }

    /**
     * {@inheritDoc}
     */
    public void validate(ValidationCompletedHandler handler) {
        ValidationResult result = new ValidationResult();
        if (checkNull()) {
            result.add(new ValidationMessage(
                    key != null ? key + KEY_MANDATORY_SUFFIX : KEY_MANDATORY_SUFFIX,
                    level,
                    label != null ? Resources.VALIDATION_I18N.MandatoryWithLabel(label) : Resources.VALIDATION_I18N.Mandatory()));
        }

        handler.completed(result);
    }

//    /**
//     * {@inheritDoc}
//     */
//    public boolean isValid() {
//        return !checkNull();
//    }

    /**
     * {@inheritDoc}
     */
    public boolean isApplicable() {
        return true;
    }

    /**
     * Implement this method to return the string value to check for.
     *
     * @return the string value to check.
     */
    public abstract String getValue();

    private boolean checkNull() {
        return getValue() == null || getValue().isEmpty();
    }

}
