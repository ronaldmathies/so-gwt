package nl.sodeso.gwt.ui.client.controllers.logging.handler;

import com.google.gwt.core.client.GWT;
import com.google.gwt.logging.client.RemoteLogHandlerBase;
import com.google.gwt.logging.shared.RemoteLoggingService;
import com.google.gwt.logging.shared.RemoteLoggingServiceAsync;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.logging.LogRecord;

/**
 * A very simple handler which sends messages to the server via GWT RPC to be
 * logged. Note that this logger does not do any intelligent batching of RPC's,
 * nor does it disable when the RPC calls fail repeatedly.
 *
 * @author Ronald Mathies
 */
public final class RemoteLogHandler extends RemoteLogHandlerBase {

    private RemoteLoggingServiceAsync service = (RemoteLoggingServiceAsync) GWT.create(RemoteLoggingService.class);

    public RemoteLogHandler() {
    }

    @Override
    public void publish(LogRecord record) {
        if (isLoggable(record)) {
            service.logOnServer(record, new AsyncCallback<String>() {
                @Override
                public void onFailure(Throwable caught) {
                    // do nothing... if it doesn't work, then don't log it.
                }

                @Override
                public void onSuccess(String result) {
                    // Do not log that has been logged.
                }
            });
        }
    }
}

