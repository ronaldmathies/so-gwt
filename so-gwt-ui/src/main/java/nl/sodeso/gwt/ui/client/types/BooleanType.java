package nl.sodeso.gwt.ui.client.types;

/**
 * @author Ronald Mathies
 */
public class BooleanType extends ValueType<Boolean> {

    /**
     * Constructs a new instance with an unspecified value.
     */
    public BooleanType() {}

    /**
     * Constructs a new instance with the specified value.
     *
     * @param value the initial value.
     */
    public BooleanType(Boolean value) {
        super(value);
    }

    /**
     * {@inheritDoc}
     */
    public void setValueAsString(String value) {
        this.setValue(Boolean.valueOf(value));
    }

}
