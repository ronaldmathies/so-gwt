package nl.sodeso.gwt.ui.client.menu;

/**
 * @author Ronald Mathies
 */
public interface ContextMenuItemClickTrigger {

    void click();

}
