package nl.sodeso.gwt.ui.client.controllers.session.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.gwt.ui.client.controllers.session.domain.LoginResult;

/**
 * @author Ronald Mathies
 */
public interface SessionRpcAsync {

    void login(String username, String password, AsyncCallback<LoginResult> result);
    void isLoggedIn(AsyncCallback<LoginResult> result);
    void logout(AsyncCallback<Void> result);
}
