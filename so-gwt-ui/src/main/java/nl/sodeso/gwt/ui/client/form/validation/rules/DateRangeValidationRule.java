package nl.sodeso.gwt.ui.client.form.validation.rules;

import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.resources.Resources;
import nl.sodeso.gwt.ui.client.util.DateFormatterUtil;

import java.util.Date;

import static nl.sodeso.gwt.ui.client.form.validation.ValidationMessage.Level;

/**
 * @author Ronald Mathies
 */
public abstract class DateRangeValidationRule implements ValidationRule {

    // Keys for so-key entries.
    private static final String KEY_MINIMUM_SUFFIX = "-minimum";
    private static final String KEY_MAXIMUM_SUFFIX = "-minimum";

    private String key = null;
    private Level level = null;

    private boolean useMinimum = false;
    private Date minimum = null;
    private boolean useMaximum = false;
    private Date maximum = null;

    /**
     * Constructs a new validation rule.
     *
     * @param key The key to use in the validation result.
     * @param level The message level to use in the validation result.
     */
    public DateRangeValidationRule(String key, Level level) {
        this.key = key;
        this.level = level;
    }

    /**
     * Set the minimum value that is required.
     *
     * @param minimum the minimum value that is required.
     *
     * @return this instance.
     */
    public DateRangeValidationRule setMinimum(Date minimum) {
        this.useMinimum = true;
        this.minimum = minimum;
        return this;
    }

    /**
     * Set the maximum value that is required.
     *
     * @param maximum the maximum value that is required.
     *
     * @return this instance.
     */
    public DateRangeValidationRule setMaximum(Date maximum) {
        this.useMaximum = true;
        this.maximum = maximum;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public void validate(ValidationCompletedHandler handler) {
        ValidationResult result = new ValidationResult();

        if (useMinimum && checkMinimum()) {
            result.add(new ValidationMessage(this.key != null ? key + KEY_MINIMUM_SUFFIX : null, level, Resources.VALIDATION_I18N.DateRangeValidationMinimum(DateFormatterUtil.formatDate(this.minimum))));
        }

        if (useMaximum && checkMaximum()) {
            result.add(new ValidationMessage(this.key != null ? key + KEY_MAXIMUM_SUFFIX : null, level, Resources.VALIDATION_I18N.DateRangeValidationMaximum(DateFormatterUtil.formatDate(this.maximum))));
        }

        handler.completed(result);
    }

//    /**
//     * {@inheritDoc}
//     */
//    public boolean isValid() {
//        if (useMinimum && !checkMinimum()) {
//            return false;
//        }
//
//        if (useMaximum && !checkMaximum()) {
//            return false;
//        }
//
//        return true;
//    }

    /**
     * {@inheritDoc}
     */
    public boolean isApplicable() {
        return true;
    }

    /**
     * Implement this method to return the string value to check for.
     *
     * @return the string value to check.
     */
    public abstract Date getValue();

    private boolean checkMinimum() {
        return getValue().getTime() < minimum.getTime();
    }

    private boolean checkMaximum() {
        return getValue().getTime() > maximum.getTime();
    }

}
