package nl.sodeso.gwt.ui.client.dialog;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.button.CancelButton;
import nl.sodeso.gwt.ui.client.form.button.YesButton;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.util.Align;

/**
 * @author Ronald Mathies
 */
public class UnsavedChangesQuestionPanel extends PopupWindowPanel {

    private SaveChangesHandler handler = null;

    public UnsavedChangesQuestionPanel(final SaveChangesHandler handler) {
        super("unsaved-changes", "Unsaved Changes", WindowPanel.Style.ALERT);

        this.handler = handler;

        addToBody(
            new EntryForm(null)
                .addEntry(new EntryWithDocumentation(null, "There are unsaved changes, would you like to save the changes before continuing?")));

        addToFooter(Align.RIGHT,
                new YesButton.WithLabel((event) -> yes()),
                new CancelButton.WithLabel((event) -> close()));
    }

    private void yes() {
        close();
        handler.confirmed();
    }
}
