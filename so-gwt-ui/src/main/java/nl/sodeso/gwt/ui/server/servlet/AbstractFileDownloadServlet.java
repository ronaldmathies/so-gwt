package nl.sodeso.gwt.ui.server.servlet;

import nl.sodeso.commons.fileutils.FileUtil;
import nl.sodeso.gwt.ui.client.util.ContentType;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public abstract class AbstractFileDownloadServlet extends HttpServlet {

    private static final String HEADER_CONTENT_DISPOSITION = "content-disposition";

    private static final Logger log = Logger.getLogger(AbstractFileDownloadServlet.class.getName());

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DownloadFile downloadFile = download(request);

        response.setContentType(downloadFile.getContentType().getContentType());
        response.setHeader(HEADER_CONTENT_DISPOSITION, "attachment; filename=\"" + downloadFile.getFilename() + "\"");

        IOUtils.copy(downloadFile.getInputStream(), response.getOutputStream());
    }

    public abstract DownloadFile download(HttpServletRequest request) throws ServletException;
}
