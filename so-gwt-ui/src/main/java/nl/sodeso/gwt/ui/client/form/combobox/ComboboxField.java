package nl.sodeso.gwt.ui.client.form.combobox;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.form.combobox.filters.OptionFilter;
import nl.sodeso.gwt.ui.client.form.combobox.ui.LIOption;
import nl.sodeso.gwt.ui.client.form.combobox.ui.ULList;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.IsValidatable;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationEventBusController;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.rules.ValidationRule;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.gwt.ui.client.util.ComputedStyleUtil;
import nl.sodeso.gwt.ui.client.util.HasKey;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Displays a entry field, when the other option is supplied the user will be able to enter
 * a custom value when the other option is selected.
 *
 * @author Ronald Mathies
 */
public class ComboboxField<T extends Option> extends FlowPanel implements IsValidatable, IsRevertable, HasKey {

    private static final String KEY_COMBOBOX_SUFFIX = "-field";

    // CSS style classes.
    private static final String STYLE_LIST_FIELD = "list-field";
    private static final String STYLE_SELECT_OPTIONS = "select-options";
    private static final String STYLE_SELECT_VALUE = "select-value";
    private static final String STYLE_OTHER_VALUE = "other-value";
    private static final String STYLE_LIST_FIELD_POPUP = "list-field-popup";
    private static final String STYLE_LIST_FIELD_CONTENTS = "contents";

    private ArrayList<ValidationRule> rules = new ArrayList<>();

    private ValidationEventBusController eventbus = new ValidationEventBusController();

    private ArrayList<T> items = new ArrayList<>();

    private SimpleButton selectButton = null;
    private TextField<StringType> selectedField = null;
    private TextField<StringType> otherField = null;

    private StringType selectedOptionValue = null;
    private StringType otherValue = null;

    private OptionType<T> optionValue = null;
    private T otherOption = null;

    private OptionFilter<T> filter = null;

    /**
     * Constructs a new Combobox field without the ability for the user to enter a custom value,
     * basically a normal drop down list.
     *
     * @param optionValue the option value for binding.
     */
    public ComboboxField(OptionType<T> optionValue) {
        this(optionValue, null, null);
    }

    /**
     * Constructs a new Combobox field with a key, but without the ability for the user to enter a custom value,
     * basically a normal drop down list.
     *
     * @param key the key of the field.
     * @param optionValue the option value for binding.
     */
    public ComboboxField(String key, OptionType<T> optionValue) {
        this(optionValue, null, null);
        setKey(key);
    }

    /**
     * Constructs a new Combobox field with the ability for the user to enter a custom value. When
     * the other option is selected.
     *
     * @param optionValue the option value for binding.
     * @param otherValue the user entered value for binding.
     * @param otherOption the other option, when selected the user will be able to enter a custom value.
     */
    public ComboboxField(final OptionType<T> optionValue, final StringType otherValue, final T otherOption) {
        this.otherOption = otherOption;
        this.optionValue = optionValue;
        this.otherValue = otherValue;

        addStyleName(STYLE_LIST_FIELD);

        selectButton = new SimpleButton("select", Icon.ChevronDown, SimpleButton.Style.BLUE);
        selectButton.addClickHandler((event) -> displayPopupList());
        selectButton.addStyleName(STYLE_SELECT_OPTIONS);
        if (this.optionValue.getValue() != null) {
            selectedOptionValue = new StringType(this.optionValue.getValue().getDisplayDescription());
        } else {
            selectedOptionValue = new StringType();
        }

        selectedField = new TextField<>("selected-value", selectedOptionValue);
        selectedField.addStyleName(STYLE_SELECT_VALUE);
        selectedField.setReadOnly(true);
        selectedField.addFocusHandler(event -> displayPopupList());
        add(selectedField);

        if (this.otherOption != null) {
            otherField = new TextField<>("other-value", otherValue);
            otherField.addStyleName(STYLE_OTHER_VALUE);
            otherField.setVisible(false);
            add(otherField);
        }

        if (this.optionValue.getValue() != null && this.optionValue.getValue().equals(otherOption)) {
            enableOtherOption(true);
        }

        this.optionValue.addValueChangedEventHandler(new ValueChangedEventHandler<OptionType>(this) {
            @Override
            public void onValueCanged(ValueChangedEvent<OptionType> event) {
                if (optionValue.getValue() != null) {
                    if (optionValue.getValue().equals(otherOption)) {
                        enableOtherOption(true);

                        selectedField.setValue(optionValue.getValue().getDisplayDescription());
                        selectedOptionValue.setValue(null);
                    } else {
                        enableOtherOption(false);

                        if (otherValue != null) {
                            otherValue.setValue(null);
                        }

                        selectedOptionValue.setValue(optionValue.getValue().getDisplayDescription());
                    }
                } else {
                    enableOtherOption(false);
                    selectedOptionValue.setValue(null);
                }
            }
        });

        add(selectButton);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_COMBOBOX_SUFFIX;
    }

    private void displayPopupList() {
        final PopupPanel listFieldPopup = new PopupPanel(true, false);
        listFieldPopup.setAnimationEnabled(true);
        listFieldPopup.setAnimationType(PopupPanel.AnimationType.ROLL_DOWN);
        listFieldPopup.addStyleName(STYLE_LIST_FIELD_POPUP);

        FlowPanel contents = new FlowPanel();
        contents.addStyleName(STYLE_LIST_FIELD_CONTENTS);
        contents.getElement().getStyle().setProperty("minWidth", ComputedStyleUtil.getStyleProperty(selectedField.getElement(), "width"));
        listFieldPopup.add(contents);

        ULList list = new ULList();

        for (final T item : items) {

            if (filter != null) {
                if (!filter.isOptionAllowed(item)) {
                    continue;
                }
            }

            LIOption<Option> liOption = new LIOption<>(item, () -> {
                optionValue.setValue(item);
                listFieldPopup.hide();

                validate(result -> { });
            });

            list.addOption(liOption);
        }

        if (otherOption != null) {
            LIOption<Option> liOption = new LIOption<>(otherOption, () -> {
                otherField.setFocus(true);
                optionValue.setValue(otherOption);
                listFieldPopup.hide();

                validate(result -> { });
            });
            list.addOption(liOption);
        }

        contents.add(list);

        listFieldPopup.showRelativeTo(this);
    }

    private void enableOtherOption(boolean enable) {
        if (this.otherOption != null) {
            otherField.setVisible(enable);
            selectedField.setVisible(!enable);
        } else {
            selectedField.setVisible(true);
        }
    }

    /**
     * Enabled / disabled the ability to change an option.
     * @param isEnabled flag indicating if the user is allowed to change the value.
     */
    public void setEnabled(boolean isEnabled) {
        this.selectButton.setVisible(isEnabled);
        this.selectedField.setEnabled(isEnabled);
    }

    /**
     * Flag indicating if the combobox is enabled or not.
     * @return true if the combobox is enabled, false if not.
     */
    public boolean isEnabled() {
        return this.selectButton.isVisible();
    }

    /**
     * Adds a filter which will check if an option is allowed to be displayed, otherwise
     * it will be hidden.
     *
     * @param filter the filter.
     */
    public void setOptionFilter(OptionFilter<T> filter) {
        this.filter = filter;
    }

    /**
     * Adds a new item to the list.
     * @param item the item to add.
     */
    public void addItem(T item) {
        this.items.add(item);
    }

    /**
     * Adds all items to the list.
     * @param items the items to add.
     */
    @SafeVarargs
    public final void addItems(T... items) {
        this.items.addAll(Arrays.asList(items));
    }

    /**
     * Adds all the items to the list.
     * @param items the items to add.
     */
    public void addItems(List<T> items) {
        this.items.addAll(items);
    }

    /**
     * Removes all items from the list.
     */
    public void removeAllItems() {
        this.items.clear();
        if (this.optionValue.getValue() != null) {
            this.optionValue.setValue(null);
        }
    }

    public void replaceItems(List<T> items, boolean selectFirst) {
        this.items.clear();
        this.addItems(items);

        setSelection(items.get(0));
    }

    /**
     * Replaces all items and tries to retain the selection you made.
     * @param items the items to add.
     */
    public void replaceItemsRetainSelection(List<T> items) {
        this.items.clear();
        this.addItems(items);

        boolean exists = false;
        for (T option : items) {
            if (option.equals(optionValue.getValue())) {
                exists = true;
            }
        }

        if (!exists) {
            clearSelection();
        }
    }

    public void clearSelection() {
        optionValue.setValue(null);
    }

    /**
     * Changes the selection to the specified option, if the option
     * does not exist within the current selection then the selection won't change.
     *
     * @param option the option to select.
     */
    public void setSelection(T option) {
        boolean exists = false;
        for (T existingOption : items) {
            if (existingOption.getKey().equals(option.getKey())) {
                exists = true;
            }
        }

        if (exists) {
            optionValue.setValue(option);
        }
    }

    /**
     * Returns the available items.
     * @return the available items.
     */
    public List<T> getItems() {
        return this.items;
    }

    /**
     * Returns the selected item.
     * @return the selected item.
     */
    public T getSelectedItem() {
        return optionValue.getValue();
    }

    /**
     * Returns the selected index for the selected item.
     * @return the selected index for the selected item.
     */
    public int getSelectedIndex() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    public ValidationEventBusController getValidationEventbus() {
        return eventbus;
    }

    /**
     * {@inheritDoc}
     */
    public ArrayList<ValidationRule> getValidationRules() {
        return this.rules;
    }

    /**
     * Reverts the values.
     */
    public void revert() {
        this.optionValue.revert();
        RevertUtil.revert(new Object[] {this.selectedField, this.otherField});
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        if (optionValue.isChanged()) {
            return true;
        } else if (otherOption != null) {
            if (otherOption.getKey().equals(optionValue.getValue().getKey())) {
                return otherValue.isChanged();
            }
        }

        return false;
    }
}
