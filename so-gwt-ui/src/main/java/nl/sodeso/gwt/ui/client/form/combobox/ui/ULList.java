package nl.sodeso.gwt.ui.client.form.combobox.ui;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.UListElement;
import com.google.gwt.user.client.ui.ComplexPanel;

/**
 * @author Ronald Mathies
 */
public class ULList extends ComplexPanel {

    // CSS style classes.
    private static final String STYLE_LIST_LIST = "list-list";

    public ULList() {
        UListElement ulList = Document.get().createULElement();
        ulList.addClassName(STYLE_LIST_LIST);
        setElement(ulList);
    }

    @SuppressWarnings("deprecation")
    public void addOption(LIOption option) {
        add(option, getElement());
    }




}
