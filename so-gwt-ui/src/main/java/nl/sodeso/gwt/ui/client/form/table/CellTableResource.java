package nl.sodeso.gwt.ui.client.form.table;

import com.google.gwt.user.cellview.client.CellTable;

/**
 * @author Ronald Mathies
 */
public interface CellTableResource extends CellTable.Resources {

    @Override
    @Source(CellTableStyle.DEFAULT_CSS)
    CellTableStyle cellTableStyle();

}
