package nl.sodeso.gwt.ui.client.application.module;

import nl.sodeso.gwt.ui.client.link.Link;

/**
 * @author Ronald Mathies
 */
public abstract class WelcomePageLink implements Link {

    public abstract String getToken();

}
