package nl.sodeso.gwt.ui.client.controllers.logging.handler.event;

import com.google.web.bindery.event.shared.Event;
import nl.sodeso.gwt.ui.client.controllers.logging.LogItem;


/**
 * @author Ronald Mathies
 */
public class LogItemEvent extends Event<LogItemEventHandler> {

    public static final Type<LogItemEventHandler> TYPE = new Type<>();

    private LogItem item;

    public LogItemEvent(LogItem item) {
        this.item = item;
    }

    @Override
    public Type<LogItemEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(LogItemEventHandler handler) {
        handler.onEvent(this);
    }

    public LogItem getLogItem() {
        return this.item;
    }
}
