package nl.sodeso.gwt.ui.client.form.input;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.PasswordTextBox;
import nl.sodeso.gwt.ui.client.form.input.filters.InputFilter;
import nl.sodeso.gwt.ui.client.form.validation.IsValidatable;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationEventBusController;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.rules.ValidationRule;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.gwt.ui.client.util.HasKey;
import nl.sodeso.gwt.ui.client.util.KeyboardUtil;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class PasswordField extends PasswordTextBox implements IsValidatable, HasKey {

    private static final String KEY_PASSWORD_SUFFIX = "-field";

    private ArrayList<InputFilter> filters = new ArrayList<>();
    private ArrayList<ValidationRule> rules = new ArrayList<>();

    private ValidationEventBusController eventbus = new ValidationEventBusController();

    private StringType valueType = null;

    public PasswordField(String key, final StringType valueType) {
        super();

        this.valueType = valueType;

        // Paste event is captured so that we can direct it to the value change handler.
        sinkEvents(Event.ONPASTE);

        // On-Blur for performing a validation
        sinkEvents(Event.ONBLUR);

        super.addKeyPressHandler(event -> {
            int keyCode = event.getNativeEvent().getKeyCode();

            if (Character.isLetterOrDigit(event.getCharCode()) ||
                    KeyboardUtil.isSpace(event.getCharCode()) ||
                    KeyboardUtil.isBackspace(keyCode) ||
                    KeyboardUtil.isDelete(KeyCodes.KEY_DELETE)) {

                if (!filters.isEmpty()) {
                    for (InputFilter filter : filters) {
                        String character = String.valueOf(KeyboardUtil.getKeyCode(event.getNativeEvent()));
                        if (!filter.isAllowedValue(character)) {
                            event.preventDefault();
                            return;
                        }
                    }
                }
            }
        });

        super.addKeyUpHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                if (!getValue().equals(valueType.getValue())) {
                    PasswordField.this.valueType.setValue(getValue());
                }

                if (!KeyboardUtil.isTab(KeyboardUtil.getKeyCode(event.getNativeEvent()))) {
                    validate(new ValidationCompletedHandler() {
                        @Override
                        public void completed(ValidationResult result) {

                        }
                    });
                }

            }
        });

        addValueChangeHandler(new ValueChangeHandler<String>() {
            @Override
            public void onValueChange(ValueChangeEvent<String> event) {
                PasswordField.this.valueType.setValue(getValue());
                validate(new ValidationCompletedHandler() {
                    @Override
                    public void completed(ValidationResult result) {

                    }
                });
            }
        });

        valueType.addValueChangedEventHandler(new ValueChangedEventHandler<StringType>(this) {
            @Override
            public void onValueCanged(ValueChangedEvent<StringType> event) {

                if (event.getValueType().getValue() != getValue()) {
                    setValue(event.getValueType().getValue());
                }
            }
        });

        setValue(valueType.getValue());
        setKey(key);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_PASSWORD_SUFFIX;
    }

    public void setId(String id) {
        this.getElement().setId(id);
    }

    public void setValue(String value) {
        if (isAllowed(value)) {
            super.setValue(value);
            validate((result) -> {});
        }
    }

    private boolean isAllowed(String value) {
        if (!filters.isEmpty()) {
            for (InputFilter filter : filters) {
                if (!filter.isAllowedValue(value)) {
                    return false;
                }
            }
        }

        return true;
    }

    public void onBrowserEvent(Event event) {
        super.onBrowserEvent(event);

        switch (event.getTypeInt()) {
            case Event.ONPASTE:
                Scheduler.get().scheduleDeferred(() -> ValueChangeEvent.fire(PasswordField.this, PasswordField.this.getValue()));
                break;
            case Event.ONBLUR:
                validate((result) -> {});
        }
    }


    public PasswordField addFilter(InputFilter filter) {
        this.filters.add(filter);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public ValidationEventBusController getValidationEventbus() {
        return eventbus;
    }

    /**
     * {@inheritDoc}
     */
    public ArrayList<ValidationRule> getValidationRules() {
        return this.rules;
    }
}
