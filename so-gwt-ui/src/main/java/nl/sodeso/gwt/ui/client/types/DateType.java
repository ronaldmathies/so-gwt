package nl.sodeso.gwt.ui.client.types;

import nl.sodeso.gwt.ui.client.util.DateFormatterUtil;

import java.util.Date;

/**
 * @author Ronald Mathies
 */
public class DateType extends ValueType<Date> {

    /**
     * Constructs a new instance with an unspecified value.
     */
    public DateType() {}

    /**
     * Constructs a new instance with the specified value.
     *
     * @param value the initial value.
     */
    public DateType(Date value) {
        super(value);
    }

    /**
     * {@inheritDoc}
     */
    public void setValueAsString(String value) {
        setValue(DateFormatterUtil.parseDate(value));
    }

}
