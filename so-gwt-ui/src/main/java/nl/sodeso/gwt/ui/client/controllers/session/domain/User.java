package nl.sodeso.gwt.ui.client.controllers.session.domain;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class User implements Serializable {

    private String username;

    private String firstname;
    private String lastname;
    private String email;

    public User() {}

    public User(String username) {
        this.username = username;
    }

    public String getUsername() {
        return this.username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
