package nl.sodeso.gwt.ui.client.dialog;

/**
 * @author Ronald Mathies
 */
public interface SaveChangesHandler {

    public void confirmed();

}
