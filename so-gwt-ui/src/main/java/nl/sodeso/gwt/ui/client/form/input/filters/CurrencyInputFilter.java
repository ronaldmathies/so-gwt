package nl.sodeso.gwt.ui.client.form.input.filters;

import com.google.gwt.i18n.client.LocaleInfo;

/**
 * @author Ronald Mathies
 */
public class CurrencyInputFilter extends NumericInputFilter{

    @Override
    public boolean isAllowedValue(String character) {
        if (super.isAllowedValue(character)) {

            // Retrieve the decimal seperator based on the locale information.
            String decimalSeparator = LocaleInfo.getCurrentLocale().getNumberConstants().decimalSeparator();

            if (character.equalsIgnoreCase(decimalSeparator)) {
                return true;
            }
        }

        return false;
    }
}
