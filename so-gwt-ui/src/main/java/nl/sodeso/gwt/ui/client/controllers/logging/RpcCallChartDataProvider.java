package nl.sodeso.gwt.ui.client.controllers.logging;

//import com.arcadiacharts.model.datatypes.IntDataType;
//import com.arcadiacharts.model.datatypes.StringDataType;
//import com.arcadiacharts.model.values.Value2D;
//import com.arcadiacharts.model.values.ValueSet;
//import com.arcadiacharts.model.values.ValueSetImpl;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.json.client.*;
import com.google.gwt.user.client.Window;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

import java.util.*;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class RpcCallChartDataProvider {

    private static final DateTimeFormat timeFormatter = DateTimeFormat.getFormat("HH:mm:ss");
    private static final int MAX_CALL_ENTRIES = 10;

    private Logger logger = Logger.getLogger("rpc");

    private RpcCallChartDataProviderChanged rpcCallChartDataProviderChanged = null;

    private SortedMap<String, ArrayList<RpcCall>> rpcCallsMap = new TreeMap<>((o1, o2) -> {
        return o1.compareTo(o2);
    });

    private ArrayList<RpcCall> activeDataset = null;
    private String activeDatasetMethodName = null;

    public RpcCallChartDataProvider() {
        addLoggingHandler();
    }

    public void setRpcCallChartDataProviderChanged(RpcCallChartDataProviderChanged rpcCallChartDataProviderChanged) {
        this.rpcCallChartDataProviderChanged = rpcCallChartDataProviderChanged;
    }

    public void setActiveDataset(DefaultOption method) {
        if (method != null && rpcCallsMap.containsKey(method.getKey())) {
            activeDataset = rpcCallsMap.get(method.getKey());
            activeDatasetMethodName = method.getKey();
        }
    }

    public List<DefaultOption> getMethodOptions() {
        List<DefaultOption> methods = new ArrayList<>();
        for (String method : rpcCallsMap.keySet()) {
            methods.add(new DefaultOption(method));
        }

        return methods;
    }

//    public ValueSet getActiveChartData() {
//        List<Value2D> values = new ArrayList<>();
//
//        for (RpcCall call : activeDataset) {
//            values.add(new Value2D(new StringDataType(call.getTime()), new IntDataType((int)call.getDuration())));
//
//        }
//        return new ValueSetImpl(values);
//    }

    private void addLoggingHandler() {
        logger.addHandler(new Handler() {
            @Override
            public void publish(LogRecord record) {
                RpcCall rpcCall = new RpcCall();

                boolean success = true;
                JSONValue jsonValue = JSONParser.parseStrict(record.getMessage());
                if (jsonValue != null) {

                    JSONObject object = jsonValue.isObject();
                    if (object != null) {

                        JSONValue method = object.get("method");
                        JSONString string;
                        if ((string = method.isString()) != null) {
                            rpcCall.setMethod(string.stringValue());
                        } else {
                            success = false;
                        }

                        JSONValue duration = object.get("duration");
                        JSONNumber number;
                        if ((number = duration.isNumber()) != null) {
                            rpcCall.setDuration(number.doubleValue());
                        } else {
                            success = false;
                        }
                    } else {
                        success = false;
                    }
                } else {
                    success = false;
                }

                if (success) {
                    rpcCall.setTime(timeFormatter.format(new Date(record.getMillis())));

                    if (!rpcCallsMap.containsKey(rpcCall.getMethod())) {
                        rpcCallsMap.put(rpcCall.getMethod(), new ArrayList<RpcCall>());
                        rpcCallChartDataProviderChanged.methodsChanged();
                    }

                    List<RpcCall> rpcCalls = rpcCallsMap.get(rpcCall.getMethod());

                    if (rpcCalls.size() > MAX_CALL_ENTRIES) {
                        rpcCalls.remove(0);
                    }

                    rpcCalls.add(rpcCall);

                    // Add it twice, otherwise the graph can't display a line.
                    if (rpcCalls.size() == 1) {
                        rpcCalls.add(rpcCall);
                    }

                    if (rpcCall.getMethod().equals(activeDatasetMethodName)) {
                        rpcCallChartDataProviderChanged.activeDatasetChanged();
                    }
                } else {
                    Window.alert("Fail: " + record.getMessage());
                }
            }

            @Override
            public void flush() {
                // ignore
            }

            @Override
            public void close() {
                // ignore
            }
        });
    }
}