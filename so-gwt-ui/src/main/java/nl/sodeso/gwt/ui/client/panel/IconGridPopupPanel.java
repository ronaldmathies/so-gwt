package nl.sodeso.gwt.ui.client.panel;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public abstract class IconGridPopupPanel extends PopupPanel {

    // CSS style classes.
    private static final String STYLE_SWITCH_POPUP = "switch-popup";
    private static final String STYLE_SWITCH_CONTENTS = "contents";
    private static final String STYLE_SWITCH_ICON = "icon";

    // Element attribute names.
    private static final String ATTR_ICON_DISABLED = "so-disabled";

    public IconGridPopupPanel(boolean autoHide, boolean modal) {
        super(autoHide, modal);
    }

    @SuppressWarnings("unchecked")
    public void switchModule() {
        this.addStyleName(STYLE_SWITCH_POPUP);

        FlowPanel contents = new FlowPanel();
        contents.addStyleName(STYLE_SWITCH_CONTENTS);
        this.add(contents);

        for (final IconGridItem item : getItems()) {

            FlowPanel icon = new FlowPanel();
            icon.setStyleName(STYLE_SWITCH_ICON);

            if (isSelected(item)) {
                icon.getElement().setAttribute(ATTR_ICON_DISABLED, "true");
            }  else {
                icon.addDomHandler((event) -> {
                    hide();
                    item.getTrigger().fire();
                }, ClickEvent.getType());
            }

            // Create the icon contents.
            Element iconElement = Document.get().createElement("I");
            iconElement.addClassName(item.getIcon().getStyle());
            icon.getElement().appendChild(iconElement);
            contents.add(icon);
        }
    }

    public abstract List<IconGridItem> getItems();

    public abstract boolean isSelected(IconGridItem iconGridItem);

    public void center() {


        super.center();
    }

    public void setPopupPositionAndShow(PositionCallback callback) {

        super.setPopupPositionAndShow(callback);
    }

    public void show() {

        super.show();
    }


}
