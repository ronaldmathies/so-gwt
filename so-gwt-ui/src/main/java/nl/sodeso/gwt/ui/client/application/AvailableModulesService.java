package nl.sodeso.gwt.ui.client.application;

import nl.sodeso.gwt.ui.client.application.module.ModuleEntryPoint;

import java.util.List;

/**
 * Class implementation created through GWT deferred binding,
 * upon compilation GWT will execute the ModuleServiceGenerator
 * which in turn will locate all modules extending the ModuleEntryPoint class.
 *
 * @author Ronald Mathies
 */
public interface AvailableModulesService {

    /**
     * Returns a list containing all modules available.
     * @return list of modules.
     */
    List<ModuleEntryPoint> availableModules();

}
