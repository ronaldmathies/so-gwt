package nl.sodeso.gwt.ui.client.rpc;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class RemoteException extends Exception implements Serializable {

    public RemoteException() {}

    public RemoteException(String message) {
        super(message);
    }

}
