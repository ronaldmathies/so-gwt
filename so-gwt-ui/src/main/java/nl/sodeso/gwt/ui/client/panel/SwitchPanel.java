package nl.sodeso.gwt.ui.client.panel;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.util.HasKey;

import java.util.ArrayList;
import java.util.List;

import static nl.sodeso.gwt.ui.client.util.Constants.FALSE;
import static nl.sodeso.gwt.ui.client.util.Constants.TRUE;

/**
 * The switch panel allows you to have multiple panels within a single container and have
 * only one panel active at a time, by using the {@link #switchTo(String)} method you can
 * active a panel while all other panels are hidden.
 *
 * @author Ronald Mathies
 */
public class SwitchPanel extends FlowPanel implements IsValidationContainer, IsRevertable, HasKey {

    private static final String KEY_SWITCH_PANEL_SUFFIX = "-switch-panel";

    // CSS style classes.
    private static final String STYLE_SWITCH_PANEL = "switch-panel";

    // Element attribute names.
    private static final String ATTR_VISIBLE = "so-visible";

    private List<Widget> widgets = new ArrayList<>();

    private boolean validateActivePanelOnly = false;

    public SwitchPanel(String key, boolean validateActivePanelOnly) {
        this.addStyleName(STYLE_SWITCH_PANEL);

        this.validateActivePanelOnly = validateActivePanelOnly;
        this.setKey(key);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_SWITCH_PANEL_SUFFIX;
    }

    /**
     * Add's all the widgets at once.
     * @param widgets the widgets to add.
     * @return this instance.
     */
    public SwitchPanel addWidgets(Widget ... widgets) {
        for (Widget widget : widgets) {
            addWidget(widget);
        }

        return this;
    }

    /**
     * Add a widget which can be activated.
     * @param widget the widget to add.
     * @return this instance.
     */
    public SwitchPanel addWidget(Widget widget) {

        if (!(widget instanceof HasKey)) {
            throw new RuntimeException("Argument widget should implement HasKey and extends Widget.");
        }

        this.widgets.add(widget);

        setWidgetVisible(widget, false);
        add(widget);

        return this;
    }

    /**
     * Makes the panel which has the specified key active and inactivates all other panels.
     * @param key the key of the panel to activate.
     */
    public void switchTo(String key) {
        for (Widget widget : widgets) {
            setWidgetVisible(widget, ((HasKey)widget).getKey().equals(key));
        }
    }

    private void setWidgetVisible(Widget widget, boolean visible) {
        widget.getElement().setAttribute(ATTR_VISIBLE, visible ? TRUE : FALSE);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        if (validateActivePanelOnly) {
            for (Widget widget : widgets) {
                boolean isVisible = !FALSE.equals(widget.getElement().getAttribute(ATTR_VISIBLE));
                if (isVisible) {
                    ValidationUtil.validate(handler, widget);
                }
            }
        } else {
            ValidationUtil.validate(handler, widgets);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(widgets);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        if (validateActivePanelOnly) {
            for (Widget widget : widgets) {
                boolean isVisible = !FALSE.equals(widget.getElement().getAttribute(ATTR_VISIBLE));
                if (isVisible) {
                    return RevertUtil.hasChanges(widget);
                }
            }
        }

        return RevertUtil.hasChanges(widgets);
    }
}
