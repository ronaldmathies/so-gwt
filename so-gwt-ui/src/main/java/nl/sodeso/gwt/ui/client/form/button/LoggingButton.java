package nl.sodeso.gwt.ui.client.form.button;

import nl.sodeso.gwt.ui.client.controllers.logging.LoggingController;
import nl.sodeso.gwt.ui.client.resources.Resources;

/**
 * Button representing an <code>Logging</code> button.
 *
 * @author Ronald Mathies
 */
public class LoggingButton extends SimpleButton {

    // Keys for so-key entries.
    public static final String KEY = "logging";

    /**
     * Constructs a new logging button with default title and style.
     *
     * When clicked it will display the logging window.
     */
    public LoggingButton() {
        this(Resources.BUTTON_I18N.Logging(), Style.RED);
    }

    /**
     * Constructs a new logging button.
     *
     * When clicked it will display the logging window.
     *
     * @param title the title of the button.
     * @param style the color of the button.
     */
    public LoggingButton(String title, Style style) {
        super(KEY, title, style);

        addClickHandler(event -> LoggingController.instance().showLoggingWindow());
    }

}
