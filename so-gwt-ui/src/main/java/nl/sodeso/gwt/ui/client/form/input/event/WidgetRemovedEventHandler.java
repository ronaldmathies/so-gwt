package nl.sodeso.gwt.ui.client.form.input.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * @author Ronald Mathies
 */
public interface WidgetRemovedEventHandler extends EventHandler {

    void onEvent(WidgetRemovedEvent event);

}
