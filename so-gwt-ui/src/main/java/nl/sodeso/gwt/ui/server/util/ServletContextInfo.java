package nl.sodeso.gwt.ui.server.util;

/**
 * @author Ronald Mathies
 */
public class ServletContextInfo {

    private static ServletContextInfo INSTANCE = null;

    private String contextPath = null;

    private ServletContextInfo() {}

    public static ServletContextInfo instance() {
        if (INSTANCE == null) {
            INSTANCE = new ServletContextInfo();
        }

        return INSTANCE;
    }

    public String getContextPath() {
        return this.contextPath;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }

}
