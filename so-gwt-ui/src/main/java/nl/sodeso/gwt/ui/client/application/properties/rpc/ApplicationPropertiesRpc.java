package nl.sodeso.gwt.ui.client.application.properties.rpc;

import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.gwt.ui.client.application.properties.ClientApplicationProperties;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
public interface ApplicationPropertiesRpc extends XsrfProtectedService {

    ClientApplicationProperties getProperties();

}
