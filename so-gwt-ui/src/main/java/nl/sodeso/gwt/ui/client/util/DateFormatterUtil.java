package nl.sodeso.gwt.ui.client.util;

import com.google.gwt.i18n.client.DateTimeFormat;

import java.util.Date;

/**
 * @author Ronald Mathies
 */
public class DateFormatterUtil {

    private static final int MINUTE = 60;
    private static final int HOUR = 60 * 60;
    private static final int DAY = 60 * 60 * 24;

    public static DateTimeFormat dateShort = DateTimeFormat.getFormat(DateTimeFormat.PredefinedFormat.DATE_SHORT);

    public static Date parseDate(String date) {
        return dateShort.parse(date);
    }

    public static String formatDate(Date date) {
        return dateShort.format(date);
    }

    public static String formatTimePassed(Date date) {
        long seconds = (new Date().getTime()-date.getTime())/1000;

        if (seconds < MINUTE) {
            return seconds + " secs";
        } else if (seconds < HOUR) {
            return Math.round(seconds / MINUTE) + " mins";
        } else if (seconds < DAY) {
            return Math.round(seconds / HOUR) + " hrs";
        }

        return "";
    }
}
