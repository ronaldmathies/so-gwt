package nl.sodeso.gwt.ui.client.form.input;

import nl.sodeso.gwt.ui.client.controllers.center.CenterController;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithSingleWidget;
import nl.sodeso.gwt.ui.client.form.button.CancelButton;
import nl.sodeso.gwt.ui.client.form.button.OkButton;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.Align;

/**
 * @author Ronald Mathies
 */
public class TextAreaZoomWindow extends PopupWindowPanel {

    private static final String PROP_MIN_WIDTH = "min-width";
    private static final String PROP_MIN_HEIGHT = "min-height";


    private StringType fieldType = new StringType();
    private TextAreaField textAreaField = null;

    public TextAreaZoomWindow(String title, TextAreaField field) {
        super("textarea-zoomed", title, WindowPanel.Style.INFO);

        this.textAreaField = field;

        String key = this.textAreaField.getKey();

        TextAreaField zoomedTextAreaField = new TextAreaField(key, fieldType);
        com.google.gwt.dom.client.Style style = zoomedTextAreaField.getElement().getStyle();
        style.setProperty(PROP_MIN_WIDTH, (int)(CenterController.instance().getWidth() * 0.6), com.google.gwt.dom.client.Style.Unit.PX);
        style.setProperty(PROP_MIN_HEIGHT, (int)(CenterController.instance().getHeight() * 0.6), com.google.gwt.dom.client.Style.Unit.PX);
        zoomedTextAreaField.setEnabled(textAreaField.isEnabled());
        zoomedTextAreaField.setReadOnly(textAreaField.isReadOnly());

        this.fieldType.setValue(this.textAreaField.getValue());
        addToBody(new EntryForm(null).addEntry(new EntryWithSingleWidget(null, zoomedTextAreaField)));

        addToFooter(Align.RIGHT, new OkButton.WithLabel((event) -> yes()));

        if (zoomedTextAreaField.isEnabled() && !zoomedTextAreaField.isReadOnly()) {
            addToFooter(Align.RIGHT, new CancelButton.WithLabel((event) -> close()));

            zoomedTextAreaField.setFocus(true);
        }

    }

    private void yes() {
        close();
        this.textAreaField.setValue(this.fieldType.getValue());
    }

}
