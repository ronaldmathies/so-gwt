package nl.sodeso.gwt.ui.client.form.revertable;

import java.util.Collection;

/**
 * @author Ronald Mathies
 */
public class RevertUtil {

    /**
     * Executes the revertible executors.
     * @param executors the revertible executors to execute.
     */
    public static void revert(RevertableExecutor ... executors) {
        for (RevertableExecutor executor : executors) {
            executor.revert();
        }
    }

    /**
     * Executes the revertible executors.
     * @param executors the revertible executors to execute.
     */
    public static void revert(Collection<RevertableExecutor> executors) {
        for (RevertableExecutor executor : executors) {
            executor.revert();
        }
    }

    /**
     * Reverts the object, in case the object is a collection or array it will try to
     * revert every entry within the collection. Otherwise it will the to revert the
     * object itself.
     *
     * @param object the object or collection / array to revert.
     */
    public static void revert(Object object) {
        if (object instanceof Collection) {
            revertCollection((Collection) object);
        } else if (object instanceof Object[]) {
            revertArray((Object[]) object);
        } else {
            revertSingle(object);
        }
    }

    /**
     * Tries to revert all the objects within the collection.
     *
     * @param objects the objects to revert.
     */
    private static void revertCollection(Collection objects) {
        for (Object object : objects) {
            revertSingle(object);
        }
    }

    /**
     * Tries to revert all the objects within the array.
     *
     * @param objects the objects to revert.
     */
    private static void revertArray(Object[] objects) {
        for (Object object : objects) {
            revertSingle(object);
        }
    }

    /**
     * Tries to revert the object, if the object implements the <code>IsRevertable</code>.
     * @param object the object to revert.
     */
    private static void revertSingle(Object object) {
        if (object instanceof IsRevertable) {
            ((IsRevertable) object).revert();
        }
    }

    public static boolean hasChanges(Object object) {
        if (object instanceof Collection) {
            return hasChangesCollection((Collection) object);
        } else if (object instanceof Object[]) {
            return hasChangesArray((Object[]) object);
        } else {
            return hasChangesSingle(object);
        }
    }


    private static boolean hasChangesCollection(Collection objects) {
        for (Object object : objects) {
            if (hasChangesSingle(object)) {
                return true;
            }
        }

        return false;
    }

    private static boolean hasChangesArray(Object[] objects) {
        for (Object object : objects) {
            if (hasChangesSingle(object)) {
                return true;
            }
        }

        return false;
    }

    private static boolean hasChangesSingle(Object object) {
        if (object instanceof IsRevertable) {
            return ((IsRevertable) object).hasChanges();
        }

        return false;
    }


}
