package nl.sodeso.gwt.ui.client.form.input;

import com.google.gwt.user.client.Timer;
import nl.sodeso.gwt.ui.client.form.input.event.SearchValueChangedEvent;
import nl.sodeso.gwt.ui.client.form.input.event.SearchValueChangedEventHandler;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.types.ValueType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class SearchField<V extends ValueType> extends TextField<V> {

    private Timer timer = null;
    private int minimumNumberOfCharacters = 3;

    private String oldValue = "";

    private ArrayList<SearchValueChangedEventHandler> searchValueChangedEventHandlers = new ArrayList<>();

    private StringType valueType = null;

    public SearchField(String key, V valueType) {
        this(key, valueType, 3);
    }

    @SuppressWarnings("unchecked")
    public SearchField(String key, V valueType, int minimumNumberOfCharacters) {
        super(key, valueType);

        this.minimumNumberOfCharacters = minimumNumberOfCharacters;

        valueType.addValueChangedEventHandler(new ValueChangedEventHandler<V>(this) {
            @Override
            public void onValueCanged(ValueChangedEvent<V> event) {
                SearchField.this.onValueCanged(event);
            }
        });
    }

    public void addSearchValueChangedEventHandler(SearchValueChangedEventHandler handler) {
        this.searchValueChangedEventHandlers.add(handler);
    }

    public void onValueCanged(ValueChangedEvent<V> event) {
        // Check if we have reached the minimum amount of characters needed
        // to start a timer or update a timer, otherwise stop the current
        // running timer or do nothing.
        if (getValue().length() >= minimumNumberOfCharacters || getValue().isEmpty()) {

            // Check if we already have a timer and the timer is running,
            // if so then we have not reached the minimum amount of waiting
            // time so reset the timer and start over.
            if (timer != null && timer.isRunning()) {
                timer.cancel();
                timer.schedule(500);

                return;
            }

            // We don't have a timer yet so create one and schedule it.
            timer = new Timer() {
                @Override
                public void run() {
                    SearchValueChangedEvent event = new SearchValueChangedEvent(SearchField.this);
                    for (SearchValueChangedEventHandler handler : searchValueChangedEventHandlers) {
                        handler.onSearchValueChangedEvent(event);
                    }
                }
            };
            timer.schedule(500);
        } else {

            // We have not reached the minimum amount, check if a timer exists
            // and stop and remove it.
            if (timer != null && timer.isRunning()) {
                timer.cancel();
                timer = null;
            }
        }
    }
}
