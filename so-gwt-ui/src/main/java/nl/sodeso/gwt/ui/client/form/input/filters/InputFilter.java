package nl.sodeso.gwt.ui.client.form.input.filters;

/**
 * @author Ronald Mathies
 */
public interface InputFilter {

    boolean isAllowedValue(String character);

}
