package nl.sodeso.gwt.ui.client.controllers.logging.handler;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.logging.impl.StackTracePrintStream;
import nl.sodeso.gwt.event.client.EventBusController;
import nl.sodeso.gwt.ui.client.controllers.logging.LogItem;
import nl.sodeso.gwt.ui.client.controllers.logging.handler.event.LogItemEvent;
import nl.sodeso.gwt.ui.client.controllers.logging.handler.event.LogItemEventHandler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * @author Ronald Mathies
 */
public class LocalLogHandler extends Handler {

    private static final String RPC_LOGGER_NAME = "rpc";
    private static final DateTimeFormat timeFormatter = DateTimeFormat.getFormat("HH:mm:ss");

    private static LocalLogHandler instance;

    private List<String> excludedLoggerNames = new ArrayList<>();

    private EventBusController eventbus = new EventBusController();

    private LocalLogHandler() {
        excludedLoggerNames.add(RPC_LOGGER_NAME);
    }

    public static LocalLogHandler instance() {
        if (instance == null) {
            instance = new LocalLogHandler();
        }

        return instance;
    }

    public void addLogItemEventHandler(LogItemEventHandler handler) {
        this.eventbus.addHandler(LogItemEvent.TYPE, handler);
    }

    public void removeLogItemEventHandler(LogItemEventHandler handler) {
        this.eventbus.removeHandler(LogItemEvent.TYPE, handler);
    }

    public void fireLogItemEvent(LogItemEvent event) {
        this.eventbus.fireEvent(event);
    }


    @Override
    public void publish(LogRecord record) {
       if (isLoggable(record)) {
           LogItem item = new LogItem();

           item.setTime(timeFormatter.format(new Date(record.getMillis())));
           item.setMessage(record.getMessage());
           item.setLevel(record.getLevel().getName());
           item.setStackTrace(extractStackTrace(record));

           fireLogItemEvent(new LogItemEvent(item));
       }
    }

    private String extractStackTrace(LogRecord record) {
        StringBuilder stackTrace = new StringBuilder();
        Throwable thrown = record.getThrown();
        if (thrown != null) {
            thrown.printStackTrace(new StackTracePrintStream(stackTrace));
        }
        return stackTrace.toString();
    }

    @Override
    public boolean isLoggable(LogRecord record) {
        return (super.isLoggable(record) && !excludedLoggerNames.contains(record.getLoggerName()));
    }

    @Override
    public void flush() {
        // ignore
    }

    @Override
    public void close() {
        // ignore
    }
}
