package nl.sodeso.gwt.ui.client.controllers.session.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class SessionRpcGateway implements RpcGateway<SessionRpcAsync>, SessionRpcAsync {

    public SessionRpcAsync getRpcAsync() {
        return GWT.create(SessionRpc.class);
    }


}
