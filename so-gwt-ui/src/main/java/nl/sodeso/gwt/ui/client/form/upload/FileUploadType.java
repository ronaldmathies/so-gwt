package nl.sodeso.gwt.ui.client.form.upload;

import nl.sodeso.gwt.ui.client.types.ValueType;
import nl.sodeso.gwt.ui.client.util.MethodNotAllowedException;

/**
 * @author Ronald Mathies
 */
public class FileUploadType extends ValueType<FileUpload> {

    private FileUpload originalValue = null;
    private FileUpload value = null;

    public FileUploadType() {}

    public FileUploadType(FileUpload value) {
        this.originalValue = value;
        this.value = value;
    }

    /**
     * {@inheritDoc}
     */
    public FileUpload getValue() {
        return this.value;
    }

    /**
     * {@inheritDoc}
     */
    public String getValueAsString() {
        throw new MethodNotAllowedException();
    }

    /**
     * {@inheritDoc}
     */
    public void setValue(FileUpload value) {
        this.value = value;

        fireEvent();
    }

    /**
     * {@inheritDoc}
     */
    public void setValueAsString(String value) {
        throw new MethodNotAllowedException();
    }

    /**
     * {@inheritDoc}
     */
    public FileUpload getOriginalValue() {
        return this.originalValue;
    }
}
