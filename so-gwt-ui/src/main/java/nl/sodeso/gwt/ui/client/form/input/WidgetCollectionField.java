package nl.sodeso.gwt.ui.client.form.input;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import nl.sodeso.gwt.event.client.EventBusController;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithWidget;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.button.*;
import nl.sodeso.gwt.ui.client.form.input.event.*;
import nl.sodeso.gwt.ui.client.form.revertable.RevertableExecutor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

/**
 * The widget collection field is used for displaying a user interface object that has the following abilities:
 *
 * <ul>
 *     <li>Add, remove widgets.</li>
 *     <li>When enabled change the ordering of the widgets displayed.</li>
 * </ul>
 *
 * @author Ronald Mathies
 */
@SuppressWarnings("unused")
public abstract class WidgetCollectionField<T extends WidgetCollectionFieldWidget> implements Serializable {

    private static final String KEY_WIDGET_COLLECTION = "widget-collection";
    private static final String KEY_WIDGETS = "widgets";
    private static final String KEY_ADD_WIDGET = "add-widget";

    private ArrayList<T> widgets = new ArrayList<>();
    private ArrayList<T> deletedWidgets = new ArrayList<>();

    private WidgetCollectionConfig config = WidgetCollectionConfig.DEFAULT;

    private transient EntryForm entryForm = null;
    private transient EntryForm widgetsForm = null;

    private transient EventBusController eventbus = null;

    public WidgetCollectionField() {
    }

    /**
     * Constructs a new widget collection field.
     *
     * @param config the widget configuration.
     */
    public WidgetCollectionField(WidgetCollectionConfig config) {
        this.config = config;
    }

    public EntryForm toEditForm() {
        if (entryForm != null) {
            return this.entryForm;
        }

        this.eventbus = new EventBusController();

        this.entryForm = new EntryForm(KEY_WIDGET_COLLECTION);
        this.widgetsForm = new EntryForm(KEY_WIDGETS);
        this.widgetsForm.addAttachHandler(event -> {
            if (event.isAttached()) {
                setSwitchingButtonStates();
            }
        });
        this.entryForm.addEntry(new EntryWithContainer(KEY_WIDGET_COLLECTION, this.widgetsForm));

        for (T fromWidget : widgets) {
            addWidgetToWidgetsForm(fromWidget, false);
        }

        EntryWithWidgetAndWidget widgets = new EntryWithWidgetAndWidget(KEY_ADD_WIDGET, null, null);
        if (config.isEnableAdding()) {
            this.entryForm.addEntry(widgets.addButton(new AddButton.WithIcon((event) -> addNewWidget())));
        }

        this.entryForm.addAttachHandler(event -> {
            if (event.isAttached()) {
                if (config.isAutoAddFirst() && this.widgets.isEmpty()) {
                    addNewWidget();
                }
            }
        });

        if (config.isEnableSwitching()) addWidgetSwitchedEventHandler(event -> setSwitchingButtonStates());
        if (config.isEnableDeleting()) addWidgetRemovedEventHandler(event -> setSwitchingButtonStates());
        if (config.isEnableAdding()) addWidgetAddedEventHandler(event -> setSwitchingButtonStates());

        return this.entryForm;
    }

    private void addNewWidget() {
        WidgetSupplier<T> widgetSupplier = createWidgetSupplier();
        widgetSupplier.create(widget -> {
            widgets.add(widget);
            addWidgetToWidgetsForm(widget, true);
        });
    }

    private void addWidgetToWidgetsForm(final T widget, final boolean addRevertableExecutor) {
        final EntryWithWidget entry = widget.toEntry();

        // If the fromWidget is user added then add a revertible executor so we can remove it when the user
        // wants to revert their changes.
        RevertableExecutor revertableExecutor = null;
        if (addRevertableExecutor) {
            revertableExecutor = () -> {
                widgets.remove(widget);
                widgetsForm.removeEntry(entry);

                eventbus.fireEvent(new WidgetRemovedEvent(this, widget));
            };

            widgetsForm.addRevertableExecutor(revertableExecutor);
        }

        if (config.isEnableDeleting()) {
            entry.addButton(new RemoveButton.WithIcon(new RemoveEntryClickHandler(entry, widget, revertableExecutor)));
        }

        if (config.isEnableSwitching()) {
            entry.addButton(new UpButton.WithIcon(new SwitchEntriesClickHandler(widget, false)));
            entry.addButton(new DownButton.WithIcon(new SwitchEntriesClickHandler(widget, true)));
        }

        widgetsForm.addEntry(entry);

        // Fire an event that a fromWidget has been added.
        eventbus.fireEvent(new WidgetAddedEvent(this, widget));
    }

    private void setSwitchingButtonStates() {
        if (config.isEnableSwitching()) {
            for (int index = 0; index < widgetsForm.getEntryCount(); index++) {
                EntryWithWidget entry = (EntryWithWidget) widgetsForm.getEntry(index);
                ArrayList<SimpleButton> buttons = entry.getButtons();
                for (SimpleButton button : buttons) {
                    if (button instanceof UpButton.WithIcon) {
                        button.setVisible(index > 0);
                    } else if (button instanceof DownButton.WithIcon) {
                        button.setVisible(index < widgets.size() - 1);
                    }
                }
            }
        }
    }

    /**
     * Add an event handler which will be notified when a fromWidget has been added.
     * @param handler the handler to be notifier.
     * @return this instance.
     */
    public WidgetCollectionField<T> addWidgetAddedEventHandler(WidgetAddedEventHandler handler) {
        this.eventbus.addHandler(WidgetAddedEvent.TYPE, handler);

        return this;
    }

    /**
     * Add an event handler which will be notified when a fromWidget has been removed.
     * @param handler the handler to be notifier.
     * @return this instance.
     */
    public WidgetCollectionField<T> addWidgetRemovedEventHandler(WidgetRemovedEventHandler handler) {
        this.eventbus.addHandler(WidgetRemovedEvent.TYPE, handler);

        return this;
    }

    /**
     * Add an event handler which will be notified when a fromWidget has been switched.
     * @param handler the handler to be notifier.
     * @return this instance.
     */
    public WidgetCollectionField<T> addWidgetSwitchedEventHandler(WidgetSwitchedEventHandler handler) {
        this.eventbus.addHandler(WidgetSwitchedEvent.TYPE, handler);

        return this;
    }

    private class SwitchEntriesClickHandler implements ClickHandler {

        private boolean down = false;
        private T fromWidget = null;

        SwitchEntriesClickHandler(T fromWidget, boolean down) {
            this.fromWidget = fromWidget;
            this.down = down;
        }

        public void onClick(ClickEvent event) {

            int fromIndex = widgets.indexOf(fromWidget);
            int toIndex = down ? fromIndex + 1 : fromIndex - 1;
            T toWidget = widgets.get(toIndex);

            Collections.swap(widgets, fromIndex, toIndex);
            widgetsForm.switchEntry(fromIndex, toIndex);

            eventbus.fireEvent(new WidgetSwitchedEvent(WidgetCollectionField.this, fromWidget, toWidget, fromIndex, toIndex));
        }

    }

    private class RemoveEntryClickHandler implements ClickHandler {

        private EntryWithWidget entry = null;
        private T widget = null;
        private RevertableExecutor revertableExecutor;

        RemoveEntryClickHandler(EntryWithWidget entry, T widget, RevertableExecutor revertableExecutor) {
            this.entry = entry;
            this.widget = widget;
            this.revertableExecutor = revertableExecutor;
        }

        public void onClick(ClickEvent event) {
            // Remove the row and either remove it completely since it was created in the first place or
            // put it asside so we can restore it when the user wants to reverts their changes.
            if (revertableExecutor != null) {
                widgets.remove(widget);
                widgetsForm.removeEntry(entry);

                widgetsForm.removeRevertableExecutor(revertableExecutor);

                eventbus.fireEvent(new WidgetRemovedEvent(WidgetCollectionField.this, widget));
            } else {
                widgets.remove(widget);
                deletedWidgets.add(widget);
                entry.setVisible(false);

                widgetsForm.addRevertableExecutor(() -> {
                    widgets.add(widget);
                    deletedWidgets.remove(widget);
                    entry.setVisible(true);

                    eventbus.fireEvent(new WidgetAddedEvent(WidgetCollectionField.this, widget));
                });

                eventbus.fireEvent(new WidgetRemovedEvent(WidgetCollectionField.this, widget));
            }
        }
    }

    public ArrayList<T> getWidgets() {
        return this.widgets;
    }

    public void setWidgets(ArrayList<T> widgets) {
        this.widgets = widgets;
    }

    public ArrayList<T> getDeletedWidgets() {
        return deletedWidgets;
    }

    protected abstract WidgetSupplier<T> createWidgetSupplier();

}
