package nl.sodeso.gwt.ui.client.controllers.session.event;

import com.google.web.bindery.event.shared.Event;
import nl.sodeso.gwt.ui.client.controllers.session.domain.LoginResult;


/**
 * @author Ronald Mathies
 */
public class UserLoginEvent extends Event<UserLoginEventHandler> {

    public static final Type<UserLoginEventHandler> TYPE = new Type<>();

    private LoginResult result;

    public UserLoginEvent(LoginResult result) {
        this.result = result;
    }

    @Override
    public Type<UserLoginEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(UserLoginEventHandler handler) {
        handler.onEvent(this);
    }

    public LoginResult getLoginResult() {
        return this.result;
    }
}
