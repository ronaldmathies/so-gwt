package nl.sodeso.gwt.ui.client.form.input;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import nl.sodeso.gwt.ui.client.form.input.event.CheckboxClickEvent;
import nl.sodeso.gwt.ui.client.form.input.event.CheckboxClickEventHandler;
import nl.sodeso.gwt.ui.client.form.validation.IsValidatable;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationEventBusController;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.rules.ValidationRule;
import nl.sodeso.gwt.ui.client.util.Align;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class CheckboxGroupField extends FlowPanel implements IsValidatable {

    private String key;

    private ArrayList<CheckboxClickEventHandler> clickHandlers = new ArrayList<>();
    private ArrayList<CheckboxField> checkboxes = new ArrayList<>();

    private ArrayList<ValidationRule> rules = new ArrayList<>();
    private ValidationEventBusController eventbus = new ValidationEventBusController();

    public CheckboxGroupField(String key) {
        super();

        this.key = key;
    }

    public void addClickHandler(CheckboxClickEventHandler handler) {
        this.clickHandlers.add(handler);
    }

    public CheckboxField addCheckbox(String key, String label) {
        final CheckboxField checkboxField = new CheckboxField(key, label);

        checkboxField.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                CheckboxClickEvent event = new CheckboxClickEvent(checkboxField, clickEvent);
                for (CheckboxClickEventHandler handler : clickHandlers) {
                    handler.onClick(event);
                }

                validate(new ValidationCompletedHandler() {
                    @Override
                    public void completed(ValidationResult result) {

                    }
                });
            }
        });

        this.checkboxes.add(checkboxField);

        add(checkboxField);

        return checkboxField;
    }

    public void check(String key, boolean isChecked, boolean fireEvent) {
        for (CheckboxField checkboxField : checkboxes) {
            if (key.equals(checkboxField.getKey())) {
                check(checkboxField, isChecked, fireEvent);

                validate(new ValidationCompletedHandler() {
                    @Override
                    public void completed(ValidationResult result) {

                    }
                });
            }
        }
    }

    public void check(CheckboxField checkboxField, boolean isChecked, boolean fireEvent) {
        checkboxField.setValue(isChecked);
    }

    public void setEnabled(boolean enabled) {
        for (CheckboxField checkboxField : checkboxes) {
            checkboxField.setEnabled(enabled);
        }
    }

    public void setAlignment(Align align) {
        for (CheckboxField checkboxField : checkboxes) {
            checkboxField.setAlignment(align);
        }
    }

    public String getKey() {
        return this.key;
    }

    /**
     * {@inheritDoc}
     */
    public ValidationEventBusController getValidationEventbus() {
        return eventbus;
    }

    /**
     * {@inheritDoc}
     */
    public ArrayList<ValidationRule> getValidationRules() {
        return this.rules;
    }

}