package nl.sodeso.gwt.ui.client.util;

/**
 * @author Ronald Mathies
 */
public class StringUtils {

    /**
     * Flag indicating if the specified value is null or is empty.
     * @param value the value to check.
     * @return true if null or empty, false if not null and containing a value.
     */
    public static boolean isEmpty(String value) {
        return value == null || value.isEmpty();
    }

    /**
     * Flag indicating if the specified value is not null and contains a value.
     * @param value the value to check.
     * @return true if the string contains a value, false if not.
     */
    public static boolean isNotEmpty(String value) {
        return !isEmpty(value);
    }

}
