package nl.sodeso.gwt.ui.client.form.input.event;

import com.google.web.bindery.event.shared.Event;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionFieldWidget;

/**
 * @author Ronald Mathies
 */
public class WidgetRemovedEvent extends Event<WidgetRemovedEventHandler> {

    public static final Event.Type<WidgetRemovedEventHandler> TYPE = new Event.Type<>();

    private WidgetCollectionField source;
    private WidgetCollectionFieldWidget widget;

    public WidgetRemovedEvent(WidgetCollectionField source, WidgetCollectionFieldWidget widget) {
        this.source = source;
        this.widget = widget;
    }

    @Override
    public Event.Type<WidgetRemovedEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(WidgetRemovedEventHandler handler) {
        handler.onEvent(this);
    }

    public WidgetCollectionField getSource() {
        return this.source;
    }

    public WidgetCollectionFieldWidget getWidget() {
        return this.widget;
    }
}