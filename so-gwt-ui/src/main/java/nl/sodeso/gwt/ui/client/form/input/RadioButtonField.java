package nl.sodeso.gwt.ui.client.form.input;

import com.google.gwt.user.client.ui.RadioButton;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.util.HasKey;
import nl.sodeso.gwt.ui.client.util.KeyUtil;

/**
 * @author Ronald Mathies
 */
public class RadioButtonField extends RadioButton implements HasKey {

    // Keys for so-key entries.
    protected final static String KEY_RADIO_BUTTON_SUFFIX = "-field";

    // Element attribute names.
    public final static String ATTR_ALIGNMENT = "so-alignment";

    protected RadioButtonField(String groupKey, String key, String label) {
        super(groupKey, label);
        setAlignment(Align.VERTICAL);

        setKey(key);
        // dit aanzetten heeft gevolgen voor het verwerken van de clicks en het zetten van de value
        // door Integer.parseInt op de key en Boolean.parseBoolean op de key.
//        setKey(key != null ? key + KEY_SUFFIX : null);
    }

    public void setAlignment(Align align) {
        if (!align.isHorizontal() && !align.isVertical()) {
            throw new RuntimeException("Only horizontal and vertical alignment is allowed");
        }

        getElement().setAttribute(ATTR_ALIGNMENT, align.getAlignment());
    }

    public Align getAlignment() {
        String value = getElement().getAttribute(ATTR_ALIGNMENT);
        return value == null || value.equals(Align.HORIZONTAL.getAlignment()) ? Align.HORIZONTAL : Align.VERTICAL;
    }

    /**
     * {@inheritDoc}
     */
    public void setKey(String key) {
        KeyUtil.setKey(getElement().getFirstChildElement(), key, KEY_RADIO_BUTTON_SUFFIX);
    }

    /**
     * {@inheritDoc}
     */
    public String getKey() {
        return KeyUtil.getKey(getElement().getFirstChildElement(), KEY_RADIO_BUTTON_SUFFIX);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_RADIO_BUTTON_SUFFIX;
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasKey() {
        return KeyUtil.hasKey(getElement().getFirstChildElement());
    }

}