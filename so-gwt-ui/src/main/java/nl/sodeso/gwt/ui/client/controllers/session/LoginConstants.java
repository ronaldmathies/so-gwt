package nl.sodeso.gwt.ui.client.controllers.session;

import com.google.gwt.i18n.client.Constants;

/**
 * @author Ronald Mathies
 */
public interface LoginConstants extends Constants {

    @DefaultStringValue("Login")
    String Title();

    @DefaultStringValue("Username")
    String Username();

    @DefaultStringValue("Password")
    String Password();

}
