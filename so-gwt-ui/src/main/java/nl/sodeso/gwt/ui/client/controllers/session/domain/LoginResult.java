package nl.sodeso.gwt.ui.client.controllers.session.domain;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class LoginResult implements Serializable {

    private User user = null;

    public LoginResult() {}

    public LoginResult(User user) {
        this.user = user;
    }

    public boolean isSuccess() {
        return user != null;
    }

    public User getUser() {
        return this.user;
    }

}
