package nl.sodeso.gwt.ui.client.application.module.properties.rpc;

import com.google.gwt.core.client.Duration;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.HasRpcToken;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.rpc.XsrfToken;
import nl.sodeso.gwt.ui.client.application.module.properties.ClientModuleProperties;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.rpc.XsrfGateway;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class ModulePropertiesGateway {

    private static Logger logger = Logger.getLogger("Application.Endpoint");

    private static ModulePropertiesRpcAsync modulePropertiesRpcAsync = GWT.create(ModulePropertiesRpc.class);

    private ModulePropertiesGateway() {}

    public static void getModuleProperties(final String moduleId, final AsyncCallback<ClientModuleProperties> callback) {
        logger.log(Level.FINE, "Performing retrieval of module properties.");

        XsrfGateway.getNewXsrfToken(new DefaultAsyncCallback<XsrfToken>() {
            @Override
            public void success(XsrfToken token) {
                final Duration duration = new Duration();

                ((HasRpcToken) modulePropertiesRpcAsync).setRpcToken(token);
                ((ServiceDefTarget) modulePropertiesRpcAsync).setServiceEntryPoint(GWT.getModuleBaseURL() + "endpoint." + moduleId + "-properties");

                modulePropertiesRpcAsync.getProperties(new AsyncCallback<ClientModuleProperties>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        logger.log(Level.SEVERE, "Failed to perform retrieval of module properties.", caught);

                        callback.onFailure(caught);
                        // Oepsie.. display error message.
                    }

                    @Override
                    public void onSuccess(ClientModuleProperties result) {
                        logger.log(Level.FINE, "Successfully performed retrieval of module properties. (duration: " + duration.elapsedMillis() + "ms)");

                        callback.onSuccess(result);
                    }
                });
            }
        });


    }
}
