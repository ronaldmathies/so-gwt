package nl.sodeso.gwt.ui.client.dialog;

/**
 * @author Ronald Mathies
 */
public interface AlertQuestionHandler {

    public void confirm();

}
