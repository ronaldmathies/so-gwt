package nl.sodeso.gwt.ui.client.form.table;

import com.google.gwt.resources.client.CssResource;
import com.google.gwt.user.cellview.client.DataGrid;

/**
 * @author Ronald Mathies
 */
@CssResource.ImportedWithPrefix("gwt-CellTable")
public interface DataGridStyle extends DataGrid.Style {

    String DEFAULT_CSS = "nl/sodeso/gwt/ui/client/form/table/DataGrid.css";

}