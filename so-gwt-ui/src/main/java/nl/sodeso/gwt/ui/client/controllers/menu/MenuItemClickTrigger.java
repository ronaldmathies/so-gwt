package nl.sodeso.gwt.ui.client.controllers.menu;

import java.util.Map;

/**
 * @author Ronald Mathies
 */
public interface MenuItemClickTrigger {

    void click(Map<String, String> arguments);

}
