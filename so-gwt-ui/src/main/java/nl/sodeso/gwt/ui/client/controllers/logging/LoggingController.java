package nl.sodeso.gwt.ui.client.controllers.logging;

import nl.sodeso.gwt.ui.client.controllers.logging.handler.LocalLogHandler;
import nl.sodeso.gwt.ui.client.controllers.logging.handler.RemoteLogHandler;

import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class LoggingController {

    private static LoggingController controller;
    private LoggingWindow loggingWindow = null;

    private LoggingController() {
        Logger.getLogger("").addHandler(LocalLogHandler.instance());
        Logger.getLogger("").addHandler(new RemoteLogHandler());
    }

    public static LoggingController instance() {
        if (controller == null) {
            controller = new LoggingController();
        }

        return controller;
    }

    public void showLoggingWindow() {
        if (loggingWindow == null) {
            loggingWindow = new LoggingWindow();
        }

        loggingWindow.center(false, false);
    }


}
