package nl.sodeso.gwt.ui.server.endpoint.properties;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class ModulePropertiesContainer {

    private Map<String, ServerModuleProperties> container =
            new HashMap<>();

    private static ModulePropertiesContainer INSTANCE = null;

    /**
     * Returns the singleton instance.
     * @return the singleton instance.
     */
    public static ModulePropertiesContainer instance() {
        if (INSTANCE == null) {
            INSTANCE = new ModulePropertiesContainer();
        }

        return INSTANCE;
    }

    /**
     * Sets the server application properties for the specified module id.
     * @param moduleId the module id.
     * @param serverModuleProperties the server module properties.
     * @throws RuntimeException when properties already have been set for the specified module id.
     */
    public void setModuleProperties(String moduleId, ServerModuleProperties serverModuleProperties) {
        if (container.containsKey(moduleId)) {
            throw new RuntimeException("Properties for module with module id " + moduleId + " has already been registered.");
        }

        container.put(moduleId, serverModuleProperties);
    }

    /**
     * Returns the application properties registered for the specified module id.
     * @param moduleId the module id.
     * @param <X>
     * @return the server side application properties.
     */
    @SuppressWarnings("unchecked")
    public <X> X getModuleProperties(String moduleId) {
        if (container.containsKey(moduleId)) {
            return (X)container.get(moduleId);
        }

        throw new RuntimeException("Properties not found!");
    }


}
