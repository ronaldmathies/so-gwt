package nl.sodeso.gwt.ui.client.form.input;

import nl.sodeso.gwt.ui.client.form.EntryWithWidget;
import nl.sodeso.gwt.ui.client.util.HasUuid;

/**
 * @author Ronald Mathies
 */
public interface WidgetCollectionFieldWidget extends HasUuid {

    EntryWithWidget toEntry();

}
