package nl.sodeso.gwt.ui.client.form.combobox.ui;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.LIElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.ComplexPanel;
import nl.sodeso.gwt.ui.client.form.combobox.Option;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.util.HasKey;

import static nl.sodeso.gwt.ui.client.util.Constants.FALSE;
import static nl.sodeso.gwt.ui.client.util.Constants.TRUE;

/**
 * @author Ronald Mathies
 */
public class LIOption<T extends Option> extends ComplexPanel implements HasKey {

    private static final String KEY_OPTION_SUFFIX = "-option";

    // CSS style classes.
    private static final String STYLE_LIST_FIELD_OPTION = "list-option";

    // Element attribute names.
    private static final String ATTR_SELECTED = "so-selected";
    private static final String ATTR_VISIBLE = "so-visible";
    private static final String ATTR_ENABLED = "so-enabled";
    private static final String ATTR_CODE = "so-code";

    private Trigger trigger = null;

    public LIOption(T item, Trigger trigger) {
        this.trigger = trigger;

        LIElement liOption = Document.get().createLIElement();
        liOption.addClassName(STYLE_LIST_FIELD_OPTION);
        setElement(liOption);

        SpanElement labelSpan = Document.get().createSpanElement();
        labelSpan.setInnerText(item.getDisplayDescription());
        liOption.appendChild(labelSpan);

        sinkEvents(Event.ONCLICK);

        setVisible(true);
        setEnabled(true);
        setSelected(false);

        setKey(item.getKey());
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_OPTION_SUFFIX;
    }

    public void onBrowserEvent(Event event) {
        super.onBrowserEvent(event);

        switch (event.getTypeInt()) {
            case Event.ONCLICK:
                trigger.fire();
                break;
        }
    }

    public void setVisible(boolean visible) {
        getElement().setAttribute(ATTR_VISIBLE, visible ? TRUE : FALSE);
    }

    public boolean isVisible() {
        String value = getElement().getAttribute(ATTR_VISIBLE);
        return value == null || value.equals(TRUE);
    }

    public void setSelected(boolean visible) {
        getElement().setAttribute(ATTR_SELECTED, visible ? TRUE : FALSE);
    }

    public boolean isSelected() {
        String value = getElement().getAttribute(ATTR_SELECTED);
        return value == null || value.equals(TRUE);
    }

    public void setEnabled(boolean enabled) {
        getElement().setAttribute(ATTR_ENABLED, enabled ? TRUE : FALSE);
    }

    public boolean isEnabled() {
        String value = getElement().getAttribute(ATTR_ENABLED);
        return value == null || value.equals(TRUE);
    }

    public void setCode(String code) {
        getElement().setAttribute(ATTR_CODE, code);
    }

    public String getCode() {
        return getElement().getAttribute(ATTR_CODE);
    }

}
