package nl.sodeso.gwt.ui.client.application.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * Interface defining the event handler for receiving the {@link ModuleSuspendEvent} events.
 *
 * @author Ronald Mathies
 */
public interface ModuleSuspendEventHandler extends EventHandler {

    /**
     * This method is invoked when a {@link ModuleSuspendEvent} has been fired.
     *
     * @param event the event.
     */
    void onEvent(ModuleSuspendEvent event);

}
