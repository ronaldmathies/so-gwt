package nl.sodeso.gwt.ui.client.panel;

import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.gwt.ui.client.trigger.Trigger;

/**
 * @author Ronald Mathies
 */
public class IconGridItem {

    private Icon icon;
    private Trigger trigger;

    public IconGridItem(Icon icon, Trigger trigger) {
        this.icon = icon;
        this.trigger = trigger;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public Trigger getTrigger() {
        return trigger;
    }

    public void setTrigger(Trigger trigger) {
        this.trigger = trigger;
    }
}
