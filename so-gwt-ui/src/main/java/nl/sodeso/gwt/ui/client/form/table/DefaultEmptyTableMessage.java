package nl.sodeso.gwt.ui.client.form.table;

import com.google.gwt.user.client.ui.FlowPanel;
import nl.sodeso.gwt.ui.client.form.input.SpanField;

/**
 * @author Ronald Mathies
 */
public class DefaultEmptyTableMessage extends FlowPanel {

    private static final String STYLE_EMPTY_TABLE_MESSAGE = "tableEmptyMessage";

    public DefaultEmptyTableMessage(String key, String message) {
        addStyleName(STYLE_EMPTY_TABLE_MESSAGE);

        SpanField spanField = new SpanField(key, message);
        add(spanField);
    }

}
