package nl.sodeso.gwt.ui.client.panel;

import com.google.gwt.dom.client.*;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.util.HasKey;

import java.util.ArrayList;

import static nl.sodeso.gwt.ui.client.util.Constants.FALSE;
import static nl.sodeso.gwt.ui.client.util.Constants.TRUE;

/**
 * @author Ronald Mathies
 */
public class LegendPanel extends ComplexPanel implements HasKey, IsValidationContainer, IsRevertable {

    // Keys for so-key entries.
    private static final String KEY_PANEL_SUFFIX = "-panel";
    private static final String KEY_COLLAPSE = "collapse";

    // CSS style classes.
    private static final String STYLE_LEGEND_PANEL = "legend-panel";
    private static final String STYLE_LEGEND_PANEL_HEADER = "header";
    private static final String STYLE_LEGEND_COLLAPSE_BUTTON = "collapse-button";

    // Element attribute names.
    private static final String ATTR_COLLAPSED = "so-collapsed";

    private FieldSetElement fieldSetElement = null;
    private LegendElement legendElement = null;
    private SpanElement titleElement = null;
    private SimpleButton collapseButton = null;

    private boolean isCollapsed = false;

    private ArrayList<Widget> widgets = new ArrayList<>();

    /**
     * Constructs a new legend panel with the specified key and title.
     *
     * @param key the key to use for identification.
     * @param title the title of the legend panel.
     */
    public LegendPanel(String key, String title) {
        this.fieldSetElement = Document.get().createFieldSetElement();
        this.fieldSetElement.setClassName(STYLE_LEGEND_PANEL);

        this.legendElement = Document.get().createLegendElement();
        this.legendElement.setClassName(STYLE_LEGEND_PANEL_HEADER);

        this.titleElement = Document.get().createSpanElement();
        this.legendElement.appendChild(titleElement);

        this.fieldSetElement.appendChild(this.legendElement);

        setElement(this.fieldSetElement);

        this.collapseButton = new SimpleButton(KEY_COLLAPSE, Icon.MinusSquareO, null);
        this.collapseButton.addStyleName(STYLE_LEGEND_COLLAPSE_BUTTON);

        setTitle(title);
        setKey(key);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_PANEL_SUFFIX;
    }

    /**
     * Flag to indicate if the collapse button in the title bar should be shown or not.
     * @param show true to show the button, false to hide.
     * @return this instance.
     */
    public LegendPanel showCollapseButton(boolean show) {
        if (show) {
            Element element = this.collapseButton.getElement();
            addCollapseHandlers(this.titleElement);
            this.titleElement.getStyle().setCursor(Style.Cursor.POINTER);

            addCollapseHandlers(element);
            this.legendElement.appendChild(element);
        }

        return this;
    }

    private void addCollapseHandlers(Element element) {
        DOM.sinkEvents(element, Event.ONCLICK);
        DOM.setEventListener(element, event -> {
            setCollapsed(!isCollapsed());
        });
    }

    /**
     * Set the title of the legend panel.
     *
     * @param title the title of the legend panel.
     */
    public void setTitle(String title) {
        this.titleElement.setInnerText(title);
    }

    /**
     * Returns the title of the legend panel.
     *
     * @return the title of the legend panel.
     */
    public String getTitle() {
        return this.legendElement.getInnerText();
    }

    public void add(Widget widget) {
        this.widgets.add(widget);

        // Redo the collapsed setting since adding a widget
        // can be performed before the widget has been added.
        setCollapsedAttribute();

        add(widget, this.fieldSetElement);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, this.widgets);
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.widgets);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.widgets);
    }

    /**
     * Sets the collapsed state of the legend panel.
     * @param collapsed the collapsed state.
     * @return this instance.
     */
    public LegendPanel setCollapsed(boolean collapsed) {
        if (this.isCollapsed != collapsed) {
            this.isCollapsed = collapsed;
            this.collapseButton.setIcon(isCollapsed() ? Icon.PlusSquareO : Icon.MinusSquareO, Align.LEFT);
            this.setCollapsedAttribute();
        }

        return this;
    }

    public boolean isCollapsed() {
        return this.isCollapsed;
    }

    private void setCollapsedAttribute() {
        for (Widget widget : this.widgets) {
            widget.getElement().setAttribute(ATTR_COLLAPSED, isCollapsed ? TRUE : FALSE);
        }
    }
}
