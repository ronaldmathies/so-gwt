package nl.sodeso.gwt.ui.client.form.validation;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.LIElement;
import com.google.gwt.dom.client.UListElement;
import com.google.gwt.user.client.ui.FlowPanel;
import nl.sodeso.gwt.ui.client.util.CollectionUtil;
import nl.sodeso.gwt.ui.client.util.KeyUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import static nl.sodeso.gwt.ui.client.form.validation.ValidationMessage.Level;

/**
 * @author Ronald Mathies
 */
public class ValidationResult implements Serializable {

    // CSS style classes.
    private transient static final String STYLE_VALIDATION_LIST = "validation-list";

    private ArrayList<ValidationMessage> messages = new ArrayList<>();

    public ValidationResult() {
    }

    /**
     * Adds a new validation message to this validation result container.
     * @param message the message to add.
     */
    public void add(ValidationMessage message) {
        this.messages.add(message);
    }

    /**
     * Adds all the validation messages to this validation result container.
     * @param messages the validation messages to add.
     */
    public void addAll(ArrayList<ValidationMessage> messages) {
        this.messages.addAll(messages);
    }

    /**
     * Merges another validation result container with this validation result container.
     * @param result the validation result container to merge with this validation result container.
     */
    public void merge(ValidationResult result) {
        this.addAll(result.getMessages());
    }

    /**
     * Merges all the other validation result containers with this validation result container.
     * @param results the validation result containers to merge with this validation result container.
     * @return this instance.
     */
    public ValidationResult mergeAll(Collection<ValidationResult> results ) {
        for (ValidationResult result : results) {
            merge(result);
        }
        return this;
    }

    /**
     * Returns all validation messages that are available within this validation result container.
     * @return validation message.
     */
    public ArrayList<ValidationMessage> getMessages() {
        return this.messages;
    }

    /**
     * Returns all validation messages that have the specified level.
     * @param level the level.
     * @return all validation messages that have the specified level.
     */
    public ArrayList<ValidationMessage> getMessages(final Level level) {
        return new CollectionUtil<ValidationMessage>().filter(messages, message -> !message.getLevel().equals(level));
    }

    /**
     * Flag indicating if this results object contains messages or not.
     * @return flag indicating if this results object contains messages or not.
     */
    public boolean hasMessages() {
        return !this.messages.isEmpty();
    }

    /**
     * Method to check if a message with a specific key is present within the validation results.
     * @param key the key to look for.
     * @return flag indicating if there is a message with the specified key.
     */
    public boolean containsMessageWithKey(String key) {
        for (ValidationMessage message : this.messages) {
            if (message.getKey().equalsIgnoreCase(key)) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    public String toString() {
        return this.messages.toString();
    }

    /**
     * Constructs a panel with all the messages added to them.
     *
     * @return a panel.
     */
    public FlowPanel toWidget() {
        UListElement uListElement = Document.get().createULElement();
        uListElement.setClassName(STYLE_VALIDATION_LIST);

        for (Level level : Level.values()) {
            for (ValidationMessage message : this.getMessages(level)) {
                uListElement.appendChild(messageToLIElement(message));
            }
        }

        FlowPanel flowPanel = new FlowPanel();
        flowPanel.getElement().appendChild(uListElement);
        return flowPanel;
    }

    private LIElement messageToLIElement(ValidationMessage message) {
        LIElement lieElement = Document.get().createLIElement();

        lieElement.setClassName(message.getLevel().getStyle());
        KeyUtil.setKey(lieElement, message.getKey(), null);
        lieElement.setInnerHTML(message.getMessage());

        return lieElement;
    }
}
