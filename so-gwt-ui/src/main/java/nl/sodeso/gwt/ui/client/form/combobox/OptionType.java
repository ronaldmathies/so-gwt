package nl.sodeso.gwt.ui.client.form.combobox;

import nl.sodeso.gwt.ui.client.types.ValueType;

/**
 * @author Ronald Mathies
 */
public class OptionType<T extends Option> extends ValueType<T> {

    public OptionType() {}

    public OptionType(T value) {
        super(value);
    }

    /**
     * {@inheritDoc}
     */
    public String getValueAsString() {
        return this.getValue().getKey();
    }

    /**
     * {@inheritDoc}
     */
    public void setValueAsString(String value) {
        throw new RuntimeException("Method is not allowed.");
    }



}
