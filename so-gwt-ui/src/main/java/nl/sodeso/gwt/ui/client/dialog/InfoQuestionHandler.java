package nl.sodeso.gwt.ui.client.dialog;

/**
 * @author Ronald Mathies
 */
public interface InfoQuestionHandler {

    void confirm();

}
