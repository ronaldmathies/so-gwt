package nl.sodeso.gwt.ui.client.controllers.notification;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.FlowPanel;
import nl.sodeso.gwt.event.client.EventBusController;
import nl.sodeso.gwt.ui.client.controllers.notification.events.CreateNotificationEvent;
import nl.sodeso.gwt.ui.client.controllers.notification.events.CreateNotificationEventHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class NotificationController extends FlowPanel implements CreateNotificationEventHandler {

    // CSS style classes.
    private static final String STYLE_NOTIFICATION = "notification-panel";

    private static final int MAX_NOTIFICATION_ENTRIES = 20;

    private static NotificationController controller;
    private EventBusController eventbus;

    private List<Notification> notifications = new ArrayList<>();

    private NotificationController() {
        eventbus = new EventBusController();
        eventbus.addHandler(CreateNotificationEvent.TYPE, this);

        setStyleName(STYLE_NOTIFICATION);
    }

    public static NotificationController instance() {
        if (controller == null) {
            controller = new NotificationController();
        }

        return controller;
    }

    public EventBusController getEventBus() {
        return this.eventbus;
    }

    @Override
    public void onEvent(CreateNotificationEvent event) {
        final Notification notification = new Notification(event.getStyle(), event.getTitle(), event.getMessage(), event.isAutohide());
        this.notifications.add(notification);
        add(notification);

        if (notifications.size() > MAX_NOTIFICATION_ENTRIES) {
            notifications.remove(0);
        }

        // Performing a transition directly after adding the item to the DOM
        // tree has as a side effect that the transitions don't work, apparently we
        // need to wait at least 1ms. (different thread maybe?) for the
        // transition to work.
        Timer timer = new Timer() {
            @Override
            public void run() {
                notification.setVisible(true);
                notification.startAutohideTimer();
            }
        };
        timer.schedule(1);
    }

    public List<Notification> getNotifications() {
        return this.notifications;
    }
}
