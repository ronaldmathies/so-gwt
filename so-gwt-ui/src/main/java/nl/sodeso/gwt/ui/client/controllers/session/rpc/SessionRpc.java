package nl.sodeso.gwt.ui.client.controllers.session.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.gwt.ui.client.controllers.session.domain.LoginResult;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("endpoint.session")
public interface SessionRpc extends XsrfProtectedService {

    LoginResult login(String username, String password);
    LoginResult isLoggedIn();
    Void logout();

}
