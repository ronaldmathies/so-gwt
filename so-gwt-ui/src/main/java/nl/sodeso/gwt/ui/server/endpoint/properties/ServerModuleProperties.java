package nl.sodeso.gwt.ui.server.endpoint.properties;

import nl.sodeso.commons.properties.PropertyConfiguration;

/**
 * @author Ronald Mathies
 */
public class ServerModuleProperties extends AbstractServerProperties {

    //String configurationFile
    public ServerModuleProperties() {
        super();
    } // configurationFile

    /**
     * Returns the version number.
     * @return the version number.
     * TODO: Zou module.version moeten zijn.
     */
    public String getVersion(String moduleId) {
        return PropertyConfiguration.getInstance().getStringProperty(moduleId, "application.version", "[no.version]");
    }

}
