package nl.sodeso.gwt.ui.client.form.input.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * @author Ronald Mathies
 */
public interface WidgetAddedEventHandler extends EventHandler {

    void onEvent(WidgetAddedEvent event);

}
