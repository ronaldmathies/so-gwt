package nl.sodeso.gwt.ui.client.controllers.navigation;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.application.GwtApplicationEntryPoint;
import nl.sodeso.gwt.ui.client.application.event.ModuleActivateEvent;
import nl.sodeso.gwt.ui.client.application.event.ModuleActivateEventHandler;
import nl.sodeso.gwt.ui.client.application.module.ModuleSwitchButton;
import nl.sodeso.gwt.ui.client.controllers.notification.NotificationHistoryButton;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.util.HasKey;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * @author Ronald Mathies
 */
public class NavigationController extends FlowPanel implements ModuleActivateEventHandler, HasKey {

    // Keys for so-key entries.
    private static final String KEY_NAVIGATION_SUFFIX = "-navigation";
    private static final String KEY_NAVIGATION_PANEL = "main";

    // CSS style classes.
    private static final String STYLE_NAVIGATION_PANEL = "navigation-panel";
    private static final String STYLE_NAVIGATION_BUTTON_LEFT = "navigation-button-left";
    private static final String STYLE_NAVIGATION_BUTTON_RIGHT = "navigation-button-right";

    public static NavigationController instance;

    private ArrayList<SimpleButton> leftAlignedButtons = new ArrayList<>();
    private ArrayList<SimpleButton> rightAlignedButtons = new ArrayList<>();

    private HashMap<String, ArrayList<SimpleButton>> moduleLeftNavigationButtons = new HashMap<>();
    private HashMap<String, ArrayList<SimpleButton>> moduleRightNavigationButtons = new HashMap<>();

    private NavigationController() {
        addStyleName(STYLE_NAVIGATION_PANEL);
        setKey(KEY_NAVIGATION_PANEL);

        GwtApplicationEntryPoint.instance().addModuleActivateEventHandler(this);

        if (GwtApplicationEntryPoint.instance().getAvailableModules().size() > 1) {
            ModuleSwitchButton switchButton = new ModuleSwitchButton();
            switchButton.addStyleName(STYLE_NAVIGATION_BUTTON_LEFT);
            add(switchButton);
        }

        NotificationHistoryButton historyButton = new NotificationHistoryButton();
        historyButton.addStyleName(STYLE_NAVIGATION_BUTTON_RIGHT);
        add(historyButton);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_NAVIGATION_SUFFIX;
    }

    public static NavigationController instance() {
        if (instance == null) {
            instance = new NavigationController();
        }

        return instance;
    }

    public void onEvent(ModuleActivateEvent event) {
        if (!moduleLeftNavigationButtons.containsKey(event.getModule().getModuleId())) {
            moduleLeftNavigationButtons.put(event.getModule().getModuleId(), new ArrayList<>());
        }

        if (!moduleRightNavigationButtons.containsKey(event.getModule().getModuleId())) {
            moduleRightNavigationButtons.put(event.getModule().getModuleId(), new ArrayList<>());
        }

        leftAlignedButtons = moduleLeftNavigationButtons.get(event.getModule().getModuleId());
        rightAlignedButtons = moduleRightNavigationButtons.get(event.getModule().getModuleId());

        redraw();
    }

    public void addButtons(Align alignment, SimpleButton ... button) {
        if (alignment.isLeft()) {
            leftAlignedButtons.addAll(Arrays.asList(button));
        } else if (alignment.isRight()) {
            rightAlignedButtons.addAll(Arrays.asList(button));
        } else {
            throw new RuntimeException("Only left and right alignment is allowed.");
        }

        redraw();
    }

    public void addButtonBefore(Align alignment, SimpleButton buttonToAdd, SimpleButton beforeButton) {
        addButtonBefore(alignment, buttonToAdd, beforeButton.getKey());
    }

    public void addButtonBefore(Align alignment, SimpleButton buttonToAdd, String key) {
        ArrayList<SimpleButton> buttons;

        if (alignment.isLeft()) {
            buttons = leftAlignedButtons;
        } else if (alignment.isRight()) {
            buttons = rightAlignedButtons;
        } else {
            throw new RuntimeException("Only left and right alignment is allowed.");
        }

        for (int index = 0; index < buttons.size(); index++) {
            SimpleButton existingButton = buttons.get(index);

            if (existingButton.getKey().equals(key)) {
                buttons.add(index, buttonToAdd);
                break;
            }
        }

        redraw();
    }

    public void addButtonAfter(Align alignment, SimpleButton buttonToAdd, SimpleButton afterButton) {
        addButtonAfter(alignment, buttonToAdd, afterButton.getKey());
    }

    public void addButtonAfter(Align alignment, SimpleButton buttonToAdd, String key) {
        ArrayList<SimpleButton> buttons;

        if (alignment.isLeft()) {
            buttons = leftAlignedButtons;
        } else if (alignment.isRight()) {
            buttons = rightAlignedButtons;
        } else {
            throw new RuntimeException("Only left and right alignment is allowed.");
        }

        for (int index = 0; index < buttons.size(); index++) {
            SimpleButton existingButton = buttons.get(index);

            if (existingButton.getKey().equals(key)) {
                buttons.add(++index, buttonToAdd);
                break;
            }
        }

        redraw();
    }

    public void removeButtons(SimpleButton ... buttons) {
        for (int activeList = 0; activeList < 2; activeList++) {
            ArrayList<SimpleButton> listWithExistingButtons = (activeList == 0 ? leftAlignedButtons : rightAlignedButtons);
            for (int index = listWithExistingButtons.size() - 1; index >= 0; index--) {
                SimpleButton existingButton = listWithExistingButtons.get(index);
                for (SimpleButton buttonToRemove : buttons) {
                    if (existingButton.getKey().equals(buttonToRemove.getKey())) {
                        remove(existingButton);
                        listWithExistingButtons.remove(index);
                    }
                }
            }
        }

        redraw();
    }

    private void redraw() {
        for (int index = 0; index < getWidgetCount(); index++) {
            Widget widget = getWidget(index);

            // These are always fixed.
            // TODO: Add interface method to keep them from being removed?
            if (widget instanceof ModuleSwitchButton || widget instanceof NotificationHistoryButton) {
                continue;
            }

            remove(widget);
        }

        for (SimpleButton simpleButton : leftAlignedButtons) {
            simpleButton.addStyleName(STYLE_NAVIGATION_BUTTON_LEFT);
            add(simpleButton);
        }

        for (SimpleButton simpleButton : rightAlignedButtons) {
            simpleButton.addStyleName(STYLE_NAVIGATION_BUTTON_RIGHT);
            add(simpleButton);
        }
    }

}
