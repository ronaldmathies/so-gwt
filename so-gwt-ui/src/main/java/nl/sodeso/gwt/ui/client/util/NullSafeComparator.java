package nl.sodeso.gwt.ui.client.util;

import java.util.Comparator;

/**
 * @author Ronald Mathies
 */
public abstract class NullSafeComparator<T> implements Comparator<T> {

    /**
     * Implement this method to complete the comparator, you can assume that
     * both value or not null.
     *
     * @param o1 the left side of the comparator.
     * @param o2 the right side of the comparator.
     * @return -1 if smaller then, 0 if equal or 1 if larger then.
     */
    public abstract int safeCompare(T o1, T o2);

    /**
     * {@inheritDoc}
     */
    @Override
    public int compare(T o1, T o2) {
        if (o1 == o2) {
            return 0;
        }

        if (o1 != null) {
            return (o2 != null) ? safeCompare(o1, o2) : 1;
        }

        return -1;
    }

}