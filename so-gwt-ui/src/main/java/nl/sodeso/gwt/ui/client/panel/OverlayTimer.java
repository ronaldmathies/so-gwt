package nl.sodeso.gwt.ui.client.panel;

import com.google.gwt.user.client.Timer;

/**
 * @author Ronald Mathies
 */
public class OverlayTimer {

    private Timer overlayTimer = null;

    public void start() {
        overlayTimer = new Timer() {
            @Override
            public void run() {
                Overlay.show();
            }
        };
        overlayTimer.schedule(1000);
    }

    public void stop() {
        if (overlayTimer != null) {
            if (overlayTimer.isRunning()) {
                overlayTimer.cancel();
            }
            overlayTimer = null;
        }
        Overlay.hide();
    }

}
