package nl.sodeso.gwt.ui.client.application.event;

import com.google.web.bindery.event.shared.Event;
import nl.sodeso.gwt.ui.client.application.module.ModuleEntryPoint;


/**
 * Event which is used when a module is going to be suspended within an application. This event will be fired
 * every time when a module is suspended (for example before activating a different module).
 *
 * To receive these events simple register a {@link ModuleSuspendEventHandler} using the {@link nl.sodeso.gwt.ui.client.application.GwtApplicationEntryPoint#addModuleSuspendEventHandler(ModuleSuspendEventHandler)}.
 *
 * @author Ronald Mathies
 */
public class ModuleSuspendEvent extends Event<ModuleSuspendEventHandler> {

    public static final Type<ModuleSuspendEventHandler> TYPE =
            new Type<>();

    private ModuleEntryPoint module = null;

    /**
     * Constrcuts a new event with the specified module.
     *
     * @param module the module that is suspended.
     */
    public ModuleSuspendEvent(ModuleEntryPoint module) {
        this.module = module;
    }

    /**
     * Returns the associated handler type.
     *
     * @return the associated handler type.
     */
    @Override
    public Type<ModuleSuspendEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(ModuleSuspendEventHandler handler) {
        handler.onEvent(this);
    }

    /**
     * Returns the module that is suspended.
     *
     * @return the module.
     */
    public ModuleEntryPoint getModule() {
        return this.module;
    }

}
