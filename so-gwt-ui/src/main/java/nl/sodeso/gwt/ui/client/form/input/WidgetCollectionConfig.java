package nl.sodeso.gwt.ui.client.form.input;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class WidgetCollectionConfig implements Serializable {

    public static WidgetCollectionConfig DEFAULT = new WidgetCollectionConfig();

    private boolean autoAddFirst = false;
    private boolean enableSwitching = false;
    private boolean enableAdding = true;
    private boolean enableDeleting = true;

    public WidgetCollectionConfig() {}

    public WidgetCollectionConfig setAutoAddFirst(boolean autoAddFirst) {
        this.autoAddFirst = autoAddFirst;
        return this;
    }

    public boolean isAutoAddFirst() {
        return this.autoAddFirst;
    }

    public WidgetCollectionConfig setEnableSwitching(boolean enableSwitching) {
        this.enableSwitching = enableSwitching;
        return this;
    }

    public boolean isEnableSwitching() {
        return this.enableSwitching;
    }

    public WidgetCollectionConfig setEnableAdding(boolean enableAdding) {
        this.enableAdding = enableAdding;
        return this;
    }

    public boolean isEnableAdding() {
        return this.enableAdding;
    }

    public WidgetCollectionConfig setEnableDeleting(boolean enableDeleting) {
        this.enableDeleting = enableDeleting;
        return this;
    }

    public boolean isEnableDeleting() {
        return this.enableDeleting;
    }

}
