package nl.sodeso.gwt.ui.server.servlet;

import nl.sodeso.commons.fileutils.FileUtil;
import nl.sodeso.gwt.ui.client.util.ContentType;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public abstract class AbstractFileUploadServlet extends HttpServlet {

    private static final String HEADER_CONTENT_DISPOSITION = "content-disposition";
    private static final String TOKEN_FILENAME = "filename";
    private static final String TEMP_FILE_EXTENSION = ".upl";

    private static final String SHA_256_ALGORITHM = "SHA_256";

    private static final Logger log = Logger.getLogger(AbstractFileUploadServlet.class.getName());

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<UploadFile> uploadedFiles = new ArrayList<>();

        try {
            UploadFile uploadFile = null;

            for (Part part : request.getParts()) {
                String filename = getFileName(part);
                if (uploadFile == null || !uploadFile.getFilename().equalsIgnoreCase(filename)) {
                    uploadFile = new UploadFile();
                    uploadFile.setFilename(filename);

                    uploadFile.setContentType(ContentType.getContentType(part.getContentType()));

                    File tempFile = new File(FileUtil.getSystemTempFolderAsFile(), UUID.randomUUID().toString() + TEMP_FILE_EXTENSION);
                    uploadFile.setTempFile(tempFile);
                    uploadedFiles.add(uploadFile);
                }

                Files.copy(part.getInputStream(), uploadFile.getTempFile().toPath());
            }

            upload(uploadedFiles, response);
        } catch (Exception e) {
            System.out.println(e);
        } finally {
        }
    }

    private String getFileName(Part part) {
        String contentDisp = part.getHeader(HEADER_CONTENT_DISPOSITION);
        String[] tokens = contentDisp.split(";");
        for (String token : tokens) {
            if (token.trim().startsWith(TOKEN_FILENAME)) {
                return token.substring(token.indexOf("=") + 2, token.length()-1);
            }
        }
        return "";
    }

    public abstract void upload(List<UploadFile> uploadedFiles, HttpServletResponse response) throws ServletException;
}
