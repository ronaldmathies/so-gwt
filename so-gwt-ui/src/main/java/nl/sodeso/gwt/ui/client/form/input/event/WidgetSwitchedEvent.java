package nl.sodeso.gwt.ui.client.form.input.event;

import com.google.web.bindery.event.shared.Event;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionFieldWidget;

/**
 * @author Ronald Mathies
 */
public class WidgetSwitchedEvent extends Event<WidgetSwitchedEventHandler> {

    public static final Type<WidgetSwitchedEventHandler> TYPE = new Type<>();

    private WidgetCollectionField source;
    private WidgetCollectionFieldWidget fromWidget;
    private WidgetCollectionFieldWidget toWidget;
    private int fromIndex;
    private int toIndex;

    public WidgetSwitchedEvent(WidgetCollectionField source, WidgetCollectionFieldWidget fromWidget, WidgetCollectionFieldWidget toWidget, int fromIndex, int toIndex) {
        this.source = source;
        this.fromWidget = fromWidget;
        this.toWidget = toWidget;
        this.fromIndex = fromIndex;
        this.toIndex = toIndex;
    }

    @Override
    public Type<WidgetSwitchedEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(WidgetSwitchedEventHandler handler) {
        handler.onEvent(this);
    }

    public WidgetCollectionField getSource() {
        return this.source;
    }

    public WidgetCollectionFieldWidget getFromWidget() {
        return this.fromWidget;
    }

    public WidgetCollectionFieldWidget getToWidget() {
        return this.toWidget;
    }

    public int getFromIndex() {
        return fromIndex;
    }

    public int getToIndex() {
        return toIndex;
    }
}