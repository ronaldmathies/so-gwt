package nl.sodeso.gwt.ui.client.types;

/**
 * @author Ronald Mathies
 */
public class StringType extends ValueType<String> {

    /**
     * Constructs a new instance with an unspecified value.
     */
    public StringType() {}

    /**
     * Constructs a new instance with the specified value.
     *
     * @param value the initial value.
     */
    public StringType(String value) {
        super(value);
    }

    @Override
    public String getValueAsString() {
        return getValue();
    }

    /**
     * {@inheritDoc}
     */
    public void setValueAsString(String value) {
        this.setValue(value);
    }

}
