package nl.sodeso.gwt.ui.client.form.button;

import com.google.gwt.event.dom.client.ClickHandler;
import nl.sodeso.gwt.ui.client.resources.Icon;

/**
 * Button representing an <code>Left</code> button.
 *
 * @author Ronald Mathies
 */
public class LeftButton {

    // Keys for so-key entries.
    public static final String KEY = "left";

    public static class WithIcon extends SimpleButton {

        public WithIcon() {
            this(null);
        }

        public WithIcon(ClickHandler handler) {
            super(KEY, Icon.ChevronLeft, Style.BLUE);

            if (handler != null) {
                addClickHandler(handler);
            }
        }
    }

}
