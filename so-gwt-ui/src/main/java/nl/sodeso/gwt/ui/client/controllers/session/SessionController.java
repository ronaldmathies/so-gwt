package nl.sodeso.gwt.ui.client.controllers.session;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.gwt.event.client.EventBusController;
import nl.sodeso.gwt.ui.client.controllers.session.domain.LoginResult;
import nl.sodeso.gwt.ui.client.controllers.session.domain.User;
import nl.sodeso.gwt.ui.client.controllers.session.event.UserLoginEvent;
import nl.sodeso.gwt.ui.client.controllers.session.event.UserLoginEventHandler;
import nl.sodeso.gwt.ui.client.controllers.session.event.UserLogoutEvent;
import nl.sodeso.gwt.ui.client.controllers.session.event.UserLogoutEventHandler;
import nl.sodeso.gwt.ui.client.controllers.session.rpc.SessionRpcGateway;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;

/**
 * @author Ronald Mathies
 */
public class SessionController {

    private static final String J_SESSIONID = "JSESSIONID";

    private static SessionController instance;

    private EventBusController eventbus = null;
    private User user = null;

    private SessionController() {
        this.eventbus = new EventBusController();
    }

    public static SessionController instance() {
        if (instance == null) {
            instance = new SessionController();
        }

        return instance;
    }

    public EventBusController getEventBus() {
        return this.eventbus;
    }

    public void addUserLoginEventHandler(UserLoginEventHandler handler) {
        this.eventbus.addHandler(UserLoginEvent.TYPE, handler);
    }

    public void addUserLogoutEventHandler(UserLogoutEventHandler handler) {
        this.eventbus.addHandler(UserLogoutEvent.TYPE, handler);
    }

    public void login(String username, String password) {
        ((SessionRpcGateway)GWT.create(SessionRpcGateway.class)).login(username, password, new AsyncCallback<LoginResult>() {
            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(LoginResult result) {
                user = result.getUser();
                eventbus.fireEvent(new UserLoginEvent(result));
            }
        });
    }

    public void isLoggedIn(final ServerLoginState state) {
        if (Cookies.isCookieEnabled()) {
            String cookie = Cookies.getCookie(J_SESSIONID);
            if (cookie == null || cookie.isEmpty()) {
                ((SessionRpcGateway)GWT.create(SessionRpcGateway.class)).isLoggedIn(new DefaultAsyncCallback<LoginResult>() {
                    @Override
                    public void success(LoginResult result) {
                        if (result.isSuccess()) {
                            user = result.getUser();
                        }

                        state.state(result);
                    }
                });
            }
        }
    }

    public void logout() {
        ((SessionRpcGateway)GWT.create(SessionRpcGateway.class)).logout(new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(Void _void) {
                if (Cookies.isCookieEnabled()) {
                    Cookies.removeCookie(J_SESSIONID);
                }

                eventbus.fireEvent(new UserLogoutEvent());
            }
        });
    }

}
