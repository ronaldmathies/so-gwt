package nl.sodeso.gwt.ui.client.controllers.logging;

/**
 * @author Ronald Mathies
 */
public interface RpcCallChartDataProviderChanged {

    public void methodsChanged();

    public void activeDatasetChanged();

}
