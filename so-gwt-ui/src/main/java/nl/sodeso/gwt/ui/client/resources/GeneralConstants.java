package nl.sodeso.gwt.ui.client.resources;

import com.google.gwt.i18n.client.Constants;

/**
 * @author Ronald Mathies
 */
public interface GeneralConstants extends Constants {

    @DefaultStringValue("Yes")
    String Yes();

    @DefaultStringValue("No")
    String No();

}
