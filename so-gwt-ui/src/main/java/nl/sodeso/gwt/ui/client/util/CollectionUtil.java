package nl.sodeso.gwt.ui.client.util;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Ronald Mathies
 */
public class CollectionUtil<O> {

    public ArrayList<O> filter(Collection<O> collection, Filter<O> filter) {
        ArrayList<O> results = new ArrayList<>();
        for (O object : collection) {
            if (!filter.filter(object)) {
                results.add(object);
            }
        }

        return results;
    }

}
