package nl.sodeso.gwt.ui.client.form.combobox;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Ronald Mathies
 */
public abstract class Option implements Serializable {

    private String key = null;
    private String code = null;
    private String description = null;

    /**
     * Constructs an empty option.
     */
    public Option() {}

    /**
     * Constructs a new option.
     *
     * @param key the key.
     * @param code the code.
     * @param description the description.
     */
    public Option(String key, String code, String description) {
        this.key = key;
        this.code = code;
        this.description = description;
    }

    /**
     * Constructs a new option in which the key and code are the same.
     * @param code the code that will also be used for the key.
     * @param description the description.
     */
    public Option(String code, String description) {
        this.key = code;
        this.code = code;
        this.description = description;
    }

    /**
     * Constructs a new option in which the key, code and description are all the same.
     * @param code
     */
    public Option(String code) {
        this.key = code;
        this.code = code;
        this.description = code;
    }

    /**
     * Sets the key.
     * @param key the key.
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Returns the key.
     * @return the key.
     */
    public String getKey() {
        return this.key;
    }

    /**
     * Returns the code.
     * @return the code.
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Sets the code.
     * @param code the code.
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Returns the description.
     * @return the description.
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Sets the description.
     * @param description the description.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Returns the value to display in the UI.
     * @return the value to display in the UI.
     */
    public String getDisplayDescription() {
        return getDescription();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DefaultOption option = (DefaultOption) o;
        return Objects.equals(key, option.getKey());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(key);
    }
}