package nl.sodeso.gwt.ui.client.form.table;

import com.google.gwt.resources.client.CssResource;
import com.google.gwt.user.cellview.client.CellTable;

/**
 * @author Ronald Mathies
 */
@CssResource.ImportedWithPrefix("gwt-CellTable")
public interface CellTableStyle extends CellTable.Style {

    String DEFAULT_CSS = "nl/sodeso/gwt/ui/client/form/table/CellTable.css";

}