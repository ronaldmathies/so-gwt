package nl.sodeso.gwt.ui.client.form.input;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.TextArea;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.gwt.ui.client.util.HasKey;
import nl.sodeso.gwt.ui.client.util.StringUtils;

/**
 * @author Ronald Mathies
 */
public class TextAreaField extends TextArea implements IsRevertable, HasKey {

    private static final String KEY_SUFFIX = "-field";

    private int maximumCharacterCount = -1;
    private String oldValue;
    private int oldCursorPos;

    private StringType valueType = null;

    public TextAreaField(String key, final StringType valueType) { // , final StringType valueType
        super();

        this.valueType = valueType;

        // Paste event is captured so that we can direct it to the value change handler.
        sinkEvents(Event.ONPASTE);
        sinkEvents(Event.ONFOCUS);
        sinkEvents(Event.ONKEYDOWN);

        super.addKeyUpHandler(event -> {
            if (!getValue().equals(valueType.getValue())) {
                valueType.setValue(getValue());
            }
        });

        valueType.addValueChangedEventHandler(new ValueChangedEventHandler<StringType>(this) {
            @Override
            public void onValueCanged(ValueChangedEvent<StringType> event) {
                if (!event.getValueType().getValue().equals(getValue())) {
                    setValue(event.getValueType().getValue());
                }
            }
        });

        addValueChangeHandler(event -> {
            if (maximumCharacterCount != -1) {
                String value = event.getValue();
                if (StringUtils.isNotEmpty(value)) {
                    if (value.length() > maximumCharacterCount) {
                        setValue(oldValue);
                        setCursorPos(oldCursorPos);
                    } else {
                        oldValue = getValue();
                        oldCursorPos = getCursorPos();
                    }
                }
            }
        });

        setValue(this.valueType.getValue());
        setKey(key);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_SUFFIX;
    }

    public void onBrowserEvent(Event event) {
        super.onBrowserEvent(event);

        switch (event.getTypeInt()) {
            case Event.ONFOCUS:
                this.oldValue = getValue();
                this.oldCursorPos = getCursorPos();
            case Event.ONKEYDOWN:
                Scheduler.get().scheduleDeferred(() -> ValueChangeEvent.fire(TextAreaField.this, getValue()));
                break;
            case Event.ONPASTE:
                Scheduler.get().scheduleDeferred(() -> ValueChangeEvent.fire(TextAreaField.this, getValue()));
                break;
        }
    }

    public TextAreaField setMaximumCharacterCount(int count) {
        this.maximumCharacterCount = count;
        return this;
    }

    public int getMaximumCharacterCount() {
        return this.maximumCharacterCount;
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        valueType.revert();
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return valueType.isChanged();
    }
}
