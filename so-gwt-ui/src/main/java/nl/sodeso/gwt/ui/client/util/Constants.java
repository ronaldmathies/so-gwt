package nl.sodeso.gwt.ui.client.util;

/**
 * @author Ronald Mathies
 */
public class Constants {

    public static final String TRUE = "true";
    public static final String FALSE = "false";
}
