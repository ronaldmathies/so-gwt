package nl.sodeso.gwt.ui.client.application;

import nl.sodeso.gwt.ui.client.controllers.buttonbar.ButtonBarController;
import nl.sodeso.gwt.ui.client.controllers.center.CenterController;
import nl.sodeso.gwt.ui.client.controllers.header.HeaderController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuController;
import nl.sodeso.gwt.ui.client.controllers.navigation.NavigationController;
import nl.sodeso.gwt.ui.client.controllers.notification.NotificationController;
import nl.sodeso.gwt.websocket.client.WebSocketClientController;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Initializes all the controllers of the application:
 *
 * <ul>
 *     <li>Header Controller</li>
 *     <li>Notification Controller</li>
 *     <li>Navigation Controller</li>
 *     <li>Center Controller</li>
 *     <li>Meu Controller</li>
 *     <li>Button Bar Controller</li>
 *     <li>Websocker Controller</li>
 * </ul>
 *
 * @author Ronald Mathies
 */
public class RootPanelSetup {

    private static Logger logger = Logger.getLogger("Desktop.Boot");

    /**
     * By invoking this method the application will setup all the controllers and
     * register each controller with the root panel when necessary.
     */
    public static void setup() {
        logger.log(Level.FINE, "Initializing header controller.");
        HeaderController headerController = HeaderController.instance();
        GwtApplicationEntryPoint.getRootPanel().add(headerController);

        logger.log(Level.FINE, "Initializing notification controller.");
        NotificationController notificationController = NotificationController.instance();
        GwtApplicationEntryPoint.getRootPanel().add(notificationController);

        logger.log(Level.FINE, "Initializing navigation controller.");
        NavigationController navigationController = NavigationController.instance();
        GwtApplicationEntryPoint.getRootPanel().add(navigationController);

        logger.log(Level.FINE, "Initializing center controller.");
        CenterController centerController = CenterController.instance();
        GwtApplicationEntryPoint.getRootPanel().add(centerController);

        logger.log(Level.FINE, "Initializing buttonbar controller.");
        ButtonBarController buttonbarController = ButtonBarController.instance();
        GwtApplicationEntryPoint.getRootPanel().add(buttonbarController);

        logger.log(Level.FINE, "Initializing menu controller.");
        MenuController menuController = MenuController.instance();
        GwtApplicationEntryPoint.getRootPanel().add(menuController);
    }

}
