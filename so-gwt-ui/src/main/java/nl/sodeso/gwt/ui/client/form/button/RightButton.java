package nl.sodeso.gwt.ui.client.form.button;

import com.google.gwt.event.dom.client.ClickHandler;
import nl.sodeso.gwt.ui.client.resources.Icon;

/**
 * Button representing an <code>Right</code> button.
 *
 * @author Ronald Mathies
 */
public class RightButton {

    // Keys for so-key entries.
    public static final String KEY = "right";

    public static class WithIcon extends SimpleButton {

        public WithIcon() {
            this(null);
        }

        public WithIcon(ClickHandler handler) {
            super(KEY, Icon.ChevronRight, Style.BLUE);

            if (handler != null) {
                addClickHandler(handler);
            }
        }
    }

}
