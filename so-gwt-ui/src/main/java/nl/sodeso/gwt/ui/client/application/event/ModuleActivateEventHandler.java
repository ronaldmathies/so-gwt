package nl.sodeso.gwt.ui.client.application.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * Interface defining the event handler for receiving the {@link ModuleActivateEvent} events.
 *
 * @author Ronald Mathies
 */
public interface ModuleActivateEventHandler extends EventHandler {

    /**
     * This method is invoked when a {@link ModuleActivateEvent} has been fired.
     *
     * @param event the event.
     */
    void onEvent(ModuleActivateEvent event);

}
