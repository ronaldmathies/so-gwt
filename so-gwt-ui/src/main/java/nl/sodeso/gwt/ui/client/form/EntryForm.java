package nl.sodeso.gwt.ui.client.form;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.revertable.RevertableExecutor;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.util.HasKey;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @author Ronald Mathies
 */
public class EntryForm extends FlowPanel implements IsValidationContainer, IsRevertable, HasKey {

    // Keys for so-key entries.
    private static final String KEY_FORM_SUFFIX = "-form";

    // CSS style classes.
    private static final String STYLE_ENTRY_FORM = "entry-form";

    private ArrayList<Entry> entries = new ArrayList<>();

    private ArrayList<RevertableExecutor> revertableExecutors = new ArrayList<>();

    public EntryForm(String key) {
        setStyleName(STYLE_ENTRY_FORM);
        setKey(key);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_FORM_SUFFIX;
    }

    /**
     * Adds an additional entry to the form.
     * @param entry the entry to add.
     * @return this instance.
     */
    public EntryForm addEntry(Entry entry) {
        insertEntry(entry, -1);
        return this;
    }

    public EntryForm addEntries(Entry... entries) {
        for (Entry entry : entries) {
            addEntry(entry);
        }

        return this;
    }

    public void insertEntry(Entry entry, int beforeIndex) {
        entry.attachToParent(this);

        if (beforeIndex == -1) {
            entries.add(entry);
            add(entry);
        } else {
            entries.add(beforeIndex, entry);
            insert(entry, beforeIndex);
        }
    }

    public void removeEntry(Entry entry) {
        entry.detachFromParent();
    }

    public void switchEntry(int fromIndex, int toIndex) {
        switchEntry(entries.get(fromIndex), entries.get(toIndex));
    }

    public void switchEntry(Entry fromEntry, Entry toEntry) {
        int fromIndex = entries.indexOf(fromEntry);
        int toIndex = entries.indexOf(toEntry);

        Collections.swap(entries, fromIndex, toIndex);

        remove(fromEntry);
        remove(toEntry);

        if (fromIndex > toIndex) { // Up
            insert(toEntry, toIndex);
            insert(fromEntry, toIndex);
        } else { // Down
            insert(fromEntry, fromIndex);
            insert(toEntry, fromIndex);
        }
    }

    public ArrayList<Entry> getEntries() {
        return this.entries;
    }

    public int getEntryCount() {
        return this.entries.size();
    }

    public Entry getEntry(int index) {
        return this.entries.get(index);
    }

    public void setAllEntriesVisible(boolean show) {
        for (Entry entry : entries) {
            entry.setVisible(show);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, entries);
    }

    public void addRevertableExecutor(RevertableExecutor revertableExecutor) {
        this.revertableExecutors.add(revertableExecutor);
    }

    public void removeRevertableExecutor(RevertableExecutor revertableExecutor) {
        this.revertableExecutors.remove(revertableExecutor);
    }

    public void setEntriesVisible(boolean visible, String ... keys) {
        for (String key : keys) {

            for (Entry entry : entries) {

                if (key.equals(entry.getKey())) {
                    entry.setVisible(visible);
                }

            }

        }
    }

    public boolean isEntryVisible(String key) {
        for (Entry entry : entries) {
            if (key.equals(entry.getKey())) {
                return entry.isVisible();
            }
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(entries);
        if (!revertableExecutors.isEmpty()) {
            RevertUtil.revert(revertableExecutors);

            // All revertible executors have been executed so they are now all useless so we can
            // clear the list.
            revertableExecutors.clear();
        }
    }

    public boolean hasChanges() {
        return RevertUtil.hasChanges(entries) || !revertableExecutors.isEmpty();
    }
}
