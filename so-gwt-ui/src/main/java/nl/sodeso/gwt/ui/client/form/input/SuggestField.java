package nl.sodeso.gwt.ui.client.form.input;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ValueBoxBase;
import nl.sodeso.gwt.ui.client.form.validation.IsValidatable;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationEventBusController;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.rules.ValidationRule;
import nl.sodeso.gwt.ui.client.util.HasKey;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class SuggestField extends SuggestBox implements IsValidatable, HasKey {

    private static final String KEY_SUGGEST_SUFFIX = "-field";

    private ArrayList<ValidationRule> rules = new ArrayList<>();

    private ValidationEventBusController eventbus = new ValidationEventBusController();

    public SuggestField(String key, SuggestOracle suggestOracle) {
        this(key, suggestOracle, new TextBox());
    }

    public SuggestField(String key, SuggestOracle suggestOracle, ValueBoxBase<String> valueBoxBase) {
        super(suggestOracle, valueBoxBase);
        setAutoSelectEnabled(true);

        DefaultSuggestionDisplay defaultSuggestionDisplay = (DefaultSuggestionDisplay)getSuggestionDisplay();
        defaultSuggestionDisplay.setAnimationEnabled(true);
        defaultSuggestionDisplay.setSuggestionListHiddenWhenEmpty(true);

        setKey(key);

        // On-Blur for performing a validation
        sinkEvents(Event.ONBLUR);

        addValueChangeHandler(new ValueChangeHandler<String>() {
            @Override
            public void onValueChange(ValueChangeEvent<String> event) {
                validate(new ValidationCompletedHandler() {
                    @Override
                    public void completed(ValidationResult result) {

                    }
                });
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_SUGGEST_SUFFIX;
    }

    public void onBrowserEvent(Event event) {
        super.onBrowserEvent(event);

        switch (event.getTypeInt()) {
            case Event.ONPASTE:
                Scheduler.get().scheduleDeferred(() -> ValueChangeEvent.fire(SuggestField.this, SuggestField.this.getValue()));
                break;
            case Event.ONBLUR:
                validate((result) -> {});
        }
    }

    /**
     * {@inheritDoc}
     */
    public ValidationEventBusController getValidationEventbus() {
        return eventbus;
    }

    /**
     * {@inheritDoc}
     */
    public ArrayList<ValidationRule> getValidationRules() {
        return this.rules;
    }
}
