package nl.sodeso.gwt.ui.client.controllers.menu;

import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.resources.Resources;

/**
 * Button representing an <code>Previous</code> button, default implementation will
 * when pressed click on the previous available / active menu item.
 *
 * @author Ronald Mathies
 */
public class PreviousButton extends SimpleButton {

    // Keys for so-key entries.
    public static final String KEY = "previous";

    /**
     * Constructs a default previous button with a default label.
     */
    public PreviousButton() {
        this(Resources.BUTTON_I18N.Previous(), Style.BLUE);
    }

    /**
     * Constructs a previous button with the specified title and style.
     *
     * @param title the title for the previous button.
     * @param style the style for the previous button.
     */
    public PreviousButton(String title, Style style) {
        super(KEY, title, style);

        addClickHandler((event) -> {
            MenuItem menuItem =
                    MenuController.instance().previous();

            if (menuItem != null) {
                menuItem.click(null);
            }
        });
    }

}
