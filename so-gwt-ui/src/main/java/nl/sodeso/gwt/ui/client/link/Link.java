package nl.sodeso.gwt.ui.client.link;

import java.util.Map;

/**
 * @author Ronald Mathies
 */
public interface Link {

    void navigate(String token, Map<String, String> arguments);

    boolean canProcessToken(String token);

}
