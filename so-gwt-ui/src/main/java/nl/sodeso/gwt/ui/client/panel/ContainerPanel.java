package nl.sodeso.gwt.ui.client.panel;

import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static nl.sodeso.gwt.ui.client.util.Constants.FALSE;
import static nl.sodeso.gwt.ui.client.util.Constants.TRUE;

/**
 * A container that can hold a single widget.
 *
 * @author Ronald Mathies
 */
public class ContainerPanel extends FlowPanel implements IsValidationContainer, IsRevertable {

    private Widget widget = null;

    /**
     * Constructs a new vertical panel.
     */
    public ContainerPanel() {
        super();
    }

    /**
     * Constructs a new vertical panel with the widgets supplied.
     * @param widget the widget to add.
     */
    public ContainerPanel(Widget widget) {
        this();
        setWidget(widget);
    }

    public void setWidget(Widget widget) {
        this.clear();
        this.add(widget);
    }

    /**
     * Returns the widget that is available on this panel.
     * @return the widget that is available on this panel.
     */
    public Widget getWidget() {
        return this.widget;
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, this.widget);
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.widget);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.widget);
    }
}
