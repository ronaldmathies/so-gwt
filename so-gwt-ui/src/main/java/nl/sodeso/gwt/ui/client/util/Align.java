package nl.sodeso.gwt.ui.client.util;

/**
 * @author Ronald Mathies
 */
public enum Align {

    LEFT("left"),
    CENTER("center"),
    RIGHT("right"),

    HORIZONTAL("horizontal"),
    VERTICAL("vertical");

    private String align = null;

    Align(String align) {
        this.align = align;
    }

    public String getAlignment() {
        return this.align;
    }

    public boolean isHorizontal() {
        return this.equals(Align.HORIZONTAL);
    }

    public boolean isVertical() {
        return this.equals(Align.VERTICAL);
    }

    public boolean isLeft() {
        return this.equals(Align.LEFT);
    }

    public boolean isCenter() {
        return this.equals(Align.CENTER);
    }

    public boolean isRight() {
        return this.equals(Align.RIGHT);
    }

}
