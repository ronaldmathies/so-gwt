package nl.sodeso.gwt.ui.client.util;

/**
 * @author Ronald Mathies
 */
public enum ContentType {

    APPLICATION_PDF("application/pdf"),
    APPLICATION_JSON("application/json"),
    OCTET_STREAM("application/octet-stream"),
    TEXT_HTML("text/html"),
    UNKNOWN("unknown");

    private String contentType = null;

    ContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentType() {
        return this.contentType;
    }

    public static ContentType getContentType(String contentType) {
        if (APPLICATION_PDF.getContentType().equalsIgnoreCase(contentType)) {
            return APPLICATION_PDF;
        } else if (OCTET_STREAM.getContentType().equalsIgnoreCase(contentType)) {
            return OCTET_STREAM;
        } else if (APPLICATION_JSON.getContentType().equalsIgnoreCase(contentType)) {
            return APPLICATION_JSON;
        } else if (TEXT_HTML.getContentType().equalsIgnoreCase(contentType)) {
            return TEXT_HTML;
        }

        return UNKNOWN;
    }

}
