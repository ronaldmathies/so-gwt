package nl.sodeso.gwt.ui.client.form.table;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.CellPreviewEvent;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.SelectionChangeEvent;

import java.util.ArrayList;
import java.util.List;

import static nl.sodeso.gwt.ui.client.util.Constants.FALSE;
import static nl.sodeso.gwt.ui.client.util.Constants.TRUE;

/**
 * @author Ronald Mathies
 */
public class CellTableField<T> extends CellTable<T> {

    // Element attribute names.
    private static final String ATTR_FULL_HEIGHT = "so-full-height";

    private static CellTableResource RESOURCE = GWT.create(CellTableResource.class);

    private ListDataProvider<T> listDataProvider = new ListDataProvider<>();

    private String key;

    public CellTableField(String key) {
        this(key, 5, RESOURCE);
    }

    public CellTableField(String key, final int pageSize) {
        this(key, pageSize, RESOURCE);
    }

    public CellTableField(String key, int pageSize, Resources resources) {
        super(pageSize, resources);
        this.key = key;

        getListDataProvider().addDataDisplay(this);

        setupRightMouseClickHandler();
    }

    public CellTableField(String key, int pageSize, ProvidesKey<T> keyProvider) {
        super(pageSize, RESOURCE, keyProvider);
        this.key = key;

        this.getListDataProvider().addDataDisplay(this);

        setupRightMouseClickHandler();
    }

    public CellTableField(String key, final int pageSize, ProvidesKey<T> keyProvider, Widget loadingIndicator) {
        super(pageSize, RESOURCE, keyProvider, loadingIndicator);
        this.key = key;

        this.getListDataProvider().addDataDisplay(this);

        setupRightMouseClickHandler();
    }

    /**
     * Adds the ability to click using the right mouse button, is for example neccesary when
     * adding a context menu and the user clicks on a different entry than that is selected.
     */
    private void setupRightMouseClickHandler() {
        this.addCellPreviewHandler(event -> {
            if (event.getNativeEvent().getButton() == NativeEvent.BUTTON_RIGHT) {
                event.getNativeEvent().stopPropagation();
                select(event.getValue());
            }
        });
    }

    public void fireSelectionChangedEvent() {
        SelectionChangeEvent.fire(getSelectionModel());
    }

    public boolean selectFirst() {
        if (!listDataProvider.getList().isEmpty()) {
            T firstObject = listDataProvider.getList().get(0);

            if (!getSelectionModel().isSelected(firstObject)) {
                getSelectionModel().setSelected(firstObject, true);
            }

            return true;
        }

        return false;
    }

    public boolean selectLast() {
        List<T> allEntries = listDataProvider.getList();
        if (!allEntries.isEmpty()) {
            T lastObject = allEntries.get(allEntries.size() - 1);

            if (!getSelectionModel().isSelected(lastObject)) {
                getSelectionModel().setSelected(lastObject, true);
            }

            return true;
        }

        return false;
    }

    public void replaceAll(List<T> items) {
        List<T> ldList = listDataProvider.getList();
        ldList.clear();
        ldList.addAll(items);
        ColumnSortEvent.fire(this, getColumnSortList());
    }

    public void add(T item) {
        listDataProvider.getList().add(item);
        ColumnSortEvent.fire(this, getColumnSortList());
        listDataProvider.flush();
    }

    public void addAll(List<T> items) {
        for (T item : items) {
            listDataProvider.getList().add(item);
        }

        ColumnSortEvent.fire(this, getColumnSortList());
        listDataProvider.flush();
    }

    public void add(T item, int position) {
        listDataProvider.getList().add(position, item);
        ColumnSortEvent.fire(this, getColumnSortList());
        listDataProvider.flush();
    }

    public T getFirstSelected() {
        for (T item : listDataProvider.getList()) {
            if (getSelectionModel().isSelected(item)) {
                return item;
            }
        }

        return null;
    }

    public List<T> getSelected() {
        List<T> selected = new ArrayList<>();

        for (T item : listDataProvider.getList()) {
            if (getSelectionModel().isSelected(item)) {
                selected.add(item);
            }
        }
        return selected;
    }

    public List<T> getUnselected() {
        List<T> unselected = new ArrayList<>();

        for (T item : listDataProvider.getList()) {
            if (!getSelectionModel().isSelected(item)) {
                unselected.add(item);
            }
        }
        return unselected;
    }

    public boolean hasSelection() {
        return getFirstSelected() != null;
    }

    public void select(T item) {
        getSelectionModel().setSelected(item, true);
    }

    public void select(List<T> items) {
        for (T item : items) {
            if (!getSelectionModel().isSelected(item)) {
                getSelectionModel().setSelected(item, true);
            }
        }
    }

    public void selectAll() {
        select(listDataProvider.getList());
    }

    public void deselect(T item) {
        if (getSelectionModel().isSelected(item)) {
            getSelectionModel().setSelected(item, false);
        }
    }

    public void deselectAll() {
        for (T item : listDataProvider.getList()) {
            if (getSelectionModel().isSelected(item)) {
                getSelectionModel().setSelected(item, false);
            }
        }
    }

    public void replace(T oldObject, T newObject) {
        List<T> allEntries = listDataProvider.getList();
        allEntries.set(allEntries.indexOf(oldObject), newObject);
        ColumnSortEvent.fire(this, getColumnSortList());
        listDataProvider.flush();
    }

    public void removeAll() {
        List<T> allEntries = listDataProvider.getList();
        allEntries.clear();
        listDataProvider.flush();
    }

    public void remove(T object) {
        List<T> allEntries = listDataProvider.getList();

        getSelectionModel().setSelected(object, false);

        int currentIndex = allEntries.indexOf(object);
        allEntries.remove(object);

        if (currentIndex < allEntries.size()) {
            getSelectionModel().setSelected(allEntries.get(currentIndex), true);
        } else if (!allEntries.isEmpty()) {
            getSelectionModel().setSelected(allEntries.get(allEntries.size() - 1), true);
        }

        listDataProvider.flush();
    }

    public List<T> allItems() {
        return listDataProvider.getList();
    }

    public ListDataProvider<T> getListDataProvider() {
        return this.listDataProvider;
    }

    /**
     * Sets the height of the widget to the full width of it's parent.
     * @param visible flag indicating if the full height should be visible.
     */
    public void setFullHeight(boolean visible) {
        getElement().setAttribute(ATTR_FULL_HEIGHT, visible ? TRUE : FALSE);
    }

    /**
     * Returns a flag indicating if the height attribute is set or not.
     * @return true if the height attribute is set, false if not.
     */
    public boolean isFullHeight() {
        String value = getElement().getAttribute(ATTR_FULL_HEIGHT);
        return value == null || value.equals(TRUE);
    }

    public void addColumn(Column<T, ?> col, Header<?> header) {
        super.addColumn(col, header);
    }

    public String getKey() {
        return this.key;
    }

}
