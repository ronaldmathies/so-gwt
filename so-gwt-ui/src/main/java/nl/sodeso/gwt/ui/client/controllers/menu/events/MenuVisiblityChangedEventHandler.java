package nl.sodeso.gwt.ui.client.controllers.menu.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * @author Ronald Mathies
 */
public interface MenuVisiblityChangedEventHandler extends EventHandler {

    void onEvent(MenuVisibilityChangedEvent event);

}
