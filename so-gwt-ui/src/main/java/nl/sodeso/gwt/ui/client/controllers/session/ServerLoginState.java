package nl.sodeso.gwt.ui.client.controllers.session;

import nl.sodeso.gwt.ui.client.controllers.session.domain.LoginResult;

/**
 * @author Ronald Mathies
 */
public interface ServerLoginState {

    void state(LoginResult state);

}
