package nl.sodeso.gwt.ui.client.form;

import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.util.HasKey;

/**
 * @author Ronald Mathies
 */
public class EntryWithContainer extends Entry implements IsValidationContainer, IsRevertable, HasKey {

    // CSS style classes.
    private final static String STYLE_ENTRY_CONTAINER = "entry-container";
    private final static String STYLE_WIDGET = "widget";

    private Widget widget;

    public EntryWithContainer(String key, Widget widget) {
        super();
        addStyleName(STYLE_ENTRY_CONTAINER);

        this.widget = widget;
        this.widget.addStyleName(STYLE_WIDGET);
        add(this.widget);

        setKey(key);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_ENTRY_SUFFIX;
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, this.widget);
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.widget);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.widget);
    }
}
