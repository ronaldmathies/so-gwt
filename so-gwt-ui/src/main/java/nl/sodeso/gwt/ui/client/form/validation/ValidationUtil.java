package nl.sodeso.gwt.ui.client.form.validation;

import nl.sodeso.gwt.ui.client.form.validation.events.ValidationChangedEvent;
import nl.sodeso.gwt.ui.client.form.validation.rules.ValidationRule;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import static nl.sodeso.gwt.ui.client.form.validation.ValidationMessage.Level;

/**
 * @author Ronald Mathies
 */
public class ValidationUtil {

    /**
     * Validate the given object. Check if the object is a <code>Collection</code>, <code>Array</code> or a single object.
     *
     * @param handler the validation handler.
     * @param object the object to validate.
     */
    @SuppressWarnings("unchecked")
    public static void validate(ValidationCompletedHandler handler, Object object) {
        if (object instanceof Collection) {
            validateCollection(handler, (Collection) object);
        } else if (object instanceof Map) {
            validateMap(handler, (Map) object);
        } else if (object instanceof Object[]) {
            validateArray(handler, (Object[]) object);
        } else {
            validateSingleValidatable(handler, object);
        }
    }

    /**
     * Returns the 'worst' level found within the validation results.
     *
     * @param result the validation results.
     * @return the 'worst' level.
     */
    public static Level findWorstLevel(ValidationResult result) {
        Level level = Level.INFO;

        for (ValidationMessage message : result.getMessages()) {
            if (message.getLevel().ordinal() > level.ordinal()) {
                level = message.getLevel();
            }
        }

        return level;
    }


    /**
     * Validates the keys and values of the map.
     *
     * @param handler the validation handler.
     * @param objects a map with objects.
     */
    private static void validateMap(final ValidationCompletedHandler handler, Map<Object, Object> objects) {
        validateCollectionLoop(new ValidationCompletedHandler() {
            @Override
            public void completed(ValidationResult result) {

                validateCollectionLoop(new ValidationCompletedHandler() {
                    @Override
                    public void completed(ValidationResult result) {
                        handler.completed(result);
                    }
                }, result, objects.values().iterator());

            }
        }, new ValidationResult(), objects.keySet().iterator());
    }

    /**
     * Validates an array of objects.
     *
     * @param handler the validation handler.
     * @param objects an array of objects.
     */
    private static void validateArray(ValidationCompletedHandler handler, Object[] objects) {
        validateCollection(handler, Arrays.asList(objects));
    }

    /**
     * Validates a collection of objects.
     *
     * @param handler the validation handler.
     * @param objects a collection of objects.
     */
    @SuppressWarnings("unchecked")
    private static void validateCollection(ValidationCompletedHandler handler, Collection objects) {
        validateCollectionLoop(handler, new ValidationResult(), objects.iterator());
    }

    /**
     * This is a constant loop, as long as we have next objects we keep on calling this loop.
     *
     * @param finalValidationCompletedHandler the final validation completed handler to call when there are no more objects within the objectsIter iterator.
     * @param mergeWithThis                   the 'master' validation result container, all validation results must be merged with this container.
     * @param objectsIter                     the objects iterator which will be used to validate each individual object.
     */
    private static void validateCollectionLoop(final ValidationCompletedHandler finalValidationCompletedHandler, ValidationResult mergeWithThis, final Iterator<Object> objectsIter) {
        if (objectsIter.hasNext()) {

            Object objectToValidate = objectsIter.next();

            validate(result -> {
                mergeWithThis.merge(result);

                if (objectsIter.hasNext()) {
                    validateCollectionLoop(finalValidationCompletedHandler, mergeWithThis, objectsIter);
                } else {
                    finalValidationCompletedHandler.completed(mergeWithThis);
                }

            }, objectToValidate);
        } else {
            finalValidationCompletedHandler.completed(mergeWithThis);
        }
    }

    /**
     * Validates a single object, the object itself can be validatable or a validation container or even both.
     *
     * @param object the object to validate.
     * @return the validation results.
     */
    private static void validateSingleValidatable(final ValidationCompletedHandler handler, final Object object) {
        if (object instanceof IsValidatable) {

            IsValidatable validatable = (IsValidatable) object;

            Iterator<ValidationRule> ruleIter = validatable.getValidationRules().iterator();

            validateSingleLoop(result -> {
                validatable.fireValidationEvent(new ValidationChangedEvent(validatable, result));

                validateSingleContainer(result1 -> handler.completed(result1), result, object);
            }, new ValidationResult(), ruleIter);
        } else {
            validateSingleContainer(handler, new ValidationResult(), object);
        }
    }


    private static void validateSingleContainer(ValidationCompletedHandler handler, ValidationResult validationResult, Object object) {
        if (object instanceof IsValidationContainer) {
            IsValidationContainer validationContainer = (IsValidationContainer) object;
            validationContainer.validateContainer(result -> {
                validationResult.merge(result);

                handler.completed(validationResult);
            });
        } else {
            handler.completed(validationResult);
        }
    }

    private static void validateSingleLoop(final ValidationCompletedHandler finalValidationCompletedHandler, final ValidationResult mergeWiththis, final Iterator<ValidationRule> rulesIter) {

        if (rulesIter.hasNext()) {

            ValidationRule rule = rulesIter.next();
            if (rule.isApplicable()) {
                rule.validate((result) -> {
                    mergeWiththis.merge(result);

                    if (rulesIter.hasNext()) {
                        validateSingleLoop(finalValidationCompletedHandler, mergeWiththis, rulesIter);
                    } else {
                        finalValidationCompletedHandler.completed(mergeWiththis);
                    }
                });
            } else {

                if (rulesIter.hasNext()) {
                    validateSingleLoop(finalValidationCompletedHandler, mergeWiththis, rulesIter);
                } else {
                    finalValidationCompletedHandler.completed(mergeWiththis);
                }

            }
        } else {
            finalValidationCompletedHandler.completed(mergeWiththis);
        }

    }

}
