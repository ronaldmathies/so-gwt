package nl.sodeso.gwt.ui.client.form.button;

import com.google.gwt.event.dom.client.ClickHandler;
import nl.sodeso.gwt.ui.client.resources.Resources;

/**
 * Button representing an <code>Add</code> button.
 *
 * @author Ronald Mathies
 */
public class OkButton {

    // Keys for so-key entries.
    public static final String KEY = "ok";

    public static class WithLabel extends SimpleButton {

        public WithLabel() {
            this(null);
        }

        public WithLabel(ClickHandler handler) {
            super(KEY, Resources.BUTTON_I18N.Ok(), Style.BLUE);

            if (handler != null) {
                addClickHandler(handler);
            }
        }
    }

}
