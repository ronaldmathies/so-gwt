package nl.sodeso.gwt.ui.client.form.table.summary;

import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.ColumnSortList;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import nl.sodeso.gwt.ui.client.form.table.CellTableField;
import nl.sodeso.gwt.ui.client.form.table.DefaultEmptyTableMessage;
import nl.sodeso.gwt.ui.client.form.table.DefaultTableCellGroupHeader;

import java.util.Comparator;

/**
 * @author Ronald Mathies
 */
public class SummaryTableField<S extends AbstractSummaryItem>  extends CellTableField<S> {

    @SuppressWarnings("unchecked")
    public SummaryTableField() {
        super("summary", 10000);

        this.setTableBuilder(new DefaultTableCellGroupHeader<S>(this, getListDataProvider()) {
            @Override
            public String getHeaderText(AbstractSummaryItem object) {
                return object.getLabel().substring(0, 1);
            }
        });

        TextColumn column = new TextColumn<AbstractSummaryItem>() {
            @Override
            public String getValue(AbstractSummaryItem object) {
                return object.getLabel();
            }
        };
        this.addColumn(column);

        ColumnSortEvent.ListHandler<S> listHandler = new ColumnSortEvent.ListHandler<>(getListDataProvider().getList());
        listHandler.setComparator(column, Comparator.comparing(AbstractSummaryItem::getLabel));
        this.addColumnSortHandler(listHandler);

        // Set the default sorting to ascending.
        column.setDefaultSortAscending(true);

        // Set the sorting order of the cell list to the first and only column.
        ColumnSortList.ColumnSortInfo columnSortInfo = new ColumnSortList.ColumnSortInfo(column, true);
        this.getColumnSortList().push(columnSortInfo);

        this.setColumnWidth(0, 100, com.google.gwt.dom.client.Style.Unit.PCT);
        this.setEmptyTableWidget(new DefaultEmptyTableMessage("no-entries", "No entries"));
    }

    public void addSelectionChangeHandler(SelectionChangeEvent.Handler handler) {
        if (getSelectionModel() == null) {
            SingleSelectionModel<S> endpointsTableSingleSelectionModel = new SingleSelectionModel<>();
            setSelectionModel(endpointsTableSingleSelectionModel);
        }

        getSelectionModel().addSelectionChangeHandler(handler);
    }

}
