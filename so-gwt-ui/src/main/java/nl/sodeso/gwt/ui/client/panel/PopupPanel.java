package nl.sodeso.gwt.ui.client.panel;

import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.util.HasKey;

/**
 * @author Ronald Mathies
 */
public class PopupPanel extends com.google.gwt.user.client.ui.PopupPanel implements IsValidationContainer, IsRevertable, HasKey {

    private static final String KEY_POPUP_SUFFIX = "-popup";

    private static boolean glasspanelVisible = false;
    private boolean iShowedGlasspanel = false;

    private WindowPanel windowPanel = null;

    /**
     * Constructs a new popup panel.
     *
     * @param key the key of the popup panel.
     * @param style the style of the popup panel.
     * @param title the title of the popup panel
     * @param autoHide when true the popup panel while hide when the user clicks outside of the popups bounds.
     * @param modal when true the only thing the user can use is the popup itself, all other page contents is blocked.
     */
    public PopupPanel(String key, WindowPanel.Style style, String title, boolean autoHide, boolean modal) {
        super(autoHide, modal);

        setAnimationEnabled(true);
        setAnimationType(AnimationType.CENTER);

        if (!glasspanelVisible) {
            setGlassEnabled(true);
            glasspanelVisible = true;
            iShowedGlasspanel = true;
        }

        windowPanel = new WindowPanel(key, title, style);
        add(windowPanel);

        setKey(key);
    }

    /**
     * {@inheritDoc}
     */
    public String getKeySuffix() {
        return KEY_POPUP_SUFFIX;
    }

    @Override
    protected void onDetach() {
        super.onDetach();

        if (iShowedGlasspanel) {
            glasspanelVisible = false;
        }
    }

    /**
     * Adds an additional style name to the window which is inside the popup.
     * @param style the style name to add.
     */
    public void addStyleNameToWindow(String style) {
        windowPanel.addStyleName(style);
    }

    /**
     * Adds a widget to the body of the window.
     * @param widget the widget to add.
     */
    public void addToBody(Widget widget) {
        windowPanel.addToBody(widget);
    }

    /**
     * Adds a widget to the footer of the window.
     *
     * @param align alignment of the widget that will be added to the footer.
     * @param widgets the widget to add to the footer.
     */
    public void addToFooter(Align align, Widget ... widgets) {
        windowPanel.addToFooter(align, widgets);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, this.windowPanel);
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.windowPanel);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.windowPanel);
    }
}
