package nl.sodeso.gwt.ui.client.controllers.menu.events;

import com.google.web.bindery.event.shared.Event;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;

/**
 * @author Ronald Mathies
 */
public class MenuItemSelectedEvent extends Event<MenuItemSelectedEventHandler>  {

    public static final Type<MenuItemSelectedEventHandler> TYPE = new Type<>();

    private MenuItem selectedMenuItem;

    public MenuItemSelectedEvent(MenuItem selectedMenuItem) {
        this.selectedMenuItem = selectedMenuItem;
    }

    @Override
    public Type<MenuItemSelectedEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(MenuItemSelectedEventHandler handler) {
        handler.onEvent(this);
    }

    public MenuItem getSelectedMenuItem() {
        return this.selectedMenuItem;
    }
}
