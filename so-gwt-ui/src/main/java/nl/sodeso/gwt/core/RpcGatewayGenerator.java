package nl.sodeso.gwt.core;

import com.google.gwt.core.ext.Generator;
import com.google.gwt.core.ext.GeneratorContext;
import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.*;
import com.google.gwt.user.rebind.ClassSourceFileComposerFactory;
import com.google.gwt.user.rebind.SourceWriter;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Iterator;

/**
 * @author Ronald Mathies
 */
public class RpcGatewayGenerator extends Generator {

    /**
     * {@inheritDoc}
     */
    @Override
    public String generate(TreeLogger logger, GeneratorContext context, String requestedClass) throws UnableToCompleteException {
        TypeOracle typeOracle = context.getTypeOracle();
        assert(typeOracle != null);
        logger.log(TreeLogger.INFO, "Generating RPC Gateway for '" + requestedClass + "'.", null);

        JClassType objectType = typeOracle.findType(requestedClass);
        if (objectType == null) {
            logger.log(TreeLogger.ERROR, "Unable to find metadata for type '" + requestedClass + "'", null);
            throw new UnableToCompleteException();
        }

        JClassType rpcAsyncClassType = getRpcAsyncInterface(objectType);
        if (rpcAsyncClassType == null) {
            logger.log(TreeLogger.ERROR, "'" + requestedClass + "' should implement the RpcGateway interface with the correct RpcAsync generic.", null);
            throw new UnableToCompleteException();
        }

        String implPackageName = objectType.getPackage().getName();
        String implClassName = objectType.getSimpleSourceName() + "Impl";
        ClassSourceFileComposerFactory composerFactory = new ClassSourceFileComposerFactory(implPackageName, implClassName);

        composerFactory.addImport(com.google.gwt.core.client.Duration.class.getCanonicalName());
        composerFactory.addImport(com.google.gwt.core.client.GWT.class.getCanonicalName());
        composerFactory.addImport(com.google.gwt.user.client.rpc.AsyncCallback.class.getCanonicalName());
        composerFactory.addImport(com.google.gwt.user.client.rpc.HasRpcToken.class.getCanonicalName());
        composerFactory.addImport(com.google.gwt.user.client.rpc.XsrfToken.class.getCanonicalName());
        composerFactory.addImport(nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback.class.getCanonicalName());
        composerFactory.addImport(nl.sodeso.gwt.ui.client.rpc.XsrfGateway.class.getCanonicalName());
        composerFactory.addImport(com.google.gwt.user.client.Timer.class.getCanonicalName());
        composerFactory.addImport(nl.sodeso.gwt.ui.client.panel.Overlay.class.getCanonicalName());
        composerFactory.addImport(nl.sodeso.gwt.ui.client.panel.OverlayTimer.class.getCanonicalName());

        composerFactory.addImport(java.util.ArrayList.class.getCanonicalName());
        composerFactory.addImport(java.util.logging.Level.class.getCanonicalName());
        composerFactory.addImport(java.util.logging.Logger.class.getCanonicalName());

        // Add the correct interface.
        composerFactory.setSuperclass(objectType.getQualifiedSourceName());

        PrintWriter printWriter = context.tryCreate(logger, implPackageName, implClassName);
        if (printWriter != null) {
            SourceWriter sourceWriter = composerFactory.createSourceWriter(context, printWriter);

            sourceWriter.println("private static Logger logger = Logger.getLogger(\"" + objectType.getSimpleSourceName() + ".Endpoint\");");
            sourceWriter.println("private static Logger rpcLogger = Logger.getLogger(\"rpc\");");
//            sourceWriter.println("private Timer overlayTimer = null;");
//            sourceWriter.println("private void startOverlayTimer() {");
//            sourceWriter.indent();
//            sourceWriter.println("overlayTimer = new Timer() {");
//            sourceWriter.indent();
//            sourceWriter.println("@Override");
//            sourceWriter.println("public void run() {");
//            sourceWriter.indent();
//            sourceWriter.println("Overlay.show();");
//            sourceWriter.outdent();
//            sourceWriter.println("}");
//            sourceWriter.outdent();
//            sourceWriter.println("};");
//            sourceWriter.println("overlayTimer.schedule(1000);");
//            sourceWriter.outdent();
//            sourceWriter.println("}");
//
//            sourceWriter.println("private void stopOverlayTimer() {");
//            sourceWriter.indent();
//            sourceWriter.println("if (overlayTimer != null) {");
//            sourceWriter.indent();
//            sourceWriter.println("if (overlayTimer.isRunning()) {");
//            sourceWriter.indent();
//            sourceWriter.println("overlayTimer.cancel();");
////            sourceWriter.outdent();
////            sourceWriter.println("} else {");
////            sourceWriter.indent();
//            sourceWriter.outdent();
//            sourceWriter.println("}");
//            sourceWriter.println("overlayTimer = null;");
//            sourceWriter.outdent();
//            sourceWriter.println("}");
//            sourceWriter.println("Overlay.hide();");
//            sourceWriter.outdent();
//            sourceWriter.println("}");

            // Loop over the rpc async class to generate the annotation.
            for (JMethod jMethod : rpcAsyncClassType.getInheritableMethods()) {
                composeMethod(sourceWriter, objectType, jMethod);
            }

            sourceWriter.commit(logger);
        }

        return implPackageName + "." + implClassName;
    }

    private JClassType getRpcAsyncInterface(JClassType objectType) {
        // Find the RpcGateway interface so we can find the generic type which will contain the
        // annotation we need to generate.
        JClassType rpcClassType = null;
        for (JClassType implementedInterfaceType : objectType.getImplementedInterfaces()) {

            if (implementedInterfaceType.getSimpleSourceName().equals(RpcGateway.class.getSimpleName())) {
                if (implementedInterfaceType.isParameterized() != null) {
                    rpcClassType = implementedInterfaceType.isParameterized().getTypeArgs()[0];
                }
            }

        }

        return rpcClassType;
    }

    private void composeMethod(SourceWriter sourceWriter, JClassType objectType, JMethod jMethod) {
        StringBuilder parameters = new StringBuilder();
        StringBuilder arguments = new StringBuilder();
        Iterator<JParameter> parameterIter = Arrays.asList(jMethod.getParameters()).iterator();

        JParameter asyncCallbackParameter = null;
        while(parameterIter.hasNext()) {
            JParameter jParameter = parameterIter.next();
            parameters.append("final " + jParameter.getType().getQualifiedSourceName() + " " + jParameter.getName());

            if (parameterIter.hasNext()) {
                parameters.append(", ");

                arguments.append(jParameter.getName());
                arguments.append(", ");
            } else {
                asyncCallbackParameter = jParameter;
            }
        }

        sourceWriter.println("public void " + jMethod.getName() + "(" + parameters.toString() + ") {");
        sourceWriter.indent();

        sourceWriter.println("final OverlayTimer overlayTimer = new OverlayTimer();");
        sourceWriter.println("overlayTimer.start();");

        sourceWriter.println("logger.log(Level.FINE, \"Performing rpc call '" + objectType.getName() + "." + jMethod.getName() + "'.\");");
        sourceWriter.println("XsrfGateway.getNewXsrfToken(new DefaultAsyncCallback<XsrfToken>() {");

        sourceWriter.indent();
        sourceWriter.println("@Override");
        sourceWriter.println("public void success(XsrfToken token) {");
        sourceWriter.indent();

        String rpcAsyncClass = objectType.getName().replace("RpcGateway", "RpcAsync");
        sourceWriter.println(rpcAsyncClass + " rpcAsync =  getRpcAsync();");
        sourceWriter.println("((HasRpcToken) rpcAsync).setRpcToken(token);");
        sourceWriter.println("final Duration duration = new Duration();");
        sourceWriter.println("rpcAsync." + jMethod.getName() + "(" + arguments + " new " + asyncCallbackParameter.getType().getParameterizedQualifiedSourceName() + "() {");
        sourceWriter.indent();
        sourceWriter.println("public void onFailure(Throwable caught) {");
        sourceWriter.indent();
        sourceWriter.println("overlayTimer.stop();");
        sourceWriter.println("logger.log(Level.SEVERE, \"Failed to perform call '" + objectType.getName() + "." + jMethod.getName() + "'.\", caught);");
        sourceWriter.println(asyncCallbackParameter.getName() + ".onFailure(caught);");
        sourceWriter.outdent();
        sourceWriter.println("}");
        String asyncCallbackGeneric = ((JParameterizedType)asyncCallbackParameter.getType()).getTypeArgs()[0].getParameterizedQualifiedSourceName();
        sourceWriter.println("public void onSuccess(" + asyncCallbackGeneric + " __result) {");
        sourceWriter.indent();
        sourceWriter.println("overlayTimer.stop();");
        sourceWriter.println("logger.log(Level.FINE, \"Successfully performed call '" + objectType.getName() + "." + jMethod.getName() + "' (duration: \" + duration.elapsedMillis() + \"ms)\");");
        sourceWriter.println("rpcLogger.log(Level.FINE, \"{ \\\"method\\\": \\\"" + objectType.getName() + "." + jMethod.getName() + "\\\", \\\"duration\\\": \" + duration.elapsedMillis() + \"}\");");
        sourceWriter.println(asyncCallbackParameter.getName() + ".onSuccess(__result);");
        sourceWriter.outdent();
        sourceWriter.println("}");
        sourceWriter.outdent();

        sourceWriter.println("});");
        sourceWriter.outdent();

        sourceWriter.println("}");
        sourceWriter.outdent();

        sourceWriter.println("});");

        sourceWriter.outdent();
        sourceWriter.println("}");

    }

}
