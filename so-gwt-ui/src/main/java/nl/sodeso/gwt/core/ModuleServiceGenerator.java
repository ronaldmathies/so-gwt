package nl.sodeso.gwt.core;

import com.google.gwt.core.ext.Generator;
import com.google.gwt.core.ext.GeneratorContext;
import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.core.ext.typeinfo.JPackage;
import com.google.gwt.core.ext.typeinfo.TypeOracle;
import com.google.gwt.user.rebind.ClassSourceFileComposerFactory;
import com.google.gwt.user.rebind.SourceWriter;
import nl.sodeso.gwt.ui.client.application.module.ModuleEntryPoint;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * GWT Generator class which will create the implementation for the AvailableModuleService
 * interface.
 *
 * This generator will locate all classes extending the ModuleEntryPoint class. After this
 * it will create the implementation which will return a list containing all the extending classes.
 *
 * @author Ronald Mathies
 */
public class ModuleServiceGenerator extends Generator {

    /**
     * {@inheritDoc}
     */
    @Override
    public String generate(TreeLogger logger, GeneratorContext context, String requestedClass) throws UnableToCompleteException {
        TypeOracle typeOracle = context.getTypeOracle();
        assert(typeOracle != null);
        logger.log(TreeLogger.INFO, "Finding available modules.", null);

        JClassType objectType = typeOracle.findType(requestedClass);
        if (objectType == null) {
            logger.log(TreeLogger.ERROR, "Unable to find metadata for type '" + requestedClass + "'", null);
            throw new UnableToCompleteException();
        }

        List<JClassType> availableModules = new ArrayList<JClassType>();
        for (JPackage jPackage : typeOracle.getPackages()) {

            for (JClassType jClassType : jPackage.getTypes()) {

                 if (jClassType.getSuperclass() != null && jClassType.getSuperclass().getSimpleSourceName().equals("ModuleEntryPoint")) {
                     logger.log(TreeLogger.INFO, "Found module '" + jClassType.getName() + "'", null);
                     availableModules.add(jClassType);
                 }

            }

        }

        String implPackageName = objectType.getPackage().getName();
        String implClassName = objectType.getSimpleSourceName() + "Impl";
        ClassSourceFileComposerFactory composerFactory = new ClassSourceFileComposerFactory(implPackageName, implClassName);

        composerFactory.addImport(Arrays.class.getCanonicalName());
        composerFactory.addImport(List.class.getCanonicalName());
        composerFactory.addImport(ModuleEntryPoint.class.getCanonicalName());
        composerFactory.addImplementedInterface(objectType.getQualifiedSourceName());

        for (JClassType moduleClassType : availableModules) {
            composerFactory.addImport(moduleClassType.getQualifiedSourceName());
        }

        PrintWriter printWriter = context.tryCreate(logger, implPackageName, implClassName);
        if (printWriter != null) {
            SourceWriter sourceWriter = composerFactory.createSourceWriter(context, printWriter);

            composeAvailableModulesMethod(sourceWriter, availableModules);
            sourceWriter.commit(logger);
        }

        return implPackageName + "." + implClassName;
    }

    private void composeAvailableModulesMethod(SourceWriter sourceWriter, List<JClassType> availableModules) {
        sourceWriter.println("public List<ModuleEntryPoint> availableModules() {");
        sourceWriter.indent();
        sourceWriter.println("return Arrays.asList(new ModuleEntryPoint[] {");
        sourceWriter.indent();

        Iterator<JClassType> availableModulesIter = availableModules.iterator();
        while (availableModulesIter.hasNext()) {
            JClassType moduleClassType = availableModulesIter.next();
            sourceWriter.println("new " + moduleClassType.getSimpleSourceName() + "()" + (availableModulesIter.hasNext() ? ", " : ""));
        }

        sourceWriter.outdent();
        sourceWriter.println("});");
        sourceWriter.outdent();
        sourceWriter.println("}");
    }
}
