package nl.sodeso.gwt.security.server;

import nl.sodeso.gwt.security.server.annotation.EndpointMethodSecurity;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Method;

/**
 * @author Ronald Mathies
 */
public aspect EndpointSecurityAspect {

    private pointcut check() : execution(* *(..)) && @EndpointMethodSecurity());

    before() returning (Object retObj): check() {
        Method method = MethodSignature.class.cast(thisJoinPoint.getSignature()).getMethod();
        EndpointMethodSecurity annotation = method.getAnnotation(EndpointMethodSecurity.class);
    }

//    @Around("execution(* *(..)) && @annotation(EndpointMethodSecurity)")
//    public Object check(ProceedingJoinPoint point) throws InvocationTargetException{
//
//        Method method = MethodSignature.class.cast(point.getSignature()).getMethod();
//        EndpointMethodSecurity annotation = method.getAnnotation(EndpointMethodSecurity.class);
//
//        Object result = null;
//        try {
//            result = point.proceed();
//        } catch (Throwable e) {
//            throw new InvocationTargetException(e, "Error during invocation of method '" + method.getName() + "'.");
//        }
//
//        return result;
//    }

}
