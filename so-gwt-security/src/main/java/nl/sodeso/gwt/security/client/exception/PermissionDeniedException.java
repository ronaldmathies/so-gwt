package nl.sodeso.gwt.security.client.exception;

/**
 * @author Ronald Mathies
 */
public class PermissionDeniedException extends Exception {

    private String key;
    private String checkFailedMessage;

    protected PermissionDeniedException() {}

    public PermissionDeniedException(String key, String checkFailedMessage) {
        super(checkFailedMessage);

        this.key = key;
        this.checkFailedMessage = checkFailedMessage;
    }

    public String getKey() {
        return key;
    }

    public String getCheckFailedMessage() {
        return checkFailedMessage;
    }
}
