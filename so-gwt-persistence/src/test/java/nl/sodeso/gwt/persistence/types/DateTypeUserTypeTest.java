package nl.sodeso.gwt.persistence.types;

import nl.sodeso.gwt.persistence.types.domain.DateEntity;
import nl.sodeso.gwt.ui.client.types.DateType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import nl.sodeso.persistence.hibernate.UnitOfWork;
import nl.sodeso.persistence.hibernate.UnitOfWorkFactory;
import org.hibernate.Session;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Ronald Mathies
 */
public class DateTypeUserTypeTest {

    private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

    private static final String PU = "usertypes-pu";

    private Session session = ThreadSafeSession.getSession(PU);

    @Test
    public void testType() {
        UnitOfWork unitOfwork = UnitOfWorkFactory.startUnitOfWork(PU);

        DateEntity entity = new DateEntity();
        entity.setDateNull(new DateType(null));
        entity.setDateValue(new DateType(new Date()));

        // step #1: Store the entity and flush and evict the entity.
        session.save(entity);
        session.flush();
        session.evict(entity);

        assertNotNull(entity.getId());

        // Step #2: Retrieve the entity to read and flush and evict the entity.
        DateEntity retreivedEntityToRead = session.get(DateEntity.class, entity.getId());
        assertNotNull(retreivedEntityToRead);

        // Step #3: Change the value
        DateType dateValue = retreivedEntityToRead.getDateValue();

        Date newDate = new Date();

        dateValue.setValue(newDate);

        // Step #4: Retreive the entity to write.
        DateEntity retreivedEntityToWrite = session.get(DateEntity.class, entity.getId());
        retreivedEntityToWrite.setDateValue(dateValue);

        session.save(retreivedEntityToWrite);
        session.flush();
        session.evict(retreivedEntityToWrite);

        // Step #4: Retrieve the entity to read.
        retreivedEntityToRead = session.get(DateEntity.class, entity.getId());
        assertEquals(sdf.format(newDate), sdf.format(retreivedEntityToRead.getDateValue().getValue()));
        assertEquals(sdf.format(newDate), sdf.format(retreivedEntityToRead.getDateValue().getOriginalValue()));

        unitOfwork.commit();
    }

}