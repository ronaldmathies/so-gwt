package nl.sodeso.gwt.persistence.types;

import nl.sodeso.commons.properties.annotations.FileResource;
import nl.sodeso.commons.properties.annotations.FileResources;
import nl.sodeso.commons.properties.annotations.Resource;
import nl.sodeso.commons.properties.containers.file.FileContainer;

/**
 * @author Ronald Mathies
 */
@Resource(
        domain = "usertypes-pu"
)
@FileResources(
        files = {
                @FileResource(file = "/persistence.properties")
        }
)
public class UnitOfWorkProperties extends FileContainer {
    
}
