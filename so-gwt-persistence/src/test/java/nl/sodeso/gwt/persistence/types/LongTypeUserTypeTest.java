package nl.sodeso.gwt.persistence.types;

import nl.sodeso.gwt.persistence.types.domain.LongEntity;
import nl.sodeso.gwt.ui.client.types.LongType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import nl.sodeso.persistence.hibernate.UnitOfWork;
import nl.sodeso.persistence.hibernate.UnitOfWorkFactory;
import org.hibernate.Session;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Ronald Mathies
 */
public class LongTypeUserTypeTest {

    private static final String PU = "usertypes-pu";

    private Session session = ThreadSafeSession.getSession(PU);

    @Test
    public void testType() {
        UnitOfWork unitOfwork = UnitOfWorkFactory.startUnitOfWork(PU);

        LongEntity entity = new LongEntity();
        entity.setLongNull(new LongType(null));
        entity.setLongValue(new LongType(10l));

        // step #1: Store the entity and flush and evict the entity.
        session.save(entity);
        session.flush();
        session.evict(entity);

        assertNotNull(entity.getId());

        // Step #2: Retrieve the entity to read and flush and evict the entity.
        LongEntity retreivedEntityToRead = session.get(LongEntity.class, entity.getId());
        assertNotNull(retreivedEntityToRead);

        // Step #3: Change the value
        LongType longValue = retreivedEntityToRead.getLongValue();
        longValue.setValue(20l);

        // Step #4: Retreive the entity to write.
        LongEntity retreivedEntityToWrite = session.get(LongEntity.class, entity.getId());
        retreivedEntityToWrite.setLongValue(longValue);

        session.save(retreivedEntityToWrite);
        session.flush();
        session.evict(retreivedEntityToWrite);

        // Step #4: Retrieve the entity to read.
        retreivedEntityToRead = session.get(LongEntity.class, entity.getId());
        assertEquals(new Long(20l), retreivedEntityToRead.getLongValue().getValue());
        assertEquals(new Long(20l), retreivedEntityToRead.getLongValue().getOriginalValue());

        unitOfwork.commit();
    }

}