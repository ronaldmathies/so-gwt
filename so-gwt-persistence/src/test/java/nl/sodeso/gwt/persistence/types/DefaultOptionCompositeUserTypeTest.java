package nl.sodeso.gwt.persistence.types;

import nl.sodeso.gwt.persistence.types.domain.OptionEntity;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import nl.sodeso.persistence.hibernate.UnitOfWork;
import nl.sodeso.persistence.hibernate.UnitOfWorkFactory;
import org.hibernate.Session;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Ronald Mathies
 */
public class DefaultOptionCompositeUserTypeTest {

    private static final String PU = "usertypes-pu";

    private Session session = ThreadSafeSession.getSession(PU);

    @Test
    public void testType() {
        UnitOfWork unitOfwork = UnitOfWorkFactory.startUnitOfWork(PU);

        OptionEntity entity = new OptionEntity();
        entity.setOptionNull(new OptionType<>(null));
        entity.setOptionValue(new OptionType<>(new DefaultOption("k1", "c1", "d1")));

        // step #1: Store the entity and flush and evict the entity.
        session.save(entity);
        session.flush();
        session.evict(entity);

        assertNotNull(entity.getId());

        // Step #2: Retrieve the entity to read and flush and evict the entity.
        OptionEntity retreivedEntityToRead = session.get(OptionEntity.class, entity.getId());
        assertNotNull(retreivedEntityToRead);

        // Step #3: Change the value
        OptionType<DefaultOption> optionValue = retreivedEntityToRead.getOptionValue();
        optionValue.setValue(new DefaultOption("k2", "c2", "d2"));

        // Step #4: Retreive the entity to write.
        OptionEntity retreivedEntityToWrite = session.get(OptionEntity.class, entity.getId());
        retreivedEntityToWrite.setOptionValue(optionValue);

        session.save(retreivedEntityToWrite);
        session.flush();
        session.evict(retreivedEntityToWrite);

        // Step #4: Retrieve the entity to read.
        retreivedEntityToRead = session.get(OptionEntity.class, entity.getId());
        assertEquals("k2", retreivedEntityToRead.getOptionValue().getValue().getKey());
        assertEquals("c2", retreivedEntityToRead.getOptionValue().getValue().getCode());
        assertEquals("d2", retreivedEntityToRead.getOptionValue().getValue().getDescription());

        assertEquals("k2", retreivedEntityToRead.getOptionValue().getOriginalValue().getKey());
        assertEquals("c2", retreivedEntityToRead.getOptionValue().getOriginalValue().getCode());
        assertEquals("d2", retreivedEntityToRead.getOptionValue().getOriginalValue().getDescription());

        unitOfwork.commit();
    }

}