package nl.sodeso.gwt.persistence.types;

import nl.sodeso.gwt.persistence.types.domain.StringEntity;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import nl.sodeso.persistence.hibernate.UnitOfWork;
import nl.sodeso.persistence.hibernate.UnitOfWorkFactory;
import org.hibernate.Session;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Ronald Mathies
 */
public class StringTypeUserTypeTest {

    private static final String PU = "usertypes-pu";

    private Session session = ThreadSafeSession.getSession(PU);
    
    @Test
    public void testType() {
        UnitOfWork unitOfwork = UnitOfWorkFactory.startUnitOfWork(PU);

        StringEntity entity = new StringEntity();
        entity.setStringNull(new StringType(null));
        entity.setStringValue(new StringType("value"));

        // step #1: Store the entity and flush and evict the entity.
        session.save(entity);
        session.flush();
        session.evict(entity);

        assertNotNull(entity.getId());

        // Step #2: Retrieve the entity to read and flush and evict the entity.
        StringEntity retreivedEntityToRead = session.get(StringEntity.class, entity.getId());
        assertNotNull(retreivedEntityToRead);

        // Step #3: Change the value
        StringType stringValue = retreivedEntityToRead.getStringValue();
        stringValue.setValue("new value");

        // Step #4: Retreive the entity to write.
        StringEntity retreivedEntityToWrite = session.get(StringEntity.class, entity.getId());
        retreivedEntityToWrite.setStringValue(stringValue);

        session.save(retreivedEntityToWrite);
        session.flush();
        session.evict(retreivedEntityToWrite);

        // Step #4: Retrieve the entity to read.
        retreivedEntityToRead = session.get(StringEntity.class, entity.getId());
        assertEquals("new value", retreivedEntityToRead.getStringValue().getValue());
        assertEquals("new value", retreivedEntityToRead.getStringValue().getOriginalValue());

        unitOfwork.commit();
    }

}