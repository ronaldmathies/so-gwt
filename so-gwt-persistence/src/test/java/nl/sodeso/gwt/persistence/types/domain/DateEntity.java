package nl.sodeso.gwt.persistence.types.domain;

import nl.sodeso.gwt.persistence.types.DateTypeUserType;
import nl.sodeso.gwt.ui.client.types.DateType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = "usertypes-pu")
@Entity
@Table(name = "t_date")
@TypeDefs(
    value = {
            @TypeDef(name = "date", typeClass = DateTypeUserType.class)
    }
)
public class DateEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id = null;

    @Type(type = "date")
    @Column(name = "dateValue", nullable = false)
    private DateType dateValue = null;

    @Type(type = "date")
    @Column(name = "dateNull", nullable = true)
    private DateType dateNull = null;

    public DateEntity() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DateType getDateValue() {
        return dateValue;
    }

    public void setDateValue(DateType dateValue) {
        this.dateValue = dateValue;
    }

    public DateType getDateNull() {
        return dateNull;
    }

    public void setDateNull(DateType dateNull) {
        this.dateNull = dateNull;
    }
}
