package nl.sodeso.gwt.persistence.types;

import nl.sodeso.gwt.persistence.types.domain.IntEntity;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import nl.sodeso.persistence.hibernate.UnitOfWork;
import nl.sodeso.persistence.hibernate.UnitOfWorkFactory;
import org.hibernate.Session;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Ronald Mathies
 */
public class IntTypeUserTypeTest {

    private static final String PU = "usertypes-pu";

    private Session session = ThreadSafeSession.getSession(PU);

    @Test
    public void testType() {
        UnitOfWork unitOfwork = UnitOfWorkFactory.startUnitOfWork(PU);

        IntEntity entity = new IntEntity();
        entity.setIntNull(new IntType(null));
        entity.setIntValue(new IntType(10));

        // step #1: Store the entity and flush and evict the entity.
        session.save(entity);
        session.flush();
        session.evict(entity);

        assertNotNull(entity.getId());

        // Step #2: Retrieve the entity to read and flush and evict the entity.
        IntEntity retreivedEntityToRead = session.get(IntEntity.class, entity.getId());
        assertNotNull(retreivedEntityToRead);

        // Step #3: Change the value
        IntType intValue = retreivedEntityToRead.getIntValue();
        intValue.setValue(20);

        // Step #4: Retreive the entity to write.
        IntEntity retreivedEntityToWrite = session.get(IntEntity.class, entity.getId());
        retreivedEntityToWrite.setIntValue(intValue);

        session.save(retreivedEntityToWrite);
        session.flush();
        session.evict(retreivedEntityToWrite);

        // Step #4: Retrieve the entity to read.
        retreivedEntityToRead = session.get(IntEntity.class, entity.getId());
        assertEquals(new Integer(20), retreivedEntityToRead.getIntValue().getValue());
        assertEquals(new Integer(20), retreivedEntityToRead.getIntValue().getOriginalValue());

        unitOfwork.commit();
    }

}