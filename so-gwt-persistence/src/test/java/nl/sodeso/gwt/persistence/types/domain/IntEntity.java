package nl.sodeso.gwt.persistence.types.domain;

import nl.sodeso.gwt.persistence.types.IntTypeUserType;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = "usertypes-pu")
@Entity
@Table(name = "t_int")
@TypeDefs(
    value = {
        @TypeDef(name = "int", typeClass = IntTypeUserType.class)
    }
)
public class IntEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id = null;

    @Type(type = "int")
    @Column(name = "intValue", nullable = false)
    private IntType intValue = null;

    @Type(type = "int")
    @Column(name = "intNull", nullable = true)
    private IntType intNull = null;

    public IntEntity() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public IntType getIntValue() {
        return intValue;
    }

    public void setIntValue(IntType intValue) {
        this.intValue = intValue;
    }

    public IntType getIntNull() {
        return intNull;
    }

    public void setIntNull(IntType intNull) {
        this.intNull = intNull;
    }
}
