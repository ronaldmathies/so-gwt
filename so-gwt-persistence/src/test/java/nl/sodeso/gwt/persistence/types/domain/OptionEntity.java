package nl.sodeso.gwt.persistence.types.domain;

import nl.sodeso.gwt.persistence.types.OptionCompositeUserType;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = "usertypes-pu")
@Entity
@Table(name = "t_option")
@TypeDefs(
    value = {
        @TypeDef(name = "option", typeClass = OptionCompositeUserType.class)
    }
)
public class OptionEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id = null;

    @Type(type = "option")
    @Columns(columns = {
            @Column(name = "optionValueKey", nullable = false),
            @Column(name = "optionValueCode", nullable = false),
            @Column(name = "optionValueDescription", nullable = false)
    })
    private OptionType<DefaultOption> optionValue = null;

    @Type(type = "option")
    @Columns(columns = {
            @Column(name = "optionNullKey", nullable = true),
            @Column(name = "optionNullCode", nullable = true),
            @Column(name = "optionNullDescription", nullable = true)
    })
    private OptionType<DefaultOption> optionNull = null;

    public OptionEntity() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OptionType<DefaultOption> getOptionValue() {
        return optionValue;
    }

    public void setOptionValue(OptionType<DefaultOption> optionValue) {
        this.optionValue = optionValue;
    }

    public OptionType<DefaultOption> getOptionNull() {
        return optionNull;
    }

    public void setOptionNull(OptionType<DefaultOption> optionNull) {
        this.optionNull = optionNull;
    }
}
