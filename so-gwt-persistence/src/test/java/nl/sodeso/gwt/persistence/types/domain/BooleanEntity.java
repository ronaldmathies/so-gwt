package nl.sodeso.gwt.persistence.types.domain;

import nl.sodeso.gwt.persistence.types.BooleanTypeUserType;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = "usertypes-pu")
@Entity
@Table(name = "t_boolean")
@TypeDefs(
    value = {
        @TypeDef(name = "boolean", typeClass = BooleanTypeUserType.class)
    }
)
public class BooleanEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id = null;

    @Type(type = "boolean")
    @Column(name = "booleanValue", nullable = false)
    private BooleanType booleanValue = null;

    @Type(type = "boolean")
    @Column(name = "booleanNull", nullable = true)
    private BooleanType booleanNull = null;

    public BooleanEntity() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BooleanType getBooleanValue() {
        return booleanValue;
    }

    public void setBooleanValue(BooleanType booleanValue) {
        this.booleanValue = booleanValue;
    }

    public BooleanType getBooleanNull() {
        return booleanNull;
    }

    public void setBooleanNull(BooleanType booleanNull) {
        this.booleanNull = booleanNull;
    }
}
