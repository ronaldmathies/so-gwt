package nl.sodeso.gwt.persistence.types.domain;

import nl.sodeso.gwt.persistence.types.LongTypeUserType;
import nl.sodeso.gwt.ui.client.types.LongType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = "usertypes-pu")
@Entity
@Table(name = "t_long")
@TypeDefs(
    value = {
        @TypeDef(name = "long", typeClass = LongTypeUserType.class)
    }
)
public class LongEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id = null;

    @Type(type = "long")
    @Column(name = "longValue", nullable = false)
    private LongType longValue = null;

    @Type(type = "long")
    @Column(name = "longNull", nullable = true)
    private LongType longNull = null;

    public LongEntity() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LongType getLongValue() {
        return longValue;
    }

    public void setLongValue(LongType longValue) {
        this.longValue = longValue;
    }

    public LongType getLongNull() {
        return longNull;
    }

    public void setLongNull(LongType longNull) {
        this.longNull = longNull;
    }
}
