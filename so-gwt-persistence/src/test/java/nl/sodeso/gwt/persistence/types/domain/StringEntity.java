package nl.sodeso.gwt.persistence.types.domain;

import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = "usertypes-pu")
@Entity
@Table(name = "t_string")
@TypeDefs(
    value = {
        @TypeDef(name = "string", typeClass = StringTypeUserType.class)
    }
)
public class StringEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id = null;

    @Type(type = "string")
    @Column(name = "stringValue", nullable = false)
    private StringType stringValue = null;

    @Type(type = "string")
    @Column(name = "stringNull", nullable = true)
    private StringType stringNull = null;

    public StringEntity() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StringType getStringValue() {
        return stringValue;
    }

    public void setStringValue(StringType stringValue) {
        this.stringValue = stringValue;
    }

    public StringType getStringNull() {
        return stringNull;
    }

    public void setStringNull(StringType stringNull) {
        this.stringNull = stringNull;
    }
}
