package nl.sodeso.gwt.persistence.types;

import nl.sodeso.gwt.persistence.types.domain.BooleanEntity;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import nl.sodeso.persistence.hibernate.UnitOfWork;
import nl.sodeso.persistence.hibernate.UnitOfWorkFactory;
import org.hibernate.Session;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Ronald Mathies
 */
public class BooleanTypeUserTypeTest {

    private static final String PU = "usertypes-pu";

    private Session session = ThreadSafeSession.getSession(PU);

    @Test
    public void testType() {
        UnitOfWork unitOfwork = UnitOfWorkFactory.startUnitOfWork(PU);

        BooleanEntity entity = new BooleanEntity();
        entity.setBooleanNull(new BooleanType(null));
        entity.setBooleanValue(new BooleanType(true));

        // step #1: Store the entity and flush and evict the entity.
        session.save(entity);
        session.flush();
        session.evict(entity);

        assertNotNull(entity.getId());

        // Step #2: Retrieve the entity to read and flush and evict the entity.
        BooleanEntity retreivedEntityToRead = session.get(BooleanEntity.class, entity.getId());
        assertNotNull(retreivedEntityToRead);

        // Step #3: Change the value
        BooleanType booleanValue = retreivedEntityToRead.getBooleanValue();
        booleanValue.setValue(false);

        // Step #4: Retreive the entity to write.
        BooleanEntity retreivedEntityToWrite = session.get(BooleanEntity.class, entity.getId());
        retreivedEntityToWrite.setBooleanValue(booleanValue);

        session.save(retreivedEntityToWrite);
        session.flush();
        session.evict(retreivedEntityToWrite);

        // Step #4: Retrieve the entity to read.
        retreivedEntityToRead = session.get(BooleanEntity.class, entity.getId());
        assertEquals(false, retreivedEntityToRead.getBooleanValue().getValue());
        assertEquals(false, retreivedEntityToRead.getBooleanValue().getOriginalValue());

        unitOfwork.commit();
    }

}