package nl.sodeso.gwt.persistence.types;

import nl.sodeso.gwt.ui.client.types.BooleanType;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Objects;

/**
 * A Hibernate user type to map a BooleanType to an INTEGER in the database.
 *
 * @author Ronald Mathies
 */
public class BooleanTypeUserType implements UserType {

    /**
     * {@inheritDoc}
     */
    @Override
    public int[] sqlTypes() {
        return new int[] {Types.INTEGER};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class returnedClass() {
        return BooleanType.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o1, Object o2) throws HibernateException {
        if (o1 == null || o2 == null) {
            return false;
        }

        if (o1 == o2) return true;
        if (o1.getClass() != o2.getClass()) return false;
        BooleanType booleanType1 = (BooleanType)o1;
        BooleanType booleanType2 = (BooleanType)o2;
        return  Objects.equals(booleanType1.getValue(), booleanType2.getValue());

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode(Object value) throws HibernateException {
        return ((BooleanType)value).getValue().hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object nullSafeGet(ResultSet resultSet, String[] names, SessionImplementor session, Object owner) throws HibernateException, SQLException {
        Boolean value = org.hibernate.type.BooleanType.INSTANCE.nullSafeGet(resultSet, names[0], session);
        return new BooleanType(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void nullSafeSet(PreparedStatement statement, Object value, int index, SessionImplementor session) throws HibernateException, SQLException {
        org.hibernate.type.BooleanType.INSTANCE.nullSafeSet(statement, ((BooleanType)value).getValue(), index, session);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object deepCopy(Object value) throws HibernateException {
        BooleanType original = (BooleanType)value;
        return new BooleanType(original.getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isMutable() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        return (BooleanType)value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return cached;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return original;
    }
}
