package nl.sodeso.gwt.persistence.types;

import nl.sodeso.gwt.ui.client.types.DateType;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import java.util.Objects;

/**
 * A Hibernate user type to map a DateType to a DATE in the database.
 *
 * @author Ronald Mathies
 */
public class DateTypeUserType implements UserType {

    /**
     * {@inheritDoc}
     */
    @Override
    public int[] sqlTypes() {
        return new int[] {Types.DATE};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class returnedClass() {
        return DateType.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o1, Object o2) throws HibernateException {
        if (o1 == null || o2 == null) {
            return false;
        }

        if (o1 == o2) return true;
        if (o1.getClass() != o2.getClass()) return false;
        DateType dateType1 = (DateType)o1;
        DateType dateType2 = (DateType)o2;
        return  Objects.equals(dateType1.getValue(), dateType2.getValue());

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode(Object value) throws HibernateException {
        return ((DateType)value).getValue().hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object nullSafeGet(ResultSet resultSet, String[] names, SessionImplementor session, Object owner) throws HibernateException, SQLException {
        Date value = org.hibernate.type.DateType.INSTANCE.nullSafeGet(resultSet, names[0], session);
        return new DateType(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void nullSafeSet(PreparedStatement statement, Object value, int index, SessionImplementor session) throws HibernateException, SQLException {
        org.hibernate.type.DateType.INSTANCE.nullSafeSet(statement, ((DateType) value).getValue(), index, session);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object deepCopy(Object value) throws HibernateException {
        DateType originalValue = (DateType)value;
        return new DateType(originalValue.getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isMutable() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        return (Serializable)deepCopy(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return deepCopy(cached);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return deepCopy(original);
    }
}
