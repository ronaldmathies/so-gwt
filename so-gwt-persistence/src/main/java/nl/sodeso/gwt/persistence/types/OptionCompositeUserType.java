package nl.sodeso.gwt.persistence.types;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.type.StringType;
import org.hibernate.type.Type;
import org.hibernate.usertype.CompositeUserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * A Hibernate user type to map an OptionType to three individual STRING types in the database.
 *
 * @author Ronald Mathies
 */
public class OptionCompositeUserType implements CompositeUserType {

    /**
     * {@inheritDoc}
     */
    @Override
    public String[] getPropertyNames() {
        return new String[] {"key", "code", "description"};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Type[] getPropertyTypes() {
        return new Type[] {StringType.INSTANCE, StringType.INSTANCE, StringType.INSTANCE};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public Object getPropertyValue(Object component, int property) throws HibernateException {
        OptionType<DefaultOption> optionType = (OptionType<DefaultOption>)component;

        if (optionType.getValue() != null) {
            switch (property) {
                case 0:
                    return optionType.getValue().getKey();
                case 1:
                    return optionType.getValue().getCode();
                case 2:
                    return optionType.getValue().getDescription();
            }
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public void setPropertyValue(Object component, int property, Object value) throws HibernateException {
        DefaultOption option = new DefaultOption();
        switch (property) {
            case 0:
                option.setKey((String) value);
            case 1:
                option.setCode((String)value);
            case 2:
                option.setDescription((String)value);
        }
        OptionType<DefaultOption> optionType = (OptionType<DefaultOption>)component;
        optionType.setValue(option);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class returnedClass() {
        return OptionType.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public boolean equals(Object o1, Object o2) throws HibernateException {
        if (o1 == null || o2 == null) {
            return false;
        }

        DefaultOption option1 = ((OptionType<DefaultOption>) o1).getValue();
        DefaultOption option2 = ((OptionType<DefaultOption>) o2).getValue();

        return !(option1 != null && option2 != null) || option1.equals(option2);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode(Object value) throws HibernateException {
        return value.hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public Object nullSafeGet(ResultSet resultSet, String[] names, SessionImplementor session, Object owner) throws HibernateException, SQLException {
        DefaultOption option = null;
        String key = org.hibernate.type.StringType.INSTANCE.nullSafeGet(resultSet, names[0], session);
        if (!resultSet.wasNull()) {
            option = new DefaultOption();

            option.setKey(key);
            option.setCode(org.hibernate.type.StringType.INSTANCE.nullSafeGet(resultSet, names[1], session));
            option.setDescription(org.hibernate.type.StringType.INSTANCE.nullSafeGet(resultSet, names[2], session));
        }

        return new OptionType(option);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public void nullSafeSet(PreparedStatement preparedStatement, Object value, int property, SessionImplementor session) throws HibernateException, SQLException {
        if (null == value || ((OptionType)value).getValue() == null) {
            preparedStatement.setNull(property, StringType.INSTANCE.sqlType());
            preparedStatement.setNull(property + 1, StringType.INSTANCE.sqlType());
            preparedStatement.setNull(property + 2, StringType.INSTANCE.sqlType());
        } else {
            final OptionType<DefaultOption> optionType = (OptionType) value;

            org.hibernate.type.StringType.INSTANCE.nullSafeSet(preparedStatement, optionType.getValue().getKey(), property, session);
            org.hibernate.type.StringType.INSTANCE.nullSafeSet(preparedStatement, optionType.getValue().getCode(), property + 1, session);
            org.hibernate.type.StringType.INSTANCE.nullSafeSet(preparedStatement, optionType.getValue().getDescription(), property + 2, session);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public Object deepCopy(Object value) throws HibernateException {
        OptionType<DefaultOption> optionType = (OptionType) value;
        DefaultOption option = optionType.getValue();

        DefaultOption copyOption = null;
        if (option != null) {
            copyOption = new DefaultOption(option.getKey(), option.getCode(), option.getDescription());
        }
        return new OptionType<DefaultOption>(copyOption);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isMutable() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Serializable disassemble(Object value, SessionImplementor session) throws HibernateException {
        return (Serializable) value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object assemble(Serializable cached, SessionImplementor session, Object owner) throws HibernateException {
        return cached;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object replace(Object original, Object target, SessionImplementor session, Object owner) throws HibernateException {
        return this.deepCopy(original);
    }

}
