package nl.sodeso.gwt.persistence.types;

import nl.sodeso.gwt.ui.client.types.StringType;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Objects;

/**
 * A Hibernate user type to map a StringType to a String in the database.
 *
 * @author Ronald Mathies
 */
public class StringTypeUserType implements UserType {

    /**
     * {@inheritDoc}
     */
    @Override
    public int[] sqlTypes() {
        return new int[] {Types.VARCHAR};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class returnedClass() {
        return StringType.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o1, Object o2) throws HibernateException {
        if (o1 == null || o2 == null) {
            return false;
        }

        if (o1 == o2) return true;
        if (o1.getClass() != o2.getClass()) return false;
        StringType stringType1 = (StringType)o1;
        StringType stringType2 = (StringType)o2;
        return  Objects.equals(stringType1.getValue(), stringType2.getValue());

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode(Object value) throws HibernateException {
        return ((StringType)value).getValue().hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object nullSafeGet(ResultSet resultSet, String[] names, SessionImplementor session, Object owner) throws HibernateException, SQLException {
        String value = org.hibernate.type.StringType.INSTANCE.nullSafeGet(resultSet, names[0], session);
        return new StringType(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void nullSafeSet(PreparedStatement statement, Object value, int index, SessionImplementor session) throws HibernateException, SQLException {
        org.hibernate.type.StringType.INSTANCE.nullSafeSet(statement, ((StringType) value).getValue(), index, session);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object deepCopy(Object value) throws HibernateException {
        StringType originalValue = (StringType)value;
        return new StringType(originalValue.getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isMutable() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        return (Serializable)deepCopy(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return deepCopy(cached);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return deepCopy(original);
    }
}
