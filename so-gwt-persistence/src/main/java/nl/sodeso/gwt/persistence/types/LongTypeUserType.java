package nl.sodeso.gwt.persistence.types;

import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.LongType;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Objects;

/**
 * A Hibernate user type to map an IntType to an INTEGER in the database.
 *
 * @author Ronald Mathies
 */
public class LongTypeUserType implements UserType {

    /**
     * {@inheritDoc}
     */
    @Override
    public int[] sqlTypes() {
        return new int[] {Types.BIGINT};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class returnedClass() {
        return LongType.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o1, Object o2) throws HibernateException {
        if (o1 == null || o2 == null) {
            return false;
        }

        if (o1 == o2) return true;
        if (o1.getClass() != o2.getClass()) return false;
        LongType longType1 = (LongType)o1;
        LongType longType2 = (LongType)o2;
        return  Objects.equals(longType1.getValue(), longType2.getValue());

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode(Object value) throws HibernateException {
        return ((LongType)value).getValue().hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object nullSafeGet(ResultSet resultSet, String[] names, SessionImplementor session, Object owner) throws HibernateException, SQLException {
        Long value = org.hibernate.type.LongType.INSTANCE.nullSafeGet(resultSet, names[0], session);
        return new LongType(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void nullSafeSet(PreparedStatement statement, Object value, int index, SessionImplementor session) throws HibernateException, SQLException {
        org.hibernate.type.LongType.INSTANCE.nullSafeSet(statement, ((LongType)value).getValue(), index, session);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object deepCopy(Object value) throws HibernateException {
        LongType original = (LongType)value;
        return new LongType(original.getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isMutable() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        return (LongType)value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return cached;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return original;
    }
}
