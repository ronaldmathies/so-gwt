package nl.sodeso.gwt.event.client;

import com.google.gwt.event.shared.SimpleEventBus;
import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.ResettableEventBus;

import java.util.HashMap;
import java.util.Map;

/**
 * The event bus controller is a class which can hold handlers for certain types of events, using
 * the event bus controller you can register / de-register and fire events to all registered handlers.
 */
public class EventBusController {

    private Map<Registration, HandlerRegistration> registrations = new HashMap<>();
    private ResettableEventBus resettableEventBus;

    /**
     * Returns the event bus.
     * @return the event bus.
     */
    private ResettableEventBus getResettableEventBus() {
        if (resettableEventBus == null) {
            resettableEventBus = new ResettableEventBus(new SimpleEventBus());
        }
        return resettableEventBus;
    }

    /**
     * Adds a new handler for the specified type to the event bus.
     * @param type the type of event.
     * @param handler the handler.
     * @param <H> the generic type of the handler.
     */
    public <H> void addHandler(Event.Type<H> type, H handler) {
        if (!registrations.containsKey(new Registration(type, handler))) {
            HandlerRegistration handlerRegistration = getResettableEventBus().addHandler(type, handler);
            registrations.put(new Registration(type, handler), handlerRegistration);
        }
    }

    /**
     * Removes the specified handler from the event bus.
     * @param type the type of event.
     * @param handler the handler to remove.
     */
    public void removeHandler(Event.Type type, Object handler) {
        final Registration registration = new Registration(type, handler);
        HandlerRegistration handlerRegistration = registrations.get(registration);
        if (handlerRegistration != null) {
            handlerRegistration.removeHandler();
            registrations.remove(registration);
        }
    }

    /**
     * Fires the specified event for all registered handlers for that event type.
     * @param event the event.
     */
    public void fireEvent(Event<?> event) {
        getResettableEventBus().fireEvent(event);
    }

    /**
     * Removes all registered handlers from the event bus.
     */
    public void resetHandlers() {
        getResettableEventBus().removeHandlers();
        registrations.clear();
    }

    /**
     * A handler registration combines the handler and it's associated event in a
     * single registration.
     */
    private class Registration {
        private Event.Type type;
        private Object handler;

        /**
         * Constructs a new registration of an event and it's handler.
         * @param type the event type.
         * @param handler the handler.
         */
        public Registration(Event.Type type, Object handler) {
            this.type = type;
            this.handler = handler;
        }

        /**
         * Returns the event type.
         * @return the event type.
         */
        public Event.Type getType() {
            return type;
        }

        /**
         * Returns the handler.
         * @return the handler.
         */
        public Object getHandler() {
            return this.handler;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Registration)) {
                return false;
            }

            Registration other = (Registration)obj;
            if (other.getType().equals(this.type) &&
                    other.getHandler() == this.getHandler()) {
                return true;
            }

            return false;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int hashCode() {
            int result = 0;
            result += 31 * this.type.hashCode();
            result += 31 * this.handler.hashCode();
            return result;
        }
    }

}